# OWL Definability #
The OWL Definability API is a Java API and reference implementation for computing the definability type and the Minimal Definition Signatures (MDSs) of defined concepts and roles for OWL ontologies \[1\]. 

In addition, the API also computes Minimal Signature Coverage for abstract, and real-world  (OWL) ontologies \[2\].


### References ###
* \[1\] D. Geleta, T. R. Payne, and V. Tamma, An Investigation of Definability in Ontology Alignment.  In: The 20th International Conference on Knowledge Engineering and Knowledge Management (EKAW2016), to appear.
* \[2\] D. Geleta, T. R. Payne, and V. Tamma, Minimal Coverage for Ontology Signatures, In: 13th OWL: Experiences and Directions Workshop and 5th OWL reasoner evaluation workshop (OWLED–ORE2016), Springer, 2016, to appear.
* \[3\] OWL Definability API documentation [Javadoc](https://bitbucket.org/dgeleta/owl-definability/src/b6423bcc6e1850e36b26888fb12f221fd1600615/_DOCUMENTATION/) 
* \[4\] OWL Definability API tutorial example [HelloDefinability.java](https://bitbucket.org/dgeleta/owl-definability/src/b6423bcc6e1850e36b26888fb12f221fd1600615/_EXAMPLE/HelloDefinability.java)



Dependencies
-----------------------
In order to build include the following dependencies:

 * OWL Definability [JAR](https://bitbucket.org/dgeleta/owl-definability/src/8e35a8be2833fe3e81571b24841cf24da879e159/_DISTRIBUTIONS/owldefinability.jar)
 * Maven dependencies (from the [pom.xml](https://bitbucket.org/dgeleta/owl-definability/raw/8e35a8be2833fe3e81571b24841cf24da879e159/pom.xml)) 
 * JARs included in [_DEPENDENCIES](https://bitbucket.org/dgeleta/owl-definability/src/b6423bcc6e1850e36b26888fb12f221fd1600615/_DEPENDENCIES/).

 
 

Using the API
-----------------------
The application handles input ontologies and groups computation results according to ```DATASETNAME```.

**Input** 

In order to run the application create a folder named ```_dataset``` in the root folder of the project. 

This [folder](https://bitbucket.org/dgeleta/owl-definability/src/8e35a8be2833fe3e81571b24841cf24da879e159/_dataset) will store the different datasets (set of ontology documents) e.g.:

```xml
   	_dataset\DATASETNAME\ONTNAME_1.OWL
    					\ONTNAME_2.OWL
    					\ ...
```

The repository [provides](https://bitbucket.org/dgeleta/owl-definability/src/7b199fb4cb3f494fcce53e316144cafc2152cd4f/_dataset/Examples) several small example ontologies (that model the family domain \[1\]).

**Processing & Results**

Definability status and MDS computations could take a while (due to the exponential complexity of reasoning, i.e. entailment checks).

The process may be interrupted due to insufficient memory (JVM), however, intermediate results are preserved in an ```INPROGRESS``` file:
```xml
   _experimentComputation/DATASETNAME/DefinableConceptList_INPROGRESS_ONTNAME.OWL
```

The process always resumes.  Once the computation concludes all intermediate files are deleted and the final results are stored in the (auto-generated) folder: ```_experimentComputation/DATASETNAME```

```xml 
   _experimentComputation/DATASETNAME/
   				_cDefs/DefinableConceptList_ONTNAME.OWL.txt   concept definability status 
            	_cDefsPatterns/cDefsPatterns_ONTNAME.OWL.txt  concept definition patterns
            	_MCDSs/MCDS_disjoint_ONTNAME.OWLE.txt         disjoint concept MDSs
            	_MCDSs/MCDS_eX_ONTNAME.OWL.txt                all concept MDSs 
            	_rDefs/DefinableRoleList_ONTNAME.OWL.txt      role definability status 
            	_rDefsPatterns/rDefPatterns_ONTNAME.OWL.txt   role definition patterns
            	_MRDSs/MRDS_ONTNAME.OWLE.txt                  all role MDSs  
```



Running experiments with ```HelloDefinability.java```
-----------------------------------------------------

```HelloDefinability.java``` \[4\] provides the full code example used in the coding tutorials.

In addition, the following methods can be used to generate definability statuses, MDSs, and to approximate Minimal Cover Sets in experiments.

* ```computeDefinabilityStatus(...)``` establish definability status for all entities of an Ontology
* ```computeDisjointMDSs(...)``` compute disjoint MDSs for all defined entities of an Ontology
* ```computeAllMDSs(...)``` compute all MDSs for all defined entities of an Ontology
* ```computeDefinitionPatterns(...)``` classify defined entity MDSs as patterns for all defined entities of an Ontology
* ```computeDefinitionAxioms(...)``` compute defintion axioms for identifiable Definition Patterns (using Rule-based Rewriting) for all defined entities of an Ontology
* ```helloSignatureCoverage_realOntology(...)``` compute Minimal Signature Coverage using a real-world ontology
* ```helloSignatureCoverage_idealCover(...)``` minimise an Ontology signature



Coding with the API : (1) Definability
---------------------------------------
* Compute definability status (definability computation stage 1/3)
* Compute disjoint MDSs (definability computation stage 2/3)
* Compute all MDSs (definability computation stage 3/3)
* Classifying Definition Patterns 
* Run rule-based rewriting for identifiable patterns
* Run definability checks for entities


**Stage 1: Definability Status**

Establish entity definability status for each named concept and role of the ontology.
(i.e. defined explicitly /defined implictly, or undefined)

```java
	// Concepts 
   	Computer_DefinedConcept defConComp = new Computer_DefinedConcept
   	     	(datasetName, owlOntology, ontFile, reasonerType, reasonerTimeout, isVerbose);
  	HashMap<OWLClass, ConceptDefinabilityType> defConceptTypes = 
  												 defConComp.getDefinedConceptListByType();
  	
	// Roles
	Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
												reasonerTimeout, isVerbose, ont1, ontFile);
	HashMap<OWLEntity, RoleDefinabilityType> defRolesTypes = defRoleComp.getRoleDefinability();         
```

Print the results
```java
	// Concepts
	ConceptDefinabilityType thisConceptDefinabilityType = ConceptDefinabilityType.UNDEFINED;
	if (defConceptTypes.get(cls) != null) {
							thisConceptDefinabilityType = defConceptTypes.get(concept);}		
	System.out.println("   " + concept.getIRI() + " " + thisConceptDefinabilityType);

	// Roles
	RoleDefinabilityType thisRoleDefinabilityType = RoleDefinabilityType.UNDEFINED;
	if (defRolesTypes.get(rl) != null) {
								thisRoleDefinabilityType = defRolesTypes.get(role);}
	System.out.println("   " + role.getIRI() + " " + thisRoleDefinabilityType);

```

Concept definability statuses (```ConceptDefinabilityType```): 

* ```EXPLICIT``` there is an explicit definition for the Concept 
* ```EXPLICIT_CYCLIC``` there is only a cyclic explicit definition for the Concept  
* ```ERROR_IMPLIED_BY_EMPTYSIGMA``` the Concept is equivalent to Thing
* ```IMPLICIT``` the Concept is implicitly definable  


Role definability statuses (```RoleDefinabilityType```): 

* ```EXPLICIT``` there is an explicit definition for the Role  
* ```IMPLICIT``` the Role is implicitly definable 

Entity definability statuses (```ConceptDefinabilityType | RoleDefinabilityType```):

* ```UNDEFINED``` the Entity is undefined
* ```UNKNOWN``` the definability status of the Entity has not yet, or cannot be determined


**Stage 2: Disjoint MDSs**
```java
	// Concepts
	Computer_MinimalConceptDefinitionSignature mcdsComp = 
			new Computer_MinimalConceptDefinitionSignature(
					ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
	HashMap<OWLClass, Vector<Set<OWLEntity>>> disjMcdss = mcdsComp
														.computeMCDSsOfOntology_Disjoints();

	// Roles
	Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
												reasonerTimeout, isVerbose, ont1, ontFile);
	HashMap<OWLEntity, HashSet<Set<OWLEntity>>> disjMrdss = defRoleComp.getRoleMDSs();
```

Print the results
```java
	// Concepts
	int mdscount = 0;
	for (Set<OWLEntity> mds : disjMcdss.get(concept)) {
		System.out.println("\t MDS_" + ++mdscount + " = " + mds + "");
	}

	// Roles
	mdscount = 0;
	for (Set<OWLEntity> mds : disjMrdss.get(role)) {
		System.out.println("\t MDS_" + ++mdscount + " = " + mds + "");
	}
```

**Stage 3: All MDSs**
```java
	// Concepts
	Computer_MinimalConceptDefinitionSignature mcdsComp = 
			new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
	HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();

```
 
**Classifying Definition Patterns**
```java
	DefinitonPatternRecogniser defPattern = new DefinitonPatternRecogniser(ont1, ontFile,
									datasetName, reasonerType, reasonerTimeout, isVerbose);
					
	// Concepts
	HashMap<OWLEntity, Vector<MCDS>> conceptPatterns = defPattern
								.generateConceptDefinitionPatternsForOntology(allMcdss);

	// Roles
	HashMap<OWLEntity, Vector<MRDS>> rolePatterns = defPattern
								.generateRoleDefinitionPatternsForOntology(allMrdss);

```

Print the results
```java
	// Concepts
	for (MCDS patternMds : conceptPatterns.get(definedConcept)) {
		System.out.println("\n    " + patternMds.getPattern().toString()
									+ " | \u03A3 = " + patternMds.getSignature()
															+ "\n    Justification : ");
		for (OWLAxiom axx : patternMds.getJustification()) 
													{System.out.println("\t   " + axx);}}
													
	// Roles
	for (MRDS patternMds : rolePatterns.get(definedRole)) {
		System.out.println("\n    " + patternMds.getPattern().toString()
											+ " | \u03A3 = " + patternMds.getSignature()
															+ "\n    Justification : ");
		// could be empty, as OWLExplanation API does not yet supports it, hence 
		// an experimental method is used
		for (OWLAxiom axx : patternMds.getJustification()) {
													System.out.println("\t   " + axx);}}
```

Concept defintion patterns (```MCDS_pattern```): 

* ```EXPLICIT``` Defined explicitly (in any form)
* ```EXPLICIT_DEF``` Explicit definition (C = ...)
* ```EXPLICIT_SYNONYM``` Explicit synonym definition (C = D)
* ```EXPLICITLY_WITH_SYNONYM_ROLE``` Concept defined exp. by a given role's synonym   
* ```EXPLICITLY_WITH_INVERSE_ROLE``` Concept defined exp. by a given role's inverse 
* ```IMPLICIT_SYNONYM``` Explicit definition (C = ...)
* ```DISJOINT_UNION``` Concept defined as a part of a disjoint union 
* ```OBJPROP_DOMAIN``` Concept defined as the domain of the role (C = 3r.T)
* ```OBJPROP_RANGE``` Concept defined as the range of the role (C = 3r^-.T)
* ```OBJPROP_DOMAIN_INVERSE``` Concept defined as the domain of an inverse object property
* ```OBJPROP_RANGE_INVERSE``` Concept defined as the range of an inverse object property
* ```DATPROP_DOMAIN``` Concept defined as the domain of an inverse datatype property 

Role defintion patterns (```MRDS_pattern```): 

* ```EXPLICIT_DEF``` Explicit definition (r = s)
* ```EXPLICIT_INVERSE``` Explicitly defined (r) by inverse role (r = s^-)
* ```EXPLICIT_SYNONYM``` Explicitly defined (r) by synonym role (r = s)
* ```IMPLICIT_INVERSE``` Implicitly defined (r) by inverse role (r = s^-)
* ```IMPLICIT_SYNONYM``` Implicitly defined (r) by synonym role (r = s)
 
Entity defintion patterns (```MCDS_pattern | MRDS_pattern```):

* ```EXPLICIT``` Defined explicitly (in any form)
* ```COMPLEX``` Defined by an unidentifiable pattern (arbitrary combination of basic patterns)
* ```UNKNOWN``` Definition pattern not yet identified






**Rule-based Rewriting**
```java
	PatternBasedRewriter patRW = new PatternBasedRewriter(datasetName, reasonerType,
											reasonerTimeout, isVerbose, ont1, ontFile);
								
	// Concepts & Roles
	OWLAxiom defAxiom = patRW.generateDefinitionAxiom(patternMds);

	// VALIDATE with REASONER
	if (defAxiom != null) {
		boolean isEntailed = reasoner.isEntailed(defAxiom);
		System.out.println("   Definition AXIOM : " + defAxiom
							+ "\n\tT ? \u22A8 (" + defAxiom + ")   is " + isEntailed);} 
	else { System.out.println("   ... ERROR, no definition AXIOM produced ... " );}

```


**Definability checks** 

Concept definability checks
```java
	// (C) concept
	OWLClass concept = ...
	Set<OWLEntity> signature = ...
	
	ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker
						(ontFile, datasetFolderName, reasonerType, reasonerTimeout, false);
	
	// is C defined in any form (even modelling error)?
	boolean isConceptDefined = impDef.isConceptDefined(concept, signature);

	// is C implcitly defined by a given signature?
	boolean isConceptImplicitlyDefinedBySignature = 
									impDef.isConceptImplicitlyDefined(concept, signature);

	// is C implicitly defined by a given signature, under a module? 
	Set<OWLAxiom> moduleAxioms = ... 
	boolean isConceptImplcitlyDefinedInModule = 
					impDef.isConceptImplicitlyDefined_MOD(concept, signature, moduleAxioms);
			
	// is C explicitly defined by a concept equivalence axiom which contains a direct cycle
	OWLAxiom defAxiom = ...
	boolean isAxiomCyclic = impDef.isExplicitConceptDefinitionAcyclic(concept, defAxiom);
	

	// obtain justifications (all, or only one) if C is definable
	boolean obtainAllJustifications = false;
	Set<Explanation<OWLAxiom>> exps = 
			impDef.justifyConceptDefinability(concept, signature, obtainAllJustifications);
			
	// (2f) is C defined by a modelling error?
	boolean isConceptEquivalentToTop = 
								impDef.isConceptImplicitlyDefinedByEmptySignature(concept);
```

Role definability checks
```java
	// (R) Role
	OWLEntity role = ... 
	Set<OWLEntity> signature = ... 

	ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker
						(ontFile, datasetFolderName, reasonerType, reasonerTimeout, false);
						
	// is R implcitly defined by a given signature?
	boolean isRoleImplicitlyDefinedBySignature = 
										impDef.isRoleImplicitlyDefined(role, roleSignature);

	// is R implicitly defined by a given signature, under a module?
	Set<OWLAxiom> moduleAxioms = null; 
	boolean isRoleImplcitlyDefinedInModule = 
						impDef.isRoleImplicitlyDefined_MOD(role, signature, moduleAxioms);
```


Coding with the API : (2) Signature Coverage
---------------------------------------------
* Determine Signature Coverability
* Compute Approximated Minimal Coverage
* Compute Ideal Coverage 

**Signature Coverability**
```java
	// initalise
	Set<String> taskSignature = ...
	Set<String> restrictedSignature = ...
	 
	Computer_SignatureCoverage sigCovComp = new Computer_SignatureCoverage(datasetName,
							fileName, taskSignature, restrictedSignature, mdss, isVerbose);
							
	// check coverage
	boolean isTaskCoverableByRestrictedSignature 
									= sigCovComp.isTaskCoverableByRestrictedSignatures();

	// uncoverable 
	if (!isTaskCoverableByRestrictedSignature) {		
		// obtain uncoverable entities
		Set<String> uncoverableTaskSignatureEntities 
									= sigCovComp.getUncoverableTaskSignatureEntities();}	
	// coverable 
	else { ... }
```

**Approximated Minimal Coverage**
```java
	// approximate using Greedy1 method
	Set<String> coverSet_greedy1 = sigCovComp.computeSignatureCover_greedy1();
	
	// approximate using Greedy2 method
	Set<String> coverSet_greedy2 = sigCovComp.computeSignatureCover_greedy2();
```

**Ideal Coverage**
```java
	// task & restricted signatures are not used, only the full ontology signature
	Computer_SignatureCoverage sigCovComp = new Computer_SignatureCoverage(datasetName,
													fileName, null, null, mdss, isVerbose);
	// Compute ideal cover set
	Set<String> idealCover = sigCovComp.computeIdealCover(ontologySignature);
```
 