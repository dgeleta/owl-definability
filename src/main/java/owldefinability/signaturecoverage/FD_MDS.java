package owldefinability.signaturecoverage;

import java.util.Set;

/**
 * MDS represented as an FD (or fMDS), i.e. for an MDS = {A,B} of concept C
 * (where C = A u B) m: [A, B]-->[C]
 *
 */
public class FD_MDS {

	/**
	 * The LHS signature (the MDS) of an fMDS, i.e. [A, B] of [A, B]-->[C]
	 */
	public Set<String> sLHS;

	/**
	 * The RHS signature (the enties definable by the LHS signature) of an fMDS,
	 * i.e. [C] of [A, B]-->[C]
	 */
	public Set<String> sRHS;


	/**
	 * MDS represented as an FD (or fMDS), i.e. for an MDS = {A,B} of concept C
	 * (where C = A u B) m: [A, B]-->[C]
	 * 
	 * @param sLHS
	 *            The LHS signature (the MDS) of an fMDS, i.e. [A, B]
	 * @param sRHS
	 *            The RHS signature (the enties definable by the LHS signature)
	 *            of an fMDS i.e. [C] 
	 */
	public FD_MDS(Set<String> sLHS, Set<String> sRHS) {
		this.sLHS = sLHS;
		this.sRHS = sRHS;
	}

	public String toString() {
		return new String(sLHS + "->" + sRHS);
		// return new String(sLHS + " -> " + sRHS);
	}
}