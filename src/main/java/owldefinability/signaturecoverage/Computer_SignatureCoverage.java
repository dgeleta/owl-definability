package owldefinability.signaturecoverage;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;



/**
 * Compute Minimal Signature Coverage for a given task, using only a restricted
 * signatured, assuming some MDSs are pre-computed
 */
public class Computer_SignatureCoverage {

	// * * * VARS & Properties * * *
	private Set<String> taskSignature = new HashSet<String>();
	private Set<String> allowedSignature = new HashSet<String>();
	private Vector<FD_MDS> mdsFDs = new Vector<FD_MDS>();
	private Set<String> cover = new HashSet<String>();
	private Set<String> coverClosure = new HashSet<String>();
	private Vector<FD_MDS> mdsFDsClosure;
	private Vector<FD_MDS> mdsFDsClosure_Working;
	private HashMap<FD_MDS, Integer> mdsValueMap;
	private HashMap<FD_MDS, Integer> mdsCostMap;
	private boolean isVerbose = false;



	// * * * CONSTRUCTOR * * *
	/**
	 * Compute Minimal Signature Coverage for a given task, using only a
	 * restricted signatured, assuming some MDSs are pre-computed
	 * 
	 * @param taskSigma
	 *            (S) Task Signature (e.g. query signature)
	 * @param allowedSigma
	 *            (R) Restricted Signature (e.g. aligned entities)
	 * @param mdss
	 *            (M) Some or all MDSs of defined entities in (S,R)
	 * @param datasetId
	 *            Dataset folder name
	 * @param ontFileName
	 *            Ontology ID
	 */
	@SuppressWarnings("unchecked")
	public Computer_SignatureCoverage(String datasetId, String ontFileName, Set<String> taskSigma,
			Set<String> allowedSigma, Object mdss, boolean isVerbose) {

		HashMap<String, Set<Set<String>>> mdssAsString;
		if (mdss.getClass().equals(Vector.class)) {
			mdsFDs = (Vector<FD_MDS>) mdss;
		} else {
			// transform mdss to FDs
			mdssAsString = (HashMap<String, Set<Set<String>>>) mdss;
			mdsFDs = transformMDSsToFDs(mdssAsString);
		}

		this.taskSignature = taskSigma; // S
		this.allowedSignature = allowedSigma; // R

		this.isVerbose = isVerbose;
	}

	// * * * (C) Signature COVER * * * * * * *
	/**
	 * Computes a cover using a the slower, but more effective (wrt cover size)
	 * greedy approximation method
	 * 
	 * @return (C) non-redundant cover set
	 */
	public Set<String> computeSignatureCover_greedy1() {
		if (isVerbose) {
			System.out.println("[Compute Minimal Signature Coverage]");
			System.out
					.println("S: task sig. | R: restricted sig. | C: cover set | M:MDSs | +: closure");
			System.out.println("S = " + taskSignature);
			System.out.println("R = " + allowedSignature);
			printM(mdsFDs, "M");
		}

		if (taskSignature.isEmpty() || allowedSignature.isEmpty() || mdsFDs.isEmpty()) {
			System.out.println("Empty Signature(s) -> Uncoverable Task -> TERMINATE");
			return null;
		}

		if (isTaskCoverableByRestrictedSignatures() == false) {
			System.out.println("Uncoverable Task -> TERMINATE");
			return null;
		}


		// (1) Init datastructures:
		computeFDsClosure();

		// (2) C ← C ∪ ∀e{e ∈ (S ∩ R)}
		cover.addAll(getAllExplicitlyCovered());

		// (3) compute C+
		coverClosure = computeSetClosure(cover, "C");
		if (isVerbose)
			System.out.println("\tC+ = " + coverClosure + "\n");

		// (4) M′←M \ ∀m{m ∈ M |mLHS ⊆ C+}
		mdsFDsClosure_Working = filterM(mdsFDsClosure);

		// (5) SEARCH
		while (!coverClosure.containsAll(taskSignature)) {
			if (isVerbose)
				System.out.println("\n\n* * * LOOP * * * * * * * * * * * *");

			// value cost vector
			computeValueCostMaps();

			// select best
			FD_MDS bestChoice = selectBestFDMDS();

			// add to Cover
			cover.addAll(bestChoice.sLHS);

			// C+
			coverClosure = computeSetClosure(cover, "C");
			if (isVerbose)
				System.out.println("\tC+ = " + coverClosure);

			// filter M
			mdsFDsClosure_Working = filterM(mdsFDsClosure_Working);
		}

		// (6) REMOVE Redundant ENTITIES
		removeRedundantEntities();

		// DONE
		if (isVerbose) {
			printM(mdsFDsClosure_Working, "M'");
			System.out.println("\n\nS ⊆ C+ ");
			System.out.println("\tS  = " + taskSignature);
			System.out.println("\tC  = " + cover);
			System.out.println("\tC+ = " + coverClosure);
		}

		if (cover.size() > taskSignature.size()) {
			return taskSignature;
		} else
			return cover;
	}

	/**
	 * Computes a cover using a the faster, but less effective (wrt cover size)
	 * greedy approximation method
	 * 
	 * @return (C) non-redundant cover set
	 */
	public Set<String> computeSignatureCover_greedy2() {
		if (isVerbose) {
			System.out.println("[Compute Minimal Signature Coverage]");
			System.out.println("S = " + taskSignature);
			System.out.println("R = " + allowedSignature);
			printM(mdsFDs, "M");
		}

		if (taskSignature.isEmpty() || allowedSignature.isEmpty() || mdsFDs.isEmpty()) {
			System.out.println("Empty Signature(s) -> Uncoverable Task -> TERMINATE");
			return null;
		}

		if (isTaskCoverableByRestrictedSignatures() == false) {
			return null;
		}

		// (1) Init datastructures
		computeFDsClosure();

		// (2) C ← C ∪ ∀e{e ∈ S| e ∈ R| e != mRHS |m ∈ M}
		cover.addAll(getUndefinedAndMappedEntities());


		// (3) compute C+
		coverClosure = computeSetClosure(cover, "C");
		if (isVerbose)
			System.out.println("\tC+ = " + coverClosure + "\n");

		// (4) M′←M \ ∀m{m ∈ M |mLHS ⊆ C+}
		mdsFDsClosure_Working = filterM(mdsFDsClosure);


		// (5) SEARCH
		while (!coverClosure.containsAll(taskSignature)) {
			if (isVerbose)
				System.out.println("\n\n* * * LOOP * * * * * * * * * * * *");

			// value cost vector
			computeValueCostMaps();

			// select best
			FD_MDS bestChoice = selectBestFDMDS();

			// add to Cover
			cover.addAll(bestChoice.sLHS);

			// C+
			coverClosure = computeSetClosure(cover, "C");
			if (isVerbose)
				System.out.println("\tC+ = " + coverClosure);

			// filter M
			mdsFDsClosure_Working = filterM(mdsFDsClosure_Working);
		}

		// DONE
		if (isVerbose) {
			printM(mdsFDsClosure_Working, "M'");
			System.out.println("\n\nS ⊆ C+ ");
			System.out.println("\tS  = " + taskSignature);
			System.out.println("\tC  = " + cover);
			System.out.println("\tC+ = " + coverClosure);
		}



		if (cover.size() > taskSignature.size()) {
			return taskSignature;
		} else
			return cover;
	}

	/**
	 * Compute the ideal cover set (i.e. minimise the full ontology signature)
	 * 
	 * @param ontologySignature
	 *            TBox signature
	 * @return Ideal Cover Set
	 */
	public Set<String> computeIdealCover(Set<String> ontologySignature) {
		taskSignature = ontologySignature;
		allowedSignature = ontologySignature;

		Set<String> idealCover = new HashSet<String>();
		idealCover.addAll(ontologySignature);

		return removeRedundantIdealCoversetEntities(idealCover);
	}


	// * * * Public Methods * * *
	/**
	 * Checks whether a given Task signature is coverable by the Restricted
	 * signature
	 * 
	 * @return True if the Task signature is coverable, otherwise false
	 */
	public boolean isTaskCoverableByRestrictedSignatures() {
		if (isVerbose) {
			System.out.println("\n[S ⊆ R+ ? | Task Coverable ?]");
			System.out.println("\tS = " + taskSignature);
			System.out.println("\tR = " + allowedSignature);
		}

		Set<String> allowedSignatureClosure = computeSetClosure(allowedSignature, "R");
		if (isVerbose)
			System.out.println("\tR+ = " + allowedSignatureClosure);

		if (allowedSignatureClosure.containsAll(taskSignature)) {
			if (isVerbose)
				System.out.println("[S ⊆ R+ | Task Coverable]");
			return true;
		}
		if (isVerbose)
			System.out.println("[S NOT ⊆ R+ | Task Is Not Coverable]");
		return false;
	}

	/**
	 * If a given Task signature is uncoverable by the Restricted signature, it
	 * identifies the set of uncoverable Task signature entities
	 * 
	 * @return Set of uncoverable Task signature entities, empty if the Task is
	 *         coverable
	 */
	public Set<String> getUncoverableTaskSignatureEntities() {
		Set<String> allowedSignatureClosure = computeSetClosure(allowedSignature, "R");
		Set<String> uncoverableTaskSignatureEntities = new HashSet<String>();
		uncoverableTaskSignatureEntities.addAll(taskSignature);
		uncoverableTaskSignatureEntities.removeAll(allowedSignatureClosure);
		return uncoverableTaskSignatureEntities;
	}

	private Set<String> removeRedundantIdealCoversetEntities(Set<String> idealCover) {
		if (isVerbose)
			printM(mdsFDs, "M");

		Set<String> nonredundantEnts = new HashSet<String>();
		nonredundantEnts.addAll(idealCover);

		boolean oldIsVerbose = isVerbose;
		for (String ent : idealCover) {
			if (oldIsVerbose)
				System.out.println("   Is " + ent + " redundant ?");

			isVerbose = false;
			Set<String> nonredundantEntsClosure = computeSetClosure(nonredundantEnts, "I");
			if (isEntityImplicitlyCoveredBy(ent, nonredundantEntsClosure)) {
				nonredundantEnts.remove(ent);
				if (oldIsVerbose)
					System.out.println("\tREMOVED Redundant " + ent);
			}
			isVerbose = oldIsVerbose;
		}
		return nonredundantEnts;
	}



	// * * * Private Methods * * *
	private Set<String> getUndefinedAndMappedEntities() {
		Set<String> undefMappedEnts = new HashSet<String>();

		// e ∈ S
		for (String ent : taskSignature) {
			// e ∈ R ?
			if (allowedSignature.contains(ent) && !getDefinedEntities().contains(ent)) {
				undefMappedEnts.add(ent);
			}
		}
		if (isVerbose)
			System.out
					.println("\n[Undefined & Allowed Entities of S] \n\t∀e{e ∈ S|e ∈ R| e != mLHS | m ∈ M} = "
							+ undefMappedEnts);
		return undefMappedEnts;
	}

	private Set<String> getAllExplicitlyCovered() {
		Set<String> expMappedEnts = new HashSet<String>();

		// e ∈ S
		for (String ent : taskSignature) {
			// e ∈ R ?
			if (allowedSignature.contains(ent)) {
				expMappedEnts.add(ent);
			}
		}
		return expMappedEnts;
	}

	private Vector<FD_MDS> filterM(Vector<FD_MDS> mdsToFilter) {
		if (isVerbose)
			System.out.println("\n[Filter M']\n\tM / ∀m{m ∈ M| mLHS ⊆ C+}");
		Vector<FD_MDS> filtered = new Vector<FD_MDS>();
		for (FD_MDS fd : mdsToFilter) {

			if (!coverClosure.containsAll(fd.sLHS)) {
				filtered.add(fd);
				if (isVerbose)
					System.out.println("\t keep " + fd);
			} else {
				if (isVerbose)
					System.out.println("\t M' / {" + fd + "}");
			}
		}
		return filtered;
	}

	private void computeValueCostMaps() {
		if (isVerbose)
			System.out.println("\n[Compute Value-cost vector]");

		mdsValueMap = new HashMap<FD_MDS, Integer>();
		mdsCostMap = new HashMap<FD_MDS, Integer>();

		for (FD_MDS fd : mdsFDsClosure_Working) {
			// cost
			Set<String> diff = new HashSet<String>();
			diff.addAll(fd.sLHS);
			diff.removeAll(coverClosure);
			int cost = diff.size();
			mdsCostMap.put(fd, cost);

			// value
			diff.clear();
			diff.addAll(taskSignature);
			diff.removeAll(coverClosure);
			diff.retainAll(fd.sRHS);
			int value = diff.size();
			mdsValueMap.put(fd, value);

			if (isVerbose)
				System.out.println("\t" + fd + "\n\t   c(m) = " + cost + "   v(m) = " + value);
		}
	}

	private FD_MDS selectBestFDMDS() {
		if (isVerbose)
			System.out.println("\n[Select best m ∈ M']");

		// find highest value
		FD_MDS highestValue = mdsValueMap.keySet().iterator().next();
		for (FD_MDS fd : mdsValueMap.keySet()) {
			if (mdsValueMap.get(fd) > mdsValueMap.get(highestValue)) {
				highestValue = fd;
			}
		}
		if (isVerbose)
			System.out.println("\tHighest v(m) = " + mdsValueMap.get(highestValue));

		// find all FD with highest value
		Set<FD_MDS> highestValues = new HashSet<FD_MDS>();
		for (FD_MDS fd : mdsValueMap.keySet()) {
			if (mdsValueMap.get(fd) == mdsValueMap.get(highestValue)) {
				if (isVerbose)
					System.out.println("\t\t" + fd + " v(m) = " + mdsValueMap.get(highestValue));
				highestValues.add(fd);
			}
		}

		// find lowest cost out of highest values
		FD_MDS lowestCost = highestValues.iterator().next();
		for (FD_MDS fd : highestValues) {
			if (mdsCostMap.get(fd) < mdsCostMap.get(lowestCost)) {
				lowestCost = fd;
			}
		}
		if (isVerbose)
			System.out.println("\n\tSELECTED : " + lowestCost + "\n\t   c(m) = "
					+ mdsValueMap.get(lowestCost) + "   v(m) = " + mdsValueMap.get(lowestCost));
		return lowestCost;
	}

	private void removeRedundantEntities() {
		Set<String> workingCover = new HashSet<String>();
		workingCover.addAll(cover);
		@SuppressWarnings("unused")
		Set<String> redundantEnts = new HashSet<String>();
		for (String ent : cover) {
			if (isEntityImplicitlyCoveredBy(ent, workingCover)) {
				workingCover.remove(ent);
				if (isVerbose)
					System.out.println("\tREMOVED Redundant " + ent);
			}
		}
		cover.clear();
		cover.addAll(workingCover);
	}

	private boolean isEntityImplicitlyCoveredBy(String ent, Set<String> workingCover) {
		for (FD_MDS f : mdsFDs) {
			if (f.sRHS.contains(ent) && workingCover.containsAll(f.sLHS)) {
				return true;
			}
		}
		return false;
	}



	// * * * CLOSURES * * * * * * * * * * * *
	// (C+) Signature CLOSURE
	private Set<String> computeSetClosure(Set<String> sInput, String N) {
		if (isVerbose)
			System.out.println("\n[Compute " + N + "+]\n   |" + N + "| = " + sInput.size());

		Set<String> closure = new HashSet<String>();
		closure.addAll(sInput);

		if (isVerbose)
			System.out.println(N + "+ <- " + N + "\n   " + N + "+ = " + closure);

		boolean updated = true;
		while (updated) {
			updated = false;

			for (FD_MDS fd : mdsFDs) {
				if (isVerbose)
					System.out.println("\tm  = " + fd);
				if (closure.containsAll(fd.sLHS) && !closure.containsAll(fd.sRHS)) {
					closure.addAll(fd.sRHS);
					updated = true;

					if (isVerbose)
						System.out.println("\t\t" + N + "+ = " + closure + "\n");
				}
			}
		}
		if (isVerbose) {
			System.out.println("\n\t" + N + "+ = " + closure + "\n   |" + N + "+| = "
					+ closure.size());
			System.out.println("---------------------------");
		}
		return closure;
	}

	// (M+) FD-MDS CLOSURE
	private void computeFDsClosure() {
		if (isVerbose)
			System.out.println("\n[Compute M+]");

		Set<FD_MDS> prevFDsClosure = new HashSet<FD_MDS>();
		Set<FD_MDS> updatedFDsClosure = new HashSet<FD_MDS>();
		if (isVerbose)
			printM(mdsFDs, "Initial");

		// * * * Apply identity rule * * *
		// m = AB -> C then m+ = AB -> ABC
		for (FD_MDS fd : mdsFDs) {
			Set<String> newRHS = new HashSet<String>();
			newRHS.addAll(fd.sLHS);
			newRHS.addAll(fd.sRHS);
			FD_MDS newFD = new FD_MDS(fd.sLHS, newRHS);
			prevFDsClosure.add(newFD);
		}
		updatedFDsClosure.addAll(prevFDsClosure);

		// * * * Main Loop * * *
		boolean updated = true;
		while (updated) {
			updated = false;

			for (FD_MDS fd : prevFDsClosure) {
				if (isVerbose)
					System.out.println("\nAPPLY " + fd);

				Set<FD_MDS> updatedFDs_toAdd = new HashSet<FD_MDS>();
				Set<FD_MDS> updatedFDs_toRemove = new HashSet<FD_MDS>();
				for (FD_MDS upFD : updatedFDsClosure) {
					if (upFD != fd) {
						if (isVerbose)
							System.out.println("\tTO " + upFD);

						if (upFD.sRHS.containsAll(fd.sLHS) && !upFD.sRHS.containsAll(fd.sRHS)) {
							Set<String> newRHS = new HashSet<String>();
							newRHS.addAll(fd.sRHS);
							newRHS.addAll(upFD.sRHS);
							FD_MDS newFD = new FD_MDS(upFD.sLHS, newRHS);

							updatedFDs_toAdd.add(newFD);
							updatedFDs_toRemove.add(upFD);

							if (isVerbose)
								System.out.println("\t\tAPPLIED : " + newFD);
							updated = true;
						}
					}
				}
				updatedFDsClosure.removeAll(updatedFDs_toRemove);
				updatedFDsClosure.addAll(updatedFDs_toAdd);
			}
			prevFDsClosure.clear();
			prevFDsClosure.addAll(updatedFDsClosure);

			if (isVerbose)
				printM(updatedFDsClosure, "Updated");
		}
		mdsFDsClosure = new Vector<FD_MDS>();
		mdsFDsClosure.addAll(updatedFDsClosure);
		if (isVerbose)
			printM(updatedFDsClosure, "Final");
	}



	// * * * Helper(s) * * *
	private void printM(Collection<FD_MDS> fdsss, String n) {
		System.out.println("\n-(" + n + ")-----------------------");
		int i = 1;
		for (FD_MDS fd : fdsss) {
			System.out.println(" m" + i++ + " = " + fd);
		}
		System.out.println("---------------------------");
	}

	private Vector<FD_MDS> transformMDSsToFDs(HashMap<String, Set<Set<String>>> mdss) {

		if (isVerbose)
			System.out.println("transformMDSsToFDs");

		Vector<FD_MDS> newfds = new Vector<FD_MDS>();
		for (String st : mdss.keySet()) {
			for (Set<String> lhs : mdss.get(st)) {
				if (lhs != null) {
					Set<String> rhs = new HashSet<String>();
					rhs.add(st);
					newfds.add(new FD_MDS(lhs, rhs));
				}
			}
		}
		// printM(newfds, "MDSs");
		return newfds;
	}

	private Set<String> getDefinedEntities() {
		Set<String> definedEntities = new HashSet<String>();
		for (FD_MDS fd : mdsFDs) {
			definedEntities.addAll(fd.sRHS);
		}
		return definedEntities;
	}
}