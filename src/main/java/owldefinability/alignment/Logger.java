package owldefinability.alignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat; 
import java.util.Iterator;
import java.util.Vector;

class Logger {

	static Logger sharedInstance;
	StringBuilder logString = new StringBuilder();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	public boolean printToConsole = true;
	public boolean printToFile = true;

	protected Logger() {

	}

	public static Logger getInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Logger();
		}
		return sharedInstance;
	}

	public String getLog() {
		return logString.toString();
	}

	public void logThis(String message) {

		if (printToFile == true)
			this.logString.append("\n" + message);
		if (printToConsole == true)
			System.out.println(message);
	}

	public String printMemoryStats() {
		int mb = 1024 * 1024;

		// Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();

		// Print used memory
		double usedMem = (runtime.totalMemory() - runtime.freeMemory()) / mb;
		double totalMem = runtime.totalMemory() / mb;
		double maxMem = runtime.maxMemory() / mb;
		DecimalFormat df = new DecimalFormat("#.##");

		return new String("##### HEAP  : " + df.format(usedMem / totalMem * 100) + " % | " + usedMem + " of available " + totalMem
				+ " [MB] | Max: " + maxMem + " #####");
	}

	public void saveLogAsFile(String fileName) {

		try {
			String existingStuff = this.readFileAsString(fileName);
			if (existingStuff != null) {
				// System.out.println("appending");

				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(fileName));
					writer.write(new String(existingStuff + this.getLog()));
				} catch (IOException e) {
				} finally {
					try {
						if (writer != null)
							writer.close();
					} catch (IOException e) {
					}
				}
			}
		} catch (IOException e) {
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(fileName));
			} catch (IOException eex) {
			} finally {
				try {
					if (writer != null)
						writer.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}

	}

	public String printCorrespondenceSet(Vector<Correspondence> setToPrint, boolean setSymbolOn, boolean forceFulllength) {

		StringBuilder sb = new StringBuilder();
		if (setSymbolOn == true)
			sb.append("{");

		Iterator<Correspondence> setToPrintIt = setToPrint.iterator();

		while (setToPrintIt.hasNext()) {
			Correspondence theCorr = setToPrintIt.next();
			sb.append(theCorr.toStringNamedAfterLeft());
			if (setToPrintIt.hasNext()) {
				sb.append(", ");
			}
		}
		if (setSymbolOn == true)
			sb.append("}");

		if (sb.toString().length() > 20 && forceFulllength == false)
			return new String("{ ... }");

		return sb.toString();
	}

	private String readFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}

}
