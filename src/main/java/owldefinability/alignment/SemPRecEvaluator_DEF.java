package owldefinability.alignment;


/* 
 * $Id: SemPRecEvaluator.java 1996 2014-11-23 16:30:55Z euzenat $
 *
 * Copyright (C) INRIA, 2009-2014 
 * 
 * EXTENDED by David Geleta 2016
 */
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owl.align.Evaluator;

import fr.inrialpes.exmo.align.impl.BasicEvaluator;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.ObjectCell;
import fr.inrialpes.exmo.align.impl.URIAlignment;
import fr.inrialpes.exmo.align.impl.Annotations;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;
import fr.inrialpes.exmo.align.impl.rel.*;
import fr.inrialpes.exmo.align.impl.renderer.OWLAxiomsRendererVisitor;
import fr.inrialpes.exmo.ontowrap.Ontology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

// ---- IDDL
import fr.paris8.iut.info.iddl.IDDLReasoner;
import fr.paris8.iut.info.iddl.IDDLException;
import fr.paris8.iut.info.iddl.conf.Semantics;

// ----  HermiT Implementation 
import org.semanticweb.owlapi.io.ToStringRenderer;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

import uk.ac.manchester.cs.owl.owlapi.OWLOntologyIRIMapperImpl;
import uk.ac.manchester.cs.owlapi.dlsyntax.DLSyntaxObjectRenderer;

import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.HermiT.Reasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import owldefinability.general.Toolbox;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.Thread;
import java.lang.Runnable;
import java.lang.IllegalArgumentException;
import java.util.Properties;
import java.util.Set;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.net.URI;
 



/**
 * Evaluate proximity between two alignments. This function implements 
 * the original, and the Defianbility-based Semantic Precision/Recall. 
 * The first alignment is the expected (reference) one.
 */
@SuppressWarnings("unused")
public class SemPRecEvaluator_DEF extends PRecEvaluator implements Evaluator {
 
	// nb of returned cells entailed by the reference alignment
	private int nbfoundentailed = 0; 
	// nb of reference cells entailed by returned alignment
	private int nbexpectedentailed = 0; 


	
	// * * * Defianbility-based Semantic P/R * * * 
	public double evaLwithDef(Properties params) throws AlignmentException {
		init(params);
		nbfound = align2.nbCells();
		nbexpected = align1.nbCells();
		
		nbfoundentailed = nbEntailedCorrespondences((ObjectAlignment) align1, (ObjectAlignment) align2);
		nbexpectedentailed = nbEntailedCorrespondences((ObjectAlignment) align2, (ObjectAlignment) align1);

		precision = (double) nbfoundentailed / (double) nbfound;
		recall = (double) nbexpectedentailed / (double) nbexpected;
		return computeDerived();
	}
	
	
	
	
	// * * * Original Semantic P/R * * * 
	/**
	 *
	 * The formulas are standard: given a reference alignment A given an
	 * obtained alignment B which are sets of cells (linking one entity of
	 * ontology O to another of ontolohy O').
	 *
	 * P = |A inter B| / |B| R = |A inter B| / |A| F = 2PR/(P+R) with inter =
	 * set intersection and |.| cardinal.
	 *
	 * In the implementation |B|=nbfound, |A|=nbexpected and |A inter
	 * B|=nbcorrect.
	 * 
	 * This takes semantics as a parameter which should be a litteral of
	 * fr.paris8.iut.info.iddl.conf.Semantics
	 */
	public double eval(Properties params) throws AlignmentException {
		init(params);
		nbfound = align2.nbCells();
		nbexpected = align1.nbCells();

		nbfoundentailed = nbEntailedCorrespondences((ObjectAlignment) align1, (ObjectAlignment) align2);
		nbexpectedentailed = nbEntailedCorrespondences((ObjectAlignment) align2, (ObjectAlignment) align1);

		precision = (double) nbfoundentailed / (double) nbfound;
		recall = (double) nbexpectedentailed / (double) nbexpected;
		
		
		return computeDerived();
	}
 
	// ALPHA-consequences 
	public int nbEntailedCorrespondences(ObjectAlignment al1, ObjectAlignment al2) throws AlignmentException {	
		if (isVerbose) System.out.println("nbEntailedCorrespondences ?");
		
		// IDDL
		if (semantics != null) { 
			ArrayList<Alignment> allist = new ArrayList<Alignment>();
			allist.add(al1);
			try {
				reasoner = new IDDLReasoner(allist, semantics);
			} catch (Exception idex) { // IDDLException... useful when no IDDL
				throw new AlignmentException("Cannot create IDDLReasoner", idex);
			}
		} 
		// Hermit
		else {  
			loadAlignedOntologies(al1); 
		}
		
		// inconsistent, so everything is entailed
		if (!reasoner.isConsistent())
			return al2.nbCells();   
		
		
		if (isVerbose) System.out.println(" ");
		int entailed = 0;
		for (Cell c2 : al2) { 
			
			if (isVerbose) System.out.println("C : " + Toolbox.INSTANCE.prettyPrintCell(c2));
			
			// IDDL
			if (semantics != null) { 
				try {
					if (((IDDLReasoner) reasoner).isEntailed(al2, c2)) {
						System.out.println("\t[SemPR] " +"      --> entailed");
						entailed++;
					}
				} catch (Exception idex) { 
					// counted as non entailed
					System.out.println("\t[SemPR] " +"Cannot be translated.");
				}
			} 
			
			// Hermit
			else { 
				try {
					//System.out.println("O1 U O2 u"); 
					if (reasoner.isEntailed(correspToAxiom(al2, (ObjectCell) c2))) {
						entailed++;
					}
				} catch (AlignmentException aex) { // type mismatch -> 0
					System.out.println("\t[SemPR] " +"Cannot be translated.");
				}
			}
		}
		return entailed;
	}
 
	// Correspondece to OWLAxiom
	public OWLAxiom correspToAxiom(ObjectAlignment al, ObjectCell corresp) throws AlignmentException {
		OWLAxiom returnAxiom = null;
		
		OWLDataFactory owlfactory = manager.getOWLDataFactory();

		LoadedOntology<Object> onto1 = al.getOntologyObject1();
		LoadedOntology<Object> onto2 = al.getOntologyObject2();
		// retrieve entity1 and entity2
		// create the axiom in function of their labels
		Object e1 = corresp.getObject1();
		Object e2 = corresp.getObject2();
		Relation r = corresp.getRelation();
		try {
			if (onto1.isClass(e1)) {
				if (onto2.isClass(e2)) {
					OWLClass entity1 = owlfactory.getOWLClass(IRI.create(onto1.getEntityURI(e1)));
					OWLClass entity2 = owlfactory.getOWLClass(IRI.create(onto2.getEntityURI(e2)));
					if (r instanceof EquivRelation) {
						returnAxiom = owlfactory.getOWLEquivalentClassesAxiom(entity1, entity2);
					} else if (r instanceof SubsumeRelation) {
						returnAxiom = owlfactory.getOWLSubClassOfAxiom(entity2, entity1);
					} else if (r instanceof SubsumedRelation) {
						returnAxiom = owlfactory.getOWLSubClassOfAxiom(entity1, entity2);
					} else if (r instanceof IncompatRelation) {
						returnAxiom = owlfactory.getOWLDisjointClassesAxiom(entity1, entity2);
					}
				} else if (onto2.isIndividual(e2) && (r instanceof HasInstanceRelation)) {
					returnAxiom = owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(IRI.create(onto1.getEntityURI(e1))),
							owlfactory.getOWLNamedIndividual(IRI.create(onto2.getEntityURI(e2))));
				}
			} else if (onto1.isDataProperty(e1) && onto2.isDataProperty(e2)) {
				OWLDataProperty entity1 = owlfactory.getOWLDataProperty(IRI.create(onto1.getEntityURI(e1)));
				OWLDataProperty entity2 = owlfactory.getOWLDataProperty(IRI.create(onto2.getEntityURI(e2)));
				if (r instanceof EquivRelation) {
					returnAxiom = owlfactory.getOWLEquivalentDataPropertiesAxiom(entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					returnAxiom = owlfactory.getOWLSubDataPropertyOfAxiom(entity2, entity1);
				} else if (r instanceof SubsumedRelation) {
					returnAxiom = owlfactory.getOWLSubDataPropertyOfAxiom(entity1, entity2);
				} else if (r instanceof IncompatRelation) {
					returnAxiom = owlfactory.getOWLDisjointDataPropertiesAxiom(entity1, entity2);
				}
			} else if (onto1.isObjectProperty(e1) && onto2.isObjectProperty(e2)) {
				OWLObjectProperty entity1 = owlfactory.getOWLObjectProperty(IRI.create(onto1.getEntityURI(e1)));
				OWLObjectProperty entity2 = owlfactory.getOWLObjectProperty(IRI.create(onto2.getEntityURI(e2)));
				if (r instanceof EquivRelation) {
					returnAxiom = owlfactory.getOWLEquivalentObjectPropertiesAxiom(entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					returnAxiom = owlfactory.getOWLSubObjectPropertyOfAxiom(entity2, entity1);
				} else if (r instanceof SubsumedRelation) {
					returnAxiom = owlfactory.getOWLSubObjectPropertyOfAxiom(entity1, entity2);
				} else if (r instanceof IncompatRelation) {
					returnAxiom = owlfactory.getOWLDisjointObjectPropertiesAxiom(entity1, entity2);
				}
			} else if (onto1.isIndividual(e1)) {
				if (onto2.isIndividual(e2)) {
					OWLIndividual entity1 = owlfactory.getOWLNamedIndividual(IRI.create(onto1.getEntityURI(e1)));
					OWLIndividual entity2 = owlfactory.getOWLNamedIndividual(IRI.create(onto2.getEntityURI(e2)));
					if (r instanceof EquivRelation) {
						returnAxiom = owlfactory.getOWLSameIndividualAxiom(entity1, entity2);
					} else if (r instanceof IncompatRelation) {
						returnAxiom = owlfactory.getOWLDifferentIndividualsAxiom(entity1, entity2);
					}
				} else if (onto2.isClass(e2) && (r instanceof InstanceOfRelation)) {
					returnAxiom =  owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(IRI.create(onto2.getEntityURI(e2))),
							owlfactory.getOWLNamedIndividual(IRI.create(onto1.getEntityURI(e1))));
				}
			}
		} catch (OntowrapException owex) {
			throw new AlignmentException("Error interpreting URI " + owex);
		}
		 
		if (isVerbose) System.out.println("\t" + returnAxiom);
		
		if (returnAxiom == null) throw new AlignmentException("Cannot convert correspondence " + corresp);
		else return returnAxiom;
	}
	
	
	
	
	
	// * * * ACCESSORS * * *
	public int getFoundEntailed() {
		return nbfoundentailed;
	}

	public int getExpectedEntailed() {
		return nbexpectedentailed;
	}
	
	public Properties getResults() {
		Properties results = super.getResults();
		results.setProperty("nbexpectedentailed", Integer.toString(nbexpectedentailed));
		results.setProperty("nbfoundentailed", Integer.toString(nbfoundentailed));
		return results;
	}
	
	
	
	// * * * HELPERS * * *
	public void loadAlignedOntologies(ObjectAlignment align) throws AlignmentException {
		// Get the two ontologies
		Object onto1 = align.getOntology1();
		Object onto2 = align.getOntology2();
		
		// Check that they are OWLOntologies
		if (onto1 == null || !(onto1 instanceof OWLOntology))
			throw new AlignmentException("Can only use OWLOntologies : " + onto1);
		if (onto2 == null || !(onto2 instanceof OWLOntology))
			throw new AlignmentException("Can only use OWLOntologies : " + onto2);
		
		// Put their axioms in a new ontology
		manager = OWLManager.createOWLOntologyManager();
		try {
			OWLOntology ontology = manager.createOntology(IRI.create("http://www")); // should
																						// be
																						// generated
			for (OWLAxiom ax : ((OWLOntology) onto1).getAxioms()) {
				AddAxiom action = new AddAxiom(ontology, ax);
				manager.applyChange(action);
			}
			for (OWLAxiom ax : ((OWLOntology) onto2).getAxioms()) {
				AddAxiom action = new AddAxiom(ontology, ax);
				manager.applyChange(action);
			}
			
			// Put the alignment axioms in the new ontology... 
			for (Cell c : align) { 
				try {
					AddAxiom action = new AddAxiom(ontology, correspToAxiom(align, (ObjectCell) c));
					manager.applyChange(action);
				} catch (AlignmentException alex) {
					System.out.println("Cannot convert correspondence " +  alex);
				}
			}
			 
			reasoner = Toolbox.INSTANCE.getReasoner("Pellet", ontology, 600000); 
			
			/*
			try {
				reasoner = new Reasoner(ontology);
			}
			catch (org.semanticweb.HermiT.datatypes.UnsupportedDatatypeException e) {
				reasoner = Toolbox.INSTANCE.getReasoner("Pellet", ontology, 600000); 
			}*/
			
			
			
		} catch (OWLOntologyCreationException ocex) {
			throw new AlignmentException("Cannot create merged ontology for " + align, ocex);
		}
	}
	
	
	
	// * * * CONSTRUCTOR * * * 
	/**
	 * Creation Initiate Evaluator for precision and recall
	 * 
	 * @param al1
	 *            : the reference alignment
	 * @param al2
	 *            : the alignment to evaluate
	 **/
	public SemPRecEvaluator_DEF(Alignment referenceAl, Alignment pruducedAl) throws AlignmentException {
		super(referenceAl, pruducedAl); 
		convertToObjectAlignments(referenceAl, pruducedAl);
	}

	public SemPRecEvaluator_DEF init(Properties params) {
		super.init();
		nbexpectedentailed = 0;
		nbfoundentailed = 0;
		
		// Set the semantics to be used
		if (params != null) {
			String sem = params.getProperty("semantics");
			if (sem != null) {
				semantics = Semantics.valueOf(sem);
			}
		}
		
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		
		return this;
	}
	
	
	
	// * * * VARS * * *   
	// the semantics used for interpreting alignments null means that we use the reduced semantics with HermiT, 
	// otherwise, this is an IDDL semantics
	private Semantics semantics = null;
	protected OWLOntologyManager manager = null;
	protected OWLReasoner reasoner = null;

	public boolean isVerbose = false; 

}
