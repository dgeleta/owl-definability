package owldefinability.alignment;

import java.io.Serializable;
import java.net.URI;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author David
 * 
 */
class Correspondence implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 451901077434193575L;
	private Vector<MarkedCell> markedCells;
	private int frequency = 0;
	private URI entity1;
	private URI entity2;
	private int numberOfDifferentAlignments;
	private Vector<String> occursInAlignments = new Vector<String>();
	public Vector<AlignmentCorrespondeces> occursInAlignmentsAsAP = new Vector<AlignmentCorrespondeces>();

	
	public float updatedPriorProbability;
	
	private float probabilityPrior;
	private float probabilityConditional;
	private int relativeFrequency;

	public Correspondence(URI entity1, URI entity2, int numberOfDifferentAlignments) {
		this.entity1 = entity1;
		this.entity2 = entity2;
		this.markedCells = new Vector<MarkedCell>();
		this.numberOfDifferentAlignments = numberOfDifferentAlignments;
	}

	public void addOccurence(MarkedCell markedCell) {
		markedCells.add(markedCell);
		frequency = markedCells.size();

		if (this.occursInAlignments.contains(markedCell.getProducedByAlignmentApproach())) {
			System.out.println("DUPLICATE");
		} else {
			occursInAlignments.add(markedCell.getProducedByAlignmentApproach());
		}
	}

	/**
	 * @return the frequency
	 */
	public int getFrequency() {
		return markedCells.size();
	}
	
	public int getRelativeFrequency() {
		return relativeFrequency;
	}
	
	public void setRelativeFrequency(int relativeFrequency) {
		this.relativeFrequency = relativeFrequency;
	}

	/**
	 * @return the probability
	 */
	public double getProbability() {

		double probability = (double) this.frequency / (double) this.numberOfDifferentAlignments;
		return probability;
	}

	/**
	 * @return the entity1
	 */
	public URI getEntity1() {
		return entity1;
	}

	/**
	 * @return the entity2
	 */
	public URI getEntity2() {
		return entity2;
	}

	public Vector<MarkedCell> getOccurences() {
		return markedCells;
	}

	
	
	
	public String toString() {
		return new String("<" + entity1.toString().substring(7) + "-" + entity2.toString().substring(7) + ">");
	}
	
	public String toString2() {
		return new String(entity1.toString() + "-" + entity2.toString());
	}

	public String toStringShort() {
		return new String("<" + getEntityNameFromURI(entity1) + " - " + getEntityNameFromURI(entity2) + ">");
	}

	public String toStringNamedAfterLeft() {

		if (getEntityNameFromURI(entity1).length() == 1 && getEntityNameFromURI(entity1).equals(getEntityNameFromURI(entity2))) {
			return getEntityNameFromURI(entity1);
		}

		return this.toStringShort();
	}

	public String toStringShortHMTL() {
		return new String(getEntityNameFromURI(entity1) + " - " + getEntityNameFromURI(entity2));
	}

	public String toStringForCompare() {
		return new String(getEntityNameFromURI(entity1) + getEntityNameFromURI(entity2));
	}
	
	public String toStringName(boolean left) {
		if (left) return getEntityNameFromURI(entity1);
		return getEntityNameFromURI(entity2);
	}
	
	public String toStringOneEntity(boolean left) {
		if (left) return getEntityNameFromURI(entity1);
		return getEntityNameFromURI(entity2);
	}
	
	
	
	
	

	private String getEntityNameFromURI(URI uri) {
		String uriString = uri.toString();
		return uriString.substring(uriString.lastIndexOf('#') + 1, uriString.length());
	}

	public Vector<String> getOccursInAlignments() {
		return occursInAlignments;
	}

	public float getAverageMeasure() {
		float result = 0;

		Iterator<MarkedCell> markedIt = this.markedCells.iterator();
		while (markedIt.hasNext()) {

			MarkedCell mCell = markedIt.next();
			result = result + (float) mCell.getMeasure();
		}

		result = result / this.markedCells.size();

		return result;
	}

	public String printContainedInAlignments() {

		StringBuilder sb = new StringBuilder();
		boolean first = false;

		Iterator<AlignmentCorrespondeces> alIt = this.occursInAlignmentsAsAP.iterator();
		while (alIt.hasNext()) {
			AlignmentCorrespondeces theAl = alIt.next();
			if (first == false) {
				first = true;
				sb.append("A" + theAl.getindexInAlignemts());
			} else {
				sb.append(", A" + theAl.getindexInAlignemts());
			}
		}
		return sb.toString();
	}

	public float getProbabilityPrior() {
		return probabilityPrior;
	}

	public void setProbabilityPrior(float probabilityPrior) {
		this.probabilityPrior = probabilityPrior;
	}

	public float getProbabilityConditional() {
		return probabilityConditional;
	}

	public void setProbabilityConditional(float probabilityConditional) {
		this.probabilityConditional = probabilityConditional;
	}

}