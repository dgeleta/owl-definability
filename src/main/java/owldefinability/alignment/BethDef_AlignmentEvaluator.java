package owldefinability.alignment;

import java.io.File;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
 




import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;

import owldefinability.definability.ImplicitDefinablityChecker;
import owldefinability.general.Toolbox;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;

@SuppressWarnings({ "unused", "unchecked" })
public class BethDef_AlignmentEvaluator {

	// * * * Constructor * * *
	// normal
	public BethDef_AlignmentEvaluator(String datasetID, String ontPair, File o1_file, File o2_file, boolean onlyRefAlignment) {
		System.out.println("\n\n ... BethDef_AlignmentEvaluator INITALISE " + df_day.format(new Date()) + " ... ");

		this.datasetID = datasetID;
		this.ontPairNames = ontPair;
		this.o1_file = o1_file;
		this.o2_file = o2_file;

		// Load ONTOLOGY
		this.o1_ont = tb.loadOntology(o1_file);
		this.o2_ont = tb.loadOntology(o2_file);

		// Load DEFINED concepts
		o1_definedConcepts = tb.getConceptDefasStrings(this.datasetID, this.o1_file.getName());
		o1_deftypesCount = tb.countDefConcepts(this.datasetID, this.o1_file.getName());
		o2_definedConcepts = tb.getConceptDefasStrings(this.datasetID, this.o2_file.getName());
		o2_deftypesCount = tb.countDefConcepts(this.datasetID, this.o2_file.getName());

		// Load DEFINED roles
		o1_definedRoles = tb.getRoleDefasStrings(this.datasetID, this.o1_file.getName());
		o2_definedRoles = tb.getRoleDefasStrings(this.datasetID, this.o2_file.getName());

		// Load MCDSs
		Object[] res = tb.getConceptMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mcdsComplete = (boolean) res[0];
		o1_mcdss = (HashMap<String, Set<Set<String>>>) res[1];
		res = tb.getConceptMDSsAsStringsWithCompletness(this.datasetID, this.o2_file.getName());
		o2_mcdsComplete = (boolean) res[0];
		o2_mcdss = (HashMap<String, Set<Set<String>>>) res[1];

		// Load MRDSs
		res = tb.getRoleMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mrdsComplete = (boolean) res[0];
		o1_mrdss = (HashMap<String, Set<Set<String>>>) res[1];
		res = tb.getRoleMDSsAsStringsWithCompletness(this.datasetID, this.o2_file.getName());
		o2_mrdsComplete = (boolean) res[0];
		o2_mrdss = (HashMap<String, Set<Set<String>>>) res[1];

		// Load ALIGNMENTS
		Object[] almns = tb.loadAlignments(datasetID, ontPair);
		alignment_names = (Vector<String>) almns[0];
		alignment_systems = (Vector<Alignment>) almns[1];
		alignment_reference = tb.getReferenceAlignment(almns);
		if (onlyRefAlignment) {
			alignment_names.clear();
			alignment_systems.clear();
			alignment_names.add("REF");
			alignment_systems.add(alignment_reference);
		} else {
			alignment_names.remove(alignment_systems.indexOf(alignment_reference));
			alignment_systems.remove(alignment_reference);
		}

		foundDss = new HashMap<String, Set<String>>();

		// DONE
		// checkData();
		System.out.println("\n\n ... BethDef_AlignmentEvaluator INITALISED " + df_day.format(new Date()) + " ... ");

		sb = new StringBuilder();
		sb.append("Dataset, OntPair, Alignment, Entity, IsImpDef?\n");
		sb.append(datasetID + ", " + ontPair + "\n");
	}

	// for one Ontology perspective
	public BethDef_AlignmentEvaluator(String datasetID, String ontPair, File o1_file, File o2_file) {
		System.out.println("\n\n ... BethDef_AlignmentEvaluator INITALISE " + df_day.format(new Date()) + " ... ");

		this.datasetID = datasetID;
		this.ontPairNames = ontPair;
		this.o1_file = o1_file;
		this.o2_file = o2_file;

		// Load ONTOLOGY
		this.o1_ont = tb.loadOntology(o1_file);

		// Load DEFINED concepts
		o1_definedConcepts = tb.getConceptDefasStrings(this.datasetID, this.o1_file.getName());
		o1_deftypesCount = tb.countDefConcepts(this.datasetID, this.o1_file.getName());
		o2_definedConcepts = tb.getConceptDefasStrings(this.datasetID, this.o2_file.getName());
		o2_deftypesCount = tb.countDefConcepts(this.datasetID, this.o2_file.getName());

		// Load DEFINED roles
		o1_definedRoles = tb.getRoleDefasStrings(this.datasetID, this.o1_file.getName());
		o2_definedRoles = tb.getRoleDefasStrings(this.datasetID, this.o2_file.getName());

		// Load MCDSs
		Object[] res = tb.getConceptMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mcdsComplete = (boolean) res[0];
		o1_mcdss = (HashMap<String, Set<Set<String>>>) res[1];

		res = tb.getConceptMDSsAsStringsWithCompletness(this.datasetID, this.o2_file.getName());
		o2_mcdsComplete = (boolean) res[0];
		o2_mcdss = (HashMap<String, Set<Set<String>>>) res[1];

		// Load MRDSs
		res = tb.getRoleMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mrdsComplete = (boolean) res[0];
		o1_mrdss = (HashMap<String, Set<Set<String>>>) res[1];
		res = tb.getRoleMDSsAsStringsWithCompletness(this.datasetID, this.o2_file.getName());
		o2_mrdsComplete = (boolean) res[0];
		o2_mrdss = (HashMap<String, Set<Set<String>>>) res[1];

		// Load ALIGNMENTS
		Object[] almns = tb.loadAlignments(datasetID, ontPair);
		alignment_names = (Vector<String>) almns[0];
		alignment_systems = (Vector<Alignment>) almns[1];
		alignment_reference = tb.getReferenceAlignment(almns);


		foundDss = new HashMap<String, Set<String>>();

		// DONE
		// checkData();
		System.out.println("\n\n ... BethDef_AlignmentEvaluator INITALISED " + df_day.format(new Date()) + " ... ");

		sb = new StringBuilder();
		sb.append("Dataset, OntPair, Alignment, Entity, IsImpDef?\n");
		sb.append(datasetID + ", " + ontPair + "\n");
	}

	// for one Ontology
	public BethDef_AlignmentEvaluator(String datasetID, File o1_file) {

		this.datasetID = datasetID;
		this.o1_file = o1_file;

		// Load ONTOLOGY
		this.o1_ont = tb.loadOntology(o1_file);

		// Load DEFINED concepts
		o1_definedConcepts = tb.getConceptDefasStrings(this.datasetID, this.o1_file.getName());
		o1_deftypesCount = tb.countDefConcepts(this.datasetID, this.o1_file.getName());

		// Load DEFINED roles
		o1_definedRoles = tb.getRoleDefasStrings(this.datasetID, this.o1_file.getName());

		// Load MCDSs
		Object[] res = tb.getConceptMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mcdsComplete = (boolean) res[0];
		o1_mcdss = (HashMap<String, Set<Set<String>>>) res[1];

		// Load MRDSs
		res = tb.getRoleMDSsAsStringsWithCompletness(this.datasetID, this.o1_file.getName());
		o1_mrdsComplete = (boolean) res[0];
		o1_mrdss = (HashMap<String, Set<Set<String>>>) res[1];

		alignment_names = null;
		alignment_systems = null;
		alignment_reference = null;

		// checkData();

	}



	// * * * Coverage-based measures * * *
	public Set<String> getCovered(Set<String> signature, Alignment al) {
		String ontName = getOntName(signature);
		Set<String> res = new HashSet<String>();
		Iterator<Cell> alIt = al.iterator();
		while (alIt.hasNext()) {
			Cell cl = alIt.next();
			if (cl.getObject1().toString().startsWith(new String(ontName + "#"))) {
				if (signature.contains(cl.getObject1().toString())) {
					res.add(cl.getObject1().toString());
				}
			} else if (cl.getObject2().toString().startsWith(new String(ontName + "#"))) {
				if (signature.contains(cl.getObject2().toString())) {
					res.add(cl.getObject2().toString());
				}
			}
		}
		return res;
	}

	public Set<String> getDefCovered(Set<String> signature, Alignment al, String alignmentName) {
		boolean isO1signature = isOnt1Signature(signature, al);
		// System.out.println("\n\ngetDefCovered \n\t" + signature +
		// "\n\tIS ONT1 ? " + isO1signature);

		Set<String> explicitlyCovered = getCovered(signature, al);
		Set<String> implicitlyCovered = new HashSet<String>();
		Vector<String> unCovered = new Vector<String>();
		unCovered.addAll(signature);
		unCovered.removeAll(explicitlyCovered);
		Collections.sort(unCovered, String.CASE_INSENSITIVE_ORDER);

		// System.out.println("\n\n");
		int entCount = 0;
		for (String ent : unCovered) {

			System.out.println("\n(((GETCOVERED " + ++entCount + " of " + unCovered.size() + "))) : " + ent);

			if (isO1signature) {
				if (o1_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
					// System.out.println("UNCOVERED but DEFINED in O1 " + ent);
					if (isImplicitlyDefinableByAlignment(ent, true, al, null, alignmentName))
						implicitlyCovered.add(ent);
				} else {
					// System.out.println("UNCOVERED and UNDEFINED " + ent);
				}
			} else {
				if (o2_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
					// System.out.println("UNCOVERED but DEFINED in O2 " + ent);
					if (isImplicitlyDefinableByAlignment(ent, false, al, null, alignmentName))
						implicitlyCovered.add(ent);
				} else {
					// System.out.println("UNCOVERED and UNDEFINED " + ent);
				}
			}
		}
		explicitlyCovered.addAll(implicitlyCovered);

		return explicitlyCovered;
	}

	public Set<String> getCoveredMappingSet(Set<String> signature, Set<Cell> al) {
		String ontName = getOntName(signature);
		Set<String> res = new HashSet<String>();
		Iterator<Cell> alIt = al.iterator();
		while (alIt.hasNext()) {
			Cell cl = alIt.next();
			if (cl.getObject1().toString().startsWith(new String(ontName + "#"))) {
				if (signature.contains(cl.getObject1().toString())) {
					res.add(cl.getObject1().toString());
				}
			} else if (cl.getObject2().toString().startsWith(new String(ontName + "#"))) {
				if (signature.contains(cl.getObject2().toString())) {
					res.add(cl.getObject2().toString());
				}
			}
		}
		return res;
	}

	public Set<String> getDefCoveredMappingSet(Set<String> signature, Set<Cell> al, String alignmentName) {
		boolean isO1signature = isOnt1Signature(signature, al);
		// System.out.println("\n\ngetDefCovered \n\t" + signature +
		// "\n\tIS ONT1 ? " + isO1signature);

		Set<String> explicitlyCovered = getCoveredMappingSet(signature, al);
		Set<String> implicitlyCovered = new HashSet<String>();
		Set<String> unCovered = new HashSet<String>();
		unCovered.addAll(signature);
		unCovered.removeAll(explicitlyCovered);

		// System.out.println("\n\n");
		int entCount = 0;
		for (String ent : unCovered) {

			System.out.println("\n(((GETCOVERED " + ++entCount + " of " + unCovered.size() + ")))");

			if (isO1signature) {
				if (o1_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
					// System.out.println("UNCOVERED but DEFINED in O1 " + ent);
					if (isImplicitlyDefinableByAlignment(ent, true, null, al, alignmentName))
						implicitlyCovered.add(ent);
				} else {
					// System.out.println("UNCOVERED and UNDEFINED " + ent);
				}
			} else {
				if (o2_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
					// System.out.println("UNCOVERED but DEFINED in O2 " + ent);
					if (isImplicitlyDefinableByAlignment(ent, false, null, al, alignmentName))
						implicitlyCovered.add(ent);
				} else {
					// System.out.println("UNCOVERED and UNDEFINED " + ent);
				}
			}
		}
		explicitlyCovered.addAll(implicitlyCovered);

		return explicitlyCovered;
	}

	// for generated alignemnts
	public Set<String> getCovered(Set<String> signature, Set<String> al) {
		Set<String> covered = new HashSet<String>();
		covered.addAll(signature);
		covered.retainAll(al);
		return covered;
	}

	public Set<String> getDefCovered(Set<String> signature, Set<String> al) {

		Set<String> explicitlyCovered = getCovered(signature, al);
		Set<String> implicitlyCovered = new HashSet<String>();
		Vector<String> unCovered = new Vector<String>();
		unCovered.addAll(signature);
		unCovered.removeAll(explicitlyCovered);
		Collections.sort(unCovered, String.CASE_INSENSITIVE_ORDER);

		// System.out.println("\n\n");
		int entCount = 0;
		for (String ent : unCovered) {

			System.out.println("\n(((GETCOVERED " + ++entCount + " of " + unCovered.size() + "))) : " + ent);

			if (o1_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
				boolean isConcept = true;
				Set<Set<String>> mdss = null;

				// Concept or Role
				if (!isConcept(ent, true)) {
					isConcept = false;
					mdss = o1_mrdss.get(ent);
				} else {
					isConcept = true;
					mdss = o1_mcdss.get(ent);
				}

				boolean isImpDef = false;
				Set<String> expCoveredEntities = al;
				for (Set<String> thisMds : mdss) {
					// Req.1: all descritpion entities are explicitly covered
					if (expCoveredEntities.containsAll(thisMds)) {
						isImpDef = true;
						break;
					}
				}
				if (!isImpDef) {
					if (isConcept && !o1_mcdsComplete) {
						System.out.println("\nINCOMPLETE MCDSs, do ImpDefCheck");
						isImpDef = checkImpDef(ent, true, true, expCoveredEntities, "GEN");
					}
					if (!isConcept && !o1_mrdsComplete) {
						System.out.println("\nINCOMPLETE MRDSs, do ImpDefCheck");
						isImpDef = checkImpDef(ent, true, false, expCoveredEntities, "GEN");
					}
				}

				if (isImpDef) {
					implicitlyCovered.add(ent);
				}
			} else {
				// System.out.println("UNCOVERED and UNDEFINED " + ent);
			}

		}
		explicitlyCovered.addAll(implicitlyCovered);
		return explicitlyCovered;
	}



	// * * * Precision and Recall * * *
	public Properties getDefPrecNRecall(Alignment al_ref, Alignment al, String alName, boolean onlyDefClosure) {
		System.out.println("\n\n\nEVALUATE " + alName);
		loadImplictMappings();

		// Results
		double precision = 1.;
		double recall = 1.;
		double overall = 0.;
		double fmeasure = 0.;
		double result = 1.;
		int nbexpected = 0;
		int nbfound = 0;
		int nbcorrect = 0;

		// definability-closure
		if (loadedClosures == null) {
			defClosure_al = getAlignmentDefClosure(al, alName);
			if (defClosure_alRef == null)
				defClosure_alRef = getAlignmentDefClosure(al_ref, "REF");
		} else {
			defClosure_al = loadedClosures.get(alName);
			if (defClosure_al == null)
				defClosure_al = new Vector<Cell_MDS>();
			defClosure_alRef = loadedClosures.get("REF");
			if (defClosure_alRef == null)
				defClosure_alRef = new Vector<Cell_MDS>();
		}

		// ---------------------------------------------------
		// (1) evaluate (R,A)
		// ---------------------------------------------------
		nbfound = al.nbCells();
		try {
			for (Cell c1 : al_ref) {
				URI uri1 = c1.getObject2AsURI();
				nbexpected++;
				Set<Cell> s2 = al.getAlignCells1(c1.getObject1());
				if (s2 != null) {
					for (Cell c2 : s2) {
						URI uri2 = c2.getObject2AsURI();
						if (uri1.equals(uri2) && c1.getRelation().equals(c2.getRelation())) {
							// System.out.println("\tCORRECT   " +
							// Toolbox.INSTANCE.prettyPrintCell(c1));
							nbcorrect++;
							break;
						}
					}
				}
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}

		// ---------------------------------------------------
		// (2) evaluate ((R+ \ R), (A+ \ A))
		// ---------------------------------------------------
		// Def-based corr. forms:
		nbfound += defClosure_al.size();
		for (Cell_MDS clm : defClosure_alRef) {
			System.out.println("\n REF: " + clm.prettyPrint());
			Object ent_ref_o1 = clm.getO1_aligned();
			Object ent_ref_o2 = clm.getO2_aligned();
			nbexpected++;

			Vector<Cell_MDS> alignedCells = getAlignedCells(ent_ref_o1, defClosure_al);

			// compare
			for (Cell_MDS clm2 : alignedCells) {
				if (compareAlignedObjects(ent_ref_o2, clm2.getO2_aligned())) {
					nbcorrect++;
					clm2.evalResult = true;
					System.out.println("\tCORRECT : " + clm.prettyPrint());
					break;
				}
			}
		}

		// ---------------------------------------------------
		// (3) results
		// ---------------------------------------------------
		// precision
		if (nbfound != 0)
			precision = (double) nbcorrect / (double) nbfound;

		// recall
		if (nbexpected != 0)
			recall = (double) nbcorrect / (double) nbexpected;

		// derived
		if (precision != 0.) {
			fmeasure = 2 * precision * recall / (precision + recall);
			overall = recall * (2 - (1 / precision));
			result = recall / precision;
		} else {
			result = 0.;
		}

		Properties results = new Properties();
		results.setProperty("precision", Double.toString(precision));
		results.setProperty("recall", Double.toString(recall));
		results.setProperty("overall", Double.toString(overall));
		results.setProperty("fmeasure", Double.toString(fmeasure));
		results.setProperty("nbexpected", Integer.toString(nbexpected));
		results.setProperty("nbfound", Integer.toString(nbfound));
		results.setProperty("true positive", Integer.toString(nbcorrect)); 
		 

		return results;
	}
	
	public Properties getDefPrecNRecall2(Alignment al_ref, Alignment al, String alName, boolean onlyDefClosure) {
		System.out.println("\n\n\nEVALUATE " + alName);
		loadImplictMappings();

		// Results
		double precision = 1.;
		double recall = 1.;
		double overall = 0.;
		double fmeasure = 0.;
		double result = 1.;
		int nbexpected = 0;
		int nbfound = 0;
		int nbcorrect = 0;

		// definability-closure
		if (loadedClosures == null) {
			defClosure_al = getAlignmentDefClosure(al, alName);
			if (defClosure_alRef == null)
				defClosure_alRef = getAlignmentDefClosure(al_ref, "REF");
		} else {
			defClosure_al = loadedClosures.get(alName);
			if (defClosure_al == null)
				defClosure_al = new Vector<Cell_MDS>();
			defClosure_alRef = loadedClosures.get("REF");
			if (defClosure_alRef == null)
				defClosure_alRef = new Vector<Cell_MDS>();
		}

		// ---------------------------------------------------
		// (1) evaluate (R,A)
		// ---------------------------------------------------
		nbfound = al.nbCells();
		try {
			for (Cell c1 : al_ref) {
				URI uri1 = c1.getObject2AsURI();
				nbexpected++;
				Set<Cell> s2 = al.getAlignCells1(c1.getObject1());
				if (s2 != null) {
					for (Cell c2 : s2) {
						URI uri2 = c2.getObject2AsURI();
						if (uri1.equals(uri2) && c1.getRelation().equals(c2.getRelation())) {
							// System.out.println("\tCORRECT   " +
							// Toolbox.INSTANCE.prettyPrintCell(c1));
							nbcorrect++;
							break;
						}
					}
				}
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}

		// ---------------------------------------------------
		// (2) evaluate ((R+ \ R), (A+ \ A))
		// ---------------------------------------------------
		// Def-based corr. forms:
		nbfound += defClosure_al.size();
		for (Cell_MDS clm : defClosure_alRef) {
			System.out.println("\n REF: " + clm.prettyPrint());
			Object ent_ref_o1 = clm.getO1_aligned();
			Object ent_ref_o2 = clm.getO2_aligned();
			nbexpected++;

			/*
			Vector<Cell_MDS> alignedCells = getAlignedCells(ent_ref_o1, defClosure_al);

			// compare
			for (Cell_MDS clm2 : alignedCells) {
				if (compareAlignedObjects(ent_ref_o2, clm2.getO2_aligned())) {
					nbcorrect++;
					clm2.evalResult = true;
					System.out.println("\tCORRECT : " + clm.prettyPrint());
					break;
				}
			}
			*/
		}

		// ---------------------------------------------------
		// (3) results
		// ---------------------------------------------------
		// precision
		if (nbfound != 0)
			precision = (double) nbcorrect / (double) nbfound;

		// recall
		if (nbexpected != 0)
			recall = (double) nbcorrect / (double) nbexpected;

		// derived
		if (precision != 0.) {
			fmeasure = 2 * precision * recall / (precision + recall);
			overall = recall * (2 - (1 / precision));
			result = recall / precision;
		} else {
			result = 0.;
		}

		Properties results = new Properties();
		results.setProperty("precision", Double.toString(precision));
		results.setProperty("recall", Double.toString(recall));
		results.setProperty("overall", Double.toString(overall));
		results.setProperty("fmeasure", Double.toString(fmeasure));
		results.setProperty("nbexpected", Integer.toString(nbexpected));
		results.setProperty("nbfound", Integer.toString(nbfound));
		results.setProperty("true positive", Integer.toString(nbcorrect));

		return results;
	}

	
	
	private Vector<Cell_MDS> getAlignmentDefClosure(Alignment al, String alName) {
		// System.out.println("getAlignmentDefClosure " + alName + " " +
		// al.nbCells());
		Vector<Cell_MDS> closure = new Vector<Cell_MDS>();
		closure.addAll(getAlignmentDefClosureOfOntology(al, alName, true));
		closure.addAll(getAlignmentDefClosureOfOntology(al, alName, false));
		return closure;
	}

	private Vector<Cell_MDS> getAlignmentDefClosureOfOntology(Alignment al, String alName, boolean isO1) {
		Vector<Cell_MDS> closure = new Vector<Cell_MDS>();
		OWLOntology thisOnt = o1_ont;
		if (!isO1)
			thisOnt = o2_ont;

		// only def covered entities, create MDS correspondeces
		Set<String> o_sigma = tb.getConceptAndRoleNamesAsStrings(thisOnt);
		Set<String> o_explicitlyCovered = getCovered(o_sigma, al);
		Set<String> o_checkIfImpCovered = new HashSet<String>();
		o_checkIfImpCovered.addAll(o_sigma);
		o_checkIfImpCovered.removeAll(o_explicitlyCovered);

		// System.out.println("O check " + o_checkIfImpCovered.size());

		for (String ent : o_checkIfImpCovered) {
			if (isO1) {
				if (o1_definedConcepts.keySet().contains(ent) || o1_definedRoles.keySet().contains(ent)) {
					Cell_MDS impMapping = isThereAnImplicitMapping(ent, isO1, al, null, alName);
					if (impMapping != null)
						closure.add(impMapping);
				}
			} else {
				if (o2_definedConcepts.keySet().contains(ent) || o2_definedRoles.keySet().contains(ent)) {
					Cell_MDS impMapping = isThereAnImplicitMapping(ent, isO1, al, null, alName);
					if (impMapping != null)
						closure.add(impMapping);
				}
			}
		}
		return closure;
	}

	private Vector<Cell_MDS> getAlignedCells(Object ent, Vector<Cell_MDS> alg) {

		Vector<Cell_MDS> cells = new Vector<Cell_MDS>();
		for (Cell_MDS clm : alg) {
			if (compareAlignedObjects(ent, clm.getO1_aligned())) {
				cells.add(clm);
			}
		}
		return cells;
	}

	private boolean compareAlignedObjects(Object o1, Object o2) {
		// System.out.println("compareAlignedObjects " + o1 + "  " + o2);

		if (o1.getClass().equals(String.class)) {
			// e ? e
			if (o2.getClass().equals(String.class)) {
				if (((String) o1).equals(((String) o2))) {
					return true;
				}
			}
			// e ? {e}
			else {
				if (((Set<String>) o2).size() == 1 && ((Set<String>) o2).contains((String) o1)) {
					return true;
				}
			}
		} else {
			// {e} ? e
			if (o2.getClass().equals(String.class)) {
				if (((Set<String>) o1).size() == 1 && ((Set<String>) o1).contains((String) o2)) {
					return true;
				}
			}
			// {e} ? {e}
			else {
				if (((Set<String>) o1).containsAll((Set<String>) o2) && ((Set<String>) o1).size() == ((Set<String>) o2).size()) {
					return true;
				}
			}
		}
		return false;
	}



	// * * * Using Precision * * *
	public Alignment getCorrectCorrespondeces(Alignment alg) {
		HashSet<Cell> removes = new HashSet<Cell>();

		System.out.println("INPUT " + alg.nbCells());

		try {
			for (Cell c1 : alg) {
				URI uri1 = c1.getObject2AsURI();
				Set<Cell> s2 = alignment_reference.getAlignCells1(c1.getObject1());
				boolean cellcorrect = false;

				if (s2 != null) {
					for (Cell c2 : s2) {
						URI uri2 = c2.getObject2AsURI();
						if (uri1.equals(uri2) && c1.getRelation().equals(c2.getRelation())) {
							cellcorrect = true;
							break;
						}
					}
				}
				if (!cellcorrect) {
					removes.add(c1);
				}
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}

		for (Cell rm : removes) {
			try {
				alg.remCell(rm);
			} catch (AlignmentException e) {
				e.printStackTrace();
			}
		}
		System.out.println("INPUT CUT  " + alg.nbCells());
		return alg;
	}



	// * * * TODO * * *
	public Set<String> getDistinguishable(Set<String> signature, Alignment al) {

		String ontName = getOntName(signature);
		Set<String> res = new HashSet<String>();

		// get all e'
		Iterator<Cell> alIt = al.iterator();
		while (alIt.hasNext()) {
			Cell cll = alIt.next();

			// System.out.println("??? " + cll.getObject1().toString() + " " +
			// cll.getObject2().toString());

			// e in S
			if (signature.contains(cll.getObject1().toString()) || signature.contains(cll.getObject2().toString())) {

				// e - e'
				if (cll.getObject1().toString().startsWith(new String(ontName + "#"))) {
					res.add(cll.getObject2().toString());
				}

				// e'- e
				else if (cll.getObject2().toString().startsWith(new String(ontName + "#"))) {
					res.add(cll.getObject1().toString());
				}
			}
		}
		return res;
	}



	// * * * Definability * * *
	private boolean isImplicitlyDefinableByAlignment(String entity, boolean isO1entity, Alignment al, Set<Cell> alSet, String alignmentName) {

		boolean isConcept = true;
		Set<Set<String>> mdss = null;

		// Defined in Ont1
		if (isO1entity) {
			// Concept or Role
			if (!isConcept(entity, isO1entity)) {
				isConcept = false;
				mdss = o1_mrdss.get(entity);
			} else {
				isConcept = true;
				mdss = o1_mcdss.get(entity);
			}
		}
		// Defined in Ont2
		else {
			// Concept or Role
			if (!isConcept(entity, isO1entity)) {
				isConcept = false;
				mdss = o2_mrdss.get(entity);
			} else {
				isConcept = true;
				mdss = o2_mcdss.get(entity);
			}
		}
		// System.out.println("\n   isImplicitlyDefinableByAlignment ?   " +
		// entity + "    isO1 " + isO1entity + "\n" + "      CONCEPT? " +
		// isConcept+ "    |MDS| ?" + mdss);

		// System.out.println("O1? " + isO1entity + " );


		// search MDSs
		Set<String> expCoveredEntities = null;
		if (al != null) {
			expCoveredEntities = getAlignmentEntities(isO1entity, al, null);
		} else {
			expCoveredEntities = getAlignmentEntities(isO1entity, null, alSet);
		}


		for (Set<String> thisMds : mdss) {
			// System.out.println("   MDS : " + thisMds);

			// Req.1: all descritpion entities are explicitly covered
			// System.out.println("??? " + "\n\t" + thisMds + "\n\t" +
			// expCoveredEntities + "\n\t CONTAINED ??? " );

			if (expCoveredEntities.containsAll(thisMds)) {

				// System.out.println("MDS is exp mapped " + "\n\t" + thisMds +
				// "\n\t" + expCoveredEntities+"\n\n");
				return true;

				// Req.2: with the same relation
			}
		}


		// if MDSs are incomplete, just check whether the alignment signature is
		// impDef
		boolean isimpDef = false;

		if (isO1entity) {
			if (isConcept && !o1_mcdsComplete) {
				System.out.println("\nINCOMPLETE MCDSs, do ImpDefCheck");
				isimpDef = checkImpDef(entity, true, true, expCoveredEntities, alignmentName);
			}
			if (!isConcept && !o1_mrdsComplete) {
				System.out.println("\nINCOMPLETE MRDSs, do ImpDefCheck");
				isimpDef = checkImpDef(entity, true, false, expCoveredEntities, alignmentName);
			}
		} else {
			if (isConcept && !o2_mcdsComplete) {
				System.out.println("\nINCOMPLETE MCDSs, do ImpDefCheck");
				isimpDef = checkImpDef(entity, false, true, expCoveredEntities, alignmentName);
			}
			if (!isConcept && !o2_mrdsComplete) {
				System.out.println("\nINCOMPLETE MRDSs, do ImpDefCheck");
				isimpDef = checkImpDef(entity, false, false, expCoveredEntities, alignmentName);
			}
		}

		if (isimpDef) {
			foundDss.put(entity, expCoveredEntities);
			saveDSs();
		}

		String line = new String(", , " + alignmentName + ", " + entity + ", " + isimpDef);
		if (!sb.toString().contains(line))
			sb.append(line + "\n");
		// tb.writeToFile("_experiments/tkde2016/" + datasetID +
		// "_impDefCheckList.csv", sb.toString());
		// / tb.writeToFile(resultsFoler + "/impDefCheckList_" + datasetID + "_"
		// + ontPairNames + ".csv", sb.toString());
		
		if (!new File(new String("_experimentComputations/" + datasetID + "/_implictMappings/" + ontPairNames
				+ "_implictMappings.csv")).exists()) {
			tb.writeToFile("_experimentComputations/_impDefCheck/" + datasetID +  "/impDefCheckList_" + datasetID + "_" + ontPairNames + "_" + alignmentName + ".csv", sb.toString());
		} 
		return isimpDef;
	}

	private Cell_MDS isThereAnImplicitMapping(String entity, boolean isO1entity, Alignment al, Set<Cell> alSet, String alignmentName) {
		boolean isConcept = true;
		Set<Set<String>> mdss = null;

		// Defined in Ont1
		if (isO1entity) {
			// Concept or Role
			if (!isConcept(entity, isO1entity)) {
				isConcept = false;
				mdss = o1_mrdss.get(entity);
			} else {
				isConcept = true;
				mdss = o1_mcdss.get(entity);
			}
		}
		// Defined in Ont2
		else {
			// Concept or Role
			if (!isConcept(entity, isO1entity)) {
				isConcept = false;
				mdss = o2_mrdss.get(entity);
			} else {
				isConcept = true;
				mdss = o2_mcdss.get(entity);
			}
		}

		// search MDSs
		Set<String> expCoveredEntities = null;
		if (al != null) {
			expCoveredEntities = getAlignmentEntities(isO1entity, al, null);
		} else {
			expCoveredEntities = getAlignmentEntities(isO1entity, null, alSet);
		}

		for (Set<String> thisMds : mdss) {
			if (expCoveredEntities.containsAll(thisMds)) {
				Cell_MDS newmds = generateImplicitMapping(entity, isO1entity, thisMds, al);
				if (alignmentName.equals("REF"))
					newmds.evalResult = true;
				return newmds;
			}
		}
		return null;
	}

	private Cell_MDS generateImplicitMapping(String entity, boolean isO1entity, Set<String> entityMds, Alignment al) {
		// System.out.println("\ngenerateImplicitMapping: " + entity +
		// "\n\tfrom MDS: " + entityMds + "\n\t is O1? " + isO1entity);
		HashMap<String, Vector<Cell>> mappings = new HashMap<String, Vector<Cell>>();

		for (String mdsEnt : entityMds) {
			// System.out.println("\t" + mdsEnt);
			mappings.put(mdsEnt, getMappingsForEntity(mdsEnt, isO1entity, al));
		}

		Object[] result = reconcileMappings(mappings);
		if (result != null) {
			if (isO1entity) {
				return new Cell_MDS(entity, getAlignmentEntities2(false, (Vector<Cell>) result[1]), (Relation) result[0]);
			} else {
				return new Cell_MDS(getAlignmentEntities2(true, (Vector<Cell>) result[1]), entity, (Relation) result[0]);
			}
		}
		return null;
	}

	private Vector<Cell> getMappingsForEntity(String entity, boolean isO1entity, Alignment al) {
		Vector<Cell> cells = new Vector<Cell>();
		Iterator<Cell> alit = al.iterator();
		while (alit.hasNext()) {
			Cell cl = alit.next();

			if (isO1entity) {
				if (entity.equals(cl.getObject1().toString())) {
					cells.add(cl);
				}
			} else {
				if (cl.getObject2().toString().equals(entity)) {
					cells.add(cl);
				}
			}
		}
		return cells;
	}

	private Object[] reconcileMappings(HashMap<String, Vector<Cell>> mappings) {
		Object[] result = new Object[2];
		Vector<Cell> reconcilledMappings = new Vector<Cell>();

		// EQV
		for (String mdsEnt : mappings.keySet()) {
			for (Cell cl : mappings.get(mdsEnt)) {
				if (cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.EquivRelation.class)) {
					reconcilledMappings.add(cl);
					break;
				}
			}
		}
		if (reconcilledMappings.size() == mappings.keySet().size()) {
			result[0] = new fr.inrialpes.exmo.align.impl.rel.EquivRelation();
			result[1] = reconcilledMappings;

			// System.out.println("EQV MAPPING !!! ");
			return result;
		} else {
			reconcilledMappings.clear();
		}


		// SUBSET
		for (String mdsEnt : mappings.keySet()) {
			for (Cell cl : mappings.get(mdsEnt)) {
				if (cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.SubsumedRelation.class)
						|| cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.EquivRelation.class)) {
					reconcilledMappings.add(cl);
					break;
				}
			}
		}
		if (reconcilledMappings.size() == mappings.keySet().size()) {
			result[0] = new fr.inrialpes.exmo.align.impl.rel.SubsumedRelation();
			result[1] = reconcilledMappings;

			// System.out.println("SubsumedRelation MAPPING !!! ");
			return result;
		} else {
			reconcilledMappings.clear();
		}


		// SUPSET
		for (String mdsEnt : mappings.keySet()) {
			for (Cell cl : mappings.get(mdsEnt)) {
				if (cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.SubsumeRelation.class)
						|| cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.EquivRelation.class)) {
					reconcilledMappings.add(cl);
					break;
				}
			}
		}
		if (reconcilledMappings.size() == mappings.keySet().size()) {
			result[0] = new fr.inrialpes.exmo.align.impl.rel.SubsumeRelation();
			result[1] = reconcilledMappings;

			// System.out.println("SubsumeRelation MAPPING !!! ");
			return result;
		} else {
			reconcilledMappings.clear();
		}



		// DISJ
		for (String mdsEnt : mappings.keySet()) {
			for (Cell cl : mappings.get(mdsEnt)) {
				if (cl.getRelation().getClass().equals(fr.inrialpes.exmo.align.impl.rel.IncompatRelation.class)) {
					reconcilledMappings.add(cl);
					break;
				}
			}
		}
		if (reconcilledMappings.size() == mappings.keySet().size()) {
			result[0] = new fr.inrialpes.exmo.align.impl.rel.IncompatRelation();
			result[1] = reconcilledMappings;

			// System.out.println("Disjoint MAPPING !!! ");
			return result;
		} else {
			reconcilledMappings.clear();
		}


		// UnReconcillable
		System.out.println("UNRECONCILLED MAPPING SET");
		return null;
	}

	private void loadImplictMappings() {
		if (loadedClosures == null) {
			File impFile = new File(new String("_experimentComputations/" + datasetID + "/_implictMappings/" + ontPairNames
					+ "_implictMappings.csv"));

			System.out.println("\n\n\nloadImplictMappings " + impFile.getName() + " " + impFile.exists());
			if (impFile.exists()) {
				loadedClosures = new HashMap<String, Vector<Cell_MDS>>();
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(impFile.getAbsolutePath()));
				scanner.nextLine();
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					String[] words = line.split(", ");
					// System.out.println("   " + line);

					String alName = words[0];
					String relation = words[3];

					Cell_MDS clm = new Cell_MDS(parseAlignedObject(words[1]), parseAlignedObject(words[2]), parseRelation(relation));
					clm.evalResult = Boolean.valueOf(words[4]);
					// System.out.println("\tCELL : " + clm.prettyPrint());

					if (loadedClosures.get(alName) == null) {
						Vector<Cell_MDS> vec = new Vector<Cell_MDS>();
						vec.add(clm);
						loadedClosures.put(alName, vec);
					} else {
						loadedClosures.get(alName).add(clm);
					}
				}
			}
		}
	}

	private Object parseAlignedObject(String input) {
		if (input.toString().startsWith("{")) {
			input = input.substring(1);
			input = input.substring(0, input.length() - 1);
			Set<String> res = new HashSet<String>();
			String[] words = input.split(" ");
			for (int i = 0; i < words.length; i++) {
				res.add(words[i]);
			}
			return res;
		} else {
			return input;
		}
	}

	private Relation parseRelation(String input) {
		switch (input) {
		case "=":
			return new fr.inrialpes.exmo.align.impl.rel.EquivRelation();
		case "<":
			return new fr.inrialpes.exmo.align.impl.rel.SubsumedRelation();
		case ">":
			return new fr.inrialpes.exmo.align.impl.rel.SubsumeRelation();
		case "%":
			return new fr.inrialpes.exmo.align.impl.rel.IncompatRelation();
		}
		return null;
	}


	// * * * Helpers * * *
	private void checkData() {

		// Ontology 1
		System.out.println("\n");
		for (String st : o1_definedConcepts.keySet()) {
			System.out.println("   O1 : " + st + " " + o1_definedConcepts.get(st) + "   |MDSs| = " + o1_mcdss.get(st).size());

			for (Set<String> ms : o1_mcdss.get(st)) {
				System.out.println("\t" + ms);
			}
		}
		for (String st : o1_definedRoles.keySet()) {
			System.out.println("   O1 : " + st + " " + o1_definedRoles.get(st) + "   |MDSs| = " + o1_mrdss.get(st).size());

			for (Set<String> ms : o1_mrdss.get(st)) {
				System.out.println("\t" + ms);
			}
		}

		// Ontology 2
		System.out.println("\n");
		for (String st : o2_definedConcepts.keySet()) {
			System.out.println("   O2 : " + st + " " + o2_definedConcepts.get(st) + "   |MDSs| = " + o2_mcdss.get(st).size());

			for (Set<String> ms : o2_mcdss.get(st)) {
				System.out.println("\t" + ms);
			}
		}
		for (String st : o2_definedRoles.keySet()) {
			System.out.println("   O2 : " + st + " " + o2_definedRoles.get(st) + "   |MDSs| = " + o2_mrdss.get(st).size());

			for (Set<String> ms : o2_mrdss.get(st)) {
				System.out.println("\t" + ms);
			}
		}

		// Alignments
		System.out.println("\n");
		for (int i = 0; i < alignment_names.size(); i++) {
			System.out.println("\n   (A" + i + ") : " + alignment_names.elementAt(i));

			Iterator<Cell> alIt = alignment_systems.elementAt(i).iterator();
			while (alIt.hasNext()) {
				Cell cl = alIt.next();
				System.out.println("\t<" + cl.getObject1().toString() + ", " + cl.getRelation().getRelation() + ", "
						+ cl.getObject2().toString() + ">");
			}
		}
		System.out.println("\n   (A-REF)");
		Iterator<Cell> alIt = alignment_reference.iterator();
		while (alIt.hasNext()) {
			Cell cl = alIt.next();
			System.out.println("\t<" + cl.getObject1().toString() + ", " + cl.getRelation().getRelation() + ", "
					+ cl.getObject2().toString() + ">");
		}
	}

	private String getOntName(Set<String> signature) {
		String ent = signature.iterator().next();
		ent = ent.split("#")[0];
		ent = ent.replace("<http://", "");
		return ent;
	}

	private boolean isOnt1Signature(Set<String> signature, Alignment al) {
		String ont1name = getOntName(signature);
		Iterator<Cell> alIt = al.iterator();

		while (alIt.hasNext()) {
			Cell cl = alIt.next();

			if (!cl.getObject1().toString().endsWith("#Thing") && !cl.getObject1().toString().endsWith("#Nothing")) {
				String oclOnt = cl.getObject1().toString().split("#")[0];
				// System.out.println("\n\nisOnt1Signature\n\t" + signature
				// +"\n\t" + oclOnt +"\n\n");
				if (oclOnt.endsWith(new String(ont1name))) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	private boolean isOnt1Signature(Set<String> signature, Set<Cell> al) {
		String ont1name = getOntName(signature);
		Iterator<Cell> alIt = al.iterator();

		while (alIt.hasNext()) {
			Cell cl = alIt.next();

			if (!cl.getObject1().toString().endsWith("#Thing") && !cl.getObject1().toString().endsWith("#Nothing")) {
				String oclOnt = cl.getObject1().toString().split("#")[0];
				// System.out.println("\n\nisOnt1Signature\n\t" + signature
				// +"\n\t" + oclOnt +"\n\n");
				if (oclOnt.endsWith(new String(ont1name))) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	private Set<String> getAlignmentEntities(boolean getO1entities, Alignment al, Set<Cell> alSet) {
		Set<String> alEnts = new HashSet<String>();

		Iterator<Cell> alIt;

		if (al != null) {
			alIt = al.iterator();
		} else {
			alIt = alSet.iterator();
		}

		while (alIt.hasNext()) {
			Cell cl = alIt.next();
			if (getO1entities) {
				alEnts.add(cl.getObject1().toString());
			} else {
				alEnts.add(cl.getObject2().toString());
			}
		}
		return alEnts;
	}

	private Set<String> getAlignmentEntities2(boolean getO1entities, Vector<Cell> mappings) {
		Set<String> alEnts = new HashSet<String>();
		for (Cell cl : mappings) {
			if (getO1entities) {
				alEnts.add(cl.getObject1().toString());
			} else {
				alEnts.add(cl.getObject2().toString());
			}
		}
		return alEnts;
	}

	private boolean checkImpDef(String entity, boolean isO1, boolean isConcept, Set<String> expCoveredEntities, String alignmentName) {

		// check precomputed
		if (impDefResult != null) {
			if (impDefResult.keySet().contains(entity)) {
				System.out.println("CHECKING precomputed " + entity);

				if (impDefResult.get(entity) == 0) {
					return false;
				} else {
					return true;
				}
			}
		}

		// check
		boolean isImpDef = false;
		OWLOntology thisont = null;
		File thisOntFile = null;
		if (isO1) {
			thisont = o1_ont;
			thisOntFile = o1_file;
		} else {
			thisont = o2_ont;
			thisOntFile = o2_file;
		}

		OWLEntity ent = tb.getEntity(IRI.create(entity), thisont);

		Set<OWLEntity> sigma = new HashSet<OWLEntity>();
		for (String st : expCoveredEntities) {
			sigma.add(tb.getEntity(IRI.create(entity), thisont));
		}


		// MOD
		Set<OWLAxiom> ontMod = null;
		OWLOntology thisOnt = o1_ont;
		if (!isO1) {
			thisOnt = o2_ont;
		}
		try {
			Set<OWLEntity> singeltonSignature = new HashSet<OWLEntity>();
			singeltonSignature.add(ent);
			ontMod = tb.extractModuleAxioms(singeltonSignature, thisOnt, ModuleType.STAR);
			System.out.println("  MODULARIZED\n\t|Ax(O)| = " + thisOnt.getLogicalAxiomCount() + "\n\t|Ax(M)| = " + ontMod.size());

		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}



		// CHECK IMPDEF


		try {
			ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker(thisOntFile, datasetID, "Hermit", 600000, false);
			if (isConcept) {
				isImpDef = impDef.isConceptImplicitlyDefined_MOD((OWLClass) ent, sigma, ontMod);
			} else {
				isImpDef = impDef.isRoleImplicitlyDefined(ent, sigma);
			}
		} catch (org.semanticweb.owlapi.reasoner.InconsistentOntologyException e) {
			// e.printStackTrace();
		} catch (org.semanticweb.HermiT.datatypes.UnsupportedDatatypeException e) {
			// e.printStackTrace();
			try {
				System.out.println("\n IS IMPDEF? switching to PELLET\n");
				ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker(o1_file, datasetID, "Pellet", 600000, false);
				if (isConcept) {
					isImpDef = impDef.isConceptImplicitlyDefined_MOD((OWLClass) ent, sigma, ontMod);
				} else {
					isImpDef = impDef.isRoleImplicitlyDefined(ent, sigma);
				}
			} catch (Exception f1) {
				// f1.printStackTrace();
			}
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}

		System.out.println(ontPairNames + " " + alignmentName + "\n" + "   IS IMPDEF ? " + entity + " by ... " + "\n\t RESULT : "
				+ isImpDef + "\t" + df_full.format(new Date()));

		return isImpDef;
	}

	private boolean isConcept(String entName, boolean o1Entity) {
		OWLOntology ont = null;
		if (o1Entity) {
			ont = o1_ont;
		} else {
			ont = o2_ont;
		}
		OWLEntity ent = tb.getEntity(IRI.create(entName), ont);

		if (ent == null)
			return false;

		return ent.isOWLClass();
	}

	private void saveDSs() {
		StringBuilder sb = new StringBuilder();
		for (String ent : foundDss.keySet()) {
			sb.append("E: " + ent + "\n");
			sb.append("DS:");
			for (String mdsst : foundDss.get(ent)) {
				sb.append(" " + mdsst);
			}
			sb.append("\n");
		}
		tb.writeToFile(resultsFoler + "/" + datasetID + "/" + df_day.format(new Date()) + "_" + ontPairNames + "_newDSs.txt", sb.toString());
	}

	public void loadImpDefChecks(String alignmentName) {
		impDefResult = new HashMap<String, Integer>();
		File f = new File("_experimentComputations/" + datasetID + "/_impDefCheck/impDefCheckList_" + datasetID + "_" + ontPairNames + "_"
				+ alignmentName + ".csv");

		if (f.exists()) {
			Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(f.getAbsolutePath()));
			scanner.nextLine();
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line != null) {
					// System.out.println("???" + line);
					String[] words = line.split(",");
					sb.append(line + "\n");

					for (int i = 0; i < words.length; i++) {
						// System.out.println("\t" + i + "   '" +
						// words[i].trim() + "'");


						if (words[4].trim().equals("false")) {
							impDefResult.put(words[3].trim(), 0);
						} else {
							impDefResult.put(words[3].trim(), 1);
						}
					}
				}
			}
			scanner.close();
		}

		/*
		for (String st : impDefResult.keySet()) {
			System.out.println("C: " + st + " " + impDefResult.get(st));
		}
		System.exit(0);
		 */
	}



	// * * * VARS * * *
	private String datasetID, ontPairNames;
	private File o1_file, o2_file;
	public OWLOntology o1_ont, o2_ont;
	private HashMap<String, String> o1_definedConcepts, o2_definedConcepts;
	private HashMap<String, Set<Set<String>>> o1_mcdss, o2_mcdss;
	private HashMap<String, Set<String>> foundDss;
	private HashMap<String, String> o1_definedRoles, o2_definedRoles;
	private HashMap<String, Set<Set<String>>> o1_mrdss, o2_mrdss;
	private int[] o1_deftypesCount, o2_deftypesCount;
	private boolean o1_mcdsComplete, o2_mcdsComplete = false;
	private boolean o1_mrdsComplete, o2_mrdsComplete = false;

	public final Vector<Alignment> alignment_systems;
	public final Alignment alignment_reference;
	public final Vector<String> alignment_names;

	private StringBuilder sb;
	private HashMap<String, Integer> impDefResult;

	private Toolbox tb = Toolbox.INSTANCE;
	private DecimalFormat df = new DecimalFormat("#.##");
	private SimpleDateFormat df_full = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
	private SimpleDateFormat df_day = new SimpleDateFormat("hh:mm:ss");

	public String resultsFoler = "_experimentComputations/" + datasetID;

	public Vector<Cell_MDS> defClosure_al;
	public Vector<Cell_MDS> defClosure_alRef;
	private HashMap<String, Vector<Cell_MDS>> loadedClosures;

}
