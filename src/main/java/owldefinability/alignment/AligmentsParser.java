package owldefinability.alignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.Iterator;
import java.util.Vector;
 



import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;

import fr.inrialpes.exmo.align.parser.AlignmentParser;

@SuppressWarnings("unused")
public class AligmentsParser {

	public String[] ontologies = new String[2];

	private Vector<String> alignmentApproaches = new Vector<String>();

	private Vector<AlignmentCorrespondeces> alignments = new Vector<AlignmentCorrespondeces>();

	private AlignmentCorrespondeces goldStandardAlignment;

	private boolean hasGoldStandard = false;

	public boolean parsingVerbose = true;

	/**
	 * P = the multi-set of all correspondeces produced by all alignment
	 * approaches, where members are allowed to appear more than once. The
	 * observation set.
	 */
	private Vector<MarkedCell> population = new Vector<MarkedCell>();

	/**
	 * C = the set of all possible correspondeces produced by at least one
	 * alignment approach. All members of the set are different. The event set.
	 */
	private Vector<Correspondence> correspondences = new Vector<Correspondence>();

	public Vector<Correspondence> correspondencesCopy = new Vector<Correspondence>();

	/**
	 * C' = the set of all possible correspondeces produced by exacty one
	 * alignment approach, where f(c) = 1
	 */
	public Vector<Correspondence> correspondencesFreq1 = new Vector<Correspondence>();

	/**
	 * C" = the set of all possible correspondences produced by more than one
	 * alignemnt approach, but not all, where 1 < f(c) < MAX (MAX = |A|)
	 */
	public Vector<Correspondence> correspondencesFreqLarger1 = new Vector<Correspondence>();

	/**
	 * C_max = the set of all possible correspondeces produced by all alignment
	 * approaches where f(c) = MAX
	 */
	public Vector<Correspondence> correspondencesFreqMax = new Vector<Correspondence>();

	Vector<Correspondence> newCorrs;

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// PARSE
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	public void parseAlignmentsInFolder(String folderPath) {
		if (parsingVerbose == true)
			Logger.getInstance().logThis(
					"========================================================================" + "\nALIGNMENT PARSER: start parsing "
							+ folderPath);

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

		Iterator<File> fileIt = this.getFilesFromFolder(folderPath).iterator();
		while (fileIt.hasNext()) {
			File file = fileIt.next();

			// Record approach
			String approachProducingAlignment = getAlignmentApproachNameFromFile(file);
			if (!alignmentApproaches.contains(approachProducingAlignment)) {
				alignmentApproaches.add(approachProducingAlignment);
			}

			AlignmentCorrespondeces thisAlignment = new AlignmentCorrespondeces(approachProducingAlignment, file,
					this.alignments.size() + 1);

			try {

				// Initalize Alignment API parser
				AlignmentParser aparser = new AlignmentParser(0);
				Alignment ref = aparser.parse(file.toURI());

				// Record Ontology names
				if (ontologies[0] == null) {
					if (ref.getOntology1URI().toString().equals("null")) {
						// System.out.println("NULL NAME" + file.toString());

						String tokens[] = file.getName().split("\\-");
						ontologies[0] = tokens[1];

						String tokens2[] = tokens[2].split("\\.");
						ontologies[1] = tokens2[0];
					} else {
						ontologies[0] = getOntologyNameFromURI(ref.getOntology1URI());
						ontologies[1] = getOntologyNameFromURI(ref.getOntology2URI());
					}
				}

				// Populate the observation set P
				Iterator<Cell> cellIt = ref.iterator();
				while (cellIt.hasNext()) {

					Cell thisCell = cellIt.next();

					// System.out.println(thisCell.getObject1AsURI(null).toString()
					// + " " + thisCell.getObject2AsURI(null).toString());

					thisAlignment.addCell(thisCell);

					MarkedCell thisMarkedCell = new MarkedCell(thisCell, approachProducingAlignment);

					// System.out.println("MarkedCell " +
					// thisMarkedCell.toString());

					this.addMarkedCellToPopulation(thisMarkedCell);
				}
				alignments.add(thisAlignment);

			} catch (AlignmentException e) {
				 if (parsingVerbose) e.printStackTrace();
			}
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// From observation set (P) populate the event set (C)
		Iterator<MarkedCell> markedCellIt = population.iterator();
		while (markedCellIt.hasNext()) {
			MarkedCell theMarkedCell = markedCellIt.next();

			// c of C
			Correspondence theCorrespondece = this.getCorrespondeceForMarkedCell(theMarkedCell);
			theCorrespondece.addOccurence(theMarkedCell);

			// if c isn't in C add it
			if (!correspondences.contains(theCorrespondece)) {
				correspondences.add(theCorrespondece);
			}
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		this.generateCorrespondecesSubsets();

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// Add c to Ai .. Ax in every alignment
		Iterator<AlignmentCorrespondeces> alignmentIt = this.alignments.iterator();
		while (alignmentIt.hasNext()) {
			AlignmentCorrespondeces thisAlignment = alignmentIt.next();

			// for each cell
			Iterator<Cell> cellIt = thisAlignment.getCells().iterator();

			while (cellIt.hasNext()) {
				Cell thisCell = cellIt.next();

				// find the correspondence for the cell
				Iterator<Correspondence> corrIt = this.correspondences.iterator();
				while (corrIt.hasNext()) {
					Correspondence thisCorr = corrIt.next();

					try {
						if (thisCorr.getEntity1().equals(thisCell.getObject1AsURI(null))
								&& thisCorr.getEntity2().equals(thisCell.getObject2AsURI(null))) {
							thisAlignment.addCorrecpondence(thisCorr);
							break;
						}
					} catch (AlignmentException e) {
						e.printStackTrace();
					}
				}
			}
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// Print alignments
		if (parsingVerbose == true)
			Logger.getInstance().logThis(
					"========================================================================" + "\n|Alignments| = "
							+ this.alignments.size() + "\n Alignments  = ");
		Iterator<AlignmentCorrespondeces> alignmentIt2 = this.alignments.iterator();
		int i = 0;
		while (alignmentIt2.hasNext()) {
			AlignmentCorrespondeces thisAlignment = alignmentIt2.next();
			if (parsingVerbose == true)
				Logger.getInstance().logThis("\t|A" + ++i + "| = " + thisAlignment.getCorrespondences().size());
			if (parsingVerbose == true)
				Logger.getInstance().logThis("\t A" + i + " = " + thisAlignment.toString());
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// Print alignments
		this.setAlignmentsWhereCorrespondeceOccurs();
		if (parsingVerbose == true)
			Logger.getInstance().logThis("========================================================================");
		if (parsingVerbose == true)
			Logger.getInstance().logThis("|Correspondences| =  " + this.correspondences.size() + "\n Correspondences  =  {");
		Iterator<Correspondence> corrIt = this.correspondences.iterator();
		while (corrIt.hasNext()) {
			Correspondence thisCorr = corrIt.next();
			float f = (float) thisCorr.getOccurences().size() / (float) this.alignments.size();
			thisCorr.updatedPriorProbability = f;
			if (parsingVerbose == true)
				Logger.getInstance().logThis(
						"\t< " + thisCorr.toStringNamedAfterLeft() + "| P(" + thisCorr.toStringNamedAfterLeft() + ") = " + f + "| Occurs "
								+ thisCorr.getOccursInAlignments().size() + " times, in {" + thisCorr.printContainedInAlignments() + "} >");
		}
		if (parsingVerbose == true)
			Logger.getInstance().logThis("    }");

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// Gold standard
		try {
			this.parseGoldStandard(folderPath);
		} catch (Exception ex) {
			Logger.getInstance().logThis("parseGoldStandard EXCEPTION: " + ex);
		}
		// Print
		if (parsingVerbose == true)
			Logger.getInstance()
					.logThis(
							"\n========================================================================\nCorrespondences that are in the GOLD standard alignment but, not found by any of the approaches:\n|NewCorrespondeces| = "
									+ newCorrs.size() + "\n NewCorrespondeces  =  {");
		Iterator<Correspondence> newCorrIt = this.newCorrs.iterator();
		while (newCorrIt.hasNext()) {
			Correspondence thisCorr = newCorrIt.next();
			if (parsingVerbose == true)
				Logger.getInstance().logThis("\t" + thisCorr.toStringShort());
		}

		this.setPriors();

		this.correspondencesCopy.addAll(this.correspondences);

	}

	private void parseGoldStandard(String folderPath) throws Exception {
		newCorrs = new Vector<Correspondence>();
		File dir = new File(folderPath + "/gold");
		File listDir[] = dir.listFiles();

		for (int i = 0; i < listDir.length; i++) {
			if (listDir[i].toString().endsWith(".rdf")) {
				File file = listDir[i];
				// System.out.println("\n\n\n\t\t\tFOUND GOLD STANDARD");

				goldStandardAlignment = new AlignmentCorrespondeces("GOLD", file, 0);

				try {
					// Initialize Alignment API parser
					AlignmentParser aparser = new AlignmentParser(0);
					Alignment ref = aparser.parse(file.toURI());

					Iterator<Cell> cellIt = ref.iterator();
					while (cellIt.hasNext()) {
						Cell thisCell = cellIt.next();
						// System.out.println("CELLS " + thisCell.toString());

						goldStandardAlignment.addCell(thisCell);
						MarkedCell mCell = this.getMarkedCellForCell(thisCell);

						if (mCell != null) {
							goldStandardAlignment.addCorrecpondence(this.getCorrespondeceForMarkedCell(mCell));
						} else {
							MarkedCell newMcell = new MarkedCell(thisCell, "GOLD");
							Correspondence newCorr = new Correspondence(newMcell.getEntity(true), newMcell.getEntity(false), 0);
							// System.out.println("\n\n NEW CORR "+
							// newCorr.toStringShort() + "\n\n");
							goldStandardAlignment.addCorrecpondence(newCorr);
							newCorrs.add(newCorr);
						}
					}

				} catch (AlignmentException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * @param folderPath
	 *            The selected library
	 * 
	 * @return Set of files (alignments), only rdf or xml
	 */
	private Vector<File> getFilesFromFolder(String folderPath) {
		Vector<File> files = new Vector<File>();

		File dir = new File(folderPath);
		File listDir[] = dir.listFiles();
		for (int i = 0; i < listDir.length; i++) {
			if (listDir[i].getName().toString().endsWith(".rdf") || listDir[i].getName().toString().endsWith(".xml")) {
				// System.out.println("Added FILE " + listDir[i].toString());
				files.add(listDir[i]);
			}

			if (listDir[i].getName().toString().equals("gold")) {
				this.hasGoldStandard = true;
			}
		}

		if (parsingVerbose == true)
			Logger.getInstance().logThis("PARSING " + files.size() + " alignments" + files.toString());

		return files;
	}

	/**
	 * Returns the name of the alignment approach from the RDF file name
	 * 
	 * @param file
	 *            The RDF file
	 * 
	 * @return Name of the approach
	 */
	private String getAlignmentApproachNameFromFile(File file) {
		String tokens[] = file.getName().split("\\-");
		return tokens[0].toUpperCase();
	}

	private void addMarkedCellToPopulation(MarkedCell markedCell) {
		if (isInPopulation(markedCell) == false) {
			this.population.add(markedCell);
		}
	}

	private boolean isInPopulation(MarkedCell markedCell) {

		Iterator<MarkedCell> markedIt = this.population.iterator();
		while (markedIt.hasNext()) {
			MarkedCell thisMCell = markedIt.next();

			if (thisMCell.toString().equals(markedCell.toString())) {
				if (thisMCell.getProducedByAlignmentApproach().equals(markedCell.getProducedByAlignmentApproach())) {
					/*
					 * System.out.println("DUPLICATE " + markedCell.toString() +
					 * "[" + markedCell.getProducedByAlignmentApproach() +
					 * "] && " + thisMCell.toString() + "[" +
					 * thisMCell.getProducedByAlignmentApproach() + "]");
					 */
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Seperates entity name from full URI, for readability and logging purposes
	 * 
	 * @param uri
	 *            Ontology name as URI
	 * 
	 * @return Ontology name stripped of URI
	 */
	private String getOntologyNameFromURI(URI uri) {
		String uriString = uri.toString();
		// System.out.println("getOntologyNameFromURI " + uriString);

		String result = uriString.substring(uriString.lastIndexOf('/') + 1, uriString.length());

		if (result.contains("#")) {
			// System.out.println("result.contains(#)");
			return uriString.substring(uriString.lastIndexOf('/') + 1, uriString.length() - 1);
		}

		return result;
	}

	private Correspondence getCorrespondeceForMarkedCell(MarkedCell markedCell) {

		// find it in C
		Iterator<Correspondence> corrIt = correspondences.iterator();
		while (corrIt.hasNext()) {
			Correspondence theCorr = corrIt.next();

			// they are the same if the entities they describe are the same
			if (markedCell.getEntity(true).equals(theCorr.getEntity1()) && markedCell.getEntity(false).equals(theCorr.getEntity2())) {
				return theCorr;
			}

		}

		// if not found -> the object does not exist -> create it
		Correspondence newCorr = new Correspondence(markedCell.getEntity(true), markedCell.getEntity(false),
				this.alignmentApproaches.size());

		return newCorr;
	}

	private void generateCorrespondecesSubsets() {

		if (parsingVerbose == true)
			Logger.getInstance().logThis("\nGenerating correspondece subsets");

		Iterator<Correspondence> corrIt = correspondences.iterator();
		while (corrIt.hasNext()) {
			Correspondence theCorr = corrIt.next();
			if (theCorr.getFrequency() == this.alignmentApproaches.size()) {
				this.correspondencesFreqMax.add(theCorr);
			} else if (theCorr.getFrequency() > 1) {
				this.correspondencesFreqLarger1.add(theCorr);
			} else {
				this.correspondencesFreq1.add(theCorr);
			}
		}

		if (parsingVerbose == true)
			Logger.getInstance().logThis(
					"   FREQ = 1 :\t" + this.correspondencesFreq1.size() + "\n   FREQ > 1 :\t" + this.correspondencesFreqLarger1.size()
							+ "\n   FREQ = MAX :\t" + this.correspondencesFreqMax.size());
	}

	private void setAlignmentsWhereCorrespondeceOccurs() {

		// for each correspondence
		Iterator<Correspondence> corrIt = this.correspondences.iterator();
		while (corrIt.hasNext()) {
			Correspondence thisCorr = corrIt.next();

			// set the alignemnt where it occurs
			Iterator<AlignmentCorrespondeces> aligIt = this.alignments.iterator();
			while (aligIt.hasNext()) {
				AlignmentCorrespondeces thisAlig = aligIt.next();
				if (thisAlig.getCorrespondences().contains(thisCorr)) {
					thisCorr.occursInAlignmentsAsAP.add(thisAlig);
				}
			}
		}
	}

	private MarkedCell getMarkedCellForCell(Cell cell) {

		Iterator<MarkedCell> markIt = this.population.iterator();
		while (markIt.hasNext()) {
			MarkedCell mCell = markIt.next();

			try {
				// System.out.println("CHECKING mCell " +
				// mCell.getCell().getObject1AsURI(null) + " <-> "
				// +cell.getObject1AsURI(null) );

				if (cell.getObject1AsURI(null).toString().equals(mCell.getCell().getObject1AsURI(null).toString())) {

					// System.out.println("FOUND Cell for MarkedCell LEFT match "
					// + cell.getObject1AsURI(null) + " " +
					// mCell.getCell().getObject1AsURI(null));

					if (cell.getObject2AsURI(null).toString().equals(mCell.getCell().getObject2AsURI(null).toString())) {
						// System.out.println("FOUND Cell for MarkedCell RIGHT match "
						// + cell.getObject2AsURI(null) + " " +
						// mCell.getCell().getObject2AsURI(null));
						return mCell;
					}
				}
			} catch (AlignmentException e) {
				e.printStackTrace();
			}

		}

		return null;
	}

	private void setPriors() {
		Iterator<Correspondence> corrIt = this.correspondences.iterator();
		while (corrIt.hasNext()) {
			Correspondence theCorr = corrIt.next();

			float probabilityPrior = (float) theCorr.getFrequency() / (float) this.alignmentApproaches.size();

			theCorr.setProbabilityPrior(probabilityPrior);

		}
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// ACCESSOR
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	public Vector<AlignmentCorrespondeces> getAlignments() {
		Vector<AlignmentCorrespondeces> vec = new Vector<AlignmentCorrespondeces>();
		vec.addAll(this.alignments);
		return vec;
	}

	public Vector<Correspondence> getCorrepondences() {
		Vector<Correspondence> vec = new Vector<Correspondence>();
		vec.addAll(this.correspondences);
		return vec;
	}

	public Vector<Correspondence> getCorrFreqOne() {
		Vector<Correspondence> vec = new Vector<Correspondence>();
		vec.addAll(this.correspondencesFreq1);
		return vec;
	}

	public Vector<Correspondence> getCorrFreqLTO() {
		Vector<Correspondence> vec = new Vector<Correspondence>();
		vec.addAll(this.correspondencesFreqLarger1);
		return vec;
	}

	public Vector<Correspondence> getCorrFreqMax() {
		Vector<Correspondence> vec = new Vector<Correspondence>();
		vec.addAll(this.correspondencesFreqMax);
		return vec;
	}

	/**
	 * @return Alignment approach names
	 */
	public Vector<String> getAlignemntApproaches() {
		return this.alignmentApproaches;
	}

	public AlignmentCorrespondeces getGoldStandardAlignment() {
		return goldStandardAlignment;
	}

	public void setGoldStandardAlignment(AlignmentCorrespondeces goldStandardAlignment) {
		this.goldStandardAlignment = goldStandardAlignment;
	}

	public String[] getOntologies() {
		return this.ontologies;
	}

	public Vector<MarkedCell> getPopulation() {
		return this.population;
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// REDUCTION of C
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	public void reduceWith(Vector<Correspondence> reduceWithThis) {

		Logger.getInstance().logThis("REDUCING Correspondecnes with previous results" + reduceWithThis.size() + " " + reduceWithThis);

		Vector<Correspondence> reduceThis = new Vector<Correspondence>();
		Iterator<Correspondence> reduceIt = reduceWithThis.iterator();

		while (reduceIt.hasNext()) {
			Correspondence theCorr = reduceIt.next();

			Iterator<Correspondence> findIt = this.correspondences.iterator();
			while (findIt.hasNext()) {
				Correspondence theCorrRef = findIt.next();

				if (theCorrRef.toStringShort().equals(theCorr.toStringShort())) {
					// System.out.println("\t"+ " EQUALS " +
					// theCorrRef.toStringShort());

					reduceThis.add(theCorrRef);
				}
			}
		}

		this.correspondences.removeAll(reduceThis);
		this.correspondencesFreq1.removeAll(reduceThis);
		this.correspondencesFreqLarger1.removeAll(reduceThis);
		this.correspondencesFreqMax.removeAll(reduceThis);
	}

}