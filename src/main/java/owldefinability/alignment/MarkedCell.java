package owldefinability.alignment;

import java.io.Serializable;
import java.net.URI;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;


/**
 * A correspondence as in Alignment API (Cell class) with an additional 
 * property that shows its origin (the alignment approach that produces it)
 *
 */
class MarkedCell  implements Serializable{
	
	private static final long serialVersionUID = 402029327337412284L;

	/**
	 * The Alignmet API output format.  "Represents an correspondence between 
	 * ontology entities. An ontology comprises a number of collections. Each 
	 * ontology has a number of classes, properties and individuals, along with
	 *  a number of axioms asserting information about those objects."
	 */
	private Cell cell;
	
	/**
	 * The alignment approach producing this Correspondence
	 */
	private String producedByAlignmentApproach;
	
	public MarkedCell(Cell cell, String producedByAlignmentApproach) {
		this.cell = cell;
		this.producedByAlignmentApproach = producedByAlignmentApproach;
	}
	
	/**
	 * @return The alignment approach producing this Correspondence
	 */
	public String getProducedByAlignmentApproach() {
		return producedByAlignmentApproach;
	}

	/**
	 * @return the cell
	 */
	public Cell getCell() {
		return cell;
	}
	
	/**
	 * @return "(value: float between 0. and 1., default: 1.) The confidence that 
	 * the relation holds between the first and the second entity. Since many
	 *  matching methods compute a strength of the relation between entities, 
	 *  this strength can be provided as a normalised measure. The measure should 
	 *  belong to an ordered set M including a maximum element ⊤ and a minimum 
	 *  element ⊥. Currently, we restrict this value to be a float value between
	 *   0. and 1.. If found useful, this could be generalised into any lattice domain"
	 */
	public double getMeasure() {
		return cell.getStrength();
	}
	
	/**
	 * @return "The relation holding between the two entities. It is not restricted 
	 * to the equivalence relation, but can be more sophisticated" > (subsumes), < 
	 * (is subsumed), = (equivalent), % (incompatible), HasInstance, InstanceOf
	 */
	public Relation getRelation() {
		return cell.getRelation();
	}
	
	/**
	 * @param if true return left entity, if false returns right entity
	 * 
	 * @return the name of the entity
	 */
	public URI getEntity(boolean left) {
		try {
			if (left == true) {
				return cell.getObject1AsURI(null);
			}
			else
			{
				return cell.getObject2AsURI(null);
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Seperates entity name from full URI, for readability and logging purposes
	 * 
	 * @param fullEntityName
	 *            Entity name as URI
	 * 
	 * @return Entity name (base)
	 */
	private String getEntityName(String fullEntityName) {
		return fullEntityName.substring(fullEntityName.lastIndexOf('#') + 1,
				fullEntityName.length());
	}
	
	public String toString() {
		try {
			return new String(getEntityName(cell.getObject1AsURI(null).toString()) +  
					" - " + getEntityName(cell.getObject2AsURI(null).toString()));
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
		return null;
	}
}
