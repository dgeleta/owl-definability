package owldefinability.alignment;  

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

import org.semanticweb.owl.align.Cell;

@SuppressWarnings("unused")
class AlignmentCorrespondeces  implements Serializable {
	
	private static final long serialVersionUID = 3579870056191552779L;
	private File file;
	private String producedByAlignmentApproach;
	private Vector<Cell> cells;
	private Vector<Correspondence> correspondeces;
	
	// GOLD
	private int indexInAlignemts; // -1 -> ref   0 ... n -> index
	private Vector<Cell> cellsUnfound =  new Vector<Cell>();  
	private Vector<Correspondence> correspondecesNotFound;
	
	public AlignmentCorrespondeces(String producedByAlignmentApproach, File file, int indexInAlignemts) {
		this.producedByAlignmentApproach = producedByAlignmentApproach;
		cells = new Vector<Cell>();
		correspondeces = new Vector<Correspondence>();
		this.file = file;
		this.indexInAlignemts = indexInAlignemts;
	}
	
	public String getindexInAlignemts() {
		return new String(Integer.toString(indexInAlignemts));
	}
	
	public void addCell(Cell cell) {
		cells.add(cell);
	}
	
	public void addCorrecpondence(Correspondence correspondece) {
		//System.out.println("ADDED correspondece" + correspondece.toString());
		
		correspondeces.add(correspondece);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
				
		Iterator<Correspondence> corIt = this.correspondeces.iterator();
		boolean first = false;
		
		while (corIt.hasNext()) {
			Correspondence theCorr = corIt.next();
			
			if (first == false) {
				first = true;
				sb.append(theCorr.toStringNamedAfterLeft());
			}
			else {
				sb.append(", " + theCorr.toStringNamedAfterLeft());
			}		
		}
		
		sb.append("}");
		
		return sb.toString();
	}
		
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// ACCESSOR
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public Vector<Correspondence> getCorrespondences() {
		return this.correspondeces;
	}
	
	/**
	 * @return the producedByAlignmentApproach
	 */
	public String getProducedByAlignmentApproach() {
		return producedByAlignmentApproach;
	}

	/**
	 * @return the cells
	 */
	public Vector<Cell> getCells() {
		return cells;
	}

	public Vector<Cell> getCellsUnfound() {
		return cellsUnfound;
	}
	
	public void addToUnfoundCells(Cell cell) {
		this.cellsUnfound.add(cell);
	}

	
}

