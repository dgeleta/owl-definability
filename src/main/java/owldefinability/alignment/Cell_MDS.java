package owldefinability.alignment;

import org.semanticweb.owl.align.Relation;

class Cell_MDS {
	
	private Object o1_aligned;
	private Object o2_aligned; 
	private Relation relation;
	
	public boolean evalResult = false;

	public Cell_MDS(Object o1_aligned, Object o2_aligned, Relation relation) {
		this.o1_aligned = o1_aligned;
		this.o2_aligned = o2_aligned;
		this.relation = relation;
	}

	public Object getO1_aligned() {
		return o1_aligned;
	} 
	
	public Object getO2_aligned() {
		return o2_aligned;
	} 
	
	public Relation getRelation() {
		return relation;
	}  
	
	public String prettyPrint() {
		return new String("<" + o1_aligned +", " + o2_aligned + ", " + relation.getRelation() + ">");
	}
}
