package owldefinability.alignment;

import java.util.Vector;

class Cluster {

	private Vector<Correspondence> correspondences;
	private float precision;
	private float recall;
	
	public Cluster(Vector<Correspondence> correspondences, float precision, float recall) {
		this.correspondences = correspondences;
		this.precision = precision;
		this.recall = recall;
	}

	public Vector<Correspondence> getCorrespondences() {
		return correspondences;
	}
	
	public int getSize() {
		return this.correspondences.size();
	}

	public float getPrecision() {
		return precision;
	}

	public float getRecall() {
		return recall;
	}

}
