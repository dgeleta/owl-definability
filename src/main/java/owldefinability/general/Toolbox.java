package owldefinability.general;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.apache.commons.math3.transform.DstNormalization;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.profiles.OWL2DLProfile;
import org.semanticweb.owlapi.profiles.OWL2ELProfile;
import org.semanticweb.owlapi.profiles.OWL2QLProfile;
import org.semanticweb.owlapi.profiles.OWL2RLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.DLExpressivityChecker;
import org.semanticweb.owlapi.util.DLExpressivityChecker.Construct;

import owldefinability.definability.DefinedConcept;
import owldefinability.definability.DefinedRole;
import owldefinability.definability.ImplicitDefinablityChecker;
import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;
// import uk.ac.manchester.cs.jfact.JFactFactory;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

import fr.inrialpes.exmo.align.parser.AlignmentParser;

@SuppressWarnings("unused")
public class Toolbox {

	// * * * VARS * * *
	public final static Toolbox INSTANCE = new Toolbox();
	private StringBuilder sb = new StringBuilder();
	private String sessionName;
	private DecimalFormat df = new DecimalFormat("#.##");
	private SimpleDateFormat df_full = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
	private SimpleDateFormat df_day = new SimpleDateFormat("hh:mm:ss");
	private boolean isLastLoadedConceptMDSsAsStringsComplete = false;
	private boolean isLastLoadedRoleMDSsAsStringsComplete = false;

	// * * * SINGLETON Constructor * * *
	private Toolbox() {
	}



	// * * * ONTOLOGY operations * * *
	public OWLOntology loadOntology(File ontFile) {
		OWLOntology ont = null;
		try {
			OWLOntologyManager owlOntologyManager = OWLManager.createOWLOntologyManager();
			ont = owlOntologyManager.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		return ont;
	}

	public OWLOntology getRbox(OWLOntology o) {
		OWLDataFactory datf = OWLManager.getOWLDataFactory();
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o2 = null;
		try {
			o2 = m.createOntology(o.getOntologyID().getOntologyIRI());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		for (OWLAxiom axx : o.getLogicalAxioms()) {
			boolean allRole = true;

			Vector<OWLEntity> roles = new Vector<OWLEntity>();
			roles.addAll(axx.getSignature());
			roles.remove(datf.getOWLThing());
			roles.remove(datf.getOWLNothing());
			roles.remove(datf.getOWLTopDataProperty());
			roles.remove(datf.getOWLTopObjectProperty());
			roles.remove(datf.getOWLBottomDataProperty());
			roles.remove(datf.getOWLBottomObjectProperty());

			for (OWLEntity ent : roles) {
				if (!ent.isOWLDataProperty() && !ent.isOWLObjectProperty()) {
					allRole = false;
					break;
				}
			}
			if (allRole) {
				System.out.println("\t" + axx);
			}
		}


		Set<OWLAxiom> rbox = new HashSet<OWLAxiom>();
		rbox.addAll(o.getRBoxAxioms(false));
		rbox.addAll(o.getAxioms(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY));
		rbox.addAll(o.getAxioms(AxiomType.INVERSE_OBJECT_PROPERTIES));
		rbox.addAll(o.getAxioms(AxiomType.EQUIVALENT_OBJECT_PROPERTIES));
		rbox.addAll(o.getAxioms(AxiomType.EQUIVALENT_DATA_PROPERTIES));
		rbox.addAll(o.getAxioms(AxiomType.DISJOINT_DATA_PROPERTIES));
		rbox.addAll(o.getAxioms(AxiomType.DISJOINT_OBJECT_PROPERTIES));
		rbox.addAll(o.getAxioms(AxiomType.SUB_OBJECT_PROPERTY));
		rbox.addAll(o.getAxioms(AxiomType.SUB_DATA_PROPERTY));
		rbox.addAll(o.getAxioms(AxiomType.SUB_PROPERTY_CHAIN_OF));
		rbox.addAll(o.getAxioms(AxiomType.TRANSITIVE_OBJECT_PROPERTY));
		// rbox.addAll(o.getAxioms(AxiomType.RBoxAxiomTypes));


		for (OWLAxiom axx : rbox) {
			AddAxiom addAxiom = new AddAxiom(o2, axx);
			m.applyChange(addAxiom);
		}

		System.out.println("RBOX ");
		for (OWLAxiom axx : o2.getLogicalAxioms()) {
			System.out.println("\t" + axx);
		}

		return o2;
	}



	// * * * SIGNATURE * * *
	public Set<OWLEntity> getSignature(Collection<OWLAxiom> axioms, boolean onlyLogicalAxioms) {
		Set<OWLEntity> sigma = new HashSet<OWLEntity>();
		for (OWLAxiom axx : axioms) {
			if (axx.isLogicalAxiom()) {
				sigma.addAll(axx.getSignature());
			}
		}
		return sigma;
	}

	public Set<OWLEntity> getTBoxSignature(OWLOntology ont) {
		Set<OWLEntity> tboxSignature = new HashSet<OWLEntity>();
		for (OWLAxiom axx : ont.getTBoxAxioms(true)) {
			tboxSignature.addAll(axx.getSignature());
		}
		OWLDataFactory df = OWLManager.getOWLDataFactory();
		tboxSignature.remove(df.getOWLThing());
		tboxSignature.remove(df.getOWLNothing());
		return tboxSignature;
	}

	public Set<String> getTBoxSignatureAsStringsUnordered(boolean getShortForm, OWLOntology ont) {
		Set<String> signature = new HashSet<String>();
		for (OWLEntity ent : ont.getSignature()) {
			if (!ent.isBottomEntity() && !ent.isTopEntity() && !ent.isOWLDatatype()) {
				if (getShortForm)
					signature.add(ent.getIRI().getShortForm());
				else
					signature.add(ent.getIRI().toString());
			}
		}
		return signature;
	}

	public Vector<String> getTBoxSignatureAsStringsOrdered(boolean getShortForm, OWLOntology ont) {
		Vector<String> signature = new Vector<String>();
		for (OWLEntity ent : ont.getSignature()) {
			if (!ent.isBottomEntity() && !ent.isTopEntity() && !ent.isOWLDatatype()) {
				if (getShortForm)
					signature.add(ent.getIRI().getShortForm());
				else
					signature.add(ent.getIRI().toString());
			}
		}
		signature = sortStringAlphabetcially(signature);
		return signature;
	}



	public Set<OWLEntity> getRBoxSignature(OWLOntology ont) {
		Set<OWLEntity> tboxSignature = new HashSet<OWLEntity>();
		for (OWLAxiom axx : ont.getLogicalAxioms()) {
			tboxSignature.addAll(axx.getSignature());
		}
		OWLDataFactory df = OWLManager.getOWLDataFactory();
		tboxSignature.remove(df.getOWLThing());
		tboxSignature.remove(df.getOWLNothing());
		return tboxSignature;
	}

	public Set<OWLEntity> getTBoxSignature2(OWLOntology ont) {
		Set<OWLEntity> tboxSignature = new HashSet<OWLEntity>();
		for (OWLAxiom axx : ont.getTBoxAxioms(true)) {
			tboxSignature.addAll(axx.getSignature());
		}
		return tboxSignature;
	}

	public OWLClass getClassEntity(IRI entityName, OWLOntology ont) {
		for (OWLClass ent : ont.getClassesInSignature()) {
			if (ent.getIRI().equals(entityName)) {
				return (OWLClass) ent;
			}
		}
		return null;
	}

	public Set<OWLEntity> convertEntityVectorToSet(Vector<OWLEntity> inputset) {
		Set<OWLEntity> output = new HashSet<OWLEntity>();
		output.addAll(inputset);
		return output;
	}

	public Set<String> getConceptAndRoleNamesAsStrings(OWLOntology ont) {
		Set<String> signature = new HashSet<String>();
		signature.addAll(getConceptNamesAsStrings(ont));
		signature.addAll(getRoleNamesAsStrings(ont));
		return signature;
	}

	public Set<String> getConceptNamesAsStrings(OWLOntology ont) {
		OWLDataFactory dff = OWLManager.getOWLDataFactory();
		Set<String> signature = new HashSet<String>();

		Set<OWLClass> cls = new HashSet<OWLClass>();
		cls.addAll(ont.getClassesInSignature());
		cls.remove(dff.getOWLThing());
		cls.remove(dff.getOWLNothing());

		for (OWLEntity ent : cls) {
			String entst = ent.getIRI().toString().replace("<", "").replace(">", "");
			signature.add(entst);
		}

		return signature;
	}

	public Set<String> getRoleNamesAsStrings(OWLOntology ont) {
		OWLDataFactory dff = OWLManager.getOWLDataFactory();
		Set<String> signature = new HashSet<String>();

		Set<OWLObjectProperty> objs = new HashSet<OWLObjectProperty>();
		objs.addAll(ont.getObjectPropertiesInSignature());
		objs.remove(dff.getOWLTopObjectProperty());
		objs.remove(dff.getOWLBottomObjectProperty());
		for (OWLEntity ent : objs) {
			String entst = ent.getIRI().toString().replace("<", "").replace(">", "");
			signature.add(entst);
		}

		Set<OWLDataProperty> dats = new HashSet<OWLDataProperty>();
		dats.addAll(ont.getDataPropertiesInSignature());
		dats.remove(dff.getOWLTopDataProperty());
		dats.remove(dff.getOWLBottomDataProperty());
		for (OWLEntity ent : dats) {
			String entst = ent.getIRI().toString().replace("<", "").replace(">", "");
			signature.add(entst);
		}

		return signature;
	}

	public Set<String> getIndividualNamesAsStrings(OWLOntology ont) {
		Set<String> result = new HashSet<String>();
		for (OWLEntity ent : ont.getIndividualsInSignature()) {
			result.add(ent.toString());
		}
		return result;
	}

	public Set<String> convertEntitySetToStringSet(Set<OWLEntity> ents) {
		Set<String> result = new HashSet<String>();
		for (OWLEntity ent : ents) {
			result.add(ent.toString());
		}
		return result;
	}

	public OWLEntity getEntity(IRI entityName, OWLOntology ont) {
		for (OWLEntity ent : ont.getEntitiesInSignature(entityName)) {
			if (ent.getIRI().equals(entityName)) {
				return ent;
			}
		}
		/*
		 * for (OWLClass cls : ont.getClassesInSignature()) {
		 * System.out.println(cls.getIRI().getFragment() + " " +
		 * entityName.getFragment()); if
		 * (cls.getIRI().getFragment().equals(entityName.getFragment())) {
		 * return cls; } }
		 */
		return null;
	}



	// * * * Time * * *
	public String convertTime(Number time) {
		if (time == null)
			return "n/a";
		long millis = TimeUnit.NANOSECONDS.toMillis(time.longValue());

		return String.format(
				"%02d:%02d:%02d:%04d",
				TimeUnit.MILLISECONDS.toHours(millis),

				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),

				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)),
				millis);
	}



	// * * * REASONER * * *
	public OWLReasoner getReasoner(String type, OWLOntology theOnt, long timeOut) {
		OWLReasoner reasoner = null;
		OWLReasonerFactory reasonerFactory = null;

		if (type.equals("Pellet")) {
			reasonerFactory = new PelletReasonerFactory();
			OWLReasonerConfiguration config = new SimpleConfiguration(timeOut);
			reasoner = reasonerFactory.createReasoner(theOnt, config);
		} else if (type.equals("Hermit")) {
			reasoner = new org.semanticweb.HermiT.Reasoner(theOnt);
		}
		/*
		 * else if (type.equals("JFact")) { reasonerFactory = new
		 * JFactFactory(); OWLReasonerConfiguration config = new
		 * SimpleConfiguration(timeOut); reasoner =
		 * reasonerFactory.createReasoner(theOnt, config); }
		 */
		return reasoner;
	}

	public OWLReasonerFactory getReasonerFactor(String reasonerType) {
		if (reasonerType.equals("Pellet")) {
			return new PelletReasonerFactory();
		} else if (reasonerType.equals("Hermit")) {
			return new org.semanticweb.HermiT.Reasoner.ReasonerFactory();
		}

		/*
		 * else if (reasonerType.equals("JFact")) { return new JFactFactory(); }
		 */
		return null;
	}



	// * * * Logging * * *
	public void logThis(String fileName, String dataSet, String msg) {
		if (sessionName == null)
			sessionName = new String(dataSet + "_" + df_full.format(new Date()));

		sb.append("[" + df_full.format(new Date()) + "] " + msg + "\n");
		writeToFile(new String(getPathInfo(dataSet) + "/Log" + sessionName + ".txt"), sb.toString());
	}

	public String printMemoryStats() {
		int mb = 1024 * 1024;

		// Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();

		// Print used memory
		double usedMem = (runtime.totalMemory() - runtime.freeMemory()) / mb;
		double totalMem = runtime.totalMemory() / mb;
		double maxMem = runtime.maxMemory() / mb;
		DecimalFormat df = new DecimalFormat("#.##");

		return new String("##### HEAP  : " + df.format(usedMem / totalMem * 100) + " % | "
				+ usedMem + " of available " + totalMem + " [MB] | Max: " + maxMem + " #####");
	}



	// * * * Integer * * *
	public Vector<Integer> sortIntegers(Vector<Integer> inputSet) {
		Collections.sort(inputSet, new Comparator<Integer>() {
			@Override
			public int compare(Integer a1, Integer a2) {
				return a1.compareTo(a2);
			}
		});
		return inputSet;
	}



	// * * * OWL Profiles * * *
	public String getOwlProfile(OWLOntology ont) throws OWLException {
		DLExpressivityChecker dlExpressivityChecker = new DLExpressivityChecker(
				Collections.singleton(ont));
		String dlConstructs = "";
		for (Object construct : dlExpressivityChecker.getConstructs()) {
			dlConstructs = dlConstructs + " " + construct.toString();
		}

		// owl2 owl2_dl owl2_el owl2_ql owl2_rl

		System.out.println("DL Constructs: " + dlConstructs);
		return dlConstructs;
	}

	public int[] getOWL2profiles(OWLOntology ont) {
		// OWL2_EL OWL2_QL OWL2_RL OWL2_DL OWL2
		// Only_EL Only_QL Only_RL Only_DL
		int[] profiles = new int[9];

		OWL2ELProfile profileEL = new OWL2ELProfile();
		OWL2QLProfile profileQL = new OWL2QLProfile();
		OWL2RLProfile profileRL = new OWL2RLProfile();
		OWL2DLProfile profileDL = new OWL2DLProfile();

		OWLProfileReport report = profileEL.checkOntology(ont);
		System.out.println("\tOWL2 EL violations " + report.getViolations().size());
		if (report.getViolations().size() > 0)
			profiles[0] = 0;
		else
			profiles[0] = 1;

		report = profileQL.checkOntology(ont);
		System.out.println("\tOWL2 QL violations " + report.getViolations().size());
		if (report.getViolations().size() > 0)
			profiles[1] = 0;
		else
			profiles[1] = 1;

		report = profileRL.checkOntology(ont);
		System.out.println("\tOWL2 RL violations " + report.getViolations().size());
		if (report.getViolations().size() > 0)
			profiles[2] = 0;
		else
			profiles[2] = 1;

		report = profileDL.checkOntology(ont);
		System.out.println("\tOWL2 DL violations " + report.getViolations().size());
		/*
		 * for (OWLProfileViolation li : report.getViolations()) {
		 * System.out.println("VIOLATION  " + li.toString()); }
		 */

		if (report.getViolations().size() > 0)
			profiles[3] = 0;
		else
			profiles[3] = 1;

		if ((profiles[0] + profiles[1] + profiles[2] + profiles[3]) == 0) {
			profiles[4] = 0;
			System.out.println("\tNot OWL2");
		} else {
			profiles[4] = 1;
			System.out.println("\tIs OWL2");
		}

		// ONLY
		if (profiles[0] == 1 && profiles[1] == 0 && profiles[2] == 0) {
			profiles[5] = 1;
		} else {
			profiles[5] = 0;
		}
		if (profiles[0] == 0 && profiles[1] == 1 && profiles[2] == 0) {
			profiles[6] = 1;
		} else {
			profiles[6] = 0;
		}
		if (profiles[0] == 0 && profiles[1] == 0 && profiles[2] == 1) {
			profiles[7] = 1;
		} else {
			profiles[7] = 0;
		}
		if (profiles[0] == 0 && profiles[1] == 0 && profiles[2] == 0 && profiles[3] == 1) {
			profiles[8] = 1;
		} else {
			profiles[8] = 0;
		}
		return profiles;
	}

	public String constructorDistribution(List<Construct> dlConstructs) {
		StringBuilder constructs = new StringBuilder();

		// AL C D E EL EL++ F H I N O Q R S TRAN U
		if (dlConstructs.contains(Construct.AL)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.C)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.D)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.E)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.EL)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.ELPLUSPLUS)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.F)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.H)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.I)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.N)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.O)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.Q)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.R)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.S)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.TRAN)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		if (dlConstructs.contains(Construct.U)) {
			constructs.append("1, ");
		} else {
			constructs.append("0, ");
		}
		return constructs.toString();
	}



	// * * * FOLDER operations * * *
	public void createFolder(String filePath) {
		if (!new File(filePath).exists()) {
			new File(filePath).mkdir();
		}
	}

	public void createExperimentFolderStructure(String dataSetId) {

		createFolder(new String("_experimentComputations"));

		// main folder
		createFolder(new String(folderExperimets + "/" + dataSetId));

		// definability status computation
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_defComputation"));

		// definability list
		createFolder(getPathCdef(dataSetId));
		createFolder(getPathRdef(dataSetId));
		createFolder(getPathCdefCyclic(dataSetId));

		// MCDSs
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_MCDSs"));
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_MRDSs"));



		// info
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_info"));
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_info/_crash"));
		// createFolder(new String(folderExperimets + "/" + dataSetId +
		// "/_info/errors"));
		createFolder(getPathInfoTimes(dataSetId));
		// analysis summary
		// createFolder(new String(folderExperimets + "/" + dataSetId +
		// "/_analysis"));
		// XML
		// createFolder(new String(folderExperimets + "/" + dataSetId +
		// "/_definability"));


		// PATTERNS
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_cDefPatterns"));
		createFolder(new String(folderExperimets + "/" + dataSetId + "/_rDefPatterns"));
	}



	// * * * PATHS * * *
	public final String folderExperimets = "_experimentComputations";

	public String getPathCdef(String dataSetId) {
		return new String(folderExperimets + "/" + dataSetId + "/_cDefs");
	}

	public String getPathRdef(String dataSetId) {
		return new String(folderExperimets + "/" + dataSetId + "/_rDefs");
	}

	public String getPathCdefCyclic(String dataSetId) {
		return new String(folderExperimets + "/" + dataSetId + "/_cDefs/cyclicDefintions");
	}

	public String getPathFileMCDS(String dataSetId, String fileName) {
		return new String(folderExperimets + "/" + dataSetId + "/_MCDSs");
	}

	public String getPathFileMCDSexpansion(String dataSetId, String fileName) {
		return new String(folderExperimets + "/" + dataSetId + "/_MCDSs/" + fileName
				+ "/_expansions");
	}

	public String getPathInfoTimes(String dataSetId) {
		return new String(folderExperimets + "/" + dataSetId + "/_info/_times");
	}

	public String getPathInfo(String dataSetId) {
		return new String(folderExperimets + "/" + dataSetId + "/_info");
	}


	// * * * MDS File * * *
	public int hasDisjMCDSsBeenComputed(String datasetId, String fileName) {
		File disjMcds = new File("_experimentComputations/" + datasetId + "/_MCDSs/"
				+ "MCDS_disjoint_" + fileName + ".txt");
		if (doesFileExist(disjMcds.getAbsolutePath())) {
			if (disjMcds.length() > 0) {
				return 1;
			}
		}
		return 0;
	}

	public int hasAllMCDSsBeenComputed(String datasetId, String fileName) {
		File listFiles[] = new File(new String("_experimentComputations/" + datasetId + "/_MCDSs"))
				.listFiles();
		for (File f : listFiles) {
			// System.out.println("\tF: " + fileName+ " ??? " + new String("_" +
			// fileName + ".txt"));
			if (f.getName().startsWith("MCDS_eX_")
					&& f.getName().endsWith(new String("_" + fileName + ".txt"))) {
				if (!f.getName().contains("INPROGRESS")) {
					return 1;
				}
			}
		}
		return 0;
	}

	public int hasRoleDefinabilityBeenComputed(String datasetId, String fileName) {
		File rDefFile = new File("_experimentComputations/" + datasetId + "/_rDefs/"
				+ "DefinableRoleList_" + fileName + ".txt");
		if (doesFileExist(rDefFile.getAbsolutePath())) {
			return 1;
		}
		return 0;
	}

	public int hasDisjMRDSsBeenComputed(String datasetId, String fileName) {
		File disjMcds = new File("_experimentComputations/" + datasetId + "/_MCDSs_role/"
				+ "MCDSr_" + fileName + ".txt");
		if (doesFileExist(disjMcds.getAbsolutePath())) {
			if (disjMcds.length() > 0) {
				return 1;
			}
		}
		return 0;
	}



	// * * * MDS as Strings * * *
	public Vector<String> findCwithoutMDS(String datasetId, String fileName, boolean inProgress,
			boolean disjoint) {

		Vector<String> cNames = new Vector<String>();
		Vector<String> cNamesWithputMDS = new Vector<String>();
		String lastCname = null;
		int mdsCount = 0;

		// load file
		File mdsFile = null;
		if (!inProgress) {
			if (disjoint) {
				mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs/"
						+ "MCDS_disjoint_" + fileName + ".txt");
			} else {

			}
		} else {
			if (disjoint) {
				mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs/"
						+ "MCDS_disjoint_INPROGRESS_" + fileName + ".txt");
			} else {

			}
		}

		// count
		if (mdsFile != null) {
			// number of C:
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(readFile(mdsFile.getAbsolutePath()));


			boolean lastlinewasconcept = false;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				if (line != null) {
					if (line.startsWith("C:")) {
						String[] words = line.split(" ");
						String cName = words[1];
						cNames.add(cName);

						// System.out.println("L: " + line + "\t" + cName);

						if (lastlinewasconcept == true) {
							cNamesWithputMDS.add(lastCname);
						}
						lastCname = cName;
						lastlinewasconcept = true;
					} else {
						mdsCount++;
						lastlinewasconcept = false;
					}
				}
			}
		} else {
			System.out.println("ERROR no MDS file !!!");
		}
		System.out.println("MDSs : " + mdsCount);
		System.out.println("ERRORS: " + cNamesWithputMDS.size() + "\n   " + cNamesWithputMDS);

		return cNamesWithputMDS;
	}

	public HashMap<String, String> getConceptDefasStrings(String datasetId, String fileName) {
		HashMap<String, String> defConcepts = new HashMap<String, String>();
		File cDefFile = new File("_experimentComputations/" + datasetId + "/_cDefs/"
				+ "DefinableConceptList_" + fileName + ".txt");


		if (doesFileExist(cDefFile.getAbsolutePath())) {
			Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(cDefFile.getAbsolutePath()));

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				// System.out.println(line);

				if (line != null) {
					String[] cNameType = line.split(" ");

					String cName = cNameType[0];
					String ctype = cNameType[1];

					defConcepts.put(cName, ctype);
				}
			}
			scanner.close();
		}
		return defConcepts;
	}

	public HashMap<String, String> getRoleDefasStrings(String datasetId, String fileName) {

		HashMap<String, String> defRoles = new HashMap<String, String>();
		File rDefFile = new File("_experimentComputations/" + datasetId + "/_rDefs/"
				+ "DefinableRoleList_" + fileName + ".txt");

		// no file found
		if (doesFileExist(rDefFile.getAbsolutePath()) == false) {
			System.out.println("\n\n\n NO DefinableRoles FILE \n\n\n");
			return null;
		}

		// parse
		else {
			Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(rDefFile.getAbsolutePath()));

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				if (line != null) {
					String[] rNameType = line.split(" ");

					String rName = rNameType[0];
					String rtype = rNameType[1];

					if (!rtype.equals("UNDEFINED"))
						defRoles.put(rName, rtype);
				}
			}
			scanner.close();
		}

		return defRoles;
	}

	public HashMap<String, Set<Set<String>>> getConceptMDSsAsStrings(String datasetId,
			String fileName) {
		isLastLoadedConceptMDSsAsStringsComplete = false;

		HashMap<String, Set<Set<String>>> mdsss = new HashMap<String, Set<Set<String>>>();
		int mdsCount = 0;

		// disjoint or all computed
		File mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs/" + "MCDS_eX_"
				+ fileName + ".txt");
		if (!doesFileExist(mdsFile.getAbsolutePath())) {
			mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs/"
					+ "MCDS_disjoint_" + fileName + ".txt");

			if (!doesFileExist(mdsFile.getAbsolutePath())) {
				// System.out.println("   NO MDSs");
				return null;
			} else {
				// System.out.println("   Load Disjoint MDSs");
			}
		} else {
			// System.out.println("   Load ALL MDSs");
			isLastLoadedConceptMDSsAsStringsComplete = true;
		}


		// parse
		Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(mdsFile.getAbsolutePath()));

		Set<Set<String>> sigsOfCls = new HashSet<Set<String>>();
		Set<String> sig = new HashSet<String>();
		String theClass = null;

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("C:")) {
				if (theClass != null) {
					mdsss.put(theClass, sigsOfCls);
				}
				theClass = line.replace("C: ", "");

				sigsOfCls = new HashSet<Set<String>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<String>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(wd);
				}
				sigsOfCls.add(sig);
				mdsCount++;
			}
		}
		mdsss.put(theClass, sigsOfCls);
		scanner.close();

		// System.out.println("\t(" + mdsCount + ") MCDSs");

		return mdsss;
	}

	public HashMap<String, Set<Set<String>>> getRoleMDSsAsStrings(String datasetId, String fileName) {
		isLastLoadedRoleMDSsAsStringsComplete = false;

		HashMap<String, Set<Set<String>>> mdsss = new HashMap<String, Set<Set<String>>>();
		int mdsCount = 0;

		// disjoint or all computed
		File mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs_role/"
				+ "MCDS_eX_" + fileName + ".txt");
		if (!doesFileExist(mdsFile.getAbsolutePath())) {
			mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs_role/" + "MCDSr_"
					+ fileName + ".txt");

			if (!doesFileExist(mdsFile.getAbsolutePath())) {
				// System.out.println("   NO MDSs");
				return null;
			} else {
				// System.out.println("   Load Disjoint MDSs");
			}
		} else {
			// System.out.println("   Load ALL MDSs");
		}


		// parse
		Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(mdsFile.getAbsolutePath()));

		Set<Set<String>> sigsOfCls = new HashSet<Set<String>>();
		Set<String> sig = new HashSet<String>();
		String theRole = null;

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("R:")) {
				if (theRole != null) {
					mdsss.put(theRole, sigsOfCls);
				}
				theRole = line.replace("R: ", "");

				sigsOfCls = new HashSet<Set<String>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<String>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(wd);
				}
				sigsOfCls.add(sig);
				mdsCount++;
			}
		}
		mdsss.put(theRole, sigsOfCls);
		scanner.close();

		// System.out.println("\t(" + mdsCount + ") MRDSs");

		return mdsss;
	}

	public HashMap<String, Set<Set<String>>> getMDSsAsStrings(String datasetId, String fileName) {
		HashMap<String, Set<Set<String>>> mdssAll = new HashMap<String, Set<Set<String>>>();
		HashMap<String, Set<Set<String>>> mdssC = getConceptMDSsAsStrings(datasetId, fileName);
		HashMap<String, Set<Set<String>>> mdssR = getRoleMDSsAsStrings(datasetId, fileName);

		if (mdssC != null) {
			for (String kst : mdssC.keySet()) {
				mdssAll.put(kst, mdssC.get(kst));
			}
		}
		if (mdssR != null) {
			for (String kst : mdssR.keySet()) {
				mdssAll.put(kst, mdssR.get(kst));
			}
		}
		return mdssAll;
	}

	public Object[] getConceptMDSsAsStringsWithCompletness(String datasetId, String fileName) {
		Object[] result = new Object[2];
		boolean isComplete = false;
		HashMap<String, Set<Set<String>>> res = getConceptMDSsAsStrings(datasetId, fileName);
		result[0] = isLastLoadedConceptMDSsAsStringsComplete;
		result[1] = res;
		return result;
	}

	public Object[] getRoleMDSsAsStringsWithCompletness(String datasetId, String fileName) {
		Object[] result = new Object[2];
		boolean isComplete = false;
		HashMap<String, Set<Set<String>>> res = getRoleMDSsAsStrings(datasetId, fileName);
		result[0] = isLastLoadedRoleMDSsAsStringsComplete;
		result[1] = res;
		return result;
	}

	public HashMap<String, Set<Set<String>>> getMCDSsAsStrings(String datasetId, String fileName,
			boolean onlyDisj) {
		if (!onlyDisj)
			return getConceptMDSsAsStrings(datasetId, fileName);
		else {
			File mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs/"
					+ "MCDS_disjoint_" + fileName + ".txt");
			if (!doesFileExist(mdsFile.getAbsolutePath())) {
				return null;
			} else {
				return getMDSsAsStrings(mdsFile);
			}
		}
	}

	public HashMap<String, Set<Set<String>>> getMRDSsAsStrings(String datasetId, String fileName,
			boolean onlyDisj) {
		if (!onlyDisj)
			return getRoleMDSsAsStrings(datasetId, fileName);
		else {
			File mdsFile = new File("_experimentComputations/" + datasetId + "/_MCDSs_role/"
					+ "MCDSr_" + fileName + ".txt");
			if (!doesFileExist(mdsFile.getAbsolutePath())) {
				return null;
			} else {
				return getMDSsAsStrings(mdsFile);
			}
		}
	}

	public int countMDSs(HashMap<String, Set<Set<String>>> mdss) {
		if (mdss == null) {
			return 0;
		}

		int count = 0;
		for (String str : mdss.keySet()) {
			count += mdss.get(str).size();
		}
		return count;
	}

	private HashMap<String, Set<Set<String>>> getMDSsAsStrings(File mdsFile) {
		if (!mdsFile.exists())
			return null;

		HashMap<String, Set<Set<String>>> mdsss = new HashMap<String, Set<Set<String>>>();
		// parse
		Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(mdsFile.getAbsolutePath()));

		Set<Set<String>> sigsOfCls = new HashSet<Set<String>>();
		Set<String> sig = new HashSet<String>();
		String theClass = null;

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("C:")) {
				if (theClass != null) {
					mdsss.put(theClass, sigsOfCls);
				}
				theClass = line.replace("C: ", "");

				sigsOfCls = new HashSet<Set<String>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<String>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(wd);
				}
				sigsOfCls.add(sig);
			}
		}
		mdsss.put(theClass, sigsOfCls);
		scanner.close();
		return mdsss;
	}



	// * * * DEFINABILITY Aggregation * * *
	public int[] countDefConcepts(File cDefFile) {
		int[] deftypesCount = { 0, 0, 0, 0, 0, 0 };

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(cDefFile.getAbsolutePath()));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] words = line.split(" ");

			if (words[1].equals("EXPLICIT")) {
				deftypesCount[0]++;
			} else if (words[1].equals("EXPLICIT_CYCLIC")) {
				deftypesCount[1]++;
			} else if (words[1].equals("IMPLICIT")) {
				deftypesCount[2]++;
			} else if (words[1].equals("ERROR_IMPLIED_BY_EMPTYSIGMA")) {
				deftypesCount[3]++;
			} else if (words[1].equals("UNDEFINED")) {
				deftypesCount[4]++;
			} else if (words[1].equals("UNKNOWN")) {
				deftypesCount[5]++;
			}
		}
		return deftypesCount;
	}

	public int[] countDefConcepts(String datasetId, String fileName) {
		int[] deftypesCount = { 0, 0, 0, 0, 0, 0 };

		File cDefFile = new File("_experimentComputations/" + datasetId + "/_cDefs/"
				+ "DefinableConceptList_" + fileName + ".txt");

		System.out.println("\n\ncountDefConcepts " + fileName);

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(Toolbox.INSTANCE.readFile(cDefFile.getAbsolutePath()));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] words = line.split(" ");

			if (words[1].equals("EXPLICIT")) {
				deftypesCount[0]++;
			} else if (words[1].equals("EXPLICIT_CYCLIC")) {
				deftypesCount[1]++;
			} else if (words[1].equals("IMPLICIT")) {
				deftypesCount[2]++;
			} else if (words[1].equals("ERROR_IMPLIED_BY_EMPTYSIGMA")) {
				deftypesCount[3]++;
			} else if (words[1].equals("UNDEFINED")) {
				deftypesCount[4]++;
			} else if (words[1].equals("UNKNOWN")) {
				deftypesCount[5]++;
			}
		}
		return deftypesCount;
	}



	// * * * ALIGNMENTS * * *
	public Set<OWLAxiom> extractModuleAxioms(Set<OWLEntity> signature, OWLOntology o,
			ModuleType moduleType) throws OWLOntologyCreationException {
		SyntacticLocalityModuleExtractor extractor = new SyntacticLocalityModuleExtractor(
				o.getOWLOntologyManager(), o, moduleType);

		return extractor.extract(signature);
	}

	public String prettyPrintCell(Cell cl) {
		return new String("<" + cl.getObject1().toString() + " " + cl.getRelation().getRelation()
				+ " " + cl.getObject2().toString() + ">");
	}

	public int randomNumberGenerator(int max) {
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(max + 1);
	}



	// * * * STATISTICS * * *
	public double getAverage(Vector<Integer> input) {
		double result = 0;
		for (Integer i : input) {
			result += i;
		}
		result = result / (double) input.size();
		return result;
	}

	public double getMedian(Vector<Integer> input) {
		if (input.size() == 0)
			return 0;

		double result = 0;
		System.out.println("UNSORTED " + input);
		Collections.sort(input);
		System.out.println("SORTED " + input);

		if (input.size() % 2 == 0) {
			int i = input.size() / 2;
			result = (double) (input.elementAt(i - 1) + input.elementAt(i)) / (double) 2;
			return result;
		} else {
			int i = (input.size() - 1) / 2;
			return input.elementAt(i);
		}
	}



	// * * * SORTING & ORDERING * * *
	public Vector<OWLEntity> sortEntitiesAlphabetcially(Set<OWLEntity> inputSet) {
		Vector<OWLEntity> entities = new Vector<OWLEntity>();
		entities.addAll(inputSet);
		Collections.sort(entities, new Comparator<OWLEntity>() {
			@Override
			public int compare(OWLEntity a1, OWLEntity a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return entities;
	}

	public Vector<OWLObjectProperty> sortObjectpropertiesAlpha(Set<OWLObjectProperty> inputSet) {
		Vector<OWLObjectProperty> entities = new Vector<OWLObjectProperty>();
		entities.addAll(inputSet);
		Collections.sort(entities, new Comparator<OWLObjectProperty>() {
			@Override
			public int compare(OWLObjectProperty a1, OWLObjectProperty a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return entities;
	}

	public Vector<OWLEntity> sortOrderedEntitiesAlphabetcially(Vector<OWLEntity> inputSet) {
		Collections.sort(inputSet, new Comparator<OWLEntity>() {
			@Override
			public int compare(OWLEntity a1, OWLEntity a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return inputSet;
	}

	public Vector<OWLEntity> sortEntitiesAlphabetcially(Vector<OWLEntity> inputSet) {
		Collections.sort(inputSet, new Comparator<OWLEntity>() {
			@Override
			public int compare(OWLEntity a1, OWLEntity a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return inputSet;
	}

	public Vector<OWLClass> sortConceptsAlphabetcially(Vector<OWLClass> inputSet) {
		Collections.sort(inputSet, new Comparator<OWLClass>() {
			@Override
			public int compare(OWLClass a1, OWLClass a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return inputSet;
	}

	public Vector<OWLClass> sortConceptsAlphabetcially(Set<OWLClass> inputSetUnordered) {
		Vector<OWLClass> inputSet = new Vector<OWLClass>();
		inputSet.addAll(inputSetUnordered);
		Collections.sort(inputSet, new Comparator<OWLClass>() {
			@Override
			public int compare(OWLClass a1, OWLClass a2) {
				return a1.getIRI().getShortForm().compareTo(a2.getIRI().getShortForm());
			}
		});
		return inputSet;
	}

	public Vector<Set<OWLEntity>> sortEntitySetsbySize(Vector<Set<OWLEntity>> inputSets) {
		Collections.sort(inputSets, new Comparator<Set<OWLEntity>>() {
			@Override
			public int compare(Set<OWLEntity> a1, Set<OWLEntity> a2) {
				if (a1.size() > a2.size()) {
					return 1;
				} else if (a1.size() < a2.size()) {
					return -1;
				}
				return 0;
			}
		});
		return inputSets;
	}

	public Vector<Set<OWLEntity>> sortUnorderedEntitySetsbySize(Set<Set<OWLEntity>> inputSets) {
		Vector<Set<OWLEntity>> orderedSet = new Vector<Set<OWLEntity>>();
		orderedSet.addAll(inputSets);
		Collections.sort(orderedSet, new Comparator<Set<OWLEntity>>() {
			@Override
			public int compare(Set<OWLEntity> a1, Set<OWLEntity> a2) {
				if (a1.size() > a2.size()) {
					return 1;
				} else if (a1.size() < a2.size()) {
					return -1;
				}
				return 0;
			}
		});
		return orderedSet;
	}

	public Vector<DefinedConcept> orderAlphabetically(Vector<DefinedConcept> definedConcepts) {
		Collections.sort(definedConcepts, new Comparator<DefinedConcept>() {
			@Override
			public int compare(DefinedConcept a1, DefinedConcept a2) {
				return a1.getConcept().getIRI().getShortForm()
						.compareTo(a2.getConcept().getIRI().getShortForm());
			}
		});
		return definedConcepts;
	}

	public Vector<DefinedRole> orderRolesAlphabetically(Vector<DefinedRole> definedRoles) {
		Collections.sort(definedRoles, new Comparator<DefinedRole>() {
			@Override
			public int compare(DefinedRole a1, DefinedRole a2) {
				return a1.getRole().getIRI().getShortForm()
						.compareTo(a2.getRole().getIRI().getShortForm());
			}
		});
		return definedRoles;
	}

	public Vector<String> sortStringAlphabetcially(Vector<String> inputSet) {
		Collections.sort(inputSet, new Comparator<String>() {
			@Override
			public int compare(String a1, String a2) {
				return a1.toLowerCase().compareTo(a2.toLowerCase());
			}
		});
		return inputSet;
	}

	public Vector<String> stringSetOrdered(Set<String> input) {
		Vector<String> result = new Vector<String>();
		result.addAll(input);
		Collections.sort(result);
		return result;
	}



	// * * * FILE operations * * *
	public boolean writeToFile(String fileName, String text) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(text);
		} catch (IOException e) {
			return false;
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				return false;
			}
		}
		return true;
	}

	public String readFile(String filePath) {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e1) {
			// e1.printStackTrace();
			return null;
		}
		try {
			String line = br.readLine();
			while (line != null) {
				sb.append(line + "\n");
				line = br.readLine();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public boolean doesFileExist(String filePath) {
		File f = new File(filePath);
		// System.out.println("doesFileExist " + filePath);
		return f.exists();
	}

	public boolean deleteFile(String filePath) {
		// System.out.println("DELETE " + filePath);
		if (!doesFileExist(filePath)) {
			return true;
		}

		Path intermedFilePath = Paths.get(filePath);
		try {
			Files.delete(intermedFilePath);
			return true;
		} catch (NoSuchFileException x) {
			System.err.format("%s: no such" + " file or directory%n", intermedFilePath);
		} catch (DirectoryNotEmptyException x) {
			System.err.format("%s not empty%n", intermedFilePath);
		} catch (IOException x) {
			// File permission problems are caught here.
			System.err.println(x);
		}
		return false;
	}

	public Vector<File> getAllOWLOntologyFilesFromFolders(String rootFolder) {
		Vector<File> ontfiles = new Vector<File>();
		File listFiles[] = new File(rootFolder).listFiles();
		int count = 0;

		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].isDirectory() && !listFiles[i].toString().endsWith("_DONE")) {
				// System.out.println("FOLDER " + listFiles[i].getName() + " " +
				// listFiles[i].listFiles().length);
				count += listFiles[i].listFiles().length;

				File listFilesOfDir[] = listFiles[i].listFiles();

				// get all files in a folder
				for (int j = 0; j < listFilesOfDir.length; j++) {
					File ontFile = listFilesOfDir[j];
					if (!ontFile.getName().endsWith(".DS_Store")) {
						ontfiles.add(ontFile);
					}
				}
			}
		}
		return ontfiles;
	}

	public Vector<File> getAllOWLOntologyFilesFromFoldersWithRange(String rootFolder,
			String startSubfolder, String endSubfolder) {
		Vector<File> ontfiles = new Vector<File>();
		File listFiles[] = new File(rootFolder).listFiles();
		int count = 0;

		boolean withInRange = false;

		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].isDirectory() && !listFiles[i].toString().endsWith("_DONE")) {
				// System.out.println("FOLDER " + listFiles[i].getName() + " " +
				// listFiles[i].listFiles().length);
				count += listFiles[i].listFiles().length;

				if (listFiles[i].getName().equals(startSubfolder)) {
					withInRange = true;
				} else if (listFiles[i].getName().equals(endSubfolder)) {
					withInRange = false;
				}

				if (withInRange) {
					// System.out.println("\tInFOLDER ");
					File listFilesOfDir[] = listFiles[i].listFiles();

					// get all files in a folder
					for (int j = 0; j < listFilesOfDir.length; j++) {
						File ontFile = listFilesOfDir[j];
						if (!ontFile.getName().endsWith(".DS_Store")) {
							ontfiles.add(ontFile);
						}
					}
				}
			}
		}
		// System.out.println("TOOLBOX: " + ontfiles.size() + "    count: " +
		// count);

		return ontfiles;
	}

	public Vector<File> orderByFileSize(Vector<File> files) {
		Collections.sort(files, new Comparator<File>() {
			@Override
			public int compare(File a1, File a2) {
				Long l1 = a1.length();
				Long l2 = a2.length();
				return l1.compareTo(l2);
			}
		});
		return files;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Vector<File> sortFilesAlphabetically(Vector<File> files) {
		Collections.sort(files, new Comparator() {
			@Override
			public int compare(Object f1, Object f2) {
				return ((File) f1).getName().compareTo(((File) f2).getName());
			}
		});
		return files;
	}



	// * * * Loading DATASETS * * *
	public Vector<File> loadDataSet(String dataset, boolean orderByFileSize,
			boolean orderByFileSizeAndExpressivity, boolean doFiltering) {
		return getDataSet(dataset, orderByFileSize, orderByFileSizeAndExpressivity, doFiltering);
	}

	public Vector<File> getDataSet(String dataset, boolean orderByFileSize,
			boolean orderByFileSizeAndExpressivity, boolean doFiltering) {
		System.out.println("   [TOOLBOX getDataSet : " + dataset + "]");

		Vector<File> files = new Vector<File>();
		if (dataset.equals("_OAEI2014_Conference")) {
			File listFiles[] = new File("testData/ontologies/2014").listFiles();
			for (File f : listFiles) {
				if (f.isFile()) {
					files.add(f);
				}
			}
		} else if (dataset.equals("_WebCRAWL")) {
			for (File f : Toolbox.INSTANCE
					.getAllOWLOntologyFilesFromFolders("testData/web-crawl-sorted/_SORTED_small/grouped")) {
				files.add(f);
			}
		} else if (dataset.equals("_OAEI2014_LargeBio")) {
			File listFiles[] = new File("testData/largeBio").listFiles();
			for (File f : listFiles) {
				if (f.isFile()) {
					files.add(f);
				}
			}
		} else if (dataset.equals("_OAEI2014_Anatomy")) {
			File listFiles[] = new File("testData/anatomy-dataset").listFiles();
			for (File f : listFiles) {
				if (f.isFile()) {
					files.add(f);
				}
			}
		} else if (dataset.equals("_NCBO")) {
			File listFiles[] = new File("testData/BioPortal-Corpus").listFiles();
			for (File f : listFiles) {
				if (f.isFile()) {
					if (!f.isDirectory())
						files.add(f);
				}
			}
		} else if (dataset.equals("_TONES")) {
			File listFiles[] = new File("testData/TONES_repo").listFiles();
			for (File f : listFiles) {
				if (f.isFile()) {
					if (!f.isDirectory())
						files.add(f);
				}
			}
		} else if (dataset.equals("_ltfoaTest")) {
			// files.add(new File("testData/_one/ltfoaOLD.owl"));
			// files.add(new File("testData/_one/ltfoaNew.owl"));
			files.add(new File("testData/_one/ltfoaInvRoles.owl"));


		} else if (dataset.equals("_DefCorr")) {
			for (File f : new File("testData/_DefCorr").listFiles()) {
				if (!f.isDirectory())
					files.add(f);
			}
		}



		else if (dataset.equals("_OAEI2014_Benchmark")) {
			File listFolders[] = new File("testData/bench_2014").listFiles();

			for (File fld : listFolders) {
				if (!fld.isDirectory())
					files.add(fld);
			}

			/*
			 * for (File fld : listFolders) { if (fld.isDirectory()) {
			 * System.out.println( fld.getName());
			 * 
			 * for (File ff : fld.listFiles()) { System.out.println("\t" +
			 * ff.getName());
			 * 
			 * if (ff.getName().toString().equals("onto.rdf")) { ff.renameTo(new
			 * File("benchmarks_" + fld.getName() + ".owl")); files.add(ff); } }
			 * } }
			 */
		}



		// (2) Remove not ontology documents
		Vector<File> removeFiles = new Vector<File>();
		for (File f : files) {
			if (f.getName().endsWith(".DS_Store") || f.getName().endsWith("catalog-v001.xml")) {
				removeFiles.add(f);
			}
		}
		files.removeAll(removeFiles);
		removeFiles.clear();


		// (3) Filtering
		if (doFiltering) {
			for (File f : files) {
				if (!hasBeenCompletedAndContainsDefinedConcepts(f.getName(), dataset)) {
					removeFiles.add(f);
					// System.out.println("   - ZERO C_defined: " +
					// f.getName());
				}
			}
		}
		files.removeAll(removeFiles);


		// (4) Ordering
		if (orderByFileSize) {
			files = orderByFileSize(files);
		}
		if (orderByFileSizeAndExpressivity) {
			files = orderDataSetBySizeAndExpressivity(files);
		}

		if (files.size() == 0) {
			System.out.println("   [TOOLBOX " + dataset + " not found]");
		}


		// DONE
		return files;
	}

	private boolean hasBeenCompletedAndContainsDefinedConcepts(String filePath,
			String pathExperiments) {
		if (hasBeenCompleted(filePath, pathExperiments, "/_cDefs/DefinableConceptList_") == false) {
			return false;
		}
		String finalPath = new String("_experimentComputations/" + pathExperiments
				+ "/_cDefs/DefinableConceptList_" + filePath + ".txt");
		File f = new File(finalPath);
		if (f.length() == 0) {
			// System.out.println("ZERO " + f.getName());
			return false;
		}
		return true;
	}

	private boolean hasBeenCompleted(String filePath, String pathExperiments, String type) {
		String finalPath = new String("_experimentComputations/" + pathExperiments + "/" + type
				+ filePath + ".txt");
		// System.out.println(finalPath);
		return Toolbox.INSTANCE.doesFileExist(finalPath);
	}

	private Vector<File> orderDataSetBySizeAndExpressivity(Vector<File> files) {
		Vector<File> orderedSet = new Vector<File>();

		// (1) COMPLEXITY bins
		Vector<File> owl_EL = new Vector<File>();
		Vector<File> owl_QL = new Vector<File>();
		Vector<File> owl_RL = new Vector<File>();
		Vector<File> owl_DL = new Vector<File>();
		Vector<File> owl_inOWL2 = new Vector<File>();
		Vector<File> owl_other = new Vector<File>();

		// OWL2_EL OWL2_QL OWL2_RL OWL2_DL OWL2 Only_EL Only_QL Only_RL Only_DL
		int fCount = 0;
		for (File ontFile : files) {
			fCount++;
			System.out.println("( " + df.format((double) fCount / (double) files.size() * 100)
					+ "% | " + fCount + " of " + files.size() + " )   " + ontFile.getName() + " "
					+ ontFile.length());


			try {
				OWLOntology ont = Toolbox.INSTANCE.loadOntology(ontFile);

				int[] profiles = getOWL2profiles(ont);
				if (profiles[0] == 1) {
					owl_EL.add(ontFile);
				} else if (profiles[1] == 1) {
					owl_QL.add(ontFile);
				} else if (profiles[2] == 1) {
					owl_RL.add(ontFile);
				} else if (profiles[3] == 1) {
					owl_DL.add(ontFile);
				} else if (profiles[4] == 1) {
					owl_inOWL2.add(ontFile);
				} else {
					owl_other.add(ontFile);
				}
			} catch (Exception e) {
				System.out.println("\t" + ontFile.getName() + "   " + e.getMessage());
			}
		}

		// (2) SIZE bins
		owl_EL = orderByFileSize(owl_EL);
		owl_QL = orderByFileSize(owl_QL);
		owl_RL = orderByFileSize(owl_RL);
		owl_DL = orderByFileSize(owl_DL);
		owl_inOWL2 = orderByFileSize(owl_inOWL2);
		owl_other = orderByFileSize(owl_other);
		orderedSet.addAll(owl_EL);
		orderedSet.addAll(owl_QL);
		orderedSet.addAll(owl_RL);
		orderedSet.addAll(owl_DL);
		orderedSet.addAll(owl_inOWL2);
		orderedSet.addAll(owl_other);
		return orderedSet;
	}

	public Vector<File> filterDataset(Vector<File> files, String datasetid,
			boolean onlyOntologiesWithCompleteMCDSs) {
		Vector<File> filtered = new Vector<File>();

		if (onlyOntologiesWithCompleteMCDSs) {
			for (File ontFile : files) {
				File[] folderfiles = new File(getPathFileMCDS(new String(datasetid),
						ontFile.getName())).listFiles();

				for (File f : folderfiles) {
					if (f.getName().startsWith("MCDS_eX_")
							&& f.getName().endsWith(ontFile.getName() + ".txt")
							&& !f.getName().contains("INPROG")) {
						filtered.add(ontFile);
						break;
					}
				}

				if (!filtered.contains(ontFile))
					System.out.println("   EXCLUDED: " + ontFile.getName());
				;
			}
		}

		return filtered;
	}

	public File getOntologyFile(String ontName, Vector<File> corpus) {
		// System.out.println("getOntologyFile " + ontName + " Corpus = " +
		// corpus.size());
		for (File f : corpus) {
			if (f.getName().toLowerCase().startsWith(ontName.toLowerCase())) {
				return f;
			}
		}
		return null;
	}

	public Vector<File> filterUnloadableOntologies(Vector<File> input) {
		Vector<File> filtered = new Vector<File>();
		for (File f : input) {
			boolean loaded = false;
			try {
				loadOntology(f);
				loaded = true;
			} catch (Exception e) {
				loaded = false;
				System.out.println("EXCLUDED FILE : " + f.getName());
			}
			if (loaded) {
				System.out.println("INCLUDED FILE : " + f.getName());
				filtered.add(f);
			}
		}
		return filtered;
	}



	// * * * ALIGNMENTS * * *
	public String[] getAlignedOntologyPairs(String datasetId) {

		String[] opairs0 = {
		// "defCorrTestOnt1 defCorrTestOnt2",
		"o1d o2d" };

		String[] opairs1 = { "cmt conference", "cmt confOf", "cmt edas", "cmt ekaw", "cmt iasted",
				"cmt sigkdd", "conference confOf", "conference edas", "conference ekaw",
				"conference iasted", "conference sigkdd", "confOf edas", "confOf ekaw",
				"confOf iasted", "confOf sigkdd", "edas ekaw", "edas iasted", "edas sigkdd",
				"ekaw iasted", "ekaw sigkdd", "iasted sigkdd" };

		String[] opairs2 = {
				"oaei2014_FMA_small_overlapping_nci oaei2014_NCI_small_overlapping_fma.owl",
				"oaei2014_FMA_small_overlapping_snomed.owl oaei2014_SNOMED_small_overlapping_fma.owl",
				"oaei2014_SNOMED_small_overlapping_nci.owl oaei2014_NCI_small_overlapping_snomed.owl" };

		switch (datasetId) {
		case "_OAEI2014_Conference":
			return opairs1;
		case "_OAEI2014_LargeBio":
			return opairs2;
		case "_DefCorr":
			return opairs0;

		default:
			return null;
		}
	}

	public Object[] loadAlignments(String datasetId, String ontpairName) {
		Vector<String> aNames = new Vector<String>();
		Vector<Alignment> alObj = new Vector<Alignment>();

		String[] words = ontpairName.split(" ");
		String ont1 = words[0];
		String ont2 = words[1];

		switch (datasetId) {

		case "_OAEI2014_Conference":
			File listFolders[] = new File("testData/_alignments/oaei2014_allAlignments")
					.listFiles();
			for (File folder : listFolders) {
				if (folder.isDirectory()) {
					if (folder.toString().contains(ont1) && folder.toString().contains(ont2)) {
						// System.out.println("FOLDER " + folder.getName());

						for (File f : folder.listFiles()) {
							if (!f.getName().equals(".DS_Store") && !f.isDirectory()) {
								AlignmentParser aparser = new AlignmentParser();
								Alignment ref = null;
								try {
									ref = aparser.parse(f.toURI());
									alObj.add(ref);

									String alName = f.getName().toString().split("-")[0];
									aNames.add(alName);

									// System.out.println("\t" + alName + "   "
									// + f.getName());
								} catch (AlignmentException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
			break;

		case "_OAEI2014_LargeBio":
			File listFolders2[] = null;
			if (ontpairName
					.equals("oaei2014_FMA_small_overlapping_nci oaei2014_NCI_small_overlapping_fma.owl")) {
				listFolders2 = new File("testData/_alignments/oaei2013_largeBio_AllAlignments/_fn1")
						.listFiles();
			} else if (ontpairName
					.equals("oaei2014_FMA_small_overlapping_snomed.owl oaei2014_SNOMED_small_overlapping_fma.owl")) {
				listFolders2 = new File("testData/_alignments/oaei2013_largeBio_AllAlignments/_fs1")
						.listFiles();
			} else {
				listFolders2 = new File("testData/_alignments/oaei2013_largeBio_AllAlignments/_sn1")
						.listFiles();
			}

			for (File f : listFolders2) {
				AlignmentParser aparser = new AlignmentParser();
				Alignment ref = null;
				try {
					if (!f.getName().equals(".DS_Store") && !f.isDirectory()) {
						ref = aparser.parse(f.toURI());
						alObj.add(ref);

						String alName = f.getName().toString().split("_")[0];
						aNames.add(alName);

						System.out.println("\t" + alName + "   " + f.getName());
					}
				} catch (AlignmentException e) {
					e.printStackTrace();
				}
			}
			break;

		case "test_semPnR":
			for (File f : new File("testData/_alignments/test_semPnR").listFiles()) {
				if (!f.getName().equals(".DS_Store") && !f.isDirectory()) {

					AlignmentParser aparser = new AlignmentParser();
					Alignment alg = null;
					try {
						if (!f.getName().equals(".DS_Store") && !f.isDirectory()) {
							alg = aparser.parse(f.toURI());
							alObj.add(alg);
							String alName = f.getName().toString().split("_")[0];
							aNames.add(alName);
							System.out.println("\t" + alName + "   " + f.getName());
						}
					} catch (AlignmentException e) {
						e.printStackTrace();
					}
				}
			}
			break;

		default:
			break;
		}

		Object[] result = new Object[2];
		result[0] = aNames;
		result[1] = alObj;

		return result;
	}

	@SuppressWarnings("unchecked")
	public Alignment getReferenceAlignment(Object[] result) {

		Vector<Alignment> alignments = (Vector<Alignment>) result[1];
		Vector<String> alignmentNames = (Vector<String>) result[0];

		for (int i = 0; i < alignments.size(); i++) {
			String alName = alignmentNames.elementAt(i);
			Alignment al = alignments.elementAt(i);
			if (alName.equals("REF"))
				return al;
		}

		for (int i = 0; i < alignments.size(); i++) {
			String alName = alignmentNames.elementAt(i);
			Alignment al = alignments.elementAt(i);
			if (alName.equals("oaei2014") || alName.equals("oaei2013"))
				return al;
		}
		return null;
	}



	// * * * XML operations * * *
	public void createXml_DefinedConceptsWithMCDSs(Vector<DefinedConcept> definedConcepts,
			String filePath) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("definedConcepts");
			Element rootElement1 = doc.createElement("explicitly");
			Element rootElement2 = doc.createElement("implicitly");
			Element rootElement3 = doc.createElement("errorous");
			Element rootElement4 = doc.createElement("unknown");
			doc.appendChild(rootElement);
			rootElement.appendChild(rootElement1);
			rootElement.appendChild(rootElement2);
			rootElement.appendChild(rootElement3);
			rootElement.appendChild(rootElement4);

			// defined concepts elements
			for (DefinedConcept defcon : definedConcepts) {
				Element defConcept = doc.createElement("definedConcept");
				defConcept.setAttribute("clsIri", defcon.getConcept().getIRI().toString());


				// MCDSs
				if (defcon.getMcdsAllDifferent() != null) {
					Element allDiff = doc.createElement("mcdssALL");
					int mCount = 0;

					for (Set<OWLEntity> mcds : defcon.getMcdsAllDifferent()) {
						Element mcdsEl = doc.createElement(new String("m_" + mCount++));
						int eCount = 0;
						for (OWLEntity ent : mcds) {
							mcdsEl.setAttribute(new String("e_" + eCount++), ent.getIRI()
									.toString());
						}
						allDiff.appendChild(mcdsEl);
					}
					defConcept.appendChild(allDiff);
				}
				if (defcon.getMcdsDisjoint() != null) {
					Element allDiff = doc.createElement("mcdssDIS");
					int mCount = 0;

					for (Set<OWLEntity> mcds : defcon.getMcdsDisjoint()) {
						Element mcdsEl = doc.createElement(new String("m_" + mCount++));
						int eCount = 0;
						for (OWLEntity ent : mcds) {
							mcdsEl.setAttribute(new String("e_" + eCount++), ent.getIRI()
									.toString());
						}
						allDiff.appendChild(mcdsEl);
					}
					defConcept.appendChild(allDiff);
				}

				/*
				 * if (defcon.getMcdsJustifications() != null) { Element allDiff
				 * = doc.createElement("mcdssJST"); int mCount = 0;
				 * 
				 * for (Set<OWLEntity> mcds : defcon.getMcdsJustifications()) {
				 * Element mcdsEl = doc.createElement(new String("m_" +
				 * mCount++)); int eCount = 0; for (OWLEntity ent : mcds) {
				 * mcdsEl.setAttribute(new String("e_" + eCount++), ent.getIRI()
				 * .toString()); } allDiff.appendChild(mcdsEl); }
				 * defConcept.appendChild(allDiff); }
				 */

				// store by type
				if (defcon.getDefinabilityType().equals(
						ImplicitDefinablityChecker.ConceptDefinabilityType.EXPLICIT)) {
					rootElement1.appendChild(defConcept);
				} else if (defcon.getDefinabilityType().equals(
						ImplicitDefinablityChecker.ConceptDefinabilityType.IMPLICIT)) {
					rootElement2.appendChild(defConcept);
				} else if (defcon
						.getDefinabilityType()
						.equals(ImplicitDefinablityChecker.ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA)) {
					rootElement3.appendChild(defConcept);
				} else if (defcon.getDefinabilityType().equals(
						ImplicitDefinablityChecker.ConceptDefinabilityType.UNKNOWN)) {
					rootElement4.appendChild(defConcept);
				}
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File(filePath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(source, result);

			// StreamResult resultConsole = new StreamResult(System.out);
			// transformer.transform(source, resultConsole);
			// System.out.println("File saved!");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public Vector<DefinedConcept> parserDefinabilityXML(String filePath, final OWLOntology ont) {

		final Vector<DefinedConcept> defConcepts = new Vector<DefinedConcept>();
		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {


				DefinedConcept currentDefCon;

				boolean inExp = false;
				boolean inImp = false;
				boolean inErr = false;
				boolean inUn = false;
				boolean mcdsAll = false;
				boolean mcdsDIS = false;
				boolean mcdsDISRW = false;
				boolean mcdsJST = false;
				boolean mcdsJSTRW = false;

				ConceptDefinabilityType currentType;

				Vector<Set<OWLEntity>> currentMcdssss = new Vector<Set<OWLEntity>>();

				public void startElement(String uri, String localName, String qName,
						Attributes attributes) throws SAXException {
					// System.out.println("start " + qName);

					if (qName.equals("definedConcept")) {
						OWLClass cls = (OWLClass) getEntity(
								IRI.create(attributes.getValue("clsIri")), ont);
						ConceptDefinabilityType currentType = null;
						if (inExp) {
							currentType = ConceptDefinabilityType.EXPLICIT;
						} else if (inImp) {
							currentType = ConceptDefinabilityType.IMPLICIT;
						} else if (inErr) {
							currentType = ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA;
						} else if (inUn) {
							currentType = ConceptDefinabilityType.UNKNOWN;
						}
						currentDefCon = new DefinedConcept(cls, currentType, ont.getOntologyID()
								.getOntologyIRI());
					} else if (qName.equals("explicitly")) {
						inExp = true;
					} else if (qName.equals("implicitly")) {
						inImp = true;
					} else if (qName.equals("errorous")) {
						inErr = true;
					} else if (qName.equals("unknown")) {
						inUn = true;
					} else if (qName.equals("mcdssALL")) {
						currentMcdssss = new Vector<Set<OWLEntity>>();
						mcdsAll = true;
					} else if (qName.equals("mcdssDIS")) {
						currentMcdssss = new Vector<Set<OWLEntity>>();
						mcdsDIS = true;
					} else if (qName.equals("mcdssDISRW")) {
						currentMcdssss = new Vector<Set<OWLEntity>>();
						mcdsDISRW = true;
					} else if (qName.equals("mcdssJST")) {
						currentMcdssss = new Vector<Set<OWLEntity>>();
						mcdsJST = true;
					} else if (qName.equals("mcdssJSTRW")) {
						currentMcdssss = new Vector<Set<OWLEntity>>();
						mcdsJSTRW = true;
					} else if (qName.startsWith("m_")) {
						currentMcdssss.addElement(processMcds(attributes));
					}
				}

				public Set<OWLEntity> processMcds(Attributes attributes) {
					Set<OWLEntity> mcds = new HashSet<OWLEntity>();
					for (int i = 0; i < attributes.getLength(); i++) {
						// System.out.println("\t" + attributes.getQName(i) +
						// " : " + attributes.getValue(i));
						OWLEntity ent = getEntity(IRI.create(attributes.getValue(i)), ont);
						mcds.add(ent);
					}
					return mcds;
				}

				public void endElement(String uri, String localName, String qName)
						throws SAXException {
					// System.out.println("end " + qName);

					if (qName.equals("definedConcept")) {
						if (currentDefCon != null) {
							defConcepts.add(currentDefCon);
						}
					} else if (qName.equals("explicitly")) {
						inExp = false;
					} else if (qName.equals("implicitly")) {
						inImp = false;
					} else if (qName.equals("errorous")) {
						inErr = false;
					} else if (qName.equals("unknown")) {
						inUn = false;
					} else if (qName.equals("mcdssALL")) {
						mcdsAll = false;
					} else if (qName.equals("mcdssDIS")) {
						currentDefCon.setMcdsDisjoint(currentMcdssss);
						mcdsDIS = false;
					} else if (qName.equals("mcdssJST")) {
						currentDefCon.setMcdsJustifications(currentMcdssss);
						mcdsJST = false;
					}
				}

			};
			saxParser.parse(filePath, handler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return defConcepts;
	}



	// * * * ASSEMBLE * * *
	@SuppressWarnings("resource")
	public void assembleParts(String outputFileName) {
		Set<String> classNames = new HashSet<String>();
		HashMap<String, Set<Set<String>>> mcdss = new HashMap<String, Set<Set<String>>>();
		String thisConcept = null;
		Set<Set<String>> thisMcdss = new HashSet<Set<String>>();
		int mcdsCount = 0;

		File listFiles[] = new File("parts").listFiles();
		for (File f : listFiles) {
			System.out.println(f.getName());

			if (!f.getName().startsWith(".DS_Store")) {
				String fileContent = Toolbox.INSTANCE.readFile(f.getPath());
				if (fileContent != null) {
					Scanner scanner = new Scanner(fileContent);
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						String[] words = line.split(" ");

						if (line.startsWith("C: ")) {
							if (thisConcept != null) {
								mcdss.put(thisConcept, thisMcdss);
								classNames.add(thisConcept);
							}

							thisConcept = words[1];
							if (mcdss.get(thisConcept) != null) {
								thisMcdss = mcdss.get(thisConcept);
							} else {
								thisMcdss = new HashSet<Set<String>>();
							}
						} else {
							Set<String> newMcdss = new HashSet<String>();
							for (int i = 1; i < words.length; i++) {
								newMcdss.add(words[i]);
							}
							thisMcdss.add(newMcdss);
						}
					}
				}
			}
		}

		StringBuilder sb = new StringBuilder();
		Vector<String> classNames2 = new Vector<String>();
		classNames2.addAll(classNames);
		classNames2 = Toolbox.INSTANCE.sortStringAlphabetcially(classNames2);
		for (String cls : classNames2) {
			// System.out.println("   (" + cls + ")   " + mcdss.get(cls));

			sb.append("C: " + cls + "\n");
			mcdsCount += mcdss.get(cls).size();
			for (Set<String> clsMcds : mcdss.get(cls)) {
				sb.append("Σ: ");

				for (String mcdsENt : clsMcds) {
					sb.append(mcdsENt + " ");
				}
				sb.append("\n");
			}
		}

		Toolbox.INSTANCE.writeToFile("parts/" + outputFileName + "_ASM.txt", sb.toString());
		System.out.println("CLASSES : " + classNames2.size() + "    MCDSs: " + mcdsCount);
	}
}
