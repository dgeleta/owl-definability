package owldefinability.definability;

import java.io.File;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;

class SerializeDefinability {

	public boolean generateDefinablityXML(String filePath, OWLOntology ont,
			Vector<DefinedConcept> defConcepts, Vector<DefinedRole> defRoles) {
		System.out
				.println("\n\n\n---------------------------------------------------------------------------\n"
						+ "STARTED generateDefinablityXML \n\t"
						+ ont.getOntologyID().getOntologyIRI()
						+ "\n---------------------------------------------------------------------------");

		// genrate xml
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootdef = doc.createElement("definability");
			doc.appendChild(rootdef);

			// ontology properties
			Element ontProp = doc.createElement("ontology");
			ontProp.setAttribute("ontologyIRI", ont.getOntologyID().getOntologyIRI().toString());
			if (ont.getOntologyID().getVersionIRI() != null) {
				ontProp.setAttribute("versionIRI", ont.getOntologyID().getVersionIRI().toString());
			}
			rootdef.appendChild(ontProp);

			// * * * defined concepts * * *
			Element rootCdefs = doc.createElement("definedConcepts");
			rootdef.appendChild(rootCdefs);

			for (DefinedConcept dCls : defConcepts) {
				System.out.println("    " + dCls.getConcept().getIRI().getShortForm()+ " : "
						+ dCls.getDefinabilityType());

				Element defConcept = doc.createElement("definedConcept");
				defConcept.setAttribute("IRI", dCls.getConcept().getIRI().toString());

				// cDef type
				defConcept.setAttribute("conceptDefType", dCls.getDefinabilityType().toString());

				if (!dCls.getDefinabilityType().equals(
						ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA)
						&& !dCls.getDefinabilityType().equals(ConceptDefinabilityType.UNDEFINED)
						&& !dCls.getDefinabilityType().equals(ConceptDefinabilityType.UNKNOWN)) {

					// MCDSs
					Element mcdsEl = doc.createElement("MCDSs");
					defConcept.appendChild(mcdsEl);
					for (MCDS sig : dCls.mcdss) {

						// MCDS
						Element thisMCDS = doc.createElement("mcds");
						mcdsEl.appendChild(thisMCDS);
						thisMCDS.setAttribute("cDefPattern", sig.getPattern().toString());

						// Entities
						Element thisMCDSentities = doc.createElement("signature");
						thisMCDS.appendChild(thisMCDSentities);
						for (OWLEntity ent : sig.getSignature()) {

							// Entity
							Element thisMCDSanEntity = doc.createElement("entity");
							thisMCDSanEntity.setAttribute("IRI", ent.getIRI().toString());
							thisMCDSentities.appendChild(thisMCDSanEntity);
						}
					}
				}
				rootCdefs.appendChild(defConcept);
			}


			// * * * defined roles * * *
			Element rootRdefs = doc.createElement("definedRoles");
			rootdef.appendChild(rootRdefs);

			for (DefinedRole dRls : defRoles) {
				System.out.println("    " + dRls.getRole().getIRI().getShortForm() + " : "
						+ dRls.getRoleDefinabilityType());

				Element defRole = doc.createElement("definedRole");
				defRole.setAttribute("IRI", dRls.getRole().getIRI().toString());
				String roletype = "objectProperty";
				if (dRls.getRole().isOWLDataProperty())
					roletype = "dataProperty";
				defRole.setAttribute("type", roletype);

				// rDef type
				defRole.setAttribute("roleDefType", dRls.getRoleDefinabilityType().toString());

				// MCDSs
				Element mcdsEl = doc.createElement("MCDSs");
				defRole.appendChild(mcdsEl);
				for (Set<OWLEntity> sig : dRls.getMcdss()) {

					// MCDS
					Element thisMCDS = doc.createElement("mcds");
					mcdsEl.appendChild(thisMCDS);

					// Entities
					Element thisMCDSentities = doc.createElement("signature");
					thisMCDS.appendChild(thisMCDSentities);
					for (OWLEntity ent : sig) {

						// Entity
						Element thisMCDSanEntity = doc.createElement("entity");
						thisMCDSanEntity.setAttribute("IRI", ent.getIRI().toString());
						thisMCDSentities.appendChild(thisMCDSanEntity);
					}
				}
				rootRdefs.appendChild(defRole);
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			// save as XML
			StreamResult result = new StreamResult(new File(filePath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(source, result);

			// output to console
			/*
			 * StreamResult resultConsole = new StreamResult(System.out);
			 * transformer.transform(source, resultConsole);
			 */
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return false;
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return false;
		} catch (TransformerException e) {
			e.printStackTrace();
			return false;
		}
		System.out
				.println("\n\n\n---------------------------------------------------------------------------\n"
						+ "DONE generateDefinablityXML"
						+ ont.getOntologyID().getOntologyIRI()
						+ "\n---------------------------------------------------------------------------\n\n\n");
		return true;
	}

}
