package owldefinability.definability;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;
import owldefinability.general.PowerSet;
import owldefinability.general.Toolbox;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

/**
 * Compute Minimal Definition Signatures of Concepts
 * 
 */
public class Computer_MinimalConceptDefinitionSignature {

	// * * * Input * * *
	private OWLOntology ont;
	private File ontFile;

	// * * * Internal * * *
	private OWLDataFactory df = OWLManager.getOWLDataFactory();
	private ImplicitDefinablityChecker impDef;
	private boolean isVerbose;
	private boolean isExpVerbose;
	private String resultFolder;
	private String reasonerType;
	private long startTimeOntology, endTimeOntology, startTimeConcept, endTimeConcept;
	private HashMap<OWLClass, Number> definedConceptsComputationTime = new HashMap<OWLClass, Number>();
	private Vector<OWLClass> computedConcepts = new Vector<OWLClass>();

	public final String TYPE_BF = "all";
	public final String TYPE_DIS = "disjoint";
	public final String TYPE_PSX = "expanded";

	private HashMap<OWLClass, ConceptDefinabilityType> definedConceptsByType;
	private Vector<DefinedConcept> definedConcepts;
	private Set<OWLEntity> tboxSignature;
	private SimpleDateFormat df_day = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

	@SuppressWarnings("unused")
	private final int CHUNCK_SIZE = 10; // 100
	private int partThis = -1;
	private int partTotal = -1;

	public OWLClass examinedClass = null;
	public int impDefCheckCount = 0;
	public boolean isSaving = true;

	private HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcds;

	// Max MDS Union size
	private int expansionLimit = 23;



	// * * * CONSTRUCTOR * * *
	/**
	 * Compute disjoint, or all Minimal Definition Signatures of Concepts
	 * 
	 * @param ont
	 *            The pre-loaded OWLOntology
	 * @param ontFile
	 *            The ontology as File
	 * @param reasonerType
	 *            Reasoner type {Pellet, Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param datasetId
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param isVerbose
	 *            Show internal processing
	 * @param expansionLimit
	 *            Expansion threshold used in computing all MDSs, by default is
	 *            23
	 */
	public Computer_MinimalConceptDefinitionSignature(OWLOntology ont, File ontFile,
			String reasonerType, long reasonerTimeout, String datasetId, boolean isVerbose,
			String expansionLimit) {
		this.ont = ont;
		this.ontFile = ontFile;
		this.isVerbose = isVerbose;
		this.isExpVerbose = isVerbose;
		this.resultFolder = new String(datasetId);
		this.reasonerType = reasonerType;
		this.impDef = new ImplicitDefinablityChecker(this.ontFile, this.resultFolder,
				this.reasonerType, reasonerTimeout, false);
		if (expansionLimit != null)
			this.expansionLimit = Integer.valueOf(expansionLimit);
		tboxSignature = Toolbox.INSTANCE.getTBoxSignature(this.ont);
	}



	// * * * MCDS - Computation * * *
	// Single - BINARY
	private Vector<OWLEntity> removedEntities;
	private Vector<OWLEntity> startSignature;

	public Collection<OWLEntity> computeSingleMCDS_MOD(OWLClass definedConcept,
			Collection<OWLEntity> signature, Set<OWLAxiom> moduleAxioms) {

		startSignature = new Vector<OWLEntity>();
		startSignature.addAll(signature);

		removedEntities = new Vector<OWLEntity>();

		if (startSignature.size() > 1) {
			impDefCheckCount++;
			if (impDef.isConceptImplicitlyDefined_MOD(definedConcept, startSignature, moduleAxioms)) {
				splitNprune(definedConcept, startSignature, "  ");
			}
		} else {
			Set<OWLEntity> copySig = new HashSet<OWLEntity>();
			copySig.addAll(signature);
			return copySig;
		}

		// done
		Set<OWLEntity> prunedSignature = new HashSet<OWLEntity>();
		prunedSignature.addAll(startSignature);
		prunedSignature.removeAll(removedEntities);
		return prunedSignature;
	}

	private void splitNprune(OWLClass definedConcept, Vector<OWLEntity> signature, String indent) {
		System.out.println("\n" + indent + "Split&Prune [" + signature.elementAt(0)
				+ ", ... ]   |S| = " + signature.size() + " " + df_day.format(new Date()));

		if (signature.size() > 1) {
			int splitIdx = signature.size() / 2;

			Vector<OWLEntity> firstHalf = new Vector<OWLEntity>();
			Vector<OWLEntity> secondHalf = new Vector<OWLEntity>();
			for (int i = 0; i < splitIdx; i++) {
				firstHalf.add(signature.elementAt(i));
			}
			secondHalf.addAll(signature);
			secondHalf.removeAll(firstHalf);

			if (isVerbose) {
				System.out.println(indent + " (L) : [" + firstHalf.elementAt(0)
						+ ", ... ]   |S1| = " + firstHalf.size());
				System.out.println(indent + " (R) : [" + secondHalf.elementAt(0)
						+ ", ... ]   |S2| = " + secondHalf.size());
			}

			// attempt to remove left half
			Vector<OWLEntity> workingSigma = new Vector<OWLEntity>();
			workingSigma.addAll(startSignature);
			workingSigma.removeAll(removedEntities);
			workingSigma.removeAll(firstHalf);

			impDefCheckCount++;
			if (impDef.isConceptImplicitlyDefined(definedConcept, workingSigma)) {
				removedEntities.addAll(firstHalf);
				if (isVerbose)
					System.out.println(indent + "(L) REMOVED   |S1| = " + firstHalf.size() + "   "
							+ df_day.format(new Date()));
			} else {
				if (firstHalf.size() > 1) {
					if (isVerbose)
						System.out.println(indent + "(L) Split&Prune ");
					splitNprune(definedConcept, firstHalf, new String(indent + "  "));
				}
			}

			// attempt to remove right half
			workingSigma.removeAllElements();
			workingSigma.addAll(startSignature);
			workingSigma.removeAll(removedEntities);
			workingSigma.removeAll(secondHalf);
			impDefCheckCount++;
			if (impDef.isConceptImplicitlyDefined(definedConcept, workingSigma)) {
				removedEntities.addAll(secondHalf);
				if (isVerbose)
					System.out.println(indent + "(R) REMOVED   |S2| = " + secondHalf.size() + "   "
							+ df_day.format(new Date()));
			} else {
				if (secondHalf.size() > 1) {
					if (isVerbose)
						System.out.println(indent + "(R) Split&Prune");
					splitNprune(definedConcept, secondHalf, new String(indent + "  "));
				}
			}
		}
	}

	// Single - One by One
	private Set<OWLEntity> computeSingleMCDS_OLD(OWLClass definedConcept, Set<OWLEntity> signature) {
		if (isVerbose) {
			if (signature != null) {
				if (signature.size() < 50)
					System.out.println("\n\tcomputeSingleMCDS of "
							+ definedConcept.getIRI().getShortForm() + "\n\tS = "
							+ signature.size() + "\n\tS = " + signature);
				else
					System.out.println("\n\tcomputeSingleMCDS of "
							+ definedConcept.getIRI().getShortForm() + "\n\tS = "
							+ signature.size());
			}
		}
		Set<OWLEntity> sigma = new HashSet<OWLEntity>();
		sigma.addAll(signature);

		// systematically prune the input signature until it becomes minimal
		for (OWLEntity entity : signature) {
			// Signature \ {E}
			sigma.remove(entity);
			if (isVerbose)
				System.out.println("\tS \\ {" + entity + "}   " + df_day.format(new Date()));

			// IsDefinable(C, Signature) -> ?
			impDefCheckCount++;
			if (impDef.isConceptImplicitlyDefined(definedConcept, sigma) == false) {
				// Signature u {E}
				sigma.add(entity);
				if (isVerbose)
					System.out.println("\tS u {" + entity + "}   " + df_day.format(new Date()));
			}
		}
		if (isVerbose)
			System.out.println("\tS = " + sigma);
		return sigma;
	}

	// Minimality CHECKER
	public boolean isCDSminimal(OWLClass definedConcept, Set<OWLEntity> signature) {
		boolean isMinimal = signature.equals(computeSingleMCDS_OLD(definedConcept, signature));
		if (isVerbose)
			System.out.println("\tisCDSminimal? " + definedConcept + " " + signature + " "
					+ isMinimal);
		return isMinimal;
	}

	// Disjoint(s)
	/**
	 * Computes all disjoint MDSs for each defined concept in an ontology.
	 * 
	 * @return Set of defined concepts and their MDSs
	 */
	public HashMap<OWLClass, Vector<Set<OWLEntity>>> computeMCDSsOfOntology_Disjoints() {
		definedConceptsComputationTime = new HashMap<OWLClass, Number>();
		startTimeOntology = System.nanoTime();
		computedConcepts = new Vector<OWLClass>();
		HashMap<OWLClass, Vector<Set<OWLEntity>>> dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		Computer_DefinedConcept defComp = new Computer_DefinedConcept(resultFolder, ont, ontFile,
				reasonerType, 60000, false);
		if (definedConceptsByType == null)
			definedConceptsByType = defComp.getDefinedConceptListByType();

		// (1a) attempt to resume
		boolean hasResumed = false;
		dMCDSs = loadMCDSs(true, TYPE_DIS);

		// (1b) attempt to load
		if (dMCDSs == null) {
			dMCDSs = loadMCDSs(false, TYPE_DIS);
		} else {
			hasResumed = true;
			if (isVerbose)
				System.out.println("RESUMED Disjoint MCDS Computation(" + computedConcepts.size()
						+ " of " + definedConceptsByType.keySet().size() + ")");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"RESUMED Disjoint MCDS Computation (" + computedConcepts.size() + " of "
							+ definedConceptsByType.keySet().size() + ")"));
		}

		// (2) compute (remaining) per concept
		if (dMCDSs == null || hasResumed) {
			if (isVerbose)
				System.out.println("... Disjoint MCDS Computation ...");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"... Disjoint MCDS Computation ..."));

			if (!hasResumed) {
				dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			}

			Vector<OWLClass> orderedClassList = new Vector<OWLClass>();
			orderedClassList.addAll(definedConceptsByType.keySet());
			orderedClassList.removeAll(computedConcepts);
			orderedClassList = filterUncomputableConcepts(orderedClassList);
			orderedClassList = Toolbox.INSTANCE.sortConceptsAlphabetcially(orderedClassList);


			for (OWLClass cls : orderedClassList) {
				System.out.println("\n\n((" + cls + " | " + (orderedClassList.indexOf(cls) + 1)
						+ " of " + orderedClassList.size() + " )) : "
						+ definedConceptsByType.get(cls));
				Toolbox.INSTANCE.logThis("", resultFolder, new String("((" + cls + ")) : "
						+ definedConceptsByType.get(cls)));

				examinedClass = cls;

				// * * * modularize T with C * * *
				Set<OWLAxiom> ontMod = null;
				try {
					Set<OWLEntity> singeltonSignature = new HashSet<OWLEntity>();
					singeltonSignature.add(cls);
					ontMod = extractModuleAxioms(singeltonSignature, ont, ModuleType.STAR);

					System.out.println("  MODULARIZED\n\t|Ax(O)| = " + ont.getLogicalAxiomCount()
							+ "\n\t|Ax(M)| = " + ontMod.size());

				} catch (OWLOntologyCreationException e) {
					e.printStackTrace();
				}

				Set<OWLEntity> sigma = new HashSet<OWLEntity>();
				sigma.addAll(Toolbox.INSTANCE.getSignature(ontMod, true));
				sigma.remove(df.getOWLThing());
				sigma.remove(df.getOWLNothing());
				sigma.remove(cls);

				Vector<Set<OWLEntity>> dMCDSsOfConcept = computeMCDSs_Disjoints(cls, sigma, ontMod);
				dMCDSs.put(cls, dMCDSsOfConcept);
				computedConcepts.add(cls);

				// save intermediate per concept
				saveMCDSs(true, dMCDSs, TYPE_DIS);
			}

			// (3) serialize final result
			saveMCDSs(false, dMCDSs, TYPE_DIS);
		} else {
			if (isVerbose)
				System.out.println("\tLOADED Disjoint MCDSs");
		}

		// Done
		endTimeOntology = System.nanoTime();
		return dMCDSs;
	}

	private Vector<OWLClass> filterUncomputableConcepts(Vector<OWLClass> classList) {
		Vector<OWLClass> filteredClassList = new Vector<OWLClass>();
		for (OWLClass cls : classList) {
			if (definedConceptsByType.get(cls) == ConceptDefinabilityType.EXPLICIT
					|| definedConceptsByType.get(cls) == ConceptDefinabilityType.IMPLICIT) {
				filteredClassList.add(cls);
			}
		}
		return filteredClassList;
	}

	public HashMap<OWLClass, Vector<Set<OWLEntity>>> computeMCDSsOfOntology_Disjoints_Partial(
			int partIndex, int partsTotal) {
		this.partThis = partIndex;
		this.partTotal = partsTotal;

		definedConceptsComputationTime = new HashMap<OWLClass, Number>();
		startTimeOntology = System.nanoTime();
		computedConcepts = new Vector<OWLClass>();
		HashMap<OWLClass, Vector<Set<OWLEntity>>> dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		Computer_DefinedConcept defComp = new Computer_DefinedConcept(resultFolder, ont, ontFile,
				reasonerType, 60000, false);
		if (definedConceptsByType == null)
			definedConceptsByType = defComp.getDefinedConceptListByType();

		// (1a) attempt to resume
		boolean hasResumed = false;
		dMCDSs = loadMCDSs(true, TYPE_DIS);

		// (1b) attempt to load
		if (dMCDSs == null) {
			dMCDSs = loadMCDSs(false, TYPE_DIS);
		} else {
			hasResumed = true;
			if (isVerbose)
				System.out.println("RESUMED Disjoint MCDS Computation(" + computedConcepts.size()
						+ " of " + definedConceptsByType.keySet().size() + ")");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"RESUMED Disjoint MCDS Computation (" + computedConcepts.size() + " of "
							+ definedConceptsByType.keySet().size() + ")"));
		}

		// (2) compute (remaining) per concept
		if (dMCDSs == null || hasResumed) {
			if (isVerbose)
				System.out.println("... Disjoint MCDS Computation (Partial " + partIndex + " of "
						+ partsTotal + " ) ...");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"... Disjoint MCDS Computation ..."));

			if (!hasResumed) {
				dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			}

			Vector<OWLClass> orderedClassList = new Vector<OWLClass>();
			orderedClassList.addAll(definedConceptsByType.keySet());
			orderedClassList.removeAll(computedConcepts);
			orderedClassList = filterUncomputableConcepts(orderedClassList);
			orderedClassList = Toolbox.INSTANCE.sortConceptsAlphabetcially(orderedClassList);

			// parts
			orderedClassList = this.getPart(orderedClassList);

			// compute
			for (OWLClass cls : orderedClassList) {
				System.out.println("\n\n((" + cls + " | " + (orderedClassList.indexOf(cls) + 1)
						+ " of " + orderedClassList.size() + " )) : "
						+ definedConceptsByType.get(cls) + "    [PART " + partIndex + " of "
						+ partsTotal + "]");
				Toolbox.INSTANCE.logThis("", resultFolder, new String("((" + cls + ")) : "
						+ definedConceptsByType.get(cls)));

				examinedClass = cls;

				// * * * modularize T with C * * *
				Set<OWLAxiom> ontMod = null;
				try {
					Set<OWLEntity> singeltonSignature = new HashSet<OWLEntity>();
					singeltonSignature.add(cls);
					ontMod = extractModuleAxioms(singeltonSignature, ont, ModuleType.STAR);

					System.out.println("  MODULARIZED\n\t|Ax(O)| = " + ont.getLogicalAxiomCount()
							+ "\n\t|Ax(M)| = " + ontMod.size());

				} catch (OWLOntologyCreationException e) {
					e.printStackTrace();
				}

				Set<OWLEntity> sigma = new HashSet<OWLEntity>();
				sigma.addAll(Toolbox.INSTANCE.getSignature(ontMod, true));
				sigma.remove(df.getOWLThing());
				sigma.remove(df.getOWLNothing());
				sigma.remove(cls);

				Vector<Set<OWLEntity>> dMCDSsOfConcept = computeMCDSs_Disjoints(cls, sigma, ontMod);
				dMCDSs.put(cls, dMCDSsOfConcept);
				computedConcepts.add(cls);

				// save intermediate per concept
				saveMCDSs(true, dMCDSs, TYPE_DIS);
			}

			// (3) serialize final result
			saveMCDSs(false, dMCDSs, TYPE_DIS);

		} else {
			if (isVerbose)
				System.out.println("LOADED Disjoint MCDSs");
			Toolbox.INSTANCE.logThis("", resultFolder, new String("LOADED Disjoint MCDSs"));
		}

		// Done
		endTimeOntology = System.nanoTime();
		return dMCDSs;
	}

	public HashMap<OWLClass, Vector<Set<OWLEntity>>> computeMCDSsOfOntology_Disjoints_RestrictedConcepts(
			Vector<OWLClass> computeTheseConcepts, int partIndex, int partTotal) {
		this.partThis = partIndex;
		this.partTotal = partTotal;

		definedConceptsComputationTime = new HashMap<OWLClass, Number>();
		startTimeOntology = System.nanoTime();
		computedConcepts = new Vector<OWLClass>();
		HashMap<OWLClass, Vector<Set<OWLEntity>>> dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		Computer_DefinedConcept defComp = new Computer_DefinedConcept(resultFolder, ont, ontFile,
				reasonerType, 60000, false);
		if (definedConceptsByType == null)
			definedConceptsByType = defComp.getDefinedConceptListByType();

		// (1a) attempt to resume
		boolean hasResumed = false;
		dMCDSs = loadMCDSs(true, TYPE_DIS);

		// (1b) attempt to load
		if (dMCDSs == null) {
			dMCDSs = loadMCDSs(false, TYPE_DIS);
		} else {
			hasResumed = true;
			if (isVerbose)
				System.out.println("RESUMED Disjoint MCDS Computation(" + computedConcepts.size()
						+ " of " + definedConceptsByType.keySet().size() + ")");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"RESUMED Disjoint MCDS Computation (" + computedConcepts.size() + " of "
							+ definedConceptsByType.keySet().size() + ")"));
		}

		// (2) compute (remaining) per concept
		if (dMCDSs == null || hasResumed) {
			if (isVerbose)
				System.out.println("... Disjoint MCDS Computation ...");
			Toolbox.INSTANCE.logThis("", resultFolder, new String(
					"... Disjoint MCDS Computation ..."));

			if (!hasResumed) {
				dMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			}

			Vector<OWLClass> orderedClassList = new Vector<OWLClass>();
			orderedClassList.addAll(definedConceptsByType.keySet());
			orderedClassList.removeAll(computedConcepts);
			orderedClassList = Toolbox.INSTANCE.sortConceptsAlphabetcially(orderedClassList);
			Vector<OWLClass> filter = new Vector<OWLClass>();
			for (OWLClass cls : orderedClassList) {
				if (!computeTheseConcepts.contains(cls))
					filter.add(cls);
			}
			orderedClassList.removeAll(filter);

			for (OWLClass cls : orderedClassList) {
				System.out.println("\n\n((" + cls + " | " + (orderedClassList.indexOf(cls) + 1)
						+ " of " + orderedClassList.size() + " )) : "
						+ definedConceptsByType.get(cls));
				Toolbox.INSTANCE.logThis("", resultFolder, new String("((" + cls + ")) : "
						+ definedConceptsByType.get(cls)));

				examinedClass = cls;

				// * * * modularize T with C * * *
				Set<OWLAxiom> ontMod = null;
				try {
					Set<OWLEntity> singeltonSignature = new HashSet<OWLEntity>();
					singeltonSignature.add(cls);
					ontMod = extractModuleAxioms(singeltonSignature, ont, ModuleType.BOT);

					System.out.println("  MODULARIZED\n\t|Ax(O)| = " + ont.getLogicalAxiomCount()
							+ "\n\t|Ax(M)| = " + ontMod.size());
				} catch (OWLOntologyCreationException e) {
					e.printStackTrace();
				}

				Set<OWLEntity> sigma = new HashSet<OWLEntity>();
				sigma.addAll(Toolbox.INSTANCE.getSignature(ontMod, true));
				sigma.remove(df.getOWLThing());
				sigma.remove(df.getOWLNothing());
				sigma.remove(cls);

				Vector<Set<OWLEntity>> dMCDSsOfConcept = computeMCDSs_Disjoints(cls, sigma, ontMod);
				dMCDSs.put(cls, dMCDSsOfConcept);
				computedConcepts.add(cls);

				// save intermediate per concept
				saveMCDSs(true, dMCDSs, TYPE_DIS);
			}

			// (3) serialize final result
			saveMCDSs(false, dMCDSs, TYPE_DIS);
		} else {
			if (isVerbose)
				System.out.println("LOADED Disjoint MCDSs");
			Toolbox.INSTANCE.logThis("", resultFolder, new String("LOADED Disjoint MCDSs"));
		}

		// Done
		endTimeOntology = System.nanoTime();
		return dMCDSs;
	}

	public Vector<Set<OWLEntity>> computeMCDSs_Disjoints(OWLClass definedConcept,
			Set<OWLEntity> signature, Set<OWLAxiom> moduleAxioms) {
		// Toolbox.INSTANCE.logThis("", resultFolder, new
		// String("computeMCDSs_Disjoints " +
		// definedConcept.getIRI().getShortForm()));
		startTimeConcept = System.nanoTime();
		Vector<Set<OWLEntity>> disjointMCDSs = new Vector<Set<OWLEntity>>();

		Set<OWLEntity> sigma = new HashSet<OWLEntity>();
		sigma.addAll(signature);
		sigma.remove(definedConcept);
		int mcdsCount = 1;

		Vector<Set<OWLEntity>> expDefMCDSs = getSignaturesOfExplicitDefinitions(definedConcept);
		if (expDefMCDSs != null) {
			disjointMCDSs.addAll(expDefMCDSs);
			for (Set<OWLEntity> entSet : expDefMCDSs) {
				sigma.removeAll(entSet);
				System.out.println("\tMCDS(" + mcdsCount++ + ") = " + entSet);
			}
		}

		if (isVerbose) {
			System.out.println("STARTING |Signature| = " + sigma.size() + "|   |Axioms| = "
					+ moduleAxioms.size());
			if (sigma.size() < 50)
				System.out.println("\t" + sigma);
		}

		// sanity check
		impDefCheckCount++;
		if (impDef.isConceptImplicitlyDefinedByEmptySignature(definedConcept)) {
			// Toolbox.INSTANCE.logThis(new String("EmptySigDefines_" +
			// ontFile.getName() + "_" +
			// definedConcept.getIRI().getShortForm()),
			// "");
			System.out.println("\t !!! ERROR - CONCEPT IS IMPLICITLY DEFINABLE BY EMPTY SIGNATURE");
			return null;
		}

		// compute MCDSs
		while (impDef.isConceptImplicitlyDefined_MOD(definedConcept, sigma, moduleAxioms) == true) {
			impDefCheckCount++;
			Set<OWLEntity> newMcds = (Set<OWLEntity>) computeSingleMCDS_MOD(definedConcept, sigma,
					moduleAxioms);
			disjointMCDSs.add(newMcds);
			sigma.removeAll(newMcds);

			if (isVerbose)
				System.out.println("\tMCDS(" + mcdsCount++ + ") = " + newMcds);
			// Toolbox.INSTANCE.logThis("", resultFolder, new String("MCDS(" +
			// mcdsCount + ") = " + newMcds));


			System.out.println("\n\nDIFJ MCDSs " + disjointMCDSs + "\n\t" + newMcds);
		}

		endTimeConcept = System.nanoTime();
		Number timeTaken = endTimeConcept - startTimeConcept;
		definedConceptsComputationTime.put(definedConcept, timeTaken);

		return disjointMCDSs;
	}

	public Vector<Set<OWLEntity>> getSignaturesOfExplicitDefinitions(OWLClass definedConcept) {
		Vector<Set<OWLEntity>> expDefMCDSs = null;
		if (definedConceptsByType.get(definedConcept) == ConceptDefinabilityType.EXPLICIT) {
			expDefMCDSs = new Vector<Set<OWLEntity>>();
			Vector<Set<OWLEntity>> expDefMCDSsWorking = new Vector<Set<OWLEntity>>();

			for (OWLAxiom axx : ont.getReferencingAxioms(definedConcept)) {
				if (!axx.isOfType(AxiomType.DECLARATION)) {
					System.out.println("\t" + axx);
					if (axx.isOfType(AxiomType.EQUIVALENT_CLASSES)) {

						// LHS & RHS
						String[] words = axx.toString().split(" ");

						// C ≡ A
						if (words[0].equals(definedConcept.getIRI().getShortForm())
								|| words[0].equals(definedConcept.getIRI())) {
							Set<OWLEntity> axSet = axx.getSignature();
							axSet.remove(definedConcept);
							axSet.remove(df.getOWLThing());
							axSet.remove(df.getOWLNothing());
							expDefMCDSsWorking.add(axSet);
						}

						// A ≡ C
						if (words.length == 3) {
							if (words[2].equals(definedConcept.getIRI().getShortForm())
									|| words[2].equals(definedConcept.getIRI())) {
								Set<OWLEntity> axSet = axx.getSignature();
								axSet.remove(definedConcept);
								axSet.remove(df.getOWLThing());
								axSet.remove(df.getOWLNothing());
								expDefMCDSsWorking.add(axSet);
							}
						}
					}
				}
			}
			for (Set<OWLEntity> entSet : expDefMCDSsWorking) {

				// note: redundant to compute minimal, an MCDS is already
				// minimal
				// if (isCDSminimal(definedConcept, entSet)) {
				// expDefMCDSs.add(entSet);
				// } else {
				Set<OWLEntity> minSet = computeSingleMCDS_OLD(definedConcept, entSet);
				if (minSet.size() > 0)
					expDefMCDSs.add(minSet);
				// }
			}
			for (Set<OWLEntity> entset : expDefMCDSs) {
				System.out.println("\tEXPLICITLY " + definedConcept + " MCDS = " + entset);
			}
		}
		return expDefMCDSs;
	}



	// * * * SIMULATED MDS * * * //
	public Collection<OWLEntity> computeSingleMCDS_MOD_Simulated(OWLClass definedConcept,
			Collection<OWLEntity> signature, Vector<Set<OWLEntity>> mcdsOFDef) {
		startSignature = new Vector<OWLEntity>();
		startSignature.addAll(signature);

		removedEntities = new Vector<OWLEntity>();

		if (startSignature.size() > 1) {
			impDefCheckCount++;
			if (isConceptImplicitlyDefined_Simulated(definedConcept, startSignature, mcdsOFDef)) {
				splitNprune_Simulated(definedConcept, startSignature, "  ", mcdsOFDef);
			}
		} else {
			Set<OWLEntity> copySig = new HashSet<OWLEntity>();
			copySig.addAll(signature);
			return copySig;
		}

		// done
		Set<OWLEntity> prunedSignature = new HashSet<OWLEntity>();
		prunedSignature.addAll(startSignature);
		prunedSignature.removeAll(removedEntities);
		return prunedSignature;
	}

	private void splitNprune_Simulated(OWLClass definedConcept, Vector<OWLEntity> signature,
			String indent, Vector<Set<OWLEntity>> mcdsOFDef) {
		System.out.println("\n" + indent + "Split&Prune SIM [" + signature.elementAt(0)
				+ ", ... ]   |S| = " + signature.size() + " " + df_day.format(new Date()));

		if (signature.size() > 1) {
			int splitIdx = signature.size() / 2;

			Vector<OWLEntity> firstHalf = new Vector<OWLEntity>();
			Vector<OWLEntity> secondHalf = new Vector<OWLEntity>();
			for (int i = 0; i < splitIdx; i++) {
				firstHalf.add(signature.elementAt(i));
			}
			secondHalf.addAll(signature);
			secondHalf.removeAll(firstHalf);

			if (isVerbose) {
				System.out.println(indent + " (L) : [" + firstHalf.elementAt(0)
						+ ", ... ]   |S1| = " + firstHalf.size());
				System.out.println(indent + " (R) : [" + secondHalf.elementAt(0)
						+ ", ... ]   |S2| = " + secondHalf.size());
			}

			// attempt to remove left half
			Vector<OWLEntity> workingSigma = new Vector<OWLEntity>();
			workingSigma.addAll(startSignature);
			workingSigma.removeAll(removedEntities);
			workingSigma.removeAll(firstHalf);

			impDefCheckCount++;
			if (isConceptImplicitlyDefined_Simulated(definedConcept, workingSigma, mcdsOFDef)) {
				removedEntities.addAll(firstHalf);
				if (isVerbose)
					System.out.println(indent + "(L) REMOVED   |S1| = " + firstHalf.size() + "   "
							+ df_day.format(new Date()));
			} else {
				if (firstHalf.size() > 1) {
					if (isVerbose)
						System.out.println(indent + "(L) Split&Prune SIM");
					splitNprune_Simulated(definedConcept, firstHalf, new String(indent + "  "),
							mcdsOFDef);
				}
			}

			// attempt to remove right half
			workingSigma.removeAllElements();
			workingSigma.addAll(startSignature);
			workingSigma.removeAll(removedEntities);
			workingSigma.removeAll(secondHalf);
			impDefCheckCount++;
			if (isConceptImplicitlyDefined_Simulated(definedConcept, workingSigma, mcdsOFDef)) {
				removedEntities.addAll(secondHalf);
				if (isVerbose)
					System.out.println(indent + "(R) REMOVED   |S2| = " + secondHalf.size() + "   "
							+ df_day.format(new Date()));
			} else {
				if (secondHalf.size() > 1) {
					if (isVerbose)
						System.out.println(indent + "(R) Split&Prune SIM");
					splitNprune_Simulated(definedConcept, secondHalf, new String(indent + "  "),
							mcdsOFDef);
				}
			}
		}
	}

	private boolean isConceptImplicitlyDefined_Simulated(OWLClass definedConcept,
			Vector<OWLEntity> signature, Vector<Set<OWLEntity>> mcdsOFDef) {
		for (Set<OWLEntity> mcds : mcdsOFDef) {
			if (signature.containsAll(mcds)) {
				return true;
			}
		}
		return false;
	}



	// * * * EXPANSION Algorithms OLD UNUSED * * *
	@SuppressWarnings("unused")
	private HashMap<OWLClass, Vector<Set<OWLEntity>>> runExpansionsOnOntology(boolean loadOnly) {

		System.out.println("Expansions, loadOnly?: " + loadOnly + "    " + ontFile.getName());
		Toolbox.INSTANCE.logThis("", resultFolder,
				new String("runExpansionsOnOntology " + ontFile.getName()));

		Vector<DefinedConcept> processDefCons = getDefinedConcepts();
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMCDSs = null;

		// attempt to load
		allMCDSs = loadMCDSs(false, TYPE_PSX);
		if (allMCDSs != null)
			return allMCDSs;

		allMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		for (DefinedConcept defCon : processDefCons) {
			allMCDSs.put(defCon.getConcept(), defCon.getMcdsAllDifferent());
		}

		// loop
		int iterationCount = 0;
		boolean allComplete = false;
		while (!allComplete) {
			System.out.println("\n\nrunExpansionsOnOntology #" + iterationCount + "   ("
					+ this.getNbMCDSs(allMCDSs) + " MCDSs)");
			Toolbox.INSTANCE.logThis("", resultFolder,
					new String("runExpansionsOnOntology iteration #" + iterationCount + "   ("
							+ this.getNbMCDSs(allMCDSs) + " MCDSs)"));
			iterationCount++;

			int completeCount = 0;
			for (DefinedConcept defCon : processDefCons) {
				if (!defCon.isMcdsComplete()) {
					applyExpansionToConcept(defCon);
					System.out.println("\t complete? " + defCon.isMcdsComplete());
					Toolbox.INSTANCE.logThis("", resultFolder,
							new String(" complete? " + defCon.isMcdsComplete()));
				} else {
					completeCount++;
				}
			}

			allMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			for (DefinedConcept defCon : processDefCons) {
				allMCDSs.put(defCon.getConcept(), defCon.getMcdsAllDifferent());
			}


			// DONE
			if (completeCount == processDefCons.size()) {
				allComplete = true;
				saveMCDSs(false, allMCDSs, new String("eX_" + iterationCount));
				for (int i = 0; i < iterationCount; i++) {
					Toolbox.INSTANCE.deleteFile(new String(Toolbox.INSTANCE.getPathFileMCDS(
							resultFolder, ontFile.getName())
							+ "/MCDS_"
							+ new String("eX_" + i)
							+ "_INPROGRESS_" + ontFile.getName() + ".txt"));
				}
			}

			// IN PROGRESS
			else {
				HashMap<OWLClass, Vector<Set<OWLEntity>>> newmcdssss = applyDefinedConceptSubstitution(
						allMCDSs, "eX");
				for (DefinedConcept defCon : processDefCons) {
					defCon.addtoMCDSExpansions(newmcdssss.get(defCon));
				}
				saveMCDSs(true, newmcdssss, new String("eX_" + iterationCount));
			}
		}
		return allMCDSs;
	}

	private void applyExpansionToConcept(DefinedConcept defCon) {
		if (isExpVerbose)
			System.out.println("   applyExpansionToConcept (" + defCon.getConcept()
					+ ")   Union of MDSs = " + defCon.getMcdsAllDifferent().size());
		Toolbox.INSTANCE.logThis("", resultFolder, new String("   applyExpansionToConcept ("
				+ defCon.getConcept() + ") " + defCon.getMcdsAllDifferent()));

		/*
		 * // combine exisiting Set<Set<OWLEntity>> combinedMcds =
		 * combineExistingMCDSs(defCon);
		 * defCon.addtoMCDSExpansions(Toolbox.INSTANCE
		 * .sortUnorderedEntitySetsbySize(combinedMcds)); // if (isExpVerbose)
		 * System.out.println(defCon.getConcept() + " " + //
		 * defCon.getMcdsAllDifferent());
		 */

		// has all been found?
		Set<Set<OWLEntity>> newMcdss = hasAllMCDSsBeenFound(defCon);
		if (newMcdss.size() == 0)
			defCon.setMcdsComplete(true);
		else {
			defCon.addtoMCDSExpansions(Toolbox.INSTANCE.sortUnorderedEntitySetsbySize(newMcdss));
		}
		// if (isExpVerbose) System.out.println("\t" + defCon.getConcept() + " "
		// + defCon.getMcdsAllDifferent());
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private Set<Set<OWLEntity>> combineExistingMCDSs(DefinedConcept definedConcept) {
		if (isExpVerbose)
			System.out.println("\tcombineExistingMCDSs (" + definedConcept.getConcept() + ")");
		Toolbox.INSTANCE.logThis("", resultFolder, new String("combineExistingMCDSs ("
				+ definedConcept.getConcept() + ")"));
		Set<Set<OWLEntity>> newlyFoundMCDSs = new HashSet<Set<OWLEntity>>();
		Vector<Set<OWLEntity>> allExistingMcds = definedConcept.getMcdsAllDifferent();

		Set<OWLEntity> mcdsUnion = new HashSet<OWLEntity>();
		for (Set<OWLEntity> ms : allExistingMcds) {
			mcdsUnion.addAll(ms);
		}

		for (int i = 1; i < mcdsUnion.size() + 1; i++) {
			if (isExpVerbose)
				System.out.println("\t  P set SIZE " + i);
			Toolbox.INSTANCE.logThis("", resultFolder, new String("\t  P set SIZE " + i));

			PowerSet sigPowerSet = new PowerSet(mcdsUnion, 1, i);
			Iterator candidateSigIt = sigPowerSet.iterator();
			while (candidateSigIt.hasNext()) {
				Set<OWLEntity> mus = (Set<OWLEntity>) candidateSigIt.next();

				if (!allExistingMcds.contains(mus) && !newlyFoundMCDSs.contains(mus)) {
					impDefCheckCount++;
					if (impDef.isConceptImplicitlyDefined(definedConcept.getConcept(), mus)) {
						if (isCDSminimal(definedConcept.getConcept(), mus)) {
							if (isExpVerbose)
								System.out.println("\t     NEW MCDS found = " + mus);
							Toolbox.INSTANCE.logThis("", resultFolder, new String(
									"   NEW MCDS found = " + mus));
							newlyFoundMCDSs.add(mus);
						}
					}
				}
			}
		}
		return newlyFoundMCDSs;
	}

	private HashMap<OWLClass, Vector<Set<OWLEntity>>> applyDefinedConceptSubstitution(
			HashMap<OWLClass, Vector<Set<OWLEntity>>> inputMCDS, String inputType) {
		Toolbox.INSTANCE.logThis("", resultFolder, new String(" applyDefinedConceptSubstitution"));
		boolean prevVerbose = isVerbose;
		startTimeOntology = System.nanoTime();

		// (1) attempt to load
		HashMap<OWLClass, Vector<Set<OWLEntity>>> toupdateMCDS = loadMCDSs(false, new String(
				"rplcCdefs_" + inputType));

		// (2) compute
		if (toupdateMCDS == null) {
			toupdateMCDS = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			HashMap<OWLClass, Vector<Set<OWLEntity>>> previousMCDS = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
			for (OWLClass cls : inputMCDS.keySet()) {
				previousMCDS.put(cls, inputMCDS.get(cls));
			}

			boolean updated = true;
			int count = 0;
			while (updated) {
				System.out.println("------------------------------------------\nITERATION "
						+ ++count);

				if (toupdateMCDS.keySet().size() > 0) {
					previousMCDS.clear();
					for (OWLClass cls : inputMCDS.keySet()) {
						previousMCDS.put(cls, toupdateMCDS.get(cls));
					}
				}
				toupdateMCDS.clear();

				for (OWLClass cls : inputMCDS.keySet()) {

					Vector<Set<OWLEntity>> mcdssOfConcept = new Vector<Set<OWLEntity>>();
					mcdssOfConcept.addAll(previousMCDS.get(cls));

					for (Set<OWLEntity> mcds : previousMCDS.get(cls)) {
						// System.out.println("\n" + cls + " " + mcds);

						for (OWLEntity ent : mcds) {
							if (inputMCDS.keySet().contains(ent)) {
								// System.out.println("   defined C: " + ent);

								Vector<Set<OWLEntity>> mcdssOfDefCInMCDS = previousMCDS.get(ent);
								for (Set<OWLEntity> mcdsOfDefC : mcdssOfDefCInMCDS) {
									// System.out.println("      MCDS_defC: " +
									// mcdsOfDefC);

									if (!mcdsOfDefC.contains(cls)) {
										Set<OWLEntity> rewrittenMCDS = new HashSet<OWLEntity>();
										rewrittenMCDS.addAll(mcds);
										rewrittenMCDS.remove(ent);
										rewrittenMCDS.addAll(mcdsOfDefC);
										// System.out.println("         NEW: " +
										// rewrittenMCDS);

										if (!mcdssOfConcept.contains(rewrittenMCDS)) {
											prevVerbose = isVerbose;
											isVerbose = false;
											if (isCDSminimal(cls, rewrittenMCDS)) {
												mcdssOfConcept.add(rewrittenMCDS);
												System.out.println("\n" + cls + " " + mcds);
												System.out.println("         NEW ADDED: " + ent
														+ " by " + mcdsOfDefC + "\n\t"
														+ rewrittenMCDS);
											}
											isVerbose = prevVerbose;
										}
									}
								}
							}
						}
					}
					toupdateMCDS.put(cls, mcdssOfConcept);
				}
				// check update status
				if (previousMCDS.equals(toupdateMCDS)) {
					updated = false;
				}
			}
			// Done
			// saveMCDSs(false, toupdateMCDS, new String("rplcCdefs_" +
			// inputType));
		} else {
			if (isVerbose)
				System.out.println("LOADED Rewritten " + inputType + " MCDSs");
		}

		endTimeOntology = System.nanoTime();
		return toupdateMCDS;
	}



	// * * * EXPANSION * * *
	public HashMap<OWLClass, Vector<Set<OWLEntity>>> runExpansion() {
		System.out.println("\n\nMCDS COMPUTER runExpansion");

		// attempt to load uncompleted
		boolean isComplete = false;
		// System.out.println("   Try to load: INPROGRESS");
		allMcds = loadMCDSsExpanded(true);
		if (allMcds != null) {
			System.out.println("\t ... MDS computation is INPROGRESS ...");
			isComplete = false;
		} else {
			allMcds = loadMCDSsExpanded(false);
			if (allMcds != null) {
				System.out.println("\tFOUND COMPLETE MDSs");
				isComplete = true;
			}
		}

		// compute
		if (!isComplete) {
			System.out.println("\tINCOMPLETE -> Starting computation");
			int completeCount = 0;
			Vector<DefinedConcept> processDefCons = getDefinedConcepts();

			if (allMcds == null) {
				allMcds = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
				for (DefinedConcept defCon : processDefCons) {
					allMcds.put(defCon.getConcept(), defCon.getMcdsAllDifferent());
				}
			}

			System.out.println("DONE | computed concepts : " + computedConcepts);
			for (DefinedConcept defCon : processDefCons) {
				if (computedConcepts.contains(defCon.getConcept())) {
					defCon.setMcdsComplete(true);
					completeCount++;
				}
			}

			// order concepts by MDS union size
			processDefCons = orderCdefsByMDSunionSize(processDefCons);

			// run expansion
			while (completeCount < processDefCons.size()) {
				System.out.println("\n\nrunExpansionsOnOntology #" + "   \n\t("
						+ this.getNbMCDSs(allMcds) + " MCDSs)\n\t#" + +processDefCons.size()
						+ " defined concepts (" + completeCount + " completed) \n\t"
						+ df_day.format(new Date()));
				Toolbox.INSTANCE.logThis("", resultFolder, new String(
						"runExpansionsOnOntology iteration #" + "   (" + this.getNbMCDSs(allMcds)
								+ " MCDSs)"));


				for (DefinedConcept defCon : processDefCons) {
					if (!defCon.isMcdsComplete()) {
						System.out
								.println("\n\n(((" + defCon.getConcept() + "))) complete? "
										+ defCon.isMcdsComplete() + " "
										+ defCon.getMcdsAllDifferentCount());

						Set<Set<OWLEntity>> newMcdss = hasAllMCDSsBeenFound(defCon);
						if (newMcdss.size() == 0) {
							defCon.setMcdsComplete(true);
							completeCount++;
							computedConcepts.addElement(defCon.getConcept());
							System.out.println("\t   NOW Complete (" + computedConcepts.size()
									+ " of " + processDefCons.size() + ")");


							saveMCDSsExpanded(true, allMcds, computedConcepts);
						} else {
							System.out
									.println("\tNEW MCDSs " + newMcdss.size() + "\n\t" + newMcdss);
							defCon.addtoMCDSExpansions(Toolbox.INSTANCE
									.sortUnorderedEntitySetsbySize(newMcdss));

							allMcds.get(defCon.getConcept()).addAll(newMcdss);
							saveMCDSsExpanded(true, allMcds, computedConcepts);
						}
					}
				}
			}
		}
		if (!isComplete) {
			saveMCDSsExpanded(false, allMcds, computedConcepts);
		}
		return allMcds;
	}

	public HashMap<OWLClass, Vector<Set<OWLEntity>>> runExpansion_Partial(int partIndex,
			int partsTotal) {
		this.partThis = partIndex;
		this.partTotal = partsTotal;
		System.out.println("\n\nMCDS COMPUTER runExpansion PARTIAL | " + partThis + " of "
				+ partTotal);

		// attempt to load uncompleted
		boolean isComplete = false;
		System.out.println("   Try to load: INPROGRESS");
		allMcds = loadMCDSsExpanded(true);
		if (allMcds != null) {
			System.out.println("\tIS INPROGRESS");
			isComplete = false;
		} else {
			allMcds = loadMCDSsExpanded(false);
			if (allMcds != null) {
				System.out.println("\tIS COMPLETE");
				isComplete = true;
			}
		}

		// compute
		if (!isComplete) {
			System.out.println("\tINCOMPLETE -> Starting computation");
			int completeCount = 0;
			Vector<DefinedConcept> processDefCons = getDefinedConcepts();

			if (allMcds == null) {
				allMcds = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
				for (DefinedConcept defCon : processDefCons) {
					allMcds.put(defCon.getConcept(), defCon.getMcdsAllDifferent());
				}
			}

			System.out.println("DONE | Computed Concepts: " + computedConcepts);
			for (DefinedConcept defCon : processDefCons) {
				if (computedConcepts.contains(defCon.getConcept())) {
					defCon.setMcdsComplete(true);
					completeCount++;
				}
			}


			// order concepts by MDS union size
			processDefCons = orderCdefsByMDSunionSize(processDefCons);

			// partial
			processDefCons = getPart_DefinedConcept(processDefCons);

			// run expansion
			while (completeCount < processDefCons.size()) {
				System.out.println("\n\nrunExpansionsOnOntology #" + "   \n\t("
						+ this.getNbMCDSs(allMcds) + " MCDSs)\n\t#" + +processDefCons.size()
						+ " defined concepts (" + completeCount + " completed) \n\t"
						+ df_day.format(new Date()));
				Toolbox.INSTANCE.logThis("", resultFolder, new String(
						"runExpansionsOnOntology iteration #" + "   (" + this.getNbMCDSs(allMcds)
								+ " MCDSs)"));


				for (DefinedConcept defCon : processDefCons) {
					if (!defCon.isMcdsComplete()) {
						System.out
								.println("\n\n     " + defCon.getConcept() + " complete? "
										+ defCon.isMcdsComplete() + " "
										+ defCon.getMcdsAllDifferentCount());

						Set<Set<OWLEntity>> newMcdss = hasAllMCDSsBeenFound(defCon);
						if (newMcdss.size() == 0) {
							defCon.setMcdsComplete(true);
							completeCount++;
							System.out.println("\t   NOW Complete (" + computedConcepts.size()
									+ " of " + processDefCons.size() + ")");
							computedConcepts.addElement(defCon.getConcept());

							saveMCDSsExpanded(true, allMcds, computedConcepts);
						} else {
							System.out
									.println("\tNEW MCDSs " + newMcdss.size() + "\n\t" + newMcdss);
							defCon.addtoMCDSExpansions(Toolbox.INSTANCE
									.sortUnorderedEntitySetsbySize(newMcdss));

							allMcds.get(defCon.getConcept()).addAll(newMcdss);
							saveMCDSsExpanded(true, allMcds, computedConcepts);
						}
					}
				}
			}
		}
		if (!isComplete) {
			saveMCDSsExpanded(false, allMcds, computedConcepts);
		}
		return allMcds;
	}

	// @SuppressWarnings({ "unchecked", "rawtypes" })
	@SuppressWarnings("unchecked")
	public Set<Set<OWLEntity>> hasAllMCDSsBeenFound(DefinedConcept definedConcept) {
		impDefCheckCount = 0;

		if (isExpVerbose)
			System.out.println("\thasAllMCDSsBeenFound (" + definedConcept.getConcept() + ")");
		// Toolbox.INSTANCE.logThis("", resultFolder, new
		// String(" hasAllMCDSsBeenFound (" + definedConcept.getConcept() +
		// ")"));

		Set<Set<OWLEntity>> newlyFoundMCDSs = new HashSet<Set<OWLEntity>>();
		Set<Set<OWLEntity>> newlyFoundMCDSparts = new HashSet<Set<OWLEntity>>();

		// (S) = UNION of MDSs
		Vector<Set<OWLEntity>> allExistingMcds = definedConcept.getMcdsAllDifferent();
		Set<OWLEntity> mcdsUnion = new HashSet<OWLEntity>();
		for (Set<OWLEntity> ms : allExistingMcds) {
			if (ms.size() > 1) {
				mcdsUnion.addAll(ms);
			}
		}
		System.out.println("\t |MCDS| = " + allExistingMcds.size() + "\n\t   U of MCDSs = "
				+ mcdsUnion.size() + "\n\t\t" + mcdsUnion);

		if (mcdsUnion.size() > 0) {
			// (K) = Sig(M) \ (S u {C})
			Set<OWLAxiom> ontMod = null;
			try {
				Set<OWLEntity> singeltonSignature = new HashSet<OWLEntity>();
				singeltonSignature.add(definedConcept.getConcept());
				ontMod = extractModuleAxioms(singeltonSignature, ont, ModuleType.STAR);
			} catch (OWLOntologyCreationException e) {
				e.printStackTrace();
			}

			// Modularize T with {C}
			Set<OWLEntity> moduleSigma = new HashSet<OWLEntity>();
			moduleSigma.addAll(Toolbox.INSTANCE.getSignature(ontMod, true));
			moduleSigma.remove(df.getOWLThing());
			moduleSigma.remove(df.getOWLNothing());
			moduleSigma.removeAll(mcdsUnion);
			moduleSigma.remove(definedConcept.getConcept());
			System.out.println("  MODULARIZED\n\t|Ax(O)| = " + ont.getLogicalAxiomCount()
					+ "\n\t|Ax(M)| = " + ontMod.size() + "\n\t|Sig(M)| = " + moduleSigma.size());



			// SYMBOL Table to reduce P-set memory requirements
			HashMap<Integer, OWLEntity> entityTable = generateSymbolTable(mcdsUnion);
			Set<Integer> mcdsUnionAsInt = new HashSet<Integer>();
			mcdsUnionAsInt.addAll(entityTable.keySet());

			// Power sets of increasing size (optimatization to avoid computing
			// the full P-set)
			for (int i = 1; i < mcdsUnion.size() + 1; i++) {
				if (isExpVerbose)
					System.out.println("\n\tP set SIZE " + i + " of " + mcdsUnion.size()
							+ "    at " + df_day.format(new Date()));
				// Toolbox.INSTANCE.logThis("", resultFolder, new
				// String("\t  P set SIZE " + i));

				@SuppressWarnings("rawtypes")
				PowerSet sigPowerSet = new PowerSet(mcdsUnionAsInt, i, i);
				if (isExpVerbose)
					System.out.println("\tPowerSet GENERATED " + df_day.format(new Date()));

				// FOR s in P(S)
				@SuppressWarnings("rawtypes")
				Iterator candidateSigIt = sigPowerSet.iterator();
				while (candidateSigIt.hasNext()) {
					Set<Integer> musInt = (Set<Integer>) candidateSigIt.next();
					Vector<OWLEntity> mus = getEntitySetFromSymbolTable(entityTable, musInt);


					System.out.println("C SIGMA : " + mus);

					// FILTER: any existing MDS that is a sub/superset of this
					// (s)
					boolean isSupersetOfExistingMDS = false;
					boolean isSubsetOfExistingMDS = false;
					for (Set<OWLEntity> exMDS : allExistingMcds) {
						if (exMDS.containsAll(mus)) {
							// isSupersetOfExistingMDS = true;
							break;
						}
						if (mus.containsAll(exMDS)) {
							// isSubsetOfExistingMDS = true;
							break;
						}
					}

					// FILTER MDS parts
					boolean isSubsetOfexistingMDSpart = false;
					for (Set<OWLEntity> exMDSpart : newlyFoundMCDSparts) {
						if (exMDSpart.containsAll(mus)) {
							// isSubsetOfexistingMDSpart = true;
							break;
						}
					}

					// TEST signature
					if (!isSupersetOfExistingMDS && !isSubsetOfexistingMDSpart
							&& !isSubsetOfExistingMDS) {
						System.out.println("\t Is it a DS? " + df_day.format(new Date()) + " S = "
								+ mus + " u [...]");

						// (W) = K u s
						mus.addAll(moduleSigma);

						impDefCheckCount++;
						if (impDef.isConceptImplicitlyDefined(definedConcept.getConcept(), mus)) {
							System.out.println("\t   It is a DS -> Compute MDS   "
									+ df_day.format(new Date()));

							Set<OWLEntity> newMcds = (Set<OWLEntity>) computeSingleMCDS_MOD(
									definedConcept.getConcept(), mus, ontMod);
							System.out.println("\t   MDS Computed   " + df_day.format(new Date())
									+ "   " + newMcds);

							if (!allExistingMcds.contains(newMcds)) {
								if (isExpVerbose)
									System.out.println("\t     NEW MCDS found = " + newMcds
											+ "    at " + df_day.format(new Date()));
								Toolbox.INSTANCE.logThis("", resultFolder, new String(
										" NEW MCDS found = " + newMcds));

								// SAVE
								allExistingMcds.add(newMcds);
								newlyFoundMCDSs.add(newMcds);

								// SERALISE
								if (isSaving)
									saveMCDSsExpanded(true, allMcds, computedConcepts);

								// for FILTERING
								newlyFoundMCDSparts.add(Toolbox.INSTANCE
										.convertEntityVectorToSet(getEntitySetFromSymbolTable(
												entityTable, musInt)));
							} else {
								System.out.println("\t    NOT a NEW MCDS   "
										+ df_day.format(new Date()));
							}
						}
					}
				}
			}
		}
		return newlyFoundMCDSs;
	}

	private HashMap<Integer, OWLEntity> generateSymbolTable(Set<OWLEntity> signature) {
		HashMap<Integer, OWLEntity> entityTable = new HashMap<Integer, OWLEntity>();
		int i = 0;
		for (OWLEntity ent : signature) {
			entityTable.put(i++, ent);
		}
		return entityTable;
	}

	private Vector<OWLEntity> getEntitySetFromSymbolTable(HashMap<Integer, OWLEntity> entityTable,
			Set<Integer> sigInt) {
		Vector<OWLEntity> sigEnt = new Vector<OWLEntity>();
		for (Integer intg : sigInt) {
			sigEnt.add(entityTable.get(intg));
		}
		return sigEnt;
	}

	private boolean saveMCDSsExpanded(boolean inProgress,
			HashMap<OWLClass, Vector<Set<OWLEntity>>> jMCDSs, Vector<OWLClass> completedConcepts) {

		System.out.println("\n   ... SAVING MDSs ...\n");

		// (1) order alphabetically
		Vector<OWLClass> orderedClassList = new Vector<OWLClass>();
		orderedClassList.addAll(jMCDSs.keySet());
		orderedClassList = Toolbox.INSTANCE.sortConceptsAlphabetcially(orderedClassList);

		// (2) create textual representation
		StringBuilder sb = new StringBuilder();
		for (OWLClass cls : orderedClassList) {

			if (jMCDSs.get(cls) != null) {
				sb.append("C: " + cls.getIRI() + "\n");

				Vector<Set<OWLEntity>> clsMCDSs = jMCDSs.get(cls);
				for (Set<OWLEntity> mcds : clsMCDSs) {
					sb.append("\u03A3:");
					Vector<OWLEntity> orderedSignature = Toolbox.INSTANCE
							.sortEntitiesAlphabetcially(mcds);


					// System.out.println("??? orderedSignature " +
					// orderedSignature.size() + "   " + orderedSignature);

					boolean nullSig = false;
					if (orderedSignature.size() == 1) {
						if (orderedSignature.elementAt(0) == null) {
							nullSig = true;
						}
					}

					if (!nullSig) {
						for (OWLEntity sigsymbol : orderedSignature) {
							sb.append(" " + sigsymbol.getIRI());
						}
						sb.append("\n");
					}
				}
			}
		}

		// (3) serialize
		String inProgressStr = "";
		if (inProgress) {
			inProgressStr = "INPROGRESS_";
			sb.append("CompletedConcepts: ");

			if (completedConcepts != null) {
				for (OWLClass cls : completedConcepts) {
					if (cls != null)
						sb.append(cls.getIRI().toString() + " ");
				}
			}
			sb.append("\n");
		}
		String filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
				ontFile.getName())
				+ "/MCDS_eX_" + inProgressStr + ontFile.getName() + ".txt");
		if (this.partTotal > -1) {
			filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder, ontFile.getName())
					+ "/MCDS_eX_" + inProgressStr + ontFile.getName() + "_" + partThis + "of"
					+ partTotal + ".txt");
		}

		// (4) delete inprogress
		if (!inProgress) {
			if (this.partTotal > -1) {
				new File(new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
						ontFile.getName())
						+ "/MCDS_eX_INPROGRESS_"
						+ ontFile.getName()
						+ "_"
						+ partThis
						+ "of"
						+ partTotal + ".txt")).delete();
			} else {
				File[] folderfiles = new File(Toolbox.INSTANCE.getPathFileMCDS(new String(
						resultFolder), ontFile.getName())).listFiles();
				for (File f : folderfiles) {
					if (f.getName().startsWith("MCDS_eX_")
							&& f.getName().endsWith(ontFile.getName() + ".txt")
							&& f.getName().contains("INPROGRESS")) {
						f.delete();
					}
				}
			}
		}

		boolean saved = Toolbox.INSTANCE.writeToFile(filePath, sb.toString());
		System.out.println("\n   ... FINISHED SAVING, SUCCESS ?: " + saved);

		return saved;
	}

	private HashMap<OWLClass, Vector<Set<OWLEntity>>> loadMCDSsExpanded(boolean inProgress) {
		HashMap<OWLClass, Vector<Set<OWLEntity>>> loadedMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		computedConcepts.removeAllElements();

		String filePath = "";

		// PARTIAL
		if (this.partTotal > -1) {
			// System.out.println("   TRYING TO LOAD partial INPROGRESS");
			filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder, ontFile.getName())
					+ "/MCDS_eX_INPROGRESS_" + ontFile.getName() + "_" + partThis + "of"
					+ partTotal + ".txt");

			if (!Toolbox.INSTANCE.doesFileExist(filePath)) {
				// System.out.println("   TRYING TO LOAD partial ");
				filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
						ontFile.getName())
						+ "/MCDS_eX_" + ontFile.getName() + ".txt");
			}
		}

		// NON-PARTIAL
		if (!Toolbox.INSTANCE.doesFileExist(filePath)) {
			File[] folderfiles = new File(Toolbox.INSTANCE.getPathFileMCDS(
					new String(resultFolder), ontFile.getName())).listFiles();
			File largestFile = null;
			for (File f : folderfiles) {
				if (f.getName().startsWith("MCDS_eX_")
						&& f.getName().endsWith(ontFile.getName() + ".txt")) {
					if (largestFile == null)
						largestFile = f;
					// System.out.println("..." + f.getName());
					if (f.length() >= largestFile.length()) {
						largestFile = f;
					}
				}
			}

			// is in progress ?
			if (largestFile != null) {
				filePath = largestFile.getAbsolutePath();
				// System.out.println("\tLARGEST FILE " + filePath);

				if (inProgress) {
					if (!filePath.contains("INPROGRESS")) {
						// System.out.println("\tFOUND INPROGRESS NO INPROGRESS file");
						return null;
					}
				}

				System.out.println("\t file : " + filePath);
			}
		}
		if (!Toolbox.INSTANCE.doesFileExist(filePath))
			return null;

		// load file
		if (filePath.equals("")) {
			return null;
		}

		String fileContent = Toolbox.INSTANCE.readFile(filePath);
		if (fileContent == null) {
			return null;
		}

		// process text into datastructure
		Vector<Set<OWLEntity>> sigsOfCls = new Vector<Set<OWLEntity>>();
		Set<OWLEntity> sig = new HashSet<OWLEntity>();
		OWLClass theClass = null;
		Scanner scanner = new Scanner(fileContent);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("C:")) {
				if (theClass != null) {
					loadedMCDSs.put(theClass, sigsOfCls);
				}
				theClass = Toolbox.INSTANCE
						.getClassEntity(IRI.create(line.replace("C: ", "")), ont);
				sigsOfCls = new Vector<Set<OWLEntity>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<OWLEntity>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(Toolbox.INSTANCE.getEntity(IRI.create(wd), ont));
				}
				sigsOfCls.add(sig);
			} else if (line.startsWith("CompletedConcepts: ")) {
				String[] completedConcepts = line.replace("CompletedConcepts: ", "").split(" ");
				for (int i = 0; i < completedConcepts.length; i++) {
					computedConcepts.add(Toolbox.INSTANCE.getClassEntity(
							IRI.create(completedConcepts[i]), ont));
				}
				System.out.println("\t|CompletedConcepts| = " + computedConcepts.size());
			}
		}
		loadedMCDSs.put(theClass, sigsOfCls);
		scanner.close();
		return loadedMCDSs;
	}

	public Vector<DefinedConcept> orderCdefsByMDSunionSize(Vector<DefinedConcept> list) {
		final HashMap<DefinedConcept, Set<OWLEntity>> unions = new HashMap<DefinedConcept, Set<OWLEntity>>();
		for (DefinedConcept defCon : list) {
			Set<OWLEntity> union = new HashSet<OWLEntity>();
			for (Set<OWLEntity> mds : defCon.getMcdsAllDifferent()) {
				if (mds.size() > 1)
					union.addAll(mds);
			}
			unions.put(defCon, union);
		}

		// exclude too large MDS unions
		Vector<DefinedConcept> filteredList = new Vector<DefinedConcept>();
		for (DefinedConcept defCon : unions.keySet()) {
			if (unions.get(defCon).size() <= expansionLimit) {
				filteredList.add(defCon);
			} else {
				System.out.println("\t Exclude CONCEPT " + defCon + "   |MDS_union| = "
						+ unions.get(defCon).size());
			}
		}

		// sort ascending
		Collections.sort(filteredList, new Comparator<DefinedConcept>() {
			@Override
			public int compare(DefinedConcept a1, DefinedConcept a2) {
				return unions.get(a1).size() > unions.get(a2).size() ? 1
						: unions.get(a1).size() < unions.get(a2).size() ? -1 : 0;
			}
		});
		return filteredList;
	}



	// * * * MODULARIZATION * * *
	@SuppressWarnings("unused")
	private OWLOntology extractModule(Set<OWLEntity> signature, OWLOntology o, ModuleType moduleType)
			throws OWLOntologyCreationException {
		SyntacticLocalityModuleExtractor extractor = new SyntacticLocalityModuleExtractor(
				o.getOWLOntologyManager(), o, moduleType);

		Set<OWLAxiom> moduleAxioms = extractor.extract(signature);
		OWLOntologyManager omModule = OWLManager.createOWLOntologyManager();
		OWLOntology ontMod = omModule.createOntology(moduleAxioms);

		System.out.println("ORIGINAL Ontology " + o.getOntologyID().getOntologyIRI()
				+ "\n\tAxioms =  " + o.getTBoxAxioms(false).size());
		System.out.println("Ontology MODULE of " + signature.iterator().next() + " "
				+ o.getOntologyID().getOntologyIRI() + "\n\tAxioms =  "
				+ ontMod.getTBoxAxioms(false).size());
		return ontMod;
	}

	public Set<OWLAxiom> extractModuleAxioms(Set<OWLEntity> signature, OWLOntology o,
			ModuleType moduleType) throws OWLOntologyCreationException {
		SyntacticLocalityModuleExtractor extractor = new SyntacticLocalityModuleExtractor(
				o.getOWLOntologyManager(), o, moduleType);

		return extractor.extract(signature);
	}



	// * * * MCDS - Serialization & Loading * * *
	private boolean saveMCDSs(boolean inProgress, HashMap<OWLClass, Vector<Set<OWLEntity>>> jMCDSs,
			String type) {

		// (1) order alphabetically
		Vector<OWLClass> orderedClassList = new Vector<OWLClass>();
		orderedClassList.addAll(jMCDSs.keySet());
		orderedClassList = Toolbox.INSTANCE.sortConceptsAlphabetcially(orderedClassList);

		// (2) create textual representation
		StringBuilder sb = new StringBuilder();
		for (OWLClass cls : orderedClassList) {

			if (jMCDSs.get(cls) != null) {
				sb.append("C: " + cls.getIRI() + "\n");

				Vector<Set<OWLEntity>> clsMCDSs = jMCDSs.get(cls);
				for (Set<OWLEntity> mcds : clsMCDSs) {
					sb.append("\u03A3:");
					Vector<OWLEntity> orderedSignature = Toolbox.INSTANCE
							.sortEntitiesAlphabetcially(mcds);
					for (OWLEntity sigsymbol : orderedSignature) {
						sb.append(" " + sigsymbol.getIRI());
					}
					sb.append("\n");
				}
			}
		}

		// (3) serialize
		String inProgressStr = "";
		if (inProgress) {
			inProgressStr = "INPROGRESS_";
		}
		String filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
				ontFile.getName())
				+ "/MCDS_" + type + "_" + inProgressStr + ontFile.getName() + ".txt");
		if (this.partTotal > -1) {
			filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder, ontFile.getName())
					+ "/MCDS_" + type + "_" + inProgressStr + ontFile.getName() + "_" + partThis
					+ "of" + partTotal + ".txt");
		}

		// delete inprogress file
		if (!inProgress) {
			String filePathDelete = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
					ontFile.getName())
					+ "/MCDS_"
					+ type
					+ "_INPROGRESS_"
					+ ontFile.getName()
					+ ".txt");
			if (this.partTotal > -1) {
				filePathDelete = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
						ontFile.getName())
						+ "/MCDS_"
						+ type
						+ "_INPROGRESS_"
						+ ontFile.getName()
						+ "_"
						+ partThis
						+ "of" + partTotal + ".txt");
			}
			Toolbox.INSTANCE.deleteFile(filePathDelete);

			// times
			StringBuilder sbtime = new StringBuilder();
			for (OWLClass cls : definedConceptsComputationTime.keySet()) {
				sbtime.append(cls + ", " + definedConceptsComputationTime.get(cls) + "\n");
			}
		}
		return Toolbox.INSTANCE.writeToFile(filePath, sb.toString());
	}

	private HashMap<OWLClass, Vector<Set<OWLEntity>>> loadMCDSs(boolean inProgress, String type) {
		String filePath = null;
		if (this.partTotal > -1) {
			filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder, ontFile.getName())
					+ "/MCDS_" + type + "_INPROGRESS_" + ontFile.getName() + "_" + partThis + "of"
					+ partTotal + ".txt");
			if (!Toolbox.INSTANCE.doesFileExist(filePath)) {
				filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
						ontFile.getName())
						+ "/MCDS_" + type + "_" + ontFile.getName() + ".txt");
			}
		} else {
			filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder, ontFile.getName())
					+ "/MCDS_" + type + "_INPROGRESS_" + ontFile.getName() + ".txt");
			if (!inProgress) {
				filePath = new String(Toolbox.INSTANCE.getPathFileMCDS(resultFolder,
						ontFile.getName())
						+ "/MCDS_" + type + "_" + ontFile.getName() + ".txt");
			}
		}

		if (!Toolbox.INSTANCE.doesFileExist(filePath)) {
			return null;
		}
		System.out.println("\tSTART LOADING MDSs ...");

		// load file
		String fileContent = Toolbox.INSTANCE.readFile(filePath);

		// process text into datastructure
		HashMap<OWLClass, Vector<Set<OWLEntity>>> loadedMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		Vector<Set<OWLEntity>> sigsOfCls = new Vector<Set<OWLEntity>>();
		Set<OWLEntity> sig = new HashSet<OWLEntity>();
		OWLClass theClass = null;
		Scanner scanner = new Scanner(fileContent);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("C:")) {
				if (theClass != null) {
					loadedMCDSs.put(theClass, sigsOfCls);
				}
				theClass = (OWLClass) Toolbox.INSTANCE.getEntity(
						IRI.create(line.replace("C: ", "")), ont);
				sigsOfCls = new Vector<Set<OWLEntity>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<OWLEntity>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(Toolbox.INSTANCE.getEntity(IRI.create(wd), ont));
				}
				sigsOfCls.add(sig);
				// System.out.println("\t ADDED " + sig);
			}
		}
		loadedMCDSs.put(theClass, sigsOfCls);
		scanner.close();
		computedConcepts.addAll(loadedMCDSs.keySet());
		return loadedMCDSs;
	}



	public HashMap<OWLClass, Vector<Set<OWLEntity>>> loadMCDSs(String type) {
		return loadMCDSs(false, type);
	}



	// * * * DefinedConcept * * *
	private void addMcdsToDefinedConcepts(String type,
			HashMap<OWLClass, Vector<Set<OWLEntity>>> mcdsss) {
		// System.out.println("addMcdsToDefinedConcepts " + type + " " +
		// mcdsss.size());
		if (type.equals(TYPE_DIS)) {
			for (DefinedConcept defcon : definedConcepts) {
				defcon.setMcdsDisjoint(mcdsss.get(defcon.getConcept()));
			}
		} else if (type.equals(TYPE_PSX)) {
			for (DefinedConcept defcon : definedConcepts) {
				defcon.addtoMCDSExpansions(mcdsss.get(defcon.getConcept()));
			}
		}
	}

	public Vector<DefinedConcept> getDefinedConcepts() {
		Computer_DefinedConcept defComp = new Computer_DefinedConcept(resultFolder, ont, ontFile,
				reasonerType, 60000, false);
		defComp.getDefinedConceptListByType();
		definedConcepts = defComp.getDefinedConcepts();
		if (Toolbox.INSTANCE.doesFileExist(new String(Toolbox.INSTANCE.getPathFileMCDS(
				resultFolder, ontFile.getName())
				+ "/MCDS_"
				+ TYPE_DIS
				+ "_"
				+ ontFile.getName()
				+ ".txt"))) {
			addMcdsToDefinedConcepts(TYPE_DIS, loadMCDSs(TYPE_DIS));
		}

		HashMap<OWLClass, Vector<Set<OWLEntity>>> expanded = loadMCDSsExpanded(false);
		if (expanded != null) {
			addMcdsToDefinedConcepts(TYPE_PSX, expanded);
		}

		return definedConcepts;
	}



	// * * * Time * * *
	public Number getTimeTakenByOntology() {
		Number timeTaken = endTimeOntology - startTimeOntology;
		return timeTaken;
	}

	public HashMap<OWLClass, Number> getDefinedConceptsComputationTime() {
		return definedConceptsComputationTime;
	}

	public HashMap<OWLClass, ConceptDefinabilityType> getDefinedConceptsByType() {
		return definedConceptsByType;
	}



	// * * * Helper * * *
	public int getNbMCDSs(HashMap<OWLClass, Vector<Set<OWLEntity>>> mcdsToCount) {
		int count = 0;
		for (OWLClass cls : mcdsToCount.keySet()) {
			count += mcdsToCount.get(cls).size();
		}
		return count;
	}

	private Vector<OWLClass> getPart(Vector<OWLClass> conceptsToCompute) {
		Vector<OWLClass> partialSet = new Vector<OWLClass>();
		int partLength = conceptsToCompute.size() / partTotal;
		int indexFrom = 0;
		int indexTo = 0;
		if (partThis == partTotal) {
			indexFrom = (partThis - 1) * partLength;
			indexTo = conceptsToCompute.size() - 1;
		} else {
			indexFrom = (partThis - 1) * partLength;
			indexTo = (partThis * partLength) - 1;
		}
		partialSet.addAll(conceptsToCompute.subList(indexFrom, indexTo));

		System.out.println("\n\n\nGET PART " + partThis + " of " + partTotal + "\n"
				+ "TOTAL to compute = " + conceptsToCompute.size() + "\nTHIS PART = "
				+ partialSet.size());
		return partialSet;
	}

	private Vector<DefinedConcept> getPart_DefinedConcept(Vector<DefinedConcept> conceptsToCompute) {
		Vector<DefinedConcept> partialSet = new Vector<DefinedConcept>();
		int partLength = conceptsToCompute.size() / partTotal;
		int indexFrom = 0;
		int indexTo = 0;
		if (partThis == partTotal) {
			indexFrom = (partThis - 1) * partLength;
			indexTo = conceptsToCompute.size() - 1;
		} else {
			indexFrom = (partThis - 1) * partLength;
			indexTo = (partThis * partLength) - 1;
		}
		partialSet.addAll(conceptsToCompute.subList(indexFrom, indexTo));

		System.out.println("\n\n\nGET PART " + partThis + " of " + partTotal + "\n"
				+ "TOTAL to compute = " + conceptsToCompute.size() + "\nTHIS PART = "
				+ partialSet.size());
		return partialSet;
	}



	// * * * UNUSED * * *
	@SuppressWarnings("unused")
	private Set<OWLEntity> computeSingleMCDS(OWLClass definedConcept, Set<OWLEntity> signature) {
		startSignature = new Vector<OWLEntity>();
		startSignature.addAll(signature);

		removedEntities = new Vector<OWLEntity>();

		//
		if (startSignature.size() > 1) {
			impDefCheckCount++;
			if (impDef.isConceptImplicitlyDefined(definedConcept, startSignature)) {
				splitNprune(definedConcept, startSignature, "  ");
			}
		} else {
			return signature;
		}

		// done
		Set<OWLEntity> prunedSignature = new HashSet<OWLEntity>();
		prunedSignature.addAll(startSignature);
		prunedSignature.removeAll(removedEntities);
		return prunedSignature;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private Set<Set<OWLEntity>> hasAllMCDSsBeenFound_NotModularized(DefinedConcept definedConcept) {
		if (isExpVerbose)
			System.out.println("\thasAllMCDSsBeenFound (" + definedConcept.getConcept() + ")");
		Toolbox.INSTANCE.logThis("", resultFolder, new String(" hasAllMCDSsBeenFound ("
				+ definedConcept.getConcept() + ")"));


		Set<Set<OWLEntity>> newlyFoundMCDSs = new HashSet<Set<OWLEntity>>();


		// (S) = UNION of MDSs
		Vector<Set<OWLEntity>> allExistingMcds = definedConcept.getMcdsAllDifferent();
		Set<OWLEntity> mcdsUnion = new HashSet<OWLEntity>();
		for (Set<OWLEntity> ms : allExistingMcds) {
			mcdsUnion.addAll(ms);
		}
		System.out.println("\t |MCDS| = " + allExistingMcds.size() + "\n\t U of MCDSs = "
				+ mcdsUnion.size());


		// (K) = Sig(T) \ (S u {C})
		Set<OWLEntity> tboxSigma = tboxSignature;
		tboxSigma.removeAll(mcdsUnion);
		tboxSigma.remove(definedConcept.getConcept());

		for (int i = 1; i < mcdsUnion.size() + 1; i++) {
			if (isExpVerbose)
				System.out.println("\t  P set SIZE " + i + "    at " + df_day.format(new Date()));
			Toolbox.INSTANCE.logThis("", resultFolder, new String("\t  P set SIZE " + i));

			PowerSet sigPowerSet = new PowerSet(mcdsUnion, i, i);
			Iterator candidateSigIt = sigPowerSet.iterator();
			while (candidateSigIt.hasNext()) {
				Set<OWLEntity> mus = (Set<OWLEntity>) candidateSigIt.next();
				mus.addAll(tboxSigma);

				impDefCheckCount++;
				if (impDef.isConceptImplicitlyDefined(definedConcept.getConcept(), mus)) {
					// System.out.println("\t   There is a new CDS");
					Set<OWLEntity> newMcds = this.computeSingleMCDS_OLD(
							definedConcept.getConcept(), mus);


					if (!allExistingMcds.contains(newMcds) && !newlyFoundMCDSs.contains(newMcds)) {
						if (isExpVerbose)
							System.out.println("\t     NEW MCDS found = " + newMcds + "    at "
									+ df_day.format(new Date()));
						Toolbox.INSTANCE.logThis("", resultFolder, new String(" NEW MCDS found = "
								+ newMcds));
						newlyFoundMCDSs.add(newMcds);
					}
				}
			}
		}
		return newlyFoundMCDSs;
	}

	@SuppressWarnings("unused")
	private HashMap<OWLClass, Vector<Set<OWLEntity>>> searchForNewMCDSinAllSoFarFoundMCDS() {
		HashMap<OWLClass, Vector<Set<OWLEntity>>> newMCDSs = new HashMap<OWLClass, Vector<Set<OWLEntity>>>();
		Vector<DefinedConcept> defCons = getDefinedConcepts();

		for (DefinedConcept defCon : defCons) {
			System.out.println("\n\n((" + defCon.getConcept() + "))   MCDSs = "
					+ defCon.getMcdsAllDifferentCount());

			Set<Set<OWLEntity>> newlyFoundMCDSs = null; // computeNewMCDSs(defCon,
														// defCon.getMcdsAllDifferent());
			if (newlyFoundMCDSs != null) {
				if (newlyFoundMCDSs.size() > 0) {
					Vector<Set<OWLEntity>> newlyFoundMCDSsOrdered = Toolbox.INSTANCE
							.sortUnorderedEntitySetsbySize(newlyFoundMCDSs);
					// defCon.setMcdsNewFromPsetOfExisting(newlyFoundMCDSsOrdered);
					newMCDSs.put(defCon.getConcept(), newlyFoundMCDSsOrdered);
				}
			}
		}

		//
		saveMCDSs(false, newMCDSs, TYPE_PSX);
		return newMCDSs;
	}
}
