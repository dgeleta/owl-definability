package owldefinability.definability;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.TimeOutException;

import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;
import owldefinability.general.Toolbox;

/**
 * Establish concept Definability status (i.e. defined explicitly /
 * defined implictly, or undefined)
 */
public class Computer_DefinedConcept {

	// * * * VARS : INPUT * * *
	private OWLOntology ont;
	private File ontFile;
	private String datasetId;

	// * * * Internal & Settings * * *
	private boolean isVerbose = false;
	private boolean hasResumed = false;
	private String resonerType = "Pellet";
	private long reasonerTimeout = 60000;
	private long startTimeConcept, endTimeConcept;
	private OWLDataFactory df = OWLManager.getOWLDataFactory();

	// * * * VARS : PROCESSED * * *
	private int impDefCheckCount = 0;
	private Set<OWLEntity> tboxSignature;
	private StringBuilder intermedResult = new StringBuilder();
	private Set<OWLClass> computedConcepts = new HashSet<OWLClass>();
	private HashMap<OWLClass, ConceptDefinabilityType> definedConceptsByType = new HashMap<OWLClass, ConceptDefinabilityType>();
	private HashMap<OWLClass, Number> definedConceptsComputationTime = new HashMap<OWLClass, Number>();
	private Vector<DefinedConcept> definedConcepts;



	// * * * CONSTRUCTOR * * * 
	/**
	 * Establish concept Definability status (i.e. defined explicitly /
	 * defined implictly, or undefined)
	 * 
	 * @param datasetId Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param ont The pre-loaded OWLOntology
	 * @param ontFile The ontology as File
	 * @param resonerType Reasoner type {Pellet, Hermit}
	 * @param reasonerTimeout Reasoner timeout in milliseconds
	 * @param isVerbose Show internal processing 
	 */
	public Computer_DefinedConcept(String datasetId, OWLOntology ont, File ontFile,
			String resonerType, long reasonerTimeout, boolean isVerbose) {
		this.datasetId = datasetId;
		this.ont = ont;
		this.ontFile = ontFile;
		this.resonerType = resonerType;
		this.reasonerTimeout = reasonerTimeout;
		this.isVerbose = isVerbose;
		Toolbox.INSTANCE.createExperimentFolderStructure(datasetId);
	}



	// * * * MAIN * * *
	/**
	 * @return Dictionary of defined concepts and their definability type, excluding undefined concepts
	 */
	public HashMap<OWLClass, ConceptDefinabilityType> getDefinedConceptListByType() {
		tboxSignature = Toolbox.INSTANCE.getTBoxSignature(this.ont);
		HashMap<OWLClass, ConceptDefinabilityType> definedConceptsByTypeResult = getDefinedConceptsWithDefinabilityType();
		return definedConceptsByTypeResult;
	}



	// * * * C_def * * *
	private HashMap<OWLClass, ConceptDefinabilityType> getDefinedConceptsWithDefinabilityType() {

		// RESUME ?
		hasResumed = resumeWithType(ontFile.getName());
		if (hasResumed) {
			if (isVerbose) {
				System.out.println("   #Concepts TOTAL = " + ont.getClassesInSignature().size());
				System.out.println("   #Concepts DONE  = " + computedConcepts.size());
				System.out.println("   #Concepts REMAINING = "
						+ (ont.getClassesInSignature().size() - computedConcepts.size()) + "\n\n");
			}
		}

		HashMap<OWLClass, ConceptDefinabilityType> loadedDefinedConceptsByType = loadDefinableConceptsWithType(ontFile
				.getName());
		if (loadedDefinedConceptsByType != null)
			definedConceptsByType = loadedDefinedConceptsByType;

		// COMPUTE
		if (loadedDefinedConceptsByType == null) {

			Vector<OWLClass> concepts = new Vector<OWLClass>();
			concepts.addAll(ont.getClassesInSignature());
			concepts.remove(df.getOWLThing());
			concepts.remove(df.getOWLNothing());
			if (hasResumed)
				concepts.removeAll(computedConcepts);
			concepts = Toolbox.INSTANCE.sortConceptsAlphabetcially(concepts);

			int count = 0;
			int total = concepts.size();

			for (OWLClass thisCon : concepts) {
				startTimeConcept = System.nanoTime();
				ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker(ontFile,
						datasetId, resonerType, reasonerTimeout, false);

				// test empty signature for errorous Concepts
				ConceptDefinabilityType defType = null;
				if (impDef.isConceptImplicitlyDefinedByEmptySignature(thisCon)) {
					defType = ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA;
				} else {
					Set<OWLEntity> signature = new HashSet<OWLEntity>();

					// signature.addAll(ont.getSignature());
					signature.addAll(tboxSignature);
					signature.remove(df.getOWLThing());
					signature.remove(df.getOWLNothing());
					signature.remove(thisCon);

					try {
						impDefCheckCount++;
						defType = impDef.getConceptDefinability((OWLClass) thisCon, signature);
					} catch (TimeOutException tex) {
						// System.exit(0);
					}

					// couldn't compute due to reasoner timeout, skip ontology
					if (defType == null) {
						return null;
					}
				}

				if (defType != ConceptDefinabilityType.UNKNOWN
						&& defType != ConceptDefinabilityType.UNDEFINED) {
					definedConceptsByType.put((OWLClass) thisCon, defType);
				}

				intermedResult.append(thisCon.getIRI().toString() + " " + defType + "\n");
				recordIntermediateResult(intermedResult.toString());

				endTimeConcept = System.nanoTime();
				Number timeTaken = endTimeConcept - startTimeConcept;
				System.out.println("   (" + thisCon.getIRI().getShortForm() + ") " + defType
						+ "   (" + ++count + " of " + total + ") in " + timeTaken + " nanosec");

				definedConceptsComputationTime.put(thisCon, timeTaken);
			}

			// FINISHED
			recordDefinableConceptsWithType(definedConceptsByType, ontFile.getName());

			Path intermedFilePath = Paths.get(Toolbox.INSTANCE.getPathCdef(datasetId)
					+ "/DefinableConceptList_INPROGRESS_" + ontFile.getName() + ".txt");
			try {
				Files.delete(intermedFilePath);
			} catch (NoSuchFileException x) {
				System.err.format("%s: no such" + " file or directory%n", intermedFilePath);
			} catch (DirectoryNotEmptyException x) {
				System.err.format("%s not empty%n", intermedFilePath);
			} catch (IOException x) {
				// File permission problems are caught here.
				System.err.println(x);
			}
		}
		return definedConceptsByType;
	}



	// * * * LOAD & RESUME * * *
	private HashMap<OWLClass, ConceptDefinabilityType> loadDefinableConceptsWithType(
			String ontologyName) {
		HashMap<OWLClass, ConceptDefinabilityType> concepts = new HashMap<OWLClass, ConceptDefinabilityType>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new String(
					Toolbox.INSTANCE.getPathCdef(datasetId) + "/DefinableConceptList_"
							+ ontologyName + ".txt")));
		} catch (FileNotFoundException e1) {
			// e1.printStackTrace();
			return null;
		}
		try {
			String line = br.readLine();

			while (line != null) {
				String[] cNameType = line.split(" ");
				IRI clsIRI = IRI.create(cNameType[0]);
				OWLClass cName = getClassEntity(clsIRI);
				ConceptDefinabilityType ctype = getConceptDefinabilityTypeFromString(cNameType[1]);
				concepts.put(cName, ctype);

				if (isVerbose && line != null) {
					System.out.println("   " + line);
				}
				line = br.readLine();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return concepts;
	}

	private boolean resumeWithType(String ontologyName) {
		// System.out.println("resumeWithType " + ontologyName);

		HashMap<OWLClass, ConceptDefinabilityType> concepts = new HashMap<OWLClass, ConceptDefinabilityType>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new String(
					Toolbox.INSTANCE.getPathCdef(datasetId) + "/DefinableConceptList_INPROGRESS_"
							+ ontologyName + ".txt")));
		} catch (FileNotFoundException e1) {
			// System.out.println("FileNotFoundException " + e1.getMessage());
			return false;
		}
		try {
			String line = br.readLine();
			while (line != null) {
				String[] cNameType = line.split(" ");
				IRI clsIRI = IRI.create(cNameType[0]);
				OWLClass cName = getClassEntity(clsIRI);
				computedConcepts.add(cName);
				ConceptDefinabilityType ctype = getConceptDefinabilityTypeFromString(cNameType[1]);
				if (ctype != ConceptDefinabilityType.UNDEFINED) {
					concepts.put(cName, ctype);
				}
				intermedResult.append(cName.getIRI().toString() + " " + ctype.toString() + "\n");
				line = br.readLine();
			}
		} catch (Exception ex) {
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		definedConceptsByType = concepts;
		return true;
	}

	Vector<DefinedConcept> getDefinedConcepts() {
		definedConcepts = new Vector<DefinedConcept>();
		for (OWLClass cls : definedConceptsByType.keySet()) {
			DefinedConcept cdef = new DefinedConcept(cls, definedConceptsByType.get(cls), ont
					.getOntologyID().getOntologyIRI());
			definedConcepts.add(cdef);
		}
		return definedConcepts;
	}



	// * * * RECORD * * *
	public void recordDefinableConceptsWithType(
			HashMap<OWLClass, ConceptDefinabilityType> defConceptsByType, String ontologyName) {
		StringBuilder conceptNames = new StringBuilder();

		Vector<OWLClass> clsList = new Vector<OWLClass>();
		clsList.addAll(defConceptsByType.keySet());

		try {
			clsList = Toolbox.INSTANCE.sortConceptsAlphabetcially(clsList);
		} catch (Exception e) {
		}

		for (OWLClass cls : clsList) {
			conceptNames.append(cls.getIRI() + " " + defConceptsByType.get(cls) + "\n");
		}
		Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.getPathCdef(datasetId)
				+ "/DefinableConceptList_" + ontologyName + ".txt"), conceptNames.toString());
		// times
		StringBuilder sbtime = new StringBuilder();
		sbtime.append("CONCEPT_NAME, Definability Status Computation Time (nanoseconds)\n");
		for (OWLClass cls : definedConceptsComputationTime.keySet()) {
			sbtime.append(cls.getIRI().toString() + ", " + definedConceptsComputationTime.get(cls)
					+ "\n");
		}
		Toolbox.INSTANCE.writeToFile(new String("_experimentComputations/" + datasetId
				+ "/_info/_times/TimeTakenPerConcept_cDef_" + ontFile.getName() + ".csv"),
				sbtime.toString());
	}



	// * * * HELPERS * * *
	private void recordIntermediateResult(String result) {
		Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.getPathCdef(datasetId)
				+ "/DefinableConceptList_" + "INPROGRESS_" + ontFile.getName() + ".txt"), result);
	}

	private ConceptDefinabilityType getConceptDefinabilityTypeFromString(String typeStr) {
		if (typeStr.equals(ConceptDefinabilityType.EXPLICIT.toString())) {
			return ConceptDefinabilityType.EXPLICIT;
		} else if (typeStr.equals(ConceptDefinabilityType.EXPLICIT_CYCLIC.toString())) {
			return ConceptDefinabilityType.EXPLICIT_CYCLIC;
		} else if (typeStr.equals(ConceptDefinabilityType.IMPLICIT.toString())) {
			return ConceptDefinabilityType.IMPLICIT;
		} else if (typeStr.equals(ConceptDefinabilityType.UNDEFINED.toString())) {
			return ConceptDefinabilityType.UNDEFINED;
		} else if (typeStr.equals(ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA.toString())) {
			return ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA;
		}
		return ConceptDefinabilityType.UNKNOWN;
	}

	private OWLClass getClassEntity(IRI entityName) {
		for (OWLEntity ent : ont.getSignature()) {
			if (ent.getIRI().equals(entityName)) {
				if (ent.isOWLClass() == true) {
					return (OWLClass) ent;
				}
			}
		}
		return null;
	}

	public int getImpDefCheckCount() {
		return impDefCheckCount;
	}
}
