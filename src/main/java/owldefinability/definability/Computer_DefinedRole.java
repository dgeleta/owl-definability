package owldefinability.definability;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;

import owldefinability.definability.DefinedRole.RoleDefinabilityType;
import owldefinability.general.Toolbox;


/**
 * Establish Role definability status (i.e. defined explicitly / defined
 * implictly, or undefined). Compute disjoint, or all Minimal Definition
 * Signatures of Roles.
 */
// @SuppressWarnings("unused")
public class Computer_DefinedRole {

	// * * * INPUT * * *
	private OWLOntology ont;
	private File ontFile;
	private String datasetId;

	// * * * Internal & Settings * * *
	private boolean isVerbose;
	private String resonerType;
	private long reasonerTimout;
	private Vector<OWLEntity> computed = new Vector<OWLEntity>();
	private final SimpleDateFormat df_day = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public int impDefCheckCount = 0;


	// * * * CONSTRUCTOR * * *
	/**
	 * Establish Role definability status (i.e. defined explicitly / defined
	 * implictly, or undefined). Compute disjoint, or all Minimal Definition
	 * Signatures of Roles.
	 * 
	 * @param datasetId
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param resonerType
	 *            Reasoner type {Pellet, Hermit}
	 * @param reasonerTimout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 * @param ont
	 *            The pre-loaded OWLOntology
	 * @param ontFile
	 *            The ontology as File
	 */
	public Computer_DefinedRole(String datasetId, String resonerType, long reasonerTimout,
			boolean isVerbose, OWLOntology ont, File ontFile) {
		this.datasetId = datasetId;
		this.resonerType = resonerType;
		this.reasonerTimout = reasonerTimout;
		this.isVerbose = isVerbose;
		this.ont = ont;
		this.ontFile = ontFile;
	}



	// * * * Role Definability * * *
	public HashMap<OWLEntity, RoleDefinabilityType> getRoleDefinability() {
		HashMap<OWLEntity, RoleDefinabilityType> roleDef = null;

		// attempt to load completed
		roleDef = loadRoleDefResults(false);

		// attempt to load inprogress
		if (roleDef == null) {
			roleDef = loadRoleDefResults(true);
			if (roleDef == null)
				roleDef = new HashMap<OWLEntity, RoleDefinabilityType>();


			Vector<OWLEntity> signatureRoles = new Vector<OWLEntity>();

			// Object properties
			signatureRoles.addAll(ont.getObjectPropertiesInSignature());
			signatureRoles.removeAll(computed);
			signatureRoles = Toolbox.INSTANCE.sortEntitiesAlphabetcially(signatureRoles);
			int roleCount = 0;

			for (OWLEntity role : signatureRoles) {
				System.out.println("\n(((" + role.getIRI().getShortForm() + "| " + ++roleCount
						+ " of " + signatureRoles.size() + "))) ObjProp at "
						+ df_day.format(new Date()));

				RoleDefinabilityType thisDef = isRoleDefined(role);
				roleDef.put(role, thisDef);
				saveRoleDefResults(true, roleDef);
				System.out.println("\t" + thisDef);
			}

			// Datatype properties
			roleCount = 0;
			signatureRoles.removeAllElements();
			signatureRoles.addAll(ont.getDataPropertiesInSignature());
			signatureRoles.removeAll(computed);
			signatureRoles = Toolbox.INSTANCE.sortEntitiesAlphabetcially(signatureRoles);
			for (OWLEntity role : signatureRoles) {
				System.out.println("\n(((" + role.getIRI().getShortForm() + "| " + ++roleCount
						+ " of " + signatureRoles.size() + "))) DataProp at "
						+ df_day.format(new Date()));

				RoleDefinabilityType thisDef = isRoleDefined(role);
				roleDef.put(role, thisDef);
				saveRoleDefResults(true, roleDef);
				System.out.println("\t" + thisDef);
			}
			saveRoleDefResults(false, roleDef);
		}
		return roleDef;
	}

	public HashMap<OWLEntity, RoleDefinabilityType> getOnlyDefinedRoles() {
		HashMap<OWLEntity, RoleDefinabilityType> roleDef = getRoleDefinability();
		HashMap<OWLEntity, RoleDefinabilityType> roleDefOnlydefined = new HashMap<OWLEntity, RoleDefinabilityType>();
		for (OWLEntity role : roleDef.keySet()) {
			if (!roleDef.get(role).equals(RoleDefinabilityType.UNDEFINED)) {
				roleDefOnlydefined.put(role, roleDef.get(role));
			}
		}
		return roleDefOnlydefined;
	}

	private RoleDefinabilityType isRoleDefined(OWLEntity role) {
		impDefCheckCount++;

		Set<OWLEntity> sigma = ont.getSignature();
		sigma.removeAll(ont.getClassesInSignature(true));
		sigma.remove(role);

		boolean explicitlyDefined = new ImplicitDefinablityChecker(ontFile, datasetId, resonerType,
				reasonerTimout, true).isRoleExplicitlyDefined(role, sigma);
		if (!explicitlyDefined) {
			boolean impliciltyDefined = new ImplicitDefinablityChecker(ontFile, datasetId,
					resonerType, reasonerTimout, true).isRoleImplicitlyDefined(role, sigma);
			if (impliciltyDefined == true) {
				return RoleDefinabilityType.IMPLICITLY;
			} else {
				return RoleDefinabilityType.UNDEFINED;
			}
		} else {
			return RoleDefinabilityType.EXPLICITLY;
		}
	}



	// * * * Role MDS * * *
	/**
	 * Compute disjoint MDSs of all defined roles (unordered)
	 * 
	 * @return MDSs
	 */
	public HashMap<OWLEntity, HashSet<Set<OWLEntity>>> getRoleMDSs() {
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> roleMDSs = null;
		HashMap<OWLEntity, RoleDefinabilityType> defroles = getOnlyDefinedRoles();
		int roleCount = 0;

		// try to load
		boolean isComplete = true;
		roleMDSs = loadRoleMDSs(true);
		if (roleMDSs == null) {
			roleMDSs = loadRoleMDSs(false);
			if (roleMDSs == null) {
				if (isVerbose)
					System.out.println("   START Computation " + df_day.format(new Date()));
				isComplete = false;
				roleMDSs = new HashMap<OWLEntity, HashSet<Set<OWLEntity>>>();
			} else {
				if (isVerbose)
					System.out.println("   LOADED Precomputed" + df_day.format(new Date()));
			}
		} else {
			isComplete = false;
			if (isVerbose)
				System.out.println("   RESUME Computation" + df_day.format(new Date())
						+ "\n\tDONE = " + roleMDSs.keySet());
		}

		// compute
		if (!isComplete) {
			Vector<OWLEntity> remainingRoles = new Vector<OWLEntity>();
			remainingRoles.addAll(defroles.keySet());
			remainingRoles.removeAll(roleMDSs.keySet());
			for (OWLEntity definedRole : remainingRoles) {
				String type = "OBJ_PROP";
				if (!definedRole.isOWLObjectProperty())
					type = "DATA_PROP";

				if (isVerbose) {
					System.out.println("\n   Start (((" + definedRole.getIRI().getShortForm()
							+ "| " + ++roleCount + " of " + remainingRoles.size() + "))) : " + type
							+ "   at " + df_day.format(new Date()));
				}

				HashSet<Set<OWLEntity>> thisRoleMcds = getRoleMDSs(definedRole);
				if (thisRoleMcds != null) {
					roleMDSs.put(definedRole, thisRoleMcds);
					saveRoleMDSs(true, roleMDSs);
				}
			}
			saveRoleMDSs(false, roleMDSs);
		}
		return roleMDSs;
	}

	/**
	 * Compute disjoint MDSs of all defined roles (ordered)
	 * 
	 * @return MDSs
	 */
	public HashMap<OWLEntity, Vector<Set<OWLEntity>>> getRoleMDSs2() {
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> input = getRoleMDSs();
		HashMap<OWLEntity, Vector<Set<OWLEntity>>> res = new HashMap<OWLEntity, Vector<Set<OWLEntity>>>();
		for (OWLEntity ent : input.keySet()) {
			Vector<Set<OWLEntity>> mdss = new Vector<Set<OWLEntity>>();
			mdss.addAll(input.get(ent));
			res.put(ent, mdss);
		}
		return res;
	}

	HashSet<Set<OWLEntity>> getRoleMDSs(OWLEntity role) {
		HashSet<Set<OWLEntity>> roleMcds = new HashSet<Set<OWLEntity>>();

		// * * * (1) Find Explict * * *
		for (OWLAxiom axx : ont.getReferencingAxioms(role)) {
			if (axx.getSignature().size() == 2) {
				boolean isDefintion = false;
				if (axx.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
						AxiomType.EQUIVALENT_DATA_PROPERTIES)) {
					if (isVerbose)
						System.out.println("\t   EQV " + axx + " " + axx.getAxiomType());
					isDefintion = true;
				} else if (axx.isOfType(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY,
						AxiomType.INVERSE_OBJECT_PROPERTIES)) {
					if (isVerbose)
						System.out.println("\t   INV " + axx + " " + axx.getAxiomType());
					isDefintion = true;
				}

				if (isDefintion) {
					Set<OWLEntity> newMcds = new HashSet<OWLEntity>();
					newMcds.addAll(axx.getSignature());
					newMcds.remove(role);
					if (!newMcds.isEmpty())
						roleMcds.add(newMcds);
				}
			}
		}



		// * * * (2) Compute Disjoint * * *
		Set<OWLEntity> sigma = new HashSet<OWLEntity>();
		if (role.isOWLObjectProperty()) {
			sigma.addAll(ont.getObjectPropertiesInSignature());
		} else {
			sigma.addAll(ont.getDataPropertiesInSignature());
		}
		sigma.remove(role);

		boolean moreRemain = true;
		while (moreRemain) {
			// Signature
			for (Set<OWLEntity> anMcds : roleMcds) {
				sigma.removeAll(anMcds);
			}

			// compute
			impDefCheckCount++;
			if (new ImplicitDefinablityChecker(ontFile, datasetId, resonerType, reasonerTimout,
					true).isRoleImplicitlyDefined(role, sigma)) {

				// find one disjoint signature
				Set<OWLEntity> sigmaCopy = new HashSet<OWLEntity>();
				sigmaCopy.addAll(sigma);
				for (OWLEntity ent : sigma) {
					if (isVerbose)
						System.out.println("\t S \\ {" + ent.getIRI().getShortForm() + "} "
								+ df_day.format(new Date()));
					sigmaCopy.remove(ent);
					impDefCheckCount++;
					if (!new ImplicitDefinablityChecker(ontFile, datasetId, resonerType,
							reasonerTimout, true).isRoleImplicitlyDefined(role, sigmaCopy)) {
						System.out.println("\t S u {" + ent.getIRI().getShortForm() + "} "
								+ df_day.format(new Date()));
						sigmaCopy.add(ent);
					}
				}
				roleMcds.add(sigmaCopy);
			} else {
				moreRemain = false;
			}
		}



		// * * * (3) Compute ALL * * *
		@SuppressWarnings("unused")
		Set<OWLEntity> mrdsUnion = new HashSet<OWLEntity>();
		for (@SuppressWarnings("unused")
		Set<OWLEntity> mrds : roleMcds) {
			if (roleMcds.size() != 1) {
				// mrdsUnion.addAll(c)


			}
		}



		// * * * Done * * *
		if (roleMcds.isEmpty())
			return null;
		if (isVerbose)
			System.out.println("\t MCDSs = " + roleMcds);
		return roleMcds;
	}



	// * * * SAVE & LOAD - rDef * * *
	private void saveRoleDefResults(boolean inProgress,
			HashMap<OWLEntity, RoleDefinabilityType> defRoles) {
		StringBuilder sb = new StringBuilder();

		Vector<OWLEntity> roles = Toolbox.INSTANCE.sortEntitiesAlphabetcially(defRoles.keySet());
		for (OWLEntity prop : roles) {
			sb.append(prop.getIRI() + " " + defRoles.get(prop) + "\n");
		}

		if (inProgress) {
			Toolbox.INSTANCE
					.writeToFile(new String(Toolbox.INSTANCE.getPathRdef(datasetId)
							+ "/DefinableRoleList_INPROGRESS_" + ontFile.getName() + ".txt"),
							sb.toString());
		} else {
			Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.getPathRdef(datasetId)
					+ "/DefinableRoleList_" + ontFile.getName() + ".txt"), sb.toString());
			File old = new File(new String(Toolbox.INSTANCE.getPathRdef(datasetId)
					+ "/DefinableRoleList_INPROGRESS_" + ontFile.getName() + ".txt"));
			old.delete();
		}
	}

	private HashMap<OWLEntity, RoleDefinabilityType> loadRoleDefResults(boolean inProgress) {
		HashMap<OWLEntity, RoleDefinabilityType> roledefs = null;// = new
																	// HashMap<OWLEntity,
																	// RoleDefinabilityType>();

		String filePath = new String(Toolbox.INSTANCE.getPathRdef(datasetId)
				+ "/DefinableRoleList_" + ontFile.getName() + ".txt");
		if (inProgress) {
			filePath = new String(Toolbox.INSTANCE.getPathRdef(datasetId)
					+ "/DefinableRoleList_INPROGRESS_" + ontFile.getName() + ".txt");
		}

		String fileContent = Toolbox.INSTANCE.readFile(filePath);
		if (fileContent != null) {
			roledefs = new HashMap<OWLEntity, RoleDefinabilityType>();

			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(fileContent);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] words = line.split(" ");

				OWLEntity role = Toolbox.INSTANCE.getEntity(IRI.create(words[0]), ont);
				roledefs.put(role, this.getdefType(words[1]));
				computed.addElement(role);
			}
		}
		return roledefs;
	}



	// * * * SAVE & LOAD - MCDSs * * *
	private boolean saveRoleMDSs(boolean inProgress,
			HashMap<OWLEntity, HashSet<Set<OWLEntity>>> defRoles) {

		// (1) order alphabetically
		Vector<OWLEntity> orderedRoleList = new Vector<OWLEntity>();
		orderedRoleList.addAll(defRoles.keySet());
		orderedRoleList = Toolbox.INSTANCE.sortEntitiesAlphabetcially(orderedRoleList);

		// (2) create textual representation
		StringBuilder sb = new StringBuilder();
		for (OWLEntity rl : orderedRoleList) {

			if (defRoles.get(rl) != null) {
				sb.append("R: " + rl.getIRI() + "\n");

				Set<Set<OWLEntity>> clsMCDSs = defRoles.get(rl);
				for (Set<OWLEntity> mcds : clsMCDSs) {
					sb.append("\u03A3:");
					Vector<OWLEntity> orderedSignature = Toolbox.INSTANCE
							.sortEntitiesAlphabetcially(mcds);
					for (OWLEntity sigsymbol : orderedSignature) {
						sb.append(" " + sigsymbol.getIRI());
					}
					sb.append("\n");
				}
			}
		}

		// (3) serialize
		String inProgressStr = "";
		if (inProgress) {
			inProgressStr = "INPROGRESS_";
		}
		String filePath = new String("_experimentComputations/" + datasetId + "/_MRDSs/MRDS_"
				+ inProgressStr + ontFile.getName() + ".txt");

		// (4) delete inprogress
		if (!inProgress) {
			File[] folderfiles = new File(new String("_experimentComputations/" + datasetId
					+ "/_MRDSs")).listFiles();
			for (File f : folderfiles) {
				if (f.getName().startsWith("MRDS_")
						&& f.getName().endsWith(ontFile.getName() + ".txt")
						&& f.getName().contains("INPROGRESS") && !f.isHidden()) {
					// System.out.println("delete " + f);
					f.delete();
				}
			}
		}
		return Toolbox.INSTANCE.writeToFile(filePath, sb.toString());
	}

	private HashMap<OWLEntity, HashSet<Set<OWLEntity>>> loadRoleMDSs(boolean inProgress) {
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> loadedRoleMCDSs = new HashMap<OWLEntity, HashSet<Set<OWLEntity>>>();

		String filePath = new String("_experimentComputations/" + datasetId + "/_MRDSs/MRDS_"
				+ ontFile.getName() + ".txt");
		if (inProgress) {
			filePath = new String("_experimentComputations/" + datasetId
					+ "/_MCDSs_role/MRDS_INPROGRESS_" + ontFile.getName() + ".txt");
		}

		// load file
		if (!new File(filePath).exists()) {
			return null;
		}
		String fileContent = Toolbox.INSTANCE.readFile(filePath);


		// process text into datastructure
		HashSet<Set<OWLEntity>> sigsOfRls = new HashSet<Set<OWLEntity>>();
		Set<OWLEntity> sig = new HashSet<OWLEntity>();
		OWLEntity theRole = null;
		Scanner scanner = new Scanner(fileContent);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			if (line.startsWith("R:")) {
				if (theRole != null) {
					loadedRoleMCDSs.put(theRole, sigsOfRls);
				}
				theRole = Toolbox.INSTANCE.getEntity(IRI.create(line.replace("R: ", "")), ont);
				sigsOfRls = new HashSet<Set<OWLEntity>>();
			} else if (line.startsWith("Σ:")) {
				sig = new HashSet<OWLEntity>();
				String tln = line.replace("Σ: ", "");
				String[] words = tln.split(" ");
				for (String wd : words) {
					sig.add(Toolbox.INSTANCE.getEntity(IRI.create(wd), ont));
				}
				sigsOfRls.add(sig);
			}
		}
		loadedRoleMCDSs.put(theRole, sigsOfRls);
		scanner.close();
		return loadedRoleMCDSs;
	}



	// * * * Defined Roles * * *
	Vector<DefinedRole> getDefinedRoles() {
		Vector<DefinedRole> defRoles = new Vector<DefinedRole>();

		HashMap<OWLEntity, RoleDefinabilityType> defss = this.getOnlyDefinedRoles();
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> mcdsss = this.getRoleMDSs();

		for (OWLEntity defr : defss.keySet()) {
			defRoles.add(new DefinedRole(defr, defss.get(defr), ont.getOntologyID()
					.getOntologyIRI(), mcdsss.get(defr)));
		}
		defRoles = Toolbox.INSTANCE.orderRolesAlphabetically(defRoles);
		return defRoles;
	}



	// * * * Helper(s) * * *
	private RoleDefinabilityType getdefType(String deftype) {
		if (deftype.toString().equals(RoleDefinabilityType.EXPLICITLY.toString())) {
			return RoleDefinabilityType.EXPLICITLY;
		} else if (deftype.toString().equals(RoleDefinabilityType.IMPLICITLY.toString())) {
			return RoleDefinabilityType.IMPLICITLY;
		} else if (deftype.toString().equals(RoleDefinabilityType.UNDEFINED.toString())) {
			return RoleDefinabilityType.UNDEFINED;
		}
		return RoleDefinabilityType.UNKNOWN;
	}

}
