package owldefinability.definability;

import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom; 
import org.semanticweb.owlapi.model.OWLEntity;

import owldefinability.definability.Classifier_DefinitonPattern.MRDS_pattern;

/**
 * Minimal Role Definition Signature
 */
public class MRDS {	
	
	private OWLEntity definedRole;
	private MRDS_pattern pattern;
	private Set<OWLAxiom> justification;
	private Set<OWLEntity> signature;
	
	/**
	 * Minimal Role Definition Signature
	 * 
	 * @param entity Defined Role
	 * @param signature Minimal Role Definition Signature
	 * @param pattern Defintion Pattern Type
	 * @param justification The justication which entails the definition
	 */
	public MRDS(OWLEntity entity, Set<OWLEntity> signature, MRDS_pattern pattern, Set<OWLAxiom> justification) {
		this.definedRole = entity;
		this.pattern = pattern;
		this.justification = justification;
		this.signature = signature;
	}	 

	public MRDS_pattern getPattern() {
		return pattern;
	}

	public OWLEntity getRole() {
		return definedRole;
	}

	public Set<OWLAxiom> getJustification() {
		return justification;
	}

	public Set<OWLEntity> getSignature() {
		return signature;
	} 
}
