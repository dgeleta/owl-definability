package owldefinability.definability;

import java.io.File;

import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.ToStringRenderer;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import owldefinability.definability.Classifier_DefinitonPattern.MCDS_pattern;
import owldefinability.definability.Classifier_DefinitonPattern.MRDS_pattern;
import owldefinability.general.Toolbox;
import uk.ac.manchester.cs.owlapi.dlsyntax.DLSyntaxObjectRenderer;

@SuppressWarnings("unused")

/**
 * Compute Definition Axioms for identifiable Definition Patterns (Rule-based
 * Rewriting)
 */
public class DefinitionPatternBasedRewriter {

	// * * * Internal & Settings * * *
	private OWLOntology ont;
	private File ontFile;
	private String datasetId;
	private boolean isVerbose; 
	private OWLReasoner reasoner;
	private OWLDataFactory df = OWLManager.getOWLDataFactory();



	// * * * CONSTRUCTOR * * *
	/**
	 * Compute Definition Axioms for identifiable Definition Patterns
	 * (Rule-based Rewriting)
	 * 
	 * @param ont
	 *            The pre-loaded OWLOntology
	 * @param ontFile
	 *            The ontology as File
	 * @param reasonerType
	 *            Reasoner type {Pellet, Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param datasetId
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param isVerbose
	 *            Show internal processing
	 */
	public DefinitionPatternBasedRewriter(String datasetId, String reasonerType, long reasonerTimeout,
			boolean isVerbose, OWLOntology ont, File ontFile) {
		this.datasetId = datasetId; 
		this.isVerbose = isVerbose;
		this.ont = ont;
		this.ontFile = ontFile;
		reasoner = Toolbox.INSTANCE.getReasoner(reasonerType, ont, reasonerTimeout);
	}



	// * * * Public * * *
	/**
	 * Compute Definition Axioms for an MDS with an identifiable Definition
	 * Pattern
	 * 
	 * @param mds
	 *            MDS with pattern classificiation and justification
	 * @return Concept / Role equivalence definition axiom
	 */
	public OWLAxiom generateDefinitionAxiom(Object mds) {

		isVerbose = true;

		if (mds.getClass().equals(MCDS.class)) {
			if (isVerbose)
				System.out.println("\n   Generate CONCEPT definition axiom");
			return rewriteConcept((MCDS) mds);
		} else if (mds.getClass().equals(MRDS.class)) {
			if (isVerbose)
				System.out.println("\n   Generate ROLE definition axiom");
			return rewriteRole((MRDS) mds);
		}
		return null;
	}

 

	// * * * REWRITE Entity * * *
	private OWLAxiom rewriteConcept(MCDS mds) {
		if (isVerbose) {
			System.out.println("\t C = " + mds.getConcept());
			System.out.println("\t Σ = " + mds.getSignature());
			System.out.println("\t J = " + mds.getJustification());
			System.out.println("\t " + mds.getPattern() + " Pattern");
		}

		// EXPLICIT_DEF, EXPLICIT_SYNONYM
		if (mds.getPattern().equals(MCDS_pattern.EXPLICIT_DEF)
				|| mds.getPattern().equals(MCDS_pattern.EXPLICIT_SYNONYM)) {
			// CE axiom in Justification
			return mds.getJustification().iterator().next();
		}

		// IMPLICIT_SYNONYM
		else if (mds.getPattern().equals(MCDS_pattern.IMPLICIT_SYNONYM)) {
			HashSet<OWLClassExpression> cls = new HashSet<OWLClassExpression>();
			cls.add(mds.getConcept());
			cls.add((OWLClassExpression) mds.getSignature().iterator().next());
			return df.getOWLEquivalentClassesAxiom(cls);
		}

		// DISJOINT_UNION
		else if (mds.getPattern().equals(MCDS_pattern.DISJOINT_UNION)) {

			// find original LHS concept
			OWLClass originalLHS = null;
			for (OWLEntity ent : mds.getSignature()) {
				if (ent.isOWLClass()) {
					HashSet<OWLClassExpression> cls = new HashSet<OWLClassExpression>();
					cls.add(mds.getConcept());
					cls.add(ent.asOWLClass());
					OWLAxiom disjointness = df.getOWLDisjointClassesAxiom(cls);

					if (!reasoner.isEntailed(disjointness)) {
						originalLHS = (OWLClass) ent;
					}
				}
			}

			// disjoint classes
			HashSet<OWLClassExpression> cls_uni = new HashSet<OWLClassExpression>();
			for (OWLEntity ent : mds.getSignature()) {
				if (ent != originalLHS) {
					System.out.println("ent " + ent + "    " + originalLHS);
					
					cls_uni.add((OWLClassExpression) ent);
				}
			}

			// intersection of superclass
			HashSet<OWLClassExpression> cls_int = new HashSet<OWLClassExpression>();
			cls_int.add(originalLHS);
			cls_int.add(df.getOWLObjectComplementOf(df.getOWLObjectUnionOf(cls_uni)));
			OWLObjectIntersectionOf intersect = df.getOWLObjectIntersectionOf(cls_int);

			// eqv axiom
			HashSet<OWLClassExpression> cls_eqv = new HashSet<OWLClassExpression>();
			cls_eqv.add(mds.getConcept());
			cls_eqv.add(intersect);
			return df.getOWLEquivalentClassesAxiom(cls_eqv);
		}

		// OBJPROP_DOMAIN
		else if (mds.getPattern().equals(MCDS_pattern.OBJPROP_DOMAIN)) {
			HashSet<OWLClassExpression> cls = new HashSet<OWLClassExpression>();
			cls.add(mds.getConcept());
			cls.add(df.getOWLObjectSomeValuesFrom((OWLObjectPropertyExpression) mds.getSignature()
					.iterator().next(), df.getOWLThing()));
			return df.getOWLEquivalentClassesAxiom(cls);
		}

		// OBJPROP_RANGE
		else if (mds.getPattern().equals(MCDS_pattern.OBJPROP_RANGE)) {
			HashSet<OWLClassExpression> cls = new HashSet<OWLClassExpression>();
			cls.add(mds.getConcept());
			OWLObjectProperty objprop = (OWLObjectProperty) mds.getSignature().iterator().next();
			cls.add(df.getOWLObjectSomeValuesFrom(df.getOWLObjectInverseOf(objprop),
					df.getOWLThing()));
			return df.getOWLEquivalentClassesAxiom(cls);
		}


		// OBJPROP_DOMAIN_INVERSE
		else if (mds.getPattern().equals(MCDS_pattern.OBJPROP_DOMAIN_INVERSE)) {
			// TODO
		}

		// OBJPROP_RANGE_INVERSE
		else if (mds.getPattern().equals(MCDS_pattern.OBJPROP_RANGE_INVERSE)) {
			// TODO
		}

		// DATPROP_DOMAIN
		else if (mds.getPattern().equals(MCDS_pattern.DATPROP_DOMAIN)) {
			// TODO
		}

		// EXPLICITLY_WITH_INVERSE_ROLE
		else if (mds.getPattern().equals(MCDS_pattern.EXPLICITLY_WITH_INVERSE_ROLE)) {
			// TODO
		}

		// EXPLICITLY_WITH_SYNONYM_ROLE
		else if (mds.getPattern().equals(MCDS_pattern.EXPLICITLY_WITH_SYNONYM_ROLE)) {
			// TODO
		}
		return null;
	}

	private OWLAxiom rewriteRole(MRDS mds) {
		if (isVerbose) {
			System.out.println("\t R = " + mds.getRole());
			System.out.println("\t Σ = " + mds.getSignature());
			System.out.println("\t J = " + mds.getJustification());
			System.out.println("\t " + mds.getPattern() + " Pattern");
		}

		// EXPLICIT_DEF
		if (mds.getPattern().equals(MRDS_pattern.EXPLICIT_DEF)) {
			// CE axiom in Justification
			return mds.getJustification().iterator().next();
		}

		// EXPLICIT_SYNONYM
		else if (mds.getPattern().equals(MRDS_pattern.EXPLICIT_SYNONYM)) {
			if (mds.getRole().isOWLObjectProperty()) {
				Set<OWLObjectPropertyExpression> roles = new HashSet<OWLObjectPropertyExpression>();
				roles.add((OWLObjectPropertyExpression) mds.getRole());
				roles.add((OWLObjectPropertyExpression) mds.getSignature().iterator().next());
				return df.getOWLEquivalentObjectPropertiesAxiom(roles);
			} else {
				Set<OWLDataPropertyExpression> roles = new HashSet<OWLDataPropertyExpression>();
				roles.add((OWLDataPropertyExpression) mds.getRole());
				roles.add((OWLDataPropertyExpression) mds.getSignature().iterator().next());
				return df.getOWLEquivalentDataPropertiesAxiom(roles);
			}
		}

		// IMPLICIT_SYNONYM
		else if (mds.getPattern().equals(MRDS_pattern.IMPLICIT_SYNONYM)) {
			if (mds.getRole().isOWLObjectProperty()) {
				Set<OWLObjectPropertyExpression> roles = new HashSet<OWLObjectPropertyExpression>();
				roles.add((OWLObjectPropertyExpression) mds.getRole());
				roles.add((OWLObjectPropertyExpression) mds.getSignature().iterator().next());
				return df.getOWLEquivalentObjectPropertiesAxiom(roles);
			} else {
				Set<OWLDataPropertyExpression> roles = new HashSet<OWLDataPropertyExpression>();
				roles.add((OWLDataPropertyExpression) mds.getRole());
				roles.add((OWLDataPropertyExpression) mds.getSignature().iterator().next());
				return df.getOWLEquivalentDataPropertiesAxiom(roles);
			}
		}

		// EXPLICIT_INVERSE, IMPLICIT_INVERSE
		else if (mds.getPattern().equals(MRDS_pattern.EXPLICIT_INVERSE)
				|| mds.getPattern().equals(MRDS_pattern.IMPLICIT_INVERSE)) {
			if (mds.getRole().isOWLObjectProperty()) {
				Set<OWLObjectPropertyExpression> roles = new HashSet<OWLObjectPropertyExpression>();
				roles.add((OWLObjectPropertyExpression) mds.getRole());
				OWLObjectProperty invObjProp = (OWLObjectProperty) mds.getSignature().iterator()
						.next();
				roles.add(df.getOWLObjectInverseOf(invObjProp));
				return df.getOWLEquivalentObjectPropertiesAxiom(roles);
			}
		}
		return null;
	}



	// * * * HELPERS * * *
	private OWLClass getLHSconcept(OWLEquivalentClassesAxiom axx) {
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		String[] words = axx.toString().split(" ");
		String conceptStr = words[0];
		// System.out.println(" LOOKING FOR '" + conceptStr + "' !!! ");

		for (OWLEntity ent : axx.getSignature()) {
			if (ent.getIRI().getShortForm().toString().equals(conceptStr)) {
				return (OWLClass) ent;
			}
		}
		return null;
	}


}
