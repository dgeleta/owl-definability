package owldefinability.definability;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass; 
import org.semanticweb.owlapi.model.OWLEntity;

import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;
import owldefinability.general.Toolbox;

public class DefinedConcept {
	
	private OWLClass concept;
	private IRI ontologyIRI;
	private ConceptDefinabilityType conceptDefinabilityType;
	
	private Vector<Set<OWLEntity>> mcdsDisjoint; 
	private Vector<Set<OWLEntity>> mcdsJustifications; 
	private Vector<Set<OWLEntity>> mcdsExpansions;
	
	public Vector<MCDS> mcdss = new Vector<MCDS>();
	
	private boolean isMcdsComplete = false;

	
	// * * * Constructor * * *
 	public DefinedConcept(OWLClass cls, ConceptDefinabilityType conceptDefinabilityType, IRI ontologyIRI) {
		this.concept = cls;
		this.conceptDefinabilityType = conceptDefinabilityType;
		this.ontologyIRI = ontologyIRI;
	}



	// * * * Accessors * * *
	public OWLClass getConcept() {
		return concept;
	}

	public ConceptDefinabilityType getDefinabilityType() {
		return conceptDefinabilityType;
	}
	
	public IRI getOntologyIRI() {
		return ontologyIRI;
	} 

	public void setOntologyIRI(IRI ontologyIRI) {
		this.ontologyIRI = ontologyIRI;
	}

	
	public Vector<Set<OWLEntity>> getMcdsDisjoint() {
		return mcdsDisjoint;
	}
	 	
	Vector<Set<OWLEntity>> getMcdsJustifications() {
		return mcdsJustifications;
	}
		
	public Vector<Set<OWLEntity>> getMcdsNewFromPsetOfExisting() {
		return mcdsExpansions;
	}
	
	public Vector<Set<OWLEntity>> getMcdsAllDifferent() {
		Set<Set<OWLEntity>> mcdsAllDifferent = new HashSet<Set<OWLEntity>>();
		
		if (mcdsDisjoint != null) {
			mcdsAllDifferent.addAll(mcdsDisjoint);
		} 
		/*
		if (mcdsJustifications != null) {
			mcdsAllDifferent.addAll(mcdsJustifications);
		} 
		*/
		if (mcdsExpansions != null) {
			mcdsAllDifferent.addAll(mcdsExpansions);
		}
		
		Vector<Set<OWLEntity>> mcdsAllDifferentOrdered = Toolbox.INSTANCE.sortUnorderedEntitySetsbySize(mcdsAllDifferent);
		return mcdsAllDifferentOrdered;
	}
	
	
	public int getMcdsDisjointCount() {
		if (mcdsDisjoint != null)
		return mcdsDisjoint.size();
		else return -1;
	}
		
	public int getMcdsJustificationsCount() {
		if (mcdsJustifications != null)
		return mcdsJustifications.size();
		else return -1;
	}
		
	public int getMCDSExpansionsCount() {
		if (mcdsExpansions != null)
		return mcdsExpansions.size();
		else return -1;
	}
		
	public int getMcdsAllDifferentCount() { 
		return getMcdsAllDifferent().size();
	}
	
	
	public void setMcdsDisjoint(Vector<Set<OWLEntity>> mcdsDisjoint) {
		this.mcdsDisjoint = mcdsDisjoint;
	}
		
	public void setMcdsJustifications(Vector<Set<OWLEntity>> mcdsJustifications) {
		this.mcdsJustifications = mcdsJustifications;
	} 
 
	public void setMCDSExpansions(Vector<Set<OWLEntity>> mcdsExpansions) {
		this.mcdsExpansions = mcdsExpansions;
	}
	
	public void addtoMCDSExpansions(Vector<Set<OWLEntity>> newMcdsExpansions) {
		if (mcdsExpansions == null)  mcdsExpansions = new Vector<Set<OWLEntity>>();
		if (newMcdsExpansions != null) mcdsExpansions.addAll(newMcdsExpansions);
	} 
	
	public boolean isMcdsComplete() {
		return isMcdsComplete;
	} 

	public void setMcdsComplete(boolean isMcdsComplete) {
		this.isMcdsComplete = isMcdsComplete;
	}
}
