package owldefinability.definability;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owl.explanation.api.ExplanationGeneratorFactory;
import org.semanticweb.owl.explanation.api.ExplanationManager;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.ToStringRenderer;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
// import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
// import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
// import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.RemoveAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.OWLEntityRenamer;

import owldefinability.general.Toolbox;
import uk.ac.manchester.cs.owlapi.dlsyntax.DLSyntaxObjectRenderer;

/**
 * Determine definability status (defined explcitly/implicitly/by empty
 * signature, or undefined) of a given Concept or Role using a specified
 * signature, under a TBox
 */
public class ImplicitDefinablityChecker {

	// * * * Internal & Settings * * *
	private OWLDataFactory df = OWLManager.getOWLDataFactory();
	private OWLOntologyManager owlOntologyManager = OWLManager.createOWLOntologyManager();
	private OWLReasoner reasoner = null;
	private OWLReasonerFactory reasonerFactory = null;
	private boolean isVerbose = false;
	private boolean isSuperVerbose = false;
	private String reasonerType;
	private String datasetId;
	private long reasonerTimeout = 600000; // 10 minutes
	private ConceptDefinabilityType currentConceptDefinabilityType;

	public enum ConceptDefinabilityType {
		EXPLICIT, EXPLICIT_CYCLIC, IMPLICIT, UNDEFINED, UNKNOWN, ERROR_IMPLIED_BY_EMPTYSIGMA
	}

	// * * * INPUT VAR.S * * *
	OWLOntology ont = null;
	File ontFile = null;



	// * * * Constructors * * *
	/**
	 * Determine definability status (defined explciitly/implicitly/by empty
	 * signature, or undefined) of a given Concept or Role using a specified
	 * signature, under a TBox
	 * 
	 * @param ontFile
	 *            The ontology as File
	 * @param reasonerType
	 *            Reasoner type {Pellet, Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param datasetId
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param isVerbose
	 *            Show internal processing
	 */
	public ImplicitDefinablityChecker(File ontFile, String datasetId, String reasonerType,
			long reasonerTimeout, boolean isVerbose) {
		this.ontFile = ontFile;
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		try {
			ont = owlOntologyManager.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		this.reasonerType = reasonerType;
		this.reasonerTimeout = reasonerTimeout;
		this.isVerbose = isVerbose;
		this.datasetId = datasetId;
		reasoner = Toolbox.INSTANCE.getReasoner(reasonerType, ont, this.reasonerTimeout);
		this.reasonerFactory = Toolbox.INSTANCE.getReasonerFactor(reasonerType);
	}



	// * * * Definability * * *
	/**
	 * Determine whether a given concept is defined either (1) explicitly or (2)
	 * imlicitly in the ontology, using a specified signature (EXPLICIT,
	 * EXPLICIT_CYCLIC, IMPLICIT, UNDEFINED, UNKNOWN,
	 * ERROR_IMPLIED_BY_EMPTYSIGMA)
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return ConceptDefinabilityType type
	 */
	public ConceptDefinabilityType getConceptDefinability(OWLClass concept, Set<OWLEntity> signature) {
		currentConceptDefinabilityType = ConceptDefinabilityType.UNKNOWN;
		if (isConceptDefined(concept, signature) == true) {
			return currentConceptDefinabilityType;
		}
		return ConceptDefinabilityType.UNDEFINED;
	}


	/**
	 * Determine whether a given concept is defined either (1) explicitly or (2)
	 * imlicitly in the ontology, using a specified signature
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return True is definable, false otherwise
	 */
	public boolean isConceptDefined(OWLClass concept, Set<OWLEntity> signature) {
		currentConceptDefinabilityType = ConceptDefinabilityType.UNKNOWN;
		if (isVerbose)
			System.out.println("isConceptImplicitlyDefined? " + concept.getIRI().getShortForm()
					+ "\n\t" + signature + "\n\t" + reasoner.getReasonerName());

		// explicitly defined is also implicitly defined
		currentConceptDefinabilityType = isConceptExplicitlyDefined(concept);
		if (currentConceptDefinabilityType != ConceptDefinabilityType.UNKNOWN) {
			return true;
		}

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);
		// System.out.println("removed predicates: " + filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}


		// T u T' = C ≡ C' ?
		OWLClass concept_T = df.getOWLClass(IRI.create(concept.getIRI() + "_T"));
		Set<OWLClass> eqvSet = new HashSet<OWLClass>();
		eqvSet.add(concept);
		eqvSet.add(concept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);

		// check entailment
		OWLReasoner reasoner2 = Toolbox.INSTANCE
				.getReasoner(reasonerType, ontCopy, reasonerTimeout);
		boolean isEntailed = reasoner2.isEntailed(eqvAx);

		if (isEntailed == true) {
			currentConceptDefinabilityType = ConceptDefinabilityType.IMPLICIT;
		} else {
			currentConceptDefinabilityType = ConceptDefinabilityType.UNDEFINED;
		}
		return isEntailed;
	}



	// * * * CONCEPT Implicit Definability * * *
	/**
	 * Determine whether a given concept is definable imlicitly in the ontology,
	 * using a specified signature
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return True is definable, false otherwise
	 */
	public boolean isConceptImplicitlyDefined(OWLClass concept, Collection<OWLEntity> signature) {
		currentConceptDefinabilityType = ConceptDefinabilityType.UNKNOWN;

		if (isVerbose)
			System.out.println("isConceptImplicitlyDefined? " + concept.getIRI().getShortForm()
					+ "\n\t" + signature);

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}


		// T u T' |= C ≡ C' ?
		OWLClass concept_T = df.getOWLClass(IRI.create(concept.getIRI() + "_T"));
		Set<OWLClass> eqvSet = new HashSet<OWLClass>();
		eqvSet.add(concept);
		eqvSet.add(concept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);

		// check entailment
		OWLReasoner reasoner2 = Toolbox.INSTANCE
				.getReasoner(reasonerType, ontCopy, reasonerTimeout);
		boolean isEntailed = reasoner2.isEntailed(eqvAx);

		if (isEntailed == true) {
			currentConceptDefinabilityType = ConceptDefinabilityType.IMPLICIT;
		} else {
			currentConceptDefinabilityType = ConceptDefinabilityType.UNDEFINED;
		}

		if (signature.size() == 0) {
			currentConceptDefinabilityType = ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA;
		}
		return isEntailed;
	}

	/**
	 * Determine whether a given concept is definable imlicitly in a module,
	 * using a specified signature
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @param moduleAxioms
	 *            The ontology module
	 * @return True is definable, false otherwise
	 */
	public boolean isConceptImplicitlyDefined_MOD(OWLClass concept,
			Collection<OWLEntity> signature, Set<OWLAxiom> moduleAxioms) {
		currentConceptDefinabilityType = ConceptDefinabilityType.UNKNOWN;

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
			for (OWLAxiom axx : ontCopy.getAxioms()) {
				if (!moduleAxioms.contains(axx)) {
					RemoveAxiom rmAxx = new RemoveAxiom(ontCopy, axx);
					omCopy.applyChange(rmAxx);
				}
			}
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
			for (OWLAxiom axx : ontTilde.getAxioms()) {
				if (!moduleAxioms.contains(axx)) {
					RemoveAxiom rmAxx = new RemoveAxiom(ontTilde, axx);
					omTilde.applyChange(rmAxx);
				}
			}
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}


		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}


		// T u T' |= C ≡ C' ?
		OWLClass concept_T = df.getOWLClass(IRI.create(concept.getIRI() + "_T"));
		Set<OWLClass> eqvSet = new HashSet<OWLClass>();
		eqvSet.add(concept);
		eqvSet.add(concept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);

		// check entailment
		OWLReasoner reasoner2 = Toolbox.INSTANCE
				.getReasoner(reasonerType, ontCopy, reasonerTimeout);
		boolean isEntailed = reasoner2.isEntailed(eqvAx);

		if (isEntailed == true) {
			currentConceptDefinabilityType = ConceptDefinabilityType.IMPLICIT;
		} else {
			currentConceptDefinabilityType = ConceptDefinabilityType.UNDEFINED;
		}

		if (signature.size() == 0) {
			currentConceptDefinabilityType = ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA;
		}
		return isEntailed;
	}

	/**
	 * Determine whether a given concept is definable imlicitly in the ontology,
	 * using a specified signature
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return At least one justification (axiom set), if it is definable, empty
	 *         set otherwise
	 */
	Set<Explanation<OWLAxiom>> isConceptImplicitlyDefined2(OWLClass concept,
			Set<OWLEntity> signature) {
		if (isVerbose)
			System.out.println("isConceptImplicitlyDefined2? " + concept.getIRI().getShortForm()
					+ "\n\t" + signature);

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}

		if (isSuperVerbose == true) {
			Vector<OWLAxiom> logAxioms = new Vector<OWLAxiom>();
			logAxioms.addAll(ontCopy.getLogicalAxioms());
			Collections.sort(logAxioms, new Comparator<OWLAxiom>() {
				public int compare(OWLAxiom arg0, OWLAxiom arg1) {
					return (arg0.toString()).compareTo(arg1.toString());
				}
			});
		}

		// T u T' = C ≡ C' ?
		OWLClass concept_T = df.getOWLClass(IRI.create(concept.getIRI() + "_T"));
		Set<OWLClass> eqvSet = new HashSet<OWLClass>();
		eqvSet.add(concept);
		eqvSet.add(concept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);

		// generate explonation
		ExplanationGeneratorFactory<OWLAxiom> genFac = ExplanationManager
				.createExplanationGeneratorFactory(reasonerFactory);
		ExplanationGenerator<OWLAxiom> gen = genFac.createExplanationGenerator(ontCopy);

		// Set<Explanation<OWLAxiom>> expl = gen.getExplanations(eqvAx);

		int limit = 10;
		Set<Explanation<OWLAxiom>> expl = gen.getExplanations(eqvAx, limit);
		Set<Explanation<OWLAxiom>> explAll = new HashSet<Explanation<OWLAxiom>>();
		explAll.addAll(expl);
		while (expl.size() >= limit) {
			limit = limit * 5;
			explAll.addAll(expl);
			System.out.println("\t Explanation LIMIT increased to " + limit);
			expl = gen.getExplanations(eqvAx, limit);
		}
		explAll.addAll(expl);
		// System.out.println("ALL Explantions " + explAll.size());
		return explAll;
	}

	/**
	 * Determine whether a given concept is definable imlicitly in the ontology,
	 * using an empty signature, i.e. whether concept is equivalent to Top
	 * 
	 * @param concept
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return True is definable, false otherwise
	 */
	public boolean isConceptImplicitlyDefinedByEmptySignature(OWLClass concept) {
		// System.out.println("\t isConceptImplicitlyDefinedByEmptySignature " +
		// concept);
		HashSet<OWLEntity> emptySet = new HashSet<OWLEntity>();

		boolean isImpDefiened = isConceptImplicitlyDefined(concept, emptySet);
		if (currentConceptDefinabilityType == ConceptDefinabilityType.ERROR_IMPLIED_BY_EMPTYSIGMA
				&& isImpDefiened == true) {
			System.out.println("\t" + concept + " ERROR_IMPLIED_BY_EMPTYSIGMA");
			return true;
		}
		return false;
	}



	// * * * ROLE Implicit Definability * * *
	/**
	 * Determine whether a given role is definable imlicitly in a module, using
	 * a specified signature
	 * 
	 * @param role
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @return True is definable, false otherwise
	 */
	public boolean isRoleImplicitlyDefined(OWLEntity role, Set<OWLEntity> signature) {
		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T') but only role axioms !!!
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			boolean sigmaOnlyContainsProperties = true;
			Set<OWLEntity> axxSigma = axx.getSignature();
			for (OWLEntity ent : axxSigma) {
				if (!ent.isOWLObjectProperty() && !ent.isOWLDataProperty()) {
					sigmaOnlyContainsProperties = false;
					break;
				}
			}

			if (sigmaOnlyContainsProperties) {
				AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
				omCopy.applyChange(addAxiom);
			}
		}


		// T u T' |= r ≡ r' ?
		if (role.isOWLObjectProperty()) {
			OWLObjectProperty objRole = (OWLObjectProperty) role;
			OWLObjectProperty objRoleT = df
					.getOWLObjectProperty(IRI.create(objRole.getIRI() + "_T"));
			Set<OWLObjectProperty> eqvSet = new HashSet<OWLObjectProperty>();
			eqvSet.add(objRole);
			eqvSet.add(objRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentObjectPropertiesAxiom(eqvSet);

			// check entailment
			OWLReasoner reasoner2 = Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
					reasonerTimeout);
			return reasoner2.isEntailed(eqvAx);
		} else if (role.isOWLDataProperty()) {
			OWLDataProperty dRole = (OWLDataProperty) role;
			OWLDataProperty dRoleT = df.getOWLDataProperty(IRI.create(dRole.getIRI() + "_T"));
			Set<OWLDataProperty> eqvSet = new HashSet<OWLDataProperty>();
			eqvSet.add(dRole);
			eqvSet.add(dRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentDataPropertiesAxiom(eqvSet);

			// check entailment
			OWLReasoner reasoner2 = Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
					reasonerTimeout);
			return reasoner2.isEntailed(eqvAx);
		}
		return false;
	}

	/**
	 * Determine whether a given role is definable imlicitly in a module, using
	 * a specified signature
	 * 
	 * @param role
	 *            Concept
	 * @param signature
	 *            Potential defintion signature of the concept
	 * @param moduleAxioms
	 *            The ontology module
	 * @return True is definable, false otherwise
	 */
	public boolean isRoleImplicitlyDefined_MOD(OWLEntity role, Set<OWLEntity> signature,
			Set<OWLAxiom> moduleAxioms) {
		currentConceptDefinabilityType = ConceptDefinabilityType.UNKNOWN;

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
			for (OWLAxiom axx : ontCopy.getAxioms()) {
				if (!moduleAxioms.contains(axx)) {
					RemoveAxiom rmAxx = new RemoveAxiom(ontCopy, axx);
					omCopy.applyChange(rmAxx);
				}
			}
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
			for (OWLAxiom axx : ontTilde.getAxioms()) {
				if (!moduleAxioms.contains(axx)) {
					RemoveAxiom rmAxx = new RemoveAxiom(ontTilde, axx);
					omTilde.applyChange(rmAxx);
				}
			}
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T') but only role axioms !!!
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			boolean sigmaOnlyContainsProperties = true;
			Set<OWLEntity> axxSigma = axx.getSignature();
			for (OWLEntity ent : axxSigma) {
				if (!ent.isOWLObjectProperty() && !ent.isOWLDataProperty()) {
					sigmaOnlyContainsProperties = false;
					break;
				}
			}

			if (sigmaOnlyContainsProperties) {
				AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
				omCopy.applyChange(addAxiom);
			}
		}


		// T u T' |= r ≡ r' ?
		if (role.isOWLObjectProperty()) {
			OWLObjectProperty objRole = (OWLObjectProperty) role;
			OWLObjectProperty objRoleT = df
					.getOWLObjectProperty(IRI.create(objRole.getIRI() + "_T"));
			Set<OWLObjectProperty> eqvSet = new HashSet<OWLObjectProperty>();
			eqvSet.add(objRole);
			eqvSet.add(objRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentObjectPropertiesAxiom(eqvSet);

			// check entailment
			OWLReasoner reasoner2 = Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
					reasonerTimeout);
			return reasoner2.isEntailed(eqvAx);
		} else if (role.isOWLDataProperty()) {
			OWLDataProperty dRole = (OWLDataProperty) role;
			OWLDataProperty dRoleT = df.getOWLDataProperty(IRI.create(dRole.getIRI() + "_T"));
			Set<OWLDataProperty> eqvSet = new HashSet<OWLDataProperty>();
			eqvSet.add(dRole);
			eqvSet.add(dRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentDataPropertiesAxiom(eqvSet);

			// check entailment
			OWLReasoner reasoner2 = Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
					reasonerTimeout);
			return reasoner2.isEntailed(eqvAx);
		}
		return false;
	}

	public boolean isRoleExplicitlyDefined(OWLEntity role, Set<OWLEntity> signature) {
		for (OWLAxiom axx : ont.getReferencingAxioms(role)) {
			if (axx.getSignature().size() == 2) {
				if (axx.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
						AxiomType.EQUIVALENT_DATA_PROPERTIES)) {
					System.out.println("\tEQV " + axx + " " + axx.getAxiomType());
					return true;
				} else if (axx.isOfType(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY,
						AxiomType.INVERSE_OBJECT_PROPERTIES)) {
					System.out.println("\tINV " + axx + " " + axx.getAxiomType());
					return true;
				}
			}
		}
		return false;
	}

	// Doesnt work as the OLWExpl. API does not support ObjEqvAxioms
	@SuppressWarnings("unused")
	private Set<Explanation<OWLAxiom>> isRoleImplicitlyDefined2(OWLEntity role, Set<OWLEntity> signature) {
		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}


		// T u T' |= r ≡ r' ?
		ExplanationGeneratorFactory<OWLAxiom> genFac = ExplanationManager
				.createExplanationGeneratorFactory(reasonerFactory);
		ExplanationGenerator<OWLAxiom> gen = genFac.createExplanationGenerator(ontCopy);

		if (role.isOWLObjectProperty()) {
			OWLObjectProperty objRole = (OWLObjectProperty) role;
			OWLObjectProperty objRoleT = df
					.getOWLObjectProperty(IRI.create(objRole.getIRI() + "_T"));
			Set<OWLObjectProperty> eqvSet = new HashSet<OWLObjectProperty>();
			eqvSet.add(objRole);
			eqvSet.add(objRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentObjectPropertiesAxiom(eqvSet);

			// check entailment
			// OWLReasoner reasoner2 =
			// Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
			// reasonerTimeout);
			Set<Explanation<OWLAxiom>> expl = gen.getExplanations(eqvAx);

			return expl;
		} else if (role.isOWLDataProperty()) {
			OWLDataProperty dRole = (OWLDataProperty) role;
			OWLDataProperty dRoleT = df.getOWLDataProperty(IRI.create(dRole.getIRI() + "_T"));
			Set<OWLDataProperty> eqvSet = new HashSet<OWLDataProperty>();
			eqvSet.add(dRole);
			eqvSet.add(dRoleT);
			OWLAxiom eqvAx = df.getOWLEquivalentDataPropertiesAxiom(eqvSet);

			// check entailment
			// OWLReasoner reasoner2 =
			// Toolbox.INSTANCE.getReasoner(reasonerType, ontCopy,
			// reasonerTimeout);
			Set<Explanation<OWLAxiom>> expl = gen.getExplanations(eqvAx);

			return expl;
		}

		return null;
	}



	// * * * Explicit Definability * * *
	/**
	 * Determine whether a given concept is defined explicitly in the ontology
	 * 
	 * @param concept
	 *            Concept
	 * @return Either EXPLICIT or EXPLICIT_CYCLIC
	 */
	public ConceptDefinabilityType isConceptExplicitlyDefined(OWLClass concept) {
		int explicitDefintions = 0;
		int explicitCyclicDefintions = 0;

		//
		for (OWLAxiom axx : ont.getReferencingAxioms(concept)) {
			if (!axx.isOfType(AxiomType.DECLARATION)) {
				// System.out.println("\t   " + axx);
				if (axx.isOfType(AxiomType.EQUIVALENT_CLASSES)) {

					// LHS & RHS
					ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
					String[] words = axx.toString().split(" ");

					// Concept ≡ D
					if (words[0].equals(concept.getIRI().getShortForm())) {
						explicitDefintions++;
						if (isVerbose)
							System.out.println("\tEXPLICT " + axx);
						if (isExplicitConceptDefinitionAcyclic(concept, axx)) {
							explicitCyclicDefintions++;
						}
					}

					// A ≡ Concept
					else if (words.length == 3 && words[2].equals(concept.getIRI().getShortForm())) {
						explicitDefintions++;
					}
				}
			}
		}

		//
		if (explicitDefintions > 0) {
			if (explicitDefintions == explicitCyclicDefintions) {
				return ConceptDefinabilityType.EXPLICIT_CYCLIC;
			} else
				return ConceptDefinabilityType.EXPLICIT;
		}
		return ConceptDefinabilityType.UNKNOWN;
	}

	/**
	 * Determine whether a given concept equivalence defintion contains a direct cycle
	 * 
	 * @param concept Defined concept 
	 * @param axiom Concept equivalence axiom, whose LHS is the defined concept
	 * @return True if cyclic, false otherwise
	 */
	public boolean isExplicitConceptDefinitionAcyclic(OWLClass concept, OWLAxiom axiom) {
		// System.out.println("\t isConceptExplicitlyDefintionsAcyclic " +
		// concept.getIRI().getFragment());
		OWLEquivalentClassesAxiom axx = (OWLEquivalentClassesAxiom) axiom;

		boolean isLHStheConcept = false;
		Iterator<OWLClassExpression> clsXprIt = axx.getClassExpressions().iterator();
		OWLClassExpression firstClsXpr = clsXprIt.next();
		if (firstClsXpr.getSignature().contains(concept) && firstClsXpr.getSignature().size() == 1) {
			isLHStheConcept = true;
		}

		boolean doesRHScontainConcept = false;
		while (clsXprIt.hasNext()) {
			OWLClassExpression clsXp = clsXprIt.next();
			if (clsXp.getSignature().contains(concept)) {
				doesRHScontainConcept = true;
				break;
			}
		}

		if (isLHStheConcept && doesRHScontainConcept) {
			saveCyclicDefinitions(concept, axx);
			return true;
		} else
			return false;
	}


	// * * * Justify definability * * *
	/**
	 * Compute the set of all justifications for a definability case
	 * 
	 * @param concept
	 *            Defined Concept
	 * @param signature
	 *            Definition Signature
	 * @param getAllJustifications
	 *            Justification(s) that entail the case
	 * @return
	 */
	public Set<Explanation<OWLAxiom>> justifyConceptDefinability(OWLClass concept,
			Set<OWLEntity> signature, boolean getAllJustifications) {
		if (isVerbose)
			System.out.println("justifyDefinability " + concept.getIRI().getShortForm() + "\n\t"
					+ signature);

		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}

		if (isSuperVerbose == true) {
			Vector<OWLAxiom> logAxioms = new Vector<OWLAxiom>();
			logAxioms.addAll(ontCopy.getLogicalAxioms());
			Collections.sort(logAxioms, new Comparator<OWLAxiom>() {
				public int compare(OWLAxiom arg0, OWLAxiom arg1) {
					return (arg0.toString()).compareTo(arg1.toString());
				}
			});
		}

		// T u T' = C ≡ C' ?
		OWLClass concept_T = df.getOWLClass(IRI.create(concept.getIRI() + "_T"));
		Set<OWLClass> eqvSet = new HashSet<OWLClass>();
		eqvSet.add(concept);
		eqvSet.add(concept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);

		// generate explonation
		ExplanationGeneratorFactory<OWLAxiom> genFac = ExplanationManager
				.createExplanationGeneratorFactory(reasonerFactory);
		ExplanationGenerator<OWLAxiom> gen = genFac.createExplanationGenerator(ontCopy);
		Set<Explanation<OWLAxiom>> expl = null;
		if (getAllJustifications) {
			expl = gen.getExplanations(eqvAx);
		} else {
			expl = gen.getExplanations(eqvAx, 1);
		}
		return expl;
	}



	// * * * COMPLEX CONCEPT Implicit Definability * * *
	boolean isComplexConceptImplicitlyDefined(Set<OWLEntity> conceptSignature,
			Collection<OWLEntity> signature) {

		if (isVerbose)
			System.out.println("\nisComplexConceptImplicitlyDefined?\n\tSig(D)\t= "
					+ conceptSignature + "\n\tΣ\t= " + signature);

		// Sig(D) <- Sig(D) \ Σ
		// all entities are selfdefinable
		conceptSignature.removeAll(signature);
		System.out.println("\n\tSig(D)\t= " + conceptSignature);


		// generate T u T'
		OWLOntology ontCopy = null;
		OWLOntologyManager omCopy = OWLManager.createOWLOntologyManager();
		// copy T
		try {
			ontCopy = omCopy.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// create T' by introducing a new predicate P' for all P in sig(C,T)\Σ
		OWLOntology ontTilde = null;
		OWLOntologyManager omTilde = OWLManager.createOWLOntologyManager();
		try {
			ontTilde = omTilde.loadOntologyFromOntologyDocument(ontFile.getAbsoluteFile());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		Set<OWLEntity> predicatesToChange = ontTilde.getSignature();
		predicatesToChange.remove(df.getOWLThing());
		predicatesToChange.removeAll(signature);

		// filter out data types that are common
		Set<OWLEntity> filteredPredicate = new HashSet<OWLEntity>();
		for (OWLEntity ent : predicatesToChange) {
			if (ent.getIRI().toString().startsWith("http://www.w3.org")) {
				filteredPredicate.add(ent);
			}
		}
		predicatesToChange.removeAll(filteredPredicate);

		Set<OWLOntology> onts = new HashSet<OWLOntology>();
		onts.add(ontTilde);
		OWLEntityRenamer renamer = new OWLEntityRenamer(omTilde, onts);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLEntity predicate : predicatesToChange) {
			IRI newIr = IRI.create(new String(predicate.getIRI() + "_T"));
			List<OWLOntologyChange> changes = renamer.changeIRI(predicate, newIr);
			omTilde.applyChanges(changes);
		}

		// add T' axioms to T (T u T')
		for (OWLAxiom axx : ontTilde.getAxioms()) {
			AddAxiom addAxiom = new AddAxiom(ontCopy, axx);
			omCopy.applyChange(addAxiom);
		}


		// create COMPLEX C and C' in (C ≡ C')
		Set<OWLClassExpression> exprs = new HashSet<OWLClassExpression>();
		Set<OWLClassExpression> exprs_T = new HashSet<OWLClassExpression>();
		for (OWLEntity ent : conceptSignature) {

			// concept entity
			if (ent.isOWLClass()) {
				// C
				exprs.add(ent.asOWLClass());

				// C'
				OWLClass concept_T = df.getOWLClass(IRI.create(ent.asOWLClass().getIRI() + "_T"));
				exprs_T.add(concept_T);
			}

			// role entity
			else if (ent.isOWLObjectProperty()) {
				// r
				OWLClassExpression exp = df.getOWLObjectSomeValuesFrom(ent.asOWLObjectProperty(),
						df.getOWLThing());
				exprs.add(exp);

				// r'
				OWLObjectProperty objRoleT = df.getOWLObjectProperty(IRI.create(ent
						.asOWLObjectProperty().getIRI() + "_T"));
				OWLClassExpression exp_T = df
						.getOWLObjectSomeValuesFrom(objRoleT, df.getOWLThing());
				exprs_T.add(exp_T);
			} else if (ent.isOWLDataProperty()) {
				// p
				OWLClassExpression exp = df.getOWLDataHasValue(ent.asOWLDataProperty(),
						df.getOWLLiteral("123", df.getRDFPlainLiteral()));
				exprs.add(exp);

				// p'
				OWLDataProperty datRoleT = df.getOWLDataProperty(IRI.create(ent
						.asOWLObjectProperty().getIRI() + "_T"));
				OWLClassExpression exp_T = df.getOWLDataHasValue(datRoleT,
						df.getOWLLiteral("123", df.getRDFPlainLiteral()));
				exprs_T.add(exp_T);
			}
		}

		OWLObjectUnionOf complexConcept = df.getOWLObjectUnionOf(exprs);
		OWLObjectUnionOf complexConcept_T = df.getOWLObjectUnionOf(exprs_T);
		System.out.println("\tC : " + complexConcept);
		System.out.println("\tC': " + complexConcept_T);


		// create AXIOM (C ≡ C')
		Set<OWLClassExpression> eqvSet = new HashSet<OWLClassExpression>();
		eqvSet.add(complexConcept);
		eqvSet.add(complexConcept_T);
		OWLAxiom eqvAx = df.getOWLEquivalentClassesAxiom(eqvSet);
		System.out.println("    C ≡ C'\n\t" + eqvAx);

		// check entailment T u T' |= C ≡ C' ?
		OWLReasoner reasoner2 = Toolbox.INSTANCE
				.getReasoner(reasonerType, ontCopy, reasonerTimeout);
		boolean isEntailed = reasoner2.isEntailed(eqvAx);


		return isEntailed;
	}



	// * * * Helper(s) * * *
	private void saveCyclicDefinitions(OWLClass concept, OWLEquivalentClassesAxiom axiom) {
		// attempt to load
		String filePath = new String(Toolbox.INSTANCE.getPathCdefCyclic(datasetId)
				+ "/CyclicDefintions_" + ontFile.getName() + ".txt");
		String fileContent = Toolbox.INSTANCE.readFile(filePath);
		if (fileContent == null) {
			fileContent = "";
		}

		// save
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		Toolbox.INSTANCE.writeToFile(filePath, new String(fileContent + concept.getIRI() + " "
				+ axiom + "\n"));
	}
}
