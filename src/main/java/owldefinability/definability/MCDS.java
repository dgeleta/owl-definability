package owldefinability.definability;

import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;

import owldefinability.definability.Classifier_DefinitonPattern.MCDS_pattern;

/**
 * Minimal Concept Definition Signature
 */
public class MCDS {	
	
	private OWLClass concept;
	private MCDS_pattern pattern;
	private Set<OWLAxiom> justification;
	private Set<OWLEntity> signature;
	
	/**
	 * Minimal Concept Definition Signature
	 * 
	 * @param concept Defined Concept
	 * @param signature Minimal Concept Definition Signature
	 * @param pattern Defintion Pattern Type
	 * @param justification The justication which entails the definition
	 */
	public MCDS(OWLClass concept, Set<OWLEntity> signature, MCDS_pattern pattern, Set<OWLAxiom> justification) {
		this.concept = concept;
		this.pattern = pattern;
		this.justification = justification;
		this.signature = signature;
	}	 

	public MCDS_pattern getPattern() {
		return pattern;
	}

	public OWLClass getConcept() {
		return concept;
	}

	public Set<OWLAxiom> getJustification() {
		return justification;
	}

	public Set<OWLEntity> getSignature() {
		return signature;
	} 
}
