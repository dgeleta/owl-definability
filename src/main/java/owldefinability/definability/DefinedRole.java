package owldefinability.definability;

import java.util.HashSet;
import java.util.Set;


import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLEntity;

public class DefinedRole {

	private OWLEntity role;
	private IRI ontologyIRI;
	private RoleDefinabilityType roleDefinabilityType;
	private HashSet<Set<OWLEntity>> mcdss = new HashSet<Set<OWLEntity>>();


	// * * * Definability Types * * *
	public enum RoleDefinabilityType {
		EXPLICITLY, IMPLICITLY, UNDEFINED, UNKNOWN
	}
	
	
	// * * * Constructor * * *
	public DefinedRole(OWLEntity role, RoleDefinabilityType roleDefinabilityType, IRI ontologyIRI, HashSet<Set<OWLEntity>> mcdss) {
		this.role = role;
		this.roleDefinabilityType = roleDefinabilityType;
		this.ontologyIRI = ontologyIRI;
		this.mcdss = mcdss;
	}

	// * * * Accessors * * *
	public OWLEntity getRole() {
		return role;
	}

	public IRI getOntologyIRI() {
		return ontologyIRI;
	}

	public RoleDefinabilityType getRoleDefinabilityType() {
		return roleDefinabilityType;
	}

	public HashSet<Set<OWLEntity>> getMcdss() {
		return mcdss;
	}
}
