package owldefinability.definability;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import owldefinability.general.Toolbox;



/**
 * Classify defined entity MDSs as definition patterns
 *
 */
public class Classifier_DefinitonPattern {

	// * * * VARs * * *
	private OWLOntology ont = null;
	private File ontFile = null;
	private OWLDataFactory df = OWLManager.getOWLDataFactory();
	private ImplicitDefinablityChecker impDefComp;
	private boolean isVerbose;
	private OWLReasoner reasoner;
	private String dataSetID;
	private Set<String> patternNames = new HashSet<String>();
	private Toolbox tb = Toolbox.INSTANCE;
	private HashMap<OWLEntity, Vector<HashMap<String, Vector<OWLEntity>>>> loadedEntityPatterns;


	public enum MCDS_pattern {
		EXPLICIT_DEF, EXPLICIT_SYNONYM, IMPLICIT_SYNONYM, DISJOINT_UNION, OBJPROP_DOMAIN, OBJPROP_RANGE, DATPROP_DOMAIN, DATPROP_RANGE, OBJPROP_DOMAIN_INVERSE, OBJPROP_RANGE_INVERSE, EXPLICITLY_WITH_INVERSE_ROLE, EXPLICITLY_WITH_SYNONYM_ROLE, COMPLEX, UNKNOWN
	}

	public enum MRDS_pattern {
		EXPLICIT_DEF, EXPLICIT_SYNONYM, EXPLICIT_INVERSE, IMPLICIT_SYNONYM, IMPLICIT_INVERSE, COMPLEX, UNKNOWN
	}



	// * * * CONSTRUCTOR * * *
	/**
	 * Classify defined entity MDSs as definition patterns
	 * 
	 * @param ont
	 *            The pre-loaded OWLOntology
	 * @param ontFile
	 *            The ontology as File
	 * @param reasonerType
	 *            Reasoner type {Pellet, Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param dataSet
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param isVerbose
	 *            Show internal processing
	 */
	public Classifier_DefinitonPattern(OWLOntology ont, File ontFile, String dataSet,
			String reasonerType, long reasonerTimeout, boolean isVerbose) {
		this.ont = ont;
		this.ontFile = ontFile;
		this.dataSetID = dataSet;
		this.isVerbose = isVerbose;
		impDefComp = new ImplicitDefinablityChecker(ontFile, dataSet, reasonerType, 600000, false);
		reasoner = tb.getReasoner(reasonerType, ont, reasonerTimeout);
		for (MCDS_pattern pat : MCDS_pattern.values()) {
			patternNames.add(pat.toString());
		}
		for (MRDS_pattern pat : MRDS_pattern.values()) {
			patternNames.add(pat.toString());
		}
	}

	public HashMap<OWLEntity, String> loadPatterns() {
		return null;
	}

	public HashMap<OWLEntity, Vector<MCDS>> generateConceptDefinitionPatternsForOntology(
			HashMap<OWLClass, Vector<Set<OWLEntity>>> mcdss) {
		StringBuilder sbPatterns = new StringBuilder();
		HashMap<OWLEntity, Vector<MCDS>> patterns = new HashMap<OWLEntity, Vector<MCDS>>();

		Vector<OWLEntity> toDoConcepts = new Vector<OWLEntity>();
		toDoConcepts.addAll(mcdss.keySet());
		toDoConcepts = tb.sortEntitiesAlphabetcially(toDoConcepts);

		// CONCEPT
		for (OWLEntity con : toDoConcepts) {
			if (isVerbose) {
				System.out.println("   C: " + con);
			}

			sbPatterns.append(con.getIRI() + "\n");
			for (Set<OWLEntity> mcdsSig : mcdss.get(con)) {
				if (isVerbose)
					System.out.println("\t" + mcdsSig);

				if (mcdsSig.iterator().next() != null) {
					MCDS newMcds = identifyPatterns((OWLClass) con, mcdsSig);
					MCDS_pattern thisrecgPat = newMcds.getPattern();

					if (patterns.get(con) == null) {
						patterns.put(con, new Vector<MCDS>());
					}
					patterns.get(con).add(newMcds);

					if (isVerbose)
						System.out.println("\tPATTERN: " + thisrecgPat + "\n");

					// [SAVE] to PATTERN File
					sbPatterns.append("   " + thisrecgPat + "\n");
					sbPatterns.append("   " + newMcds.getSignature() + "\n");
					for (OWLAxiom axx : newMcds.getJustification()) {
						sbPatterns.append("\t" + axx + "\t" + axx.getAxiomType() + "\n");
					}
					sbPatterns.append("\n");
				}
			}
		}
		Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.folderExperimets + "/" + dataSetID
				+ "/_cDefPatterns/cDefPatterns_" + ontFile.getName() + ".txt"),
				sbPatterns.toString());

		return patterns;
	}

	public HashMap<OWLEntity, Vector<MRDS>> generateRoleDefinitionPatternsForOntology(
			HashMap<OWLEntity, Vector<Set<OWLEntity>>> mrdss) {

		if (mrdss.isEmpty() || mrdss.keySet().iterator().next() == null)
			return null;

		StringBuilder sbPatterns = new StringBuilder();
		HashMap<OWLEntity, Vector<MRDS>> patterns = new HashMap<OWLEntity, Vector<MRDS>>();

		Vector<OWLEntity> toDoRoles = new Vector<OWLEntity>();
		toDoRoles.addAll(mrdss.keySet());
		toDoRoles = tb.sortEntitiesAlphabetcially(toDoRoles);

		for (OWLEntity role : toDoRoles) {
			if (isVerbose) {
				System.out.println("   R: " + role);
			}

			sbPatterns.append(role.getIRI() + "\n");
			for (Set<OWLEntity> mrdsSig : mrdss.get(role)) {
				if (isVerbose)
					System.out.println("\t" + mrdsSig);

				if (mrdsSig.iterator().next() != null) {
					MRDS newMrds = identifyRolePatterns(role, mrdsSig);
					MRDS_pattern thisrecgPat = newMrds.getPattern();

					if (patterns.get(role) == null) {
						patterns.put(role, new Vector<MRDS>());
					}
					patterns.get(role).add(newMrds);

					if (isVerbose)
						System.out.println("\tPATTERN: " + thisrecgPat + "\n");

					// [SAVE] to PATTERN File
					sbPatterns.append("   " + thisrecgPat + "\n");
					sbPatterns.append("   " + newMrds.getSignature() + "\n");
					for (OWLAxiom axx : newMrds.getJustification()) {
						sbPatterns.append("\t" + axx + "\t" + axx.getAxiomType() + "\n");
					}
					sbPatterns.append("\n");
				}
			}
		}
		Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.folderExperimets + "/" + dataSetID
				+ "/_rDefPatterns/rDefPatterns_" + ontFile.getName() + ".txt"),
				sbPatterns.toString());

		return patterns;
	}

	public void generateConceptPatternsForOntology_PARTS(
			HashMap<OWLClass, Vector<Set<OWLEntity>>> mcdss, Vector<OWLEntity> computeThese,
			int partNb, int partsTotal) {
		StringBuilder sbPatterns = new StringBuilder();

		// attempt to load Concept patterns
		boolean loadConcepts = false;
		File cdefFile = new File(new String(Toolbox.INSTANCE.folderExperimets + "/" + dataSetID
				+ "/_cDefPatterns/cDefPatterns" + ontFile.getName() + "_" + partNb + "of"
				+ partsTotal + ".txt"));
		if (!cdefFile.exists()) {
			cdefFile = new File(new String(Toolbox.INSTANCE.folderExperimets + "/" + dataSetID
					+ "/_cDefPatterns/cDefPatterns_INPROGRESS_" + ontFile.getName() + "_" + partNb
					+ "of" + partsTotal + ".txt"));

			if (!cdefFile.exists()) {
				System.out.println("Concept Patterns : STARTING");
			} else {
				System.out.println("Concept Patterns : INPROGRESS");
				loadConcepts = true;
			}
		}
		if (loadConcepts) {
			loadConceptDefintionPatterns(cdefFile);
			Vector<OWLEntity> toDoConcepts = new Vector<OWLEntity>();
			toDoConcepts.addAll(mcdss.keySet());

			// int count = 0;
			for (OWLEntity concept : toDoConcepts) {
				// System.out.println("C: " + concept);

				if (loadedEntityPatterns.get(concept) != null) {
					if (mcdss.get(concept).size() == loadedEntityPatterns.get(concept).size()) {
						// System.out.println("Removed COMPLETED : " + concept +
						// " (" + ++count + ")");
						mcdss.remove(concept);
					}
				}
			}
			sbPatterns.append(tb.readFile(cdefFile.getAbsolutePath()) + "\n");
			System.out.println("REMAINING : " + mcdss.keySet().size());
		}

		System.out.println("Compute " + computeThese.size() + " nb of defined Concepts (patterns)");

		// compute remaining
		int remainingCount = 0;
		int totalToDo = mcdss.keySet().size();

		Vector<OWLEntity> toDoConcepts = new Vector<OWLEntity>();
		toDoConcepts.addAll(mcdss.keySet());
		toDoConcepts = tb.sortEntitiesAlphabetcially(toDoConcepts);
		toDoConcepts.retainAll(computeThese);

		for (OWLEntity con : toDoConcepts) {
			if (isVerbose)
				System.out.println(++remainingCount + " of " + totalToDo);
			if (isVerbose)
				System.out.println("   C: " + con);

			sbPatterns.append(con.getIRI() + "\n");
			for (Set<OWLEntity> mcdsSig : mcdss.get(con)) {
				if (isVerbose)
					System.out.println("\t" + mcdsSig);

				if (mcdsSig.iterator().next() != null) {
					try {
						MCDS newMcds = identifyPatterns((OWLClass) con, mcdsSig);
						MCDS_pattern thisrecgPat = newMcds.getPattern();
						if (isVerbose)
							System.out.println("\tPATTERN: " + thisrecgPat + "\n");

						// [SAVE] to PATTERN File
						sbPatterns.append("   " + thisrecgPat + "\n");
						sbPatterns.append("   " + newMcds.getSignature() + "\n");
						for (OWLAxiom axx : newMcds.getJustification()) {
							sbPatterns.append("\t" + axx + "\t" + axx.getAxiomType() + "\n");
						}
						sbPatterns.append("\n");
						Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.folderExperimets
								+ "/" + dataSetID + "/_cDefPatterns/cDefPatterns_INPROGRESS_"
								+ ontFile.getName() + "_" + partNb + "of" + partsTotal + ".txt"),
								sbPatterns.toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		// Serialise
		Toolbox.INSTANCE.writeToFile(new String(Toolbox.INSTANCE.folderExperimets + "/" + dataSetID
				+ "/_cDefPatterns/cDefPatterns_" + ontFile.getName() + "_" + partNb + "of"
				+ partsTotal + ".txt"), sbPatterns.toString());
		if (cdefFile.getName().toString().startsWith("cDefPatterns_INPROGRESS_"))
			cdefFile.delete();
	}



	// * * * IDENTIFY * * *
	public MCDS identifyPatterns(OWLClass definedConcept, Set<OWLEntity> mcds) {

		if (mcds.size() == 1) {
			if (mcds.iterator().next() == null) {
				return null;
			}
		}
		MCDS_pattern thisPattern = null;

		// Justify MCDS
		Set<Explanation<OWLAxiom>> jsts = impDefComp.justifyConceptDefinability(definedConcept, mcds,
				false);
		if (jsts == null) {
			return null;
		}
		Explanation<OWLAxiom> expl = jsts.iterator().next();
		Set<OWLAxiom> mcdsJst = getOnlyTAxioms(expl.getAxioms());

		/*
		 * for (OWLAxiom axx : mcdsJst) { System.out.println("\t - " + axx +
		 * "\t" + axx.getAxiomType()); }
		 */

		// EXPLICIT
		if (mcdsJst.size() == 1) {
			if (mcds.size() == 1) {
				thisPattern = MCDS_pattern.EXPLICIT_SYNONYM;
			} else {
				System.out.println("\t" + mcdsJst.iterator().next());
				thisPattern = MCDS_pattern.EXPLICIT_DEF;
			}
		}

		// IMPLICT
		else {
			if (mcds.size() == 1) {
				OWLEntity ent = mcds.iterator().next();

				if (ent.isOWLObjectProperty()) {
					OWLObjectProperty prop = (OWLObjectProperty) ent;
					if (prop.getDomains(ont).contains(definedConcept)) {
						thisPattern = MCDS_pattern.OBJPROP_DOMAIN;
					} else if (prop.getRanges(ont).contains(definedConcept)) {
						thisPattern = MCDS_pattern.OBJPROP_RANGE;
					}
				} else if (ent.isOWLDataProperty()) {
					OWLDataProperty prop = (OWLDataProperty) ent;
					if (prop.getDomains(ont).contains(definedConcept)) {
						thisPattern = MCDS_pattern.DATPROP_DOMAIN;
					} else if (prop.getRanges(ont).contains(definedConcept)) {
						thisPattern = MCDS_pattern.DATPROP_RANGE;
					}
				} else if (ent.isOWLClass()) {
					/*
					 * for (OWLAxiom axx :mcdsJst) { System.out.println("\t" +
					 * axx + " " + axx.getAxiomType()); }
					 */
					thisPattern = MCDS_pattern.IMPLICIT_SYNONYM;
				}
			} else {
				int axiom_disjointClasses_count = 0;
				int axiom_eqvClasses_count = 0;
				int axiom_inverseObj_count = 0;

				for (OWLAxiom axx : mcdsJst) {
					if (axx.isOfType(AxiomType.DISJOINT_CLASSES)) {
						axiom_disjointClasses_count++;
					} else if (axx.isOfType(AxiomType.EQUIVALENT_CLASSES)) {
						axiom_eqvClasses_count++;
					} else if (axx.isOfType(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)
							|| axx.isOfType(AxiomType.INVERSE_OBJECT_PROPERTIES)) {
						axiom_inverseObj_count++;
					}
				}

				//
				if (mcdsJst.size() == 2 && axiom_inverseObj_count == 1
						&& axiom_eqvClasses_count == 1) {
					for (@SuppressWarnings("unused")
					OWLAxiom axx : mcdsJst) {
						// System.out.println("\t" + axx + " " +
						// axx.getAxiomType());
					}
					thisPattern = MCDS_pattern.EXPLICITLY_WITH_INVERSE_ROLE;
				} else if (mcdsJst.size() == 2 && axiom_eqvClasses_count == 1) {
					for (@SuppressWarnings("unused")
					OWLAxiom axx : mcdsJst) {
						// System.out.println("\t" + axx + " " +
						// axx.getAxiomType());
					}
					thisPattern = MCDS_pattern.EXPLICITLY_WITH_SYNONYM_ROLE;
				} else if (mcdsJst.size() == (axiom_eqvClasses_count + axiom_disjointClasses_count)
						&& axiom_disjointClasses_count > 0) {
					thisPattern = MCDS_pattern.DISJOINT_UNION;
				}

				// COMPLEX
				for (@SuppressWarnings("unused")
				OWLAxiom axx : mcdsJst) {
					// System.out.println("\t" + axx + " " +
					// axx.getAxiomType());
				}
				if (thisPattern == null)
					thisPattern = MCDS_pattern.COMPLEX;
			}
		}

		// is a D/R concept of a property, but either
		if (mcds.size() == 1) {
			OWLEntity ent = mcds.iterator().next();
			if (ent.isOWLObjectProperty()) {

				// via a synoynm as part of an object union (which is anonymous
				// D/R concept)
				OWLObjectProperty prop = (OWLObjectProperty) ent;
				for (OWLClassExpression clsEx : prop.getDomains(ont)) {
					if (clsEx.getSignature().contains(definedConcept)) {
						/*
						 * for (OWLAxiom axx :mcdsJst) { System.out.println("\t"
						 * + axx + " " + axx.getAxiomType()); }
						 */
						thisPattern = MCDS_pattern.OBJPROP_DOMAIN;
					}
				}
				for (OWLClassExpression clsEx : prop.getRanges(ont)) {
					if (clsEx.getSignature().contains(definedConcept)) {
						thisPattern = MCDS_pattern.OBJPROP_RANGE;
					}
				}

				// or via a synonym
				if (reasoner.isEntailed(df.getOWLObjectPropertyDomainAxiom(prop, definedConcept))) {
					/*
					 * for (OWLAxiom axx :mcdsJst) { System.out.println("\t" +
					 * axx + " " + axx.getAxiomType()); }
					 */
					thisPattern = MCDS_pattern.OBJPROP_DOMAIN;
				} else if (reasoner.isEntailed(df.getOWLObjectPropertyRangeAxiom(prop,
						definedConcept))) {
					thisPattern = MCDS_pattern.OBJPROP_RANGE;
				}
			} else if (ent.isOWLDataProperty()) {
				OWLDataProperty prop = (OWLDataProperty) ent;
				for (OWLClassExpression clsEx : prop.getDomains(ont)) {
					if (clsEx.getSignature().contains(definedConcept)) {
						thisPattern = MCDS_pattern.DATPROP_DOMAIN;
					}
				}
				if (reasoner.isEntailed(df.getOWLDataPropertyDomainAxiom(prop, definedConcept))) {
					thisPattern = MCDS_pattern.DATPROP_DOMAIN;
				}
			}
		}

		// System.out.println("??? PATTERN " + thisPattern + "\n\n\n");

		if (thisPattern == null)
			thisPattern = MCDS_pattern.UNKNOWN;

		return new MCDS(definedConcept, mcds, thisPattern, mcdsJst);
	}

	public MRDS identifyRolePatterns(OWLEntity definedRole, Set<OWLEntity> mrds) {
		if (mrds.size() == 1) {
			if (mrds.iterator().next() == null) {
				return null;
			}
		}

		MRDS_pattern thisPattern = null;

		/*
		 * Doesnt work as the OLWExpl. API does not support ObjEqvAxioms
		 * 
		 * // Justify MRDS Set<Explanation<OWLAxiom>> jsts =
		 * impDefComp.isRoleImplicitlyDefined2(definedRole, mrds); if (jsts ==
		 * null) { return null; } Explanation<OWLAxiom> expl =
		 * jsts.iterator().next(); Set<OWLAxiom> mcdsJst =
		 * getOnlyTAxioms(expl.getAxioms());
		 */


		Set<OWLAxiom> justification = new HashSet<OWLAxiom>();

		// EXPLICIT INVERSE
		if (mrds.size() == 1) {
			for (OWLAxiom axx : ont.getLogicalAxioms()) {
				if (axx.getSignature().size() == 2) {
					if (axx.getSignature().contains(definedRole)
							&& axx.getSignature().containsAll(mrds)) {
						if (axx.getAxiomType().equals(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)
								|| axx.getAxiomType().equals(AxiomType.INVERSE_OBJECT_PROPERTIES)) {
							thisPattern = MRDS_pattern.EXPLICIT_INVERSE;
							justification.add(axx);

							// System.out.println("\tEXPLICIT_INVERSE : " + axx
							// + " " + axx.getAxiomType());
						}
					}
				}
			}
		}

		// EXPLICIT SYNONYM
		if (thisPattern == null && mrds.size() == 1) {
			for (OWLAxiom axx : ont.getLogicalAxioms()) {
				if (axx.getSignature().size() == 2) {
					if (axx.getSignature().contains(definedRole)
							&& axx.getSignature().containsAll(mrds)) {
						if (axx.getAxiomType().equals(AxiomType.EQUIVALENT_DATA_PROPERTIES)
								|| axx.getAxiomType()
										.equals(AxiomType.EQUIVALENT_OBJECT_PROPERTIES)) {
							thisPattern = MRDS_pattern.EXPLICIT_SYNONYM;
							justification.add(axx);

							// System.out.println("\tEXPLICIT_SYNONYM : " + axx
							// + " " + axx.getAxiomType());
						}
					}
				}
			}
		}

		// EXPLICIT DEFINTION
		if (thisPattern == null && mrds.size() > 1) {
			for (OWLAxiom axx : ont.getLogicalAxioms()) {
				if (axx.getSignature().containsAll(mrds)
						&& axx.getSignature().contains(definedRole)) {

				}
			}
		}

		// IMPLICIT SYNONYM
		if (thisPattern == null && mrds.size() == 1) {
			OWLDataFactory owlfactory = OWLManager.createOWLOntologyManager().getOWLDataFactory();
			OWLAxiom axx = null;

			if (definedRole.isOWLObjectProperty()) {
				Set<OWLObjectProperty> props = new HashSet<OWLObjectProperty>();
				props.add((OWLObjectProperty) definedRole);
				props.add((OWLObjectProperty) mrds.iterator().next());
				axx = owlfactory.getOWLEquivalentObjectPropertiesAxiom(props);
			} else {
				Set<OWLDataProperty> props = new HashSet<OWLDataProperty>();
				props.add((OWLDataProperty) definedRole);
				props.add((OWLDataProperty) mrds.iterator().next());
				axx = owlfactory.getOWLEquivalentDataPropertiesAxiom(props);
			}

			// O |= r = s
			if (reasoner.isEntailed(axx)) {
				thisPattern = MRDS_pattern.IMPLICIT_SYNONYM;
			}
		}

		// IMPLICIT INVERSE
		if (thisPattern == null && mrds.size() == 1) {
			OWLDataFactory owlfactory = OWLManager.createOWLOntologyManager().getOWLDataFactory();
			OWLAxiom axx = null;

			if (definedRole.isOWLObjectProperty()) {
				// Set<OWLObjectProperty> props = new
				// HashSet<OWLObjectProperty>();
				axx = owlfactory
						.getOWLInverseObjectPropertiesAxiom((OWLObjectProperty) definedRole,
								(OWLObjectProperty) mrds.iterator().next());
			}

			// O |= r = s^-
			if (reasoner.isEntailed(axx)) {
				thisPattern = MRDS_pattern.IMPLICIT_INVERSE;
			}
		}

		// COMPLEX
		if (thisPattern == null)
			thisPattern = MRDS_pattern.COMPLEX;


		MRDS newMrds = new MRDS(definedRole, mrds, thisPattern, justification);
		return newMrds;
	}



	// * * * LOAD * * *
	// As Strings
	public HashMap<String, Vector<HashMap<String, Vector<String>>>> loadConceptDefintionPatternsAsStrings() {
		int count = 0;

		// concept, MDS, pattern type
		HashMap<String, Vector<HashMap<String, Vector<String>>>> loadedPatterns = new HashMap<String, Vector<HashMap<String, Vector<String>>>>();
		for (MCDS_pattern pat : MCDS_pattern.values()) {
			loadedPatterns.put(pat.toString(), new Vector<HashMap<String, Vector<String>>>());
		}

		// Pattern file
		File f = new File("_experimentComputations/" + dataSetID + "/_cDefPatterns/"
				+ ontFile.getName() + "_cDefPatterns.txt");
		if (!f.exists())
			f = new File("_experimentComputations/" + dataSetID + "/_cDefPatterns/cDefPatterns_"
					+ ontFile.getName() + ".txt");

		if (isVerbose)
			System.out.println("\nloadAllDefintionPatternsForOntology " + f.getAbsolutePath());

		if (f.exists()) {
			if (isVerbose)
				System.out.println("FILE LOADED " + f.getName());

			String fileContent = Toolbox.INSTANCE.readFile(f.getPath());
			if (fileContent != null) {
				Scanner scanner = new Scanner(fileContent);

				String cName = "";
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();

					// Concept
					if (line.startsWith("http://")) {
						cName = line.trim();
					}

					// PATTERNS
					else if (line.length() > 0) {

						if (patternNames.contains(line.trim())) {
							String patType = line.trim();

							line = scanner.nextLine();
							String mds = line.trim();
							mds = mds.substring(1, mds.length() - 1);
							String[] words = mds.split(", ");
							Vector<String> mdssss = new Vector<String>();
							for (int i = 0; i < words.length; i++) {
								mdssss.add(words[i]);
								// System.out.println("\t EL:" +
								// mdssss.lastElement());
							}


							HashMap<String, Vector<String>> defSigPat = new HashMap<String, Vector<String>>();
							defSigPat.put(cName, mdssss);
							loadedPatterns.get(patType).add(defSigPat);

							count++;
						}
					}
				}
				scanner.close();
			}

		}
		if (isVerbose)
			System.out.println("COUNT = " + count);
		return loadedPatterns;
	}

	public HashMap<String, Vector<HashMap<String, Vector<String>>>> loadRoleDefintionPatternsAsStrings() {
		int count = 0;

		// concept, MDS, pattern type
		HashMap<String, Vector<HashMap<String, Vector<String>>>> loadedPatterns = new HashMap<String, Vector<HashMap<String, Vector<String>>>>();
		for (MRDS_pattern pat : MRDS_pattern.values()) {
			loadedPatterns.put(pat.toString(), new Vector<HashMap<String, Vector<String>>>());
		}

		// Pattern file
		File f = new File("_experimentComputations/" + dataSetID + "/_rDefPatterns/"
				+ ontFile.getName() + "_rDefPatterns.txt");

		if (isVerbose)
			System.out.println("\nloadAllROLEDefintionPatternsForOntology " + f.getAbsolutePath());

		if (f.exists()) {
			if (isVerbose)
				System.out.println("FILE LOADED " + f.getName());

			String fileContent = Toolbox.INSTANCE.readFile(f.getPath());
			if (fileContent != null) {
				Scanner scanner = new Scanner(fileContent);
				String cName = "";
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();

					// Concept
					if (line.startsWith("http://")) {
						cName = line.trim();
					}

					// PATTERNS
					else if (line.length() > 0) {

						if (patternNames.contains(line.trim())) {
							String patType = line.trim();

							line = scanner.nextLine();
							String mds = line.trim();
							mds = mds.substring(1, mds.length() - 1);
							String[] words = mds.split(", ");
							Vector<String> mdssss = new Vector<String>();
							for (int i = 0; i < words.length; i++) {
								mdssss.add(words[i]);
								// System.out.println("\t EL:" +
								// mdssss.lastElement());
							}

							HashMap<String, Vector<String>> defSigPat = new HashMap<String, Vector<String>>();
							defSigPat.put(cName, mdssss);
							loadedPatterns.get(patType).add(defSigPat);

							count++;
						}
					}
				}
				scanner.close();
			}
		}
		if (isVerbose)
			System.out.println("COUNT = " + count);
		return loadedPatterns;
	}

	// As OWL Entities
	public HashMap<OWLEntity, Vector<HashMap<String, Vector<OWLEntity>>>> loadConceptDefintionPatterns(
			File patFile) {
		int countConcept = 0;
		int countPattern = 0;

		// concept, MDS, pattern type
		loadedEntityPatterns = new HashMap<OWLEntity, Vector<HashMap<String, Vector<OWLEntity>>>>();

		// Pattern file
		if (isVerbose)
			System.out
					.println("\nloadAllDefintionPatternsForOntology " + patFile.getAbsolutePath());
		if (patFile.exists()) {
			if (isVerbose)
				System.out.println("FILE LOADED " + patFile.getName());

			String fileContent = Toolbox.INSTANCE.readFile(patFile.getPath());
			OWLEntity concept = null;
			if (fileContent != null) {
				Scanner scanner = new Scanner(fileContent);

				String cName = "";
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();

					// Concept
					if (line.startsWith("http://")) {
						cName = line.trim();
						concept = tb.getEntity(IRI.create(cName), ont);
						countConcept++;
					}

					// PATTERNS
					else if (line.length() > 0) {

						if (patternNames.contains(line.trim())) {
							String patType = line.trim();

							line = scanner.nextLine();
							String mds = line.trim();
							mds = mds.substring(1, mds.length() - 1);
							String[] words = mds.split(", ");

							Vector<OWLEntity> mdssss = new Vector<OWLEntity>();
							for (int i = 0; i < words.length; i++) {
								mdssss.add(tb.getEntity(IRI.create(words[i]), ont));
							}

							if (loadedEntityPatterns.get(concept) == null) {
								loadedEntityPatterns.put(concept,
										new Vector<HashMap<String, Vector<OWLEntity>>>());
							}
							HashMap<String, Vector<OWLEntity>> patternMds = new HashMap<String, Vector<OWLEntity>>();
							patternMds.put(patType, mdssss);
							loadedEntityPatterns.get(concept).add(patternMds);
							countPattern++;
						}
					}
				}
				scanner.close();
			}
		}

		if (isVerbose) {
			System.out.println("countConcept = " + countConcept);
			System.out.println("countPattern = " + countPattern);
		}
		return loadedEntityPatterns;
	}



	// * * * helper * * *
	// remove T'
	private Set<OWLAxiom> getOnlyTAxioms(Set<OWLAxiom> inputAxioms) {
		// if (isVerbose) System.out.println("   getOnlyTAxioms\n\t" +
		// inputAxioms);

		// remove T' axioms, keep T axioms
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		for (OWLAxiom axx : inputAxioms) {
			if (ont.containsAxiom(axx)) {
				axioms.add(axx);
				// if (isVerbose) System.out.println("\t     in T : " + axx);
			} else {
				// if (isVerbose) System.out.println("\t not in T : " + axx);
			}
		}
		return axioms;
	}
}
