/**
 * Tutorial
 */
package owldefinability.examples;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owlapi.io.ToStringRenderer;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import owldefinability.definability.Computer_DefinedConcept;
import owldefinability.definability.Computer_DefinedRole;
import owldefinability.definability.Computer_MinimalConceptDefinitionSignature;
import owldefinability.definability.ImplicitDefinablityChecker;
import owldefinability.definability.DefinedRole.RoleDefinabilityType;
import owldefinability.definability.Classifier_DefinitonPattern;
import owldefinability.definability.Classifier_DefinitonPattern.MCDS_pattern;
import owldefinability.definability.Classifier_DefinitonPattern.MRDS_pattern;
import owldefinability.definability.ImplicitDefinablityChecker.ConceptDefinabilityType;
import owldefinability.definability.MCDS;
import owldefinability.definability.MRDS;
import owldefinability.definability.DefinitionPatternBasedRewriter;
import owldefinability.general.Toolbox;
import owldefinability.signaturecoverage.Computer_SignatureCoverage;
import owldefinability.signaturecoverage.FD_MDS;
import uk.ac.manchester.cs.owlapi.dlsyntax.DLSyntaxObjectRenderer;

@SuppressWarnings("unused")
/**
 * Defianbility tutorials
 */
public class HelloDefinability {

	// * * * Run Tutorials * * *
	public static void main(String[] args) {
		// helloDefinability();		
		// helloSignatureCoverage();		
		// computeEntityDefinability();
	}



	// * * * TUTORIALS * * *
	/**
	 * Run all definability computation stages (1: establish entity definability
	 * status, 2: compute disjoint MDSs, 3: compute all MDSs) and classify
	 * defintion patterns
	 */
	public static void helloDefinability() {
		System.out.println("* * * STARTED helloDefinability * * *");

		// * * * ONTOLOGY * * *
		// (a) real ontology
		// String datasetFolderName = "OAEI_Conference_2014";
		// String fileName = "cmt.owl";

		// (b) example ontology
		String datasetFolderName = "Examples";
		
		// thesis/paper examples {tFamily0.owl, tFamily1.owl, tFamily2.owl}
		String fileName = "tFamily2.owl";

		printOntology(datasetFolderName, fileName);

		// * * * SETTINGS * * *
		String reasonerType = "Hermit";
		long reasonerTimeout = 60000;
		boolean isVerbose = false;
		boolean printResults = true;

		// * * * RUN * * *
		if (checkPath(datasetFolderName, fileName)) {

			// (1) definability status
			computeDefinabilityStatus(datasetFolderName, fileName, reasonerType, reasonerTimeout,
					isVerbose, printResults);

			// (2a) disjoint MDSs
			computeDisjointMDSs(datasetFolderName, fileName, reasonerType, reasonerTimeout,
					isVerbose, printResults);

			// (2b) all MDSs
			computeAllMDSs(datasetFolderName, fileName, reasonerType, reasonerTimeout, isVerbose,
					printResults);

			// (3) classify defintion patterns
			computeDefinitionPatterns(datasetFolderName, fileName, reasonerType, reasonerTimeout,
					isVerbose, printResults);

			// (4) generate (some) definition axioms
			computeDefinitionAxioms(datasetFolderName, fileName, reasonerType, reasonerTimeout,
					isVerbose, printResults);
		}
		System.out.println("\n\n* * * FINISHED helloDefinability * * *");
	}

	public static void helloSignatureCoverage() {

		// Select one {1, 2, 3}
		int OPTION = 1;

		// * * * 1 * * *
		if (OPTION == 1) {
			helloSignatureCoverage_artificalExample();
		}

		// * * * 2 - Real Ontology * * *
		else if (OPTION == 2) {
			String datasetFolderName = "OAEI_Conference_2014";
			String fileName = "cmt.owl";
			String reasonerType = "Pellet";
			long reasonerTimeout = 1;
			boolean isVerbose = true;
			boolean printResults = true;
			helloSignatureCoverage_realOntology(datasetFolderName, fileName, reasonerType,
					reasonerTimeout, isVerbose, printResults);
		}

		// * * * 3 - Ideal Cover for Real Ontology (full TBox signature) * * *
		else if (OPTION == 3) {
			String datasetFolderName = "OAEI_Conference_2014";
			String fileName = "cmt.owl";
			String reasonerType = "Pellet";
			long reasonerTimeout = 1;
			boolean isVerbose = true;
			boolean printResults = true;
			helloSignatureCoverage_idealCover(datasetFolderName, fileName, reasonerType,
					reasonerTimeout, isVerbose, printResults);
		}
	}



	// * * * DEFINABILITY Computation - ONTOLOGY * * *
	/**
	 * Stage 1: Establish entity definability status (i.e. defined explicitly /
	 * defined implictly, or undefined) for all entities of an ONTOLOGY
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeDefinabilityStatus(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {
		System.out.println("COMPUTE Definability Status");

		// (1) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());


		// (2a) COMPUTE or LOAD definability status status for all CONCEPTS
		System.out
				.println("\nSTARTED Definability Status CONCEPTS   " + df_full.format(new Date()));
		Computer_DefinedConcept defConComp = new Computer_DefinedConcept(datasetName, ont1,
				ontFile, reasonerType, reasonerTimeout, isVerbose);
		HashMap<OWLClass, ConceptDefinabilityType> defConceptTypes = defConComp
				.getDefinedConceptListByType();
		System.out.println("\nCOMPLETED Definability Status CONCEPTS  "
				+ df_full.format(new Date()));


		// (2b) COMPUTE or LOAD definability status status for all ROLES
		System.out.println("\nSTARTED Definability Status ROLES    " + df_full.format(new Date()));
		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, RoleDefinabilityType> defRolesTypes = defRoleComp.getRoleDefinability();
		System.out.println("\nCOMPLETED Definability Status ROLES  " + df_full.format(new Date()));


		// (3) PRINT Results
		if (printResults) {

			// Concepts
			System.out.println("\n* * * Concepts * * *");
			Vector<OWLClass> sortedConceptList = new Vector<OWLClass>();
			sortedConceptList = tb.sortConceptsAlphabetcially(ont1.getClassesInSignature());
			for (OWLClass cls : sortedConceptList) {
				if (!cls.isAnonymous() && !cls.isOWLThing()) {
					ConceptDefinabilityType thisConceptDefinabilityType = ConceptDefinabilityType.UNDEFINED;

					if (defConceptTypes.get(cls) != null) {
						thisConceptDefinabilityType = defConceptTypes.get(cls);
					}
					System.out.println("   " + cls.getIRI() + " " + thisConceptDefinabilityType);
				}
			}

			// Roles
			System.out.println("\n* * * Roles * * *");
			Vector<OWLObjectProperty> sortedObjPropRoleList = new Vector<OWLObjectProperty>();
			sortedObjPropRoleList = tb.sortObjectpropertiesAlpha(ont1
					.getObjectPropertiesInSignature());
			for (OWLObjectProperty rl : sortedObjPropRoleList) {
				RoleDefinabilityType thisRoleDefinabilityType = defRolesTypes.get(rl);
				System.out.println("   " + rl.getIRI() + " " + thisRoleDefinabilityType);
			}
		}
	}

	/**
	 * Stage 2: Compute disjoint MDSs for all defined entities of an ONTOLOGY
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeDisjointMDSs(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {
		System.out.println("COMPUTE disjoint MDSs");

		// (1) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		// (2a) COMPUTE Disjoint CONCEPT MDSs
		System.out.println("\nSTARTED disjoint CONCEPT MDS   " + df_full.format(new Date()));
		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> disjMcdss = mcdsComp
				.computeMCDSsOfOntology_Disjoints();
		System.out.println("\nCOMPLETED disjoint CONCEPT MDS   " + df_full.format(new Date()));

		// (2b) COMPUTE Disjoint ROLE MDSs
		System.out.println("\nSTARTED disjoint ROLE MDS   " + df_full.format(new Date()));
		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> disjMrdss = defRoleComp.getRoleMDSs();
		System.out.println("\nCOMPLETED disjoint ROLE MDS   " + df_full.format(new Date()));

		// (3) PRINT Results
		if (printResults) {

			// Concepts
			if (disjMcdss != null && !disjMcdss.isEmpty()) {
				System.out.println("\n* * * Concepts * * *");
				Vector<OWLClass> sortedDefinedConceptList = new Vector<OWLClass>();
				sortedDefinedConceptList = tb.sortConceptsAlphabetcially(disjMcdss.keySet());
				for (OWLClass cls : sortedDefinedConceptList) {
					System.out.println("\n   " + cls.getIRI().toString());

					int mdscount = 0;
					for (Set<OWLEntity> mds : disjMcdss.get(cls)) {
						Vector<OWLEntity> sortedMDS = tb.sortEntitiesAlphabetcially(mds);
						System.out.println("\t MDS_" + ++mdscount + " = " + sortedMDS + "");
					}
				}
			}

			// Roles
			if (disjMrdss != null && !disjMrdss.keySet().isEmpty()) {

				if (disjMrdss.keySet().iterator().next() != null) {
					System.out.println("\n* * * Roles * * *");
					Vector<OWLEntity> sortedObjPropRoleList = new Vector<OWLEntity>();
					sortedObjPropRoleList = tb.sortEntitiesAlphabetcially(disjMrdss.keySet());
					for (OWLEntity rl : sortedObjPropRoleList) {
						System.out.println("\n   " + rl.getIRI().toString());

						int mdscount = 0;
						for (Set<OWLEntity> mds : disjMrdss.get(rl)) {
							Vector<OWLEntity> sortedMDS = tb.sortEntitiesAlphabetcially(mds);
							System.out.println("\t MDS_" + ++mdscount + " = " + sortedMDS + "");
						}
					}
				}
			}
		}
	}

	/**
	 * Stage 3: Compute all MDSs for all defined entities of an ONTOLOGY
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeAllMDSs(String datasetName, String fileName, String reasonerType,
			long reasonerTimeout, boolean isVerbose, boolean printResults) {
		System.out.println("COMPUTE all MDSs");

		// (1) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		// (2a) COMPUTE All CONCEPT MDSs
		System.out.println("\nSTARTED all CONCEPT MDS   " + df_full.format(new Date()));
		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();
		System.out.println("\nCOMPLETED all CONCEPT MDS   " + df_full.format(new Date()));

		// (2b) COMPUTE All ROLE MDSs
		System.out.println("\nSTARTED all ROLE MDS   " + df_full.format(new Date()));
		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, HashSet<Set<OWLEntity>>> disjMrdss = defRoleComp.getRoleMDSs();
		System.out.println("\nCOMPLETED all ROLE MDS   " + df_full.format(new Date()));

		// (3) PRINT Results
		if (printResults) {

			// Concepts
			System.out.println("\n* * * Concepts * * *");
			Vector<OWLClass> sortedDefinedConceptList = new Vector<OWLClass>();
			sortedDefinedConceptList = tb.sortConceptsAlphabetcially(allMcdss.keySet());
			for (OWLClass cls : sortedDefinedConceptList) {
				System.out.println("\n   " + cls.getIRI().toString());

				int mdscount = 0;
				for (Set<OWLEntity> mds : allMcdss.get(cls)) {
					Vector<OWLEntity> sortedMDS = tb.sortEntitiesAlphabetcially(mds);
					System.out.println("\t MDS_" + ++mdscount + " = " + sortedMDS + "");
				}
			}

			// Roles
			/*
			 * System.out.println("\n* * * Roles * * *"); Vector<OWLEntity>
			 * sortedObjPropRoleList = new Vector<OWLEntity>();
			 * sortedObjPropRoleList =
			 * tb.sortEntitiesAlphabetcially(disjMrdss.keySet()); for (OWLEntity
			 * rl : sortedObjPropRoleList) { System.out.println("\n   " +
			 * rl.getIRI().toString());
			 * 
			 * int mdscount = 0; for (Set<OWLEntity> mds : disjMrdss.get(rl)) {
			 * Vector<OWLEntity> sortedMDS = tb.sortEntitiesAlphabetcially(mds);
			 * System.out.println("\t MDS_" + ++mdscount + " = " + sortedMDS +
			 * ""); } }
			 */
		}
	}

	/**
	 * Classify defined entity MDSs as definition patterns for all defined entities of an ONTOLOGY
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeDefinitionPatterns(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {
		System.out.println("COMPUTE Definition Patterns");

		// (1) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		// (2) Load MDSs
		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();

		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, Vector<Set<OWLEntity>>> allMrdss = defRoleComp.getRoleMDSs2();

		// (3) Generate Patterns
		if (tb.hasDisjMCDSsBeenComputed(datasetName, fileName) == 1) {
			Classifier_DefinitonPattern defPattern = new Classifier_DefinitonPattern(ont1, ontFile,
					datasetName, reasonerType, reasonerTimeout, isVerbose);
			HashMap<OWLEntity, Vector<MCDS>> conceptPatterns = defPattern
					.generateConceptDefinitionPatternsForOntology(allMcdss);

			HashMap<OWLEntity, Vector<MRDS>> rolePatterns = defPattern
					.generateRoleDefinitionPatternsForOntology(allMrdss);

			// (4) PRINT Results
			if (printResults) {
				for (OWLEntity definedConcept : conceptPatterns.keySet()) {
					System.out.println("\n\n  " + definedConcept.getIRI().toString());

					for (MCDS patternMds : conceptPatterns.get(definedConcept)) {
						System.out.println("\n    " + patternMds.getPattern().toString()
								+ " | \u03A3 = " + patternMds.getSignature()
								+ "\n    Justification : ");

						for (OWLAxiom axx : patternMds.getJustification()) {
							System.out.println("\t   " + axx);
						}
					}
				}

				if (rolePatterns != null && rolePatterns.keySet().iterator().next() != null) {
					for (OWLEntity definedRole : rolePatterns.keySet()) {
						System.out.println("\n\n  " + definedRole.getIRI().toString());
						for (MRDS patternMds : rolePatterns.get(definedRole)) {
							System.out.println("\n    " + patternMds.getPattern().toString()
									+ " | \u03A3 = " + patternMds.getSignature()
									+ "\n    Justification : ");

							// could be empty as Role Justification is
							// incomplete !
							for (OWLAxiom axx : patternMds.getJustification()) {
								System.out.println("\t   " + axx);
							}
						}
					}
				}
			}
		} else {
			System.out.println("Please compute the MDSs first!\n");
		}
	}

	/**
	 * Compute Defintion Axioms for identifiable Definition Patterns (Rule-based
	 * Rewriting) for all defined entities of an ONTOLOGY
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeDefinitionAxioms(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {
		System.out.println("COMPUTE Definition Patterns");

		// (1a) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		// (1b) Init REASONER
		OWLReasoner reasoner = Toolbox.INSTANCE.getReasoner(reasonerType, ont1, reasonerTimeout);

		// (2) Load MDSs
		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, false, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();

		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, false, ont1, ontFile);
		HashMap<OWLEntity, Vector<Set<OWLEntity>>> allMrdss = defRoleComp.getRoleMDSs2();

		// (3) Generate Patterns
		if (tb.hasDisjMCDSsBeenComputed(datasetName, fileName) == 1) {
			Classifier_DefinitonPattern defPattern = new Classifier_DefinitonPattern(ont1, ontFile,
					datasetName, reasonerType, reasonerTimeout, false);
			HashMap<OWLEntity, Vector<MCDS>> conceptPatterns = defPattern
					.generateConceptDefinitionPatternsForOntology(allMcdss);

			HashMap<OWLEntity, Vector<MRDS>> rolePatterns = defPattern
					.generateRoleDefinitionPatternsForOntology(allMrdss);

			DefinitionPatternBasedRewriter patRW = new DefinitionPatternBasedRewriter(datasetName,
					reasonerType, reasonerTimeout, isVerbose, ont1, ontFile);

			// (4) Generate Axioms
			for (OWLEntity definedConcept : conceptPatterns.keySet()) {

				for (MCDS patternMds : conceptPatterns.get(definedConcept)) {
					System.out.println("\n\n[" + definedConcept.getIRI().toString() + "]");

					if (!patternMds.getPattern().toString().equals("COMPLEX")) {
						OWLAxiom defAxiom = patRW.generateDefinitionAxiom(patternMds);

						// VALIDATE with REASONER
						if (defAxiom != null) {
							boolean isEntailed = reasoner.isEntailed(defAxiom);
							System.out.println("   Definition AXIOM : " + defAxiom
									+ "\n\tT ? \u22A8 (" + defAxiom + ")   is " + isEntailed);
						} else {
							System.out.println("   ... ERROR, no definition AXIOM produced ... ");
						}
					} else {
						System.out.println("\t R = " + patternMds.getConcept());
						System.out.println("\t Σ = " + patternMds.getSignature());
						System.out.println("\t J = " + patternMds.getJustification());
						System.out.println("\t " + patternMds.getPattern() + " Pattern");
						System.out
								.println("   ... COMPLEX PATTERNS are not currently supported ... ");
					}
				}
			}

			if (rolePatterns != null && rolePatterns.keySet().iterator().next() != null) {
				for (OWLEntity definedRole : rolePatterns.keySet()) {

					for (MRDS patternMds : rolePatterns.get(definedRole)) {
						System.out.println("\n\n[" + definedRole.getIRI().toString() + "]");

						if (!patternMds.getPattern().toString().equals("COMPLEX")) {
							OWLAxiom defAxiom = patRW.generateDefinitionAxiom(patternMds);

							// VALIDATE with REASONER
							if (defAxiom != null) {
								boolean isEntailed = reasoner.isEntailed(defAxiom);
								System.out.println("   Definition AXIOM : " + defAxiom
										+ "\n\tT ? \u22A8 (" + defAxiom + ")   is " + isEntailed);
							} else {
								System.out
										.println("   ... ERROR, no definition AXIOM produced ... ");
							}
						} else {
							System.out.println("\t R = " + patternMds.getRole());
							System.out.println("\t Σ = " + patternMds.getSignature());
							System.out.println("\t J = " + patternMds.getJustification());
							System.out.println("\t " + patternMds.getPattern() + " Pattern");
							System.out
									.println("   ... COMPLEX PATTERNS are not currently supported ... ");
						}
					}
				}
			}
		}
	}


	
	// * * * DEFINABILITY Computation - ENTITY * * *
	/**
	 * Defianbility computation for individual entities
	 * 
	 * @param datasetName
	 *            Dataset folder name, must be loated in (_dataset/DATASET_NAME)
	 * @param fileName
	 *            Ontology file name, must be loated in
	 *            (_dataset/DATASET_NAME/FILE_NAME)
	 * @param reasonerType
	 *            Reasoner type {Pellet,Hermit}
	 * @param reasonerTimeout
	 *            Reasoner timeout in milliseconds
	 * @param isVerbose
	 *            Show internal processing
	 */
	private static void computeEntityDefinability() {
		System.out.println("COMPUTE Entity Definability Status");

		String reasonerType = "Pellet";
		long reasonerTimeout = 60000;
		boolean isVerbose = false;
		boolean printResults = true;

		String datasetFolderName = "Examples";
		String fileName = "ltfoa2.owl";

		File ontFile = new File(DATASET + "/" + datasetFolderName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		printOntology(datasetFolderName, fileName);

		// * * * CONCEPTS * * *
		// (1) obtain concept (C) and some signature
		OWLClass concept = null;
		Set<OWLEntity> signature = null;
		if (concept != null && signature != null) {

			// (2) check definability cases
			ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker(ontFile,
					datasetFolderName, reasonerType, reasonerTimeout, false);

			// (2a) is C defined in any form (even modelling error)?
			boolean isConceptDefined = impDef.isConceptDefined(concept, signature);

			// (2b) is C implcitly defined by a given signature?
			boolean isConceptImplicitlyDefinedBySignature = impDef.isConceptImplicitlyDefined(
					concept, signature);

			// (2c) is C implicitly defined by a given signature, under a
			// module?
			// obtain module
			Set<OWLAxiom> moduleAxioms = null;
			if (moduleAxioms != null) {
				boolean isConceptImplcitlyDefinedInModule = impDef.isConceptImplicitlyDefined_MOD(
						concept, signature, moduleAxioms);
			}
			
			// (2d) is C explicitly defined by a concept equivalence axiom which contains a direct cycle
			OWLAxiom defAxiom = null;
			if (defAxiom != null) { 
				boolean isAxiomCyclic = impDef.isExplicitConceptDefinitionAcyclic(concept, defAxiom);
			}

			// (2e) obtain justifications (all, or only one) if C is definable
			boolean obtainAllJustifications = false;
			Set<Explanation<OWLAxiom>> exps = impDef.justifyConceptDefinability(concept, signature,
					obtainAllJustifications);
			for (Explanation<OWLAxiom> exp : exps) {
				for (OWLAxiom axx : exp.getAxioms()) {
					System.out.println("\t" + axx);
				}
			}
			
			// (2f) is C defined by a modelling error?
			boolean isConceptEquivalentToTop = impDef
					.isConceptImplicitlyDefinedByEmptySignature(concept);
		}

		// * * * ROLES * * *
		// (3) obtain role (R) and some (RBox) signature
		OWLEntity role = null;
		Set<OWLEntity> roleSignature = null;
		if (role != null && roleSignature != null) {

			// (4) check definability cases
			ImplicitDefinablityChecker impDef = new ImplicitDefinablityChecker(ontFile,
					datasetFolderName, reasonerType, reasonerTimeout, false);

			// (4a) is R implcitly defined by a given signature ?
			boolean isRoleImplicitlyDefinedBySignature = impDef.isRoleImplicitlyDefined(role,
					roleSignature);

			// (4b) is R implicitly defined by a given signature, under a module?
			Set<OWLAxiom> moduleAxioms = null;
			if (moduleAxioms != null) {
				boolean isRoleImplcitlyDefinedInModule = impDef.isRoleImplicitlyDefined_MOD(
						role, signature, moduleAxioms);
			} 
		}
	}



	// * * * SIGNATURE COVERAGE Computation * * *
	/**
	 * Compute Minimal Signature Coverage for an artificial example, assuming a
	 * set of pre-computed MDSs
	 */
	public static void helloSignatureCoverage_artificalExample() {

		boolean isVerbose = true;

		// (1) create signatures
		Set<String> taskSignature = new HashSet<String>();
		Set<String> restrictedSignature = new HashSet<String>();
		Set<String> ontSignature = new HashSet<String>();
		Vector<FD_MDS> mdsFDs = new Vector<FD_MDS>();

		// Task Signature
		taskSignature.add("C");
		taskSignature.add("K");
		taskSignature.add("G");
		taskSignature.add("H");
		taskSignature.add("A");
		taskSignature.add("B");
		taskSignature.add("K");
		// taskSignature.add("Uncoverable");

		// Restricted Signature
		restrictedSignature.add("A");
		restrictedSignature.add("B");
		restrictedSignature.add("K");
		restrictedSignature.add("r");
		restrictedSignature.add("G");

		// FD-MDSs
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("A", "B")), new HashSet<String>(
				Arrays.asList("C"))));
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("B")), new HashSet<String>(Arrays
				.asList("D"))));
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("D")), new HashSet<String>(Arrays
				.asList("E"))));
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("K", "G")), new HashSet<String>(
				Arrays.asList("H"))));
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("r")), new HashSet<String>(Arrays
				.asList("F"))));
		mdsFDs.add(new FD_MDS(new HashSet<String>(Arrays.asList("r")), new HashSet<String>(Arrays
				.asList("G"))));

		// Ont signature
		ontSignature.addAll(taskSignature);
		ontSignature.addAll(restrictedSignature);
		for (FD_MDS fmds : mdsFDs) {
			ontSignature.addAll(fmds.sLHS);
			ontSignature.addAll(fmds.sRHS);
		}

		// (2) initalise minimal coverage computer
		Computer_SignatureCoverage sigCovComp = new Computer_SignatureCoverage("SigCovExample_1",
				"sigCovTestOnt_1", taskSignature, restrictedSignature, mdsFDs, isVerbose);

		// (3) check whether the task is coverable
		boolean isTaskCoverableByRestrictedSignature = sigCovComp
				.isTaskCoverableByRestrictedSignatures();
		if (!isTaskCoverableByRestrictedSignature) {
			System.out.println("Task is uncoverable");

			// (3a) why not ?
			Set<String> uncoverableTaskSignatureEntities = sigCovComp
					.getUncoverableTaskSignatureEntities();
			System.out.println("\n\n * * * Uncoverable TaskSignature Entities * * *");
			int count = 0;
			for (String str : uncoverableTaskSignatureEntities) {
				System.out.println("   " + ++count + " : " + str);
			}
		} else {
			// (4) compute minimal coverage
			// (4a)
			System.out.println("\nSTARTED Greedy 1 Cover   " + df_full.format(new Date()));
			Set<String> coverSet_greedy1 = sigCovComp.computeSignatureCover_greedy1();
			System.out.println("\nFINISHED Greedy 1 Cover   " + df_full.format(new Date()));

			// (4b)
			System.out.println("\nSTARTED Greedy 2 Cover   " + df_full.format(new Date()));
			Set<String> coverSet_greedy2 = sigCovComp.computeSignatureCover_greedy2();
			System.out.println("\nFINISHED Greedy 2 Cover   " + df_full.format(new Date()));

			// (5) PRINT results
			System.out.println("\n\n\n - Ontology Signature =  " + ontSignature);
			System.out.println(" - Task Signature | S  =  " + taskSignature);
			System.out.println(" - Restricted Signature | R  =  " + restrictedSignature);
			System.out.println(" - MDSs as FDs | M  =");
			int i = 1;
			for (FD_MDS fd : mdsFDs) {
				System.out.println("\tm" + i++ + " = " + fd);
			}
			System.out.println(" - Cover Set ( Greedy 1) | C  =  " + coverSet_greedy1);
			System.out.println(" - Cover Set ( Greedy 2) | C  =  " + coverSet_greedy2);
		}
	}

	/**
	 * Compute Minimal Signature Coverage using a real-world ontology, assuming
	 * that some (or all) MDSs are pre-computed
	 */
	public static void helloSignatureCoverage_realOntology(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {

		// (1) load ontology and MDSs
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();

		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, Vector<Set<OWLEntity>>> allMrdss = defRoleComp.getRoleMDSs2();


		// (2) initalise signatures
		// Ontology Signature
		Set<String> ontologySignature = new HashSet<String>();

		// use full entity IRIs OR just short form
		boolean getEntityShortForm = false;
		ontologySignature.addAll(tb.getTBoxSignatureAsStringsOrdered(getEntityShortForm, ont1));

		// initalise Task Signature (the whole TBox signature is used as a
		// placeholder)
		Set<String> taskSignature = new HashSet<String>();
		taskSignature.addAll(ontologySignature);

		// initalise Restricted Signature (the whole TBox signature is used as a
		// placeholder)
		Set<String> restrictedSignature = new HashSet<String>();
		restrictedSignature.addAll(ontologySignature);

		// MDSs
		HashMap<String, Set<Set<String>>> mdss = transformMDStoString(allMcdss, allMrdss);


		// (3) compute minimal coverage
		Computer_SignatureCoverage sigCovComp = new Computer_SignatureCoverage(datasetName,
				fileName, taskSignature, restrictedSignature, mdss, isVerbose);


		// (3a)
		System.out.println("\nSTARTED Greedy 1 Cover   " + df_full.format(new Date()));
		Set<String> coverSet_greedy1 = sigCovComp.computeSignatureCover_greedy1();
		System.out.println("\nFINISHED Greedy 1 Cover   " + df_full.format(new Date()));

		// (3b)
		System.out.println("\nSTARTED Greedy 2 Cover   " + df_full.format(new Date()));
		Set<String> coverSet_greedy2 = sigCovComp.computeSignatureCover_greedy2();
		System.out.println("\nFINISHED Greedy 2 Cover   " + df_full.format(new Date()));

		// (4) PRINT results
		System.out.println("\n\n\n - Task Signature |S| =  " + taskSignature.size() + "\n\t S = "
				+ taskSignature);
		System.out.println(" - Restricted Signature |R| =  " + restrictedSignature.size()
				+ "\n\t R = " + restrictedSignature);
		System.out.println(" - Explicit Cover Set    |C| = " + taskSignature.size());
		System.out.println(" - Cover Set ( Greedy 1) |C| = " + coverSet_greedy1.size() + "\n\t C ="
				+ coverSet_greedy1);
		System.out.println(" - Cover Set ( Greedy 2) |C| = " + coverSet_greedy2.size() + "\n\t C ="
				+ coverSet_greedy2);
	}

	/**
	 * Compute Ideal Cover for a real-world ontology, assuming that some (or
	 * all) MDSs are pre-computed
	 */
	public static void helloSignatureCoverage_idealCover(String datasetName, String fileName,
			String reasonerType, long reasonerTimeout, boolean isVerbose, boolean printResults) {

		// (1) load ontology and MDSs
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath());

		Computer_MinimalConceptDefinitionSignature mcdsComp = new Computer_MinimalConceptDefinitionSignature(
				ont1, ontFile, reasonerType, reasonerTimeout, datasetName, true, null);
		HashMap<OWLClass, Vector<Set<OWLEntity>>> allMcdss = mcdsComp.runExpansion();

		Computer_DefinedRole defRoleComp = new Computer_DefinedRole(datasetName, reasonerType,
				reasonerTimeout, isVerbose, ont1, ontFile);
		HashMap<OWLEntity, Vector<Set<OWLEntity>>> allMrdss = defRoleComp.getRoleMDSs2();


		// (2) initalise signatures (task / restricted signature is not used
		// here)
		Set<String> ontologySignature = new HashSet<String>();
		ontologySignature.addAll(tb.getTBoxSignatureAsStringsOrdered(false, ont1));

		// MDSs
		HashMap<String, Set<Set<String>>> mdss = transformMDStoString(allMcdss, allMrdss);

		// (3) compute ideal cover
		Computer_SignatureCoverage sigCovComp = new Computer_SignatureCoverage(datasetName,
				fileName, null, null, mdss, isVerbose);
		Set<String> idealCover = sigCovComp.computeIdealCover(ontologySignature);

		// (4) PRINT results
		if (printResults) {
			System.out.println("\n\n\n - TBox Signature |T| =  " + ontologySignature.size()
					+ "\n\t T = " + ontologySignature);
			System.out.println(" - Ideal Cover |C| = " + idealCover.size() + "\n\t C ="
					+ idealCover);
		}
	}



	// * * * Helpers * * *
	private static boolean checkPath(String datasetName, String fileName) {
		String relativeFilePath = new String(DATASET + "/" + datasetName + "/" + fileName);
		File ontFile = new File(relativeFilePath);
		if (ontFile.exists()) {
			return true;
		} else {
			System.out.println("\nINVALID Ontology Path " + ontFile.getPath());
		}
		return false;
	}

	private static void printOntology(String datasetName, String fileName) {

		// (1) Load ONTOLOGY
		File ontFile = new File(DATASET + "/" + datasetName + "/" + fileName);
		OWLOntology ont1 = tb.loadOntology(ontFile);
		System.out.println("\nLOADED Ontology " + ontFile.getPath() + "\n");

		// (2) PRINT TBox
		String l = "* * * * * ";
		System.out.println(l + l + l + l + l);
		System.out.println("* ONTOLOGY TBox : " + fileName);
		System.out.println(l + l + l + l + l);
		ToStringRenderer.getInstance().setRenderer(new DLSyntaxObjectRenderer());
		for (OWLAxiom axx : ont1.getLogicalAxioms()) {
			System.out.println("   " + axx);
		}
		System.out.println(l + l + l + l + l);
	}

	private static HashMap<String, Set<Set<String>>> transformMDStoString(
			HashMap<OWLClass, Vector<Set<OWLEntity>>> mcdss,
			HashMap<OWLEntity, Vector<Set<OWLEntity>>> mrdss) {
		HashMap<String, Set<Set<String>>> result = new HashMap<String, Set<Set<String>>>();

		for (OWLClass cls : mcdss.keySet()) {
			Set<Set<String>> thismdss = new HashSet<Set<String>>();

			for (Set<OWLEntity> mds : mcdss.get(cls)) {
				Set<String> thismds = new HashSet<String>();

				for (OWLEntity ent : mds) {
					thismds.add(ent.getIRI().toString());
				}
				thismdss.add(thismds);
			}
			result.put(cls.getIRI().toString(), thismdss);
		}
		return result;
	}

	

	// * * * VAR.S * * *
	private static Toolbox tb = Toolbox.INSTANCE;
	private static final String DATASET = "_dataset";
	private static SimpleDateFormat df_full = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
}